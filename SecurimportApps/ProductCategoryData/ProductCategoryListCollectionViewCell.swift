//
//  ProductCategoryListCollectionViewCell.swift
//  SecurimportApps
//
//  Created by debabrata on 11/07/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
class ProductCategoryListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var wishlistICOn: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var stockQuantityView: UIView!
    @IBOutlet weak var productOldPrice: UILabel!
    @IBOutlet weak var modelNoLabel: UILabel!
    @IBOutlet weak var rfNoLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var buyNowBtn: UIButton!
    @IBOutlet weak var buyNowBtnHeight : NSLayoutConstraint!
    @IBOutlet weak var checkForPrice:UIButton!
    @IBOutlet weak var checkForPriceButtonHeight:NSLayoutConstraint!
    var stockQuantityBarView : StockQuantityBarView?
    var delgate: PassData?
    var wishlistId: String?
    var wishlistIds = [WishlistIds]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productView.myBorder()
        productOldPrice.SetDefaultTextColor()
        if stockQuantityBarView == nil {
            stockQuantityBarView = Bundle.loadView(fromNib: "StockQuantityBarView", withType: StockQuantityBarView.self)
            stockQuantityBarView?.frame = self.stockQuantityView.bounds
            stockQuantityBarView?.View1.layer.cornerRadius = 2
            stockQuantityBarView?.View2.layer.cornerRadius = 2
            stockQuantityBarView?.View3.layer.cornerRadius = 2
            self.stockQuantityView.addSubview(stockQuantityBarView!)
        }
        
        checkForPrice.setTitle("Check for price".localized, for: .normal)
    }
    @IBAction func WishlistPress(_ sender: UIButton) {
        if LoginCustomer.loginCheckCustomer() {
            delgate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiUse.addToWishlist)
        } else {
            LoginCustomer.showLogginMessage()
        }
        
    }
    @IBAction func buyNowBtnPress(_ sender: UIButton) {
          if item?.isCustomizableProduct ?? false{
            delgate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiUse.products)
          }else{
        if LoginCustomer.loginCheckCustomer() {
            delgate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiUse.buyNow)
        } else {
            LoginCustomer.showLogginMessage()
        }
        }
    }
    
    var item: Product? {
        
        didSet {
            checkForPriceButtonHeight.constant = 0
            price.isHidden = true
            checkForPrice.isHidden = true
            if !GlobalData.AddOnsEnabled.productsPriceAvailable {
                checkForPriceButtonHeight.constant = 30
                price.isHidden = true
                productOldPrice.isHidden = true
                checkForPrice.isHidden = false
            }else{
                checkForPriceButtonHeight.constant = 0
                price.isHidden = false
                productOldPrice.isHidden = false
                checkForPrice.isHidden = true
            }
            buyNowBtn.setTitle("buyNow".localized, for: .normal)
            buyNowBtn.isHidden = false
            buyNowBtnHeight.constant = 30
            if GlobalData.AddOnsEnabled.wishlistEnable == false {
                wishlistICOn.isHidden = true
            }
            if wishlistIds.contains(where: {$0.wishlistId == item?.productId!}) {
                wishlistICOn.setImage(UIImage(named : "Icon-Wishlist-Fill"), for: .normal)
            } else {
                wishlistICOn.setImage(UIImage(named : "Icon-Wishlist"), for: .normal)
            }
            if item?.userCustomerGrpId ?? false{
                if item?.isCustomizableProduct ?? false{
                    buyNowBtn.setTitle("view".localized, for: .normal)
                }else{
                    buyNowBtn.setTitle("buyNow".localized, for: .normal)
                }
                buyNowBtn.isHidden = false
                buyNowBtnHeight.constant = 30
               
            }else{
                buyNowBtn.isHidden = true
                buyNowBtnHeight.constant = 0
            }
            stockQuantityBarView?.setBackgroundColor(level: item?.stockLevel ?? "")
            productName.text = item?.name
            price.text = item?.reducePrice
            descriptionLabel.attributedText = item?.shortDescription
            rfNoLabel.text = item?.refCode
            modelNoLabel.text = item?.modelBarcode
            let val = (item?.pictureUrl!)!
            productImage.setImage(imageUrl: val, controller: .productCategory)
            
            if !(item?.reducePrice!.isEmpty)! {
                price.text = item?.reducePrice
                productOldPrice.text = item?.price
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: (item?.price!)!)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                productOldPrice.attributedText = attributeString
            } else {
                price.text = item?.price
                productOldPrice.text = item?.reducePrice
            }
        }
    }
    class func fromNib() -> ProductCategoryListCollectionViewCell?
    {
        var cell: ProductCategoryListCollectionViewCell?
        guard let nibViews = Bundle.main.loadNibNamed("ProductCategoryListCollectionViewCell", owner: nil, options: nil) else { return  nil}
        for nibView in nibViews {
            if let cellView = nibView as? ProductCategoryListCollectionViewCell {
                cell = cellView
            }
        }
        return cell
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    @IBAction func checkForPriceButtonPrice(){
        if let topview = UIApplication.topViewController(){
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
            let nav  = UINavigationController(rootViewController: view)
            topview.present(nav, animated: true, completion: nil)
        }
        
    }
}


