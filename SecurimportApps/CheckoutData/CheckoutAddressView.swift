//
//  CheckoutAddressView.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON

class CheckoutAddressView: UIView {

    @IBOutlet weak var addressContinue: UIButton!
    @IBOutlet weak var addressTableview: UITableView!
     var emptyView: EmptyView?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var shippingAddressId: String?
    var delegate: PassData?
    var moveDelegate: NewViewControllerOpen?
    var shiipingIndex  = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        addressContinue.setTitle("continue".localized, for: .normal)
        addressTableview.register(CheckoutShippingTableViewCell.nib, forCellReuseIdentifier: CheckoutShippingTableViewCell.identifier)
        addressTableview.register(CheckoutAddressTableViewCell.nib, forCellReuseIdentifier: CheckoutAddressTableViewCell.identifier)
        addressTableview.delegate = self
        addressTableview.delegate = self
        addressTableview.estimatedRowHeight = 100
        addressTableview.rowHeight = UITableView.automaticDimension
        addressTableview.separatorStyle = .none
        print("dshdhshsdh")

        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.bounds
            emptyView?.labelText.text = addressEmpty.localized
            emptyView?.button.setTitle(addNewAddress.localized, for: .normal)
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-address")
            emptyView?.delegate = self
            emptyView?.isHidden = true
            self.addSubview(emptyView!)
        }
        addressContinue.backgroundColor = GlobalData.Credentials.accentColor
    }

    @IBAction func AddressContinuePressed(_ sender: UIButton) {
        if GlobalData.AddOnsEnabled.allowShipping {
            delegate?.passData(id: shippingAddress?.addressId ?? "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.getShipping)
          
        } else {
            delegate?.passData(id: shippingAddress?.addressId ?? "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.getPayment)
        }
        
    }

    var billingAddress: AddressData?
    var shippingAddress: AddressData?
    var rowCount = 0

    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = CheckoutAddressModal(data : jsonData) else {
            return
        }

        if data.billingAddres != nil {
             emptyView?.isHidden = true
            billingAddress = data.billingAddres
            rowCount = 1
            if !data.shippingAddress.isEmpty {
                rowCount = 2
                if let shipId = shippingAddressId {

                    if shipId.count > 0 {
                     if  let index =   data.shippingAddress.index(where: {$0.addressId == shippingAddressId}) {
                             shippingAddress = data.shippingAddress[index]
                        shiipingIndex = index
                    }

                } else {
                     shippingAddress = data.shippingAddress[shiipingIndex]
                }
            } else {
                shippingAddress = data.shippingAddress[shiipingIndex]
            }
            completion(true)
            addressTableview.reloadData()
        } else {
        }

        } else {
            print("emptyViewShow")
            emptyView?.isHidden = false
            completion(false)
        }
    }

}

extension CheckoutAddressView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("fndnnc")
        return rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
             let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutAddressTableViewCell.identifier) as! CheckoutAddressTableViewCell
             cell.addressLabel.text = billingAddress?.address
             cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutShippingTableViewCell.identifier) as! CheckoutShippingTableViewCell
            cell.address.text = shippingAddress?.address!
            cell.delegate = self
             cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return address.localized
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            moveDelegate?.moveToController(id: (billingAddress?.addressId!)!, name: "", dict: JSON.null, Controller: Controllers.editAddress)
        } else {
            moveDelegate?.moveToController(id: (shippingAddress?.addressId!)!, name: "", dict: JSON.null, Controller: Controllers.editAddress)
        }
    }

}

extension CheckoutAddressView: NewViewControllerOpen {

    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        print("check", Controller)
       moveDelegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controller)
    }
}

extension CheckoutAddressView: EmptyDelegate {

    func buttonPressed() {
         moveDelegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.addAddress)
    }
}
