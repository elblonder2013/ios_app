//
//  CmsDataViewController.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 26/02/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit
import SwiftyJSON

class CmsDataViewController: UIViewController {
    
    @IBOutlet weak var shareBtn: UIBarButtonItem!
    @IBOutlet weak var webView: UIWebView!
    var title1: String!
    var apiCheck = ""
    var type = ""
    var url : URL!
    var urlString = ""
    var delegate:backFromReview!
    var orderReviewLongTermData = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = title1
        shareBtn.isEnabled = false
        
        if type == "DownloedData" {
            print(type)
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        if type == "" {
            self.makeRequest()
        } else if type == "DownloedData" && urlString != "" {
            shareBtn.isEnabled = true
             url = URL (string: urlString)
            let requestObj = URLRequest(url: url!)
            webView.loadRequest(requestObj)
        } else {
            self.webView.loadHTMLString(orderReviewLongTermData, baseURL: nil)
        }
        // Do any additional setup after loading the view.
    }
    
    func makeRequest() {
        var params = GlobalData.apiName.termsCondition
        if apiCheck == "gdpr" {
            params =  GlobalData.apiName.gdprDataOnsignup
        } else {
            params =  GlobalData.apiName.termsCondition
        }
        
       
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: RequestType.get, controler: Controllers.cmsController ) { (responseObject: JSON?, error: Error?) in
            
            if (error != nil) {
                print("Error logging you in!")
                
            } else {
                
                if responseObject!["term_and_condition"] != JSON.null {
                    self.webView.loadHTMLString(responseObject!["term_and_condition"].stringValue, baseURL: nil)
                } else if responseObject!["termsAndConditions"] != JSON.null {
                    self.webView.loadHTMLString(responseObject!["termsAndConditions"].stringValue, baseURL: nil)
                }
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        if type != "" && type != "DownloedData" {
            delegate.backFromReviewController()
        }
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func shareBtnAct(_ sender: Any) {
        do{
            let largeImageData = try Data(contentsOf: (url))
            let activityItems = [largeImageData]
            let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            if UI_USER_INTERFACE_IDIOM() == .phone {
                self.present(activityController, animated: true, completion: { })
            }
            else {
                let popup = UIPopoverController(contentViewController: activityController)
                popup.present(from: CGRect(x: CGFloat(self.view.frame.size.width / 2), y: CGFloat(self.view.frame.size.height / 4), width: CGFloat(0), height: CGFloat(0)), in: self.view, permittedArrowDirections: .any, animated: true)
            }
        }catch{
            
        }
    }
    
    
    
    @IBAction func ShareClicked(_ sender: Any) {
        do{
            let largeImageData = try Data(contentsOf: (url))
            let activityItems = [largeImageData]
            let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            if UI_USER_INTERFACE_IDIOM() == .phone {
                self.present(activityController, animated: true, completion: { })
            }
            else {
                let popup = UIPopoverController(contentViewController: activityController)
                popup.present(from: CGRect(x: CGFloat(self.view.frame.size.width / 2), y: CGFloat(self.view.frame.size.height / 4), width: CGFloat(0), height: CGFloat(0)), in: self.view, permittedArrowDirections: .any, animated: true)
            }
        }catch{
            
        }
    }
    
}
