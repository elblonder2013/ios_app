//
//  CartProductTableViewCell.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class CartProductTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var wishlistbutton: UIButton!
    @IBOutlet weak var removeButton: UIButton!

    @IBOutlet weak var stepperView: UIStepper!

    var delegate: PassData?
    var oldValue = Double()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cartView.layer.masksToBounds =  false
        cartView.backgroundColor = UIColor.white
        stepperView.minimumValue = 1.0
        stepperView.tintColor = GlobalData.Credentials.accentColor
        wishlistbutton.setTitle("moveToWishlist".localized, for: .normal)
        removeButton.setTitle("remove".localized, for: .normal)

        cartView.myBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: Product? {
        didSet {
            productImage.setImage(imageUrl: (item?.pictureUrl!)!, controller: .cart)
            productNameLabel.text = item?.name
            priceLabel.text = String.init(format: "%@: %@", price.localized, (item?.price)!)
            qtyLabel.text = String.init(format: "%@: %@", qty.localized, (item?.qty)!)
            subTotal.text = String.init(format: "%@: %@", subtotal.localized, (item?.totalPrice)!)
            priceLabel.halfTextColorChange(fullText: priceLabel.text!, changeText: price.localized)
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: qty.localized)
            subTotal.halfTextColorChange(fullText: subTotal.text!, changeText: subtotal.localized)
            stepperView.value = Double(item?.qty ?? "0")!

        }
    }

    @IBAction func StepperAction(_ sender: UIStepper) {

        if sender.value > oldValue {
            delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiUse.updateCartPlus)
        } else {
              delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call:  WhichApiUse.updateCartMinus)
        }
    }

    @IBAction func plusPressed(_ sender: UIButton) {

    }

    @IBAction func minusPressed(_ sender: UIButton) {

    }

    @IBAction func moveWishlistPressed(_ sender: UIButton) {
        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call:  WhichApiUse.moveWishlist)
    }

    @IBAction func removePressed(_ sender: UIButton) {
        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call:  WhichApiUse.RemoveFromCart)
    }

}
