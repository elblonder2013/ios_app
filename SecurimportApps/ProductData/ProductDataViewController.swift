//
//  ProductDataViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 02/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toaster
import FacebookShare
import Social
import SafariServices
@objcMembers
class ProductDataViewController: UIViewController {

    @IBOutlet weak var reviewViewHeight: NSLayoutConstraint!
    @IBOutlet weak var wishlistViewHeight: NSLayoutConstraint!
    @IBOutlet weak var wishlistView: UIView!
    @IBOutlet weak var infoTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topBuynowView: BuyNowView!
    @IBOutlet weak var ReviewCountLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var moreInfoTableView: UITableView!
    @IBOutlet weak var qtyStepper: UIStepper!
    @IBOutlet weak var WishlistImage: UIImageView!
    @IBOutlet weak var AddToWishlistButton: UIButton!
    @IBOutlet weak var strikePrice: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var numberViews: UILabel!
    @IBOutlet weak var addReviewButton: UIButton!
    @IBOutlet weak var StarView: UIView!
    @IBOutlet weak var ReviewView: UIView!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var cartView: BuyNowView!
    @IBOutlet weak var variantViewHeight: NSLayoutConstraint!
    @IBOutlet weak var pageControl: CHIPageControlFresno!
    @IBOutlet weak var variantView: UIView!
    @IBOutlet weak var productImageCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var ProductImageCollection: UICollectionView!
    @IBOutlet weak var ProductImageListCollection: UICollectionView!
    @IBOutlet weak var cartBtn: BadgeBarButtonItem!
    @IBOutlet weak var brandIcon: UIImageView!
    @IBOutlet weak var refLabel: UILabel!
    @IBOutlet weak var instockLabel: UILabel!
    @IBOutlet weak var instockView: UIView!
    @IBOutlet weak var addToCartButtonView: UIView!
    @IBOutlet weak var addToCartButtonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addToCartView: UIView!
    @IBOutlet weak var stockQuantityView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var checkForPrice:UIButton!
    @IBOutlet weak var Addtocart: UIButton!
    @IBOutlet weak var quentitytextField: UITextField!
    fileprivate let productPageViewModalObject = ProductPageViewModal()

    let interactor = Interactor()
    var buyNowView: BuyNowView?
     var topNowView: BuyNowView?

    var name: String?
    var templateId: String?
    var productid: String?
    var tableViewHeight: CGFloat {
        moreInfoTableView.layoutIfNeeded()
        return moreInfoTableView.contentSize.height
    }
    var wishlistIds = [String]()
 var stockQuantityBarView : StockQuantityBarView?
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.isHidden = true
        mainView.isHidden = true
      self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = name
        checkForPrice.isHidden = true
        checkForPrice.setTitle("Check for price".localized, for: .normal)

        Addtocart.setTitle("addToCart".localized, for: .normal)
        if !GlobalData.AddOnsEnabled.productsPriceAvailable {
            price.isHidden = true
            strikePrice.isHidden = true
            checkForPrice.isHidden = false
        }else{
            checkForPrice.isHidden = true
            price.isHidden = false
            strikePrice.isHidden = false
        }
        instockView.backgroundColor = UIColor().HexToColor(hexString: "3B9416")
        addToCartButtonView.backgroundColor = GlobalData.Credentials.accentColor
        Addtocart.backgroundColor = GlobalData.Credentials.accentColor
        addToCartView.layer.borderWidth = 1
        addToCartView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        addToCartView.backgroundColor = GlobalData.Credentials.accentColor
        
        
        productPageViewModalObject.ProductImageListCollection = ProductImageListCollection
        productImageCollectionHeight.constant = SCREEN_WIDTH
        ProductImageCollection.delegate = productPageViewModalObject
        ProductImageCollection.dataSource = productPageViewModalObject
        productPageViewModalObject.passDelegate = self
        productPageViewModalObject.ProductImageCollection = ProductImageCollection
        ProductImageListCollection.register(HomeBannerCollectionViewCell.nib, forCellWithReuseIdentifier: HomeBannerCollectionViewCell.identifier)
        ProductImageListCollection.delegate = productPageViewModalObject
        ProductImageListCollection.dataSource = productPageViewModalObject
        ProductImageListCollection.tag = 99
        ProductImageCollection.isPagingEnabled = true
        StarView.layer.cornerRadius = 3
        ProductImageCollection.register(HomeBannerCollectionViewCell.nib, forCellWithReuseIdentifier: HomeBannerCollectionViewCell.identifier)
        moreInfoTableView.register(SellerProductTableViewCell.nib, forCellReuseIdentifier: SellerProductTableViewCell.identifier)

        // Do any additional setup after loading the view.
        moreInfoTableView.delegate = productPageViewModalObject
        moreInfoTableView.dataSource = productPageViewModalObject
        productPageViewModalObject.delegate = self
        self.layoutSets()
        addReviewButton.setTitle("addYourReview".localized, for: .normal)
        self.makeRequest(dict: [:], call: .none)
        quentitytextField.text = "1"
        if stockQuantityBarView == nil {
            stockQuantityBarView = Bundle.loadView(fromNib: "StockQuantityBarView", withType: StockQuantityBarView.self)
            stockQuantityBarView?.frame = self.stockQuantityView.bounds
            stockQuantityBarView?.View1.layer.cornerRadius = 2
            stockQuantityBarView?.View2.layer.cornerRadius = 2
            stockQuantityBarView?.View3.layer.cornerRadius = 2
            self.stockQuantityView.addSubview(stockQuantityBarView!)
        }
        shareView.layer.cornerRadius = 20
        shareView.layer.borderWidth = 1
        shareView.layer.borderColor = UIColor.lightGray.cgColor
    }
    @IBAction func checkForPriceButtonPrice(){
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if buyNowView == nil {
            buyNowView = Bundle.loadView(fromNib: "BuyNowView", withType: BuyNowView.self)
            buyNowView?.frame = cartView.bounds
            buyNowView?.cartDelegate = self
            cartView.addSubview(buyNowView!)
            buyNowView?.addToCartBtn.setTitle("addToCart".localized, for: .normal)
            buyNowView?.buyNowBtn.setTitle("buyNow".localized, for: .normal)
            buyNowView?.addToCartBtn.backgroundColor = GlobalData.Credentials.accentColor
            buyNowView?.buyNowBtn.backgroundColor = GlobalData.Credentials.accentColor
            //            topBuynowView.addSubview(buyNowView!)
        }
        
        if topNowView == nil {
            topNowView = Bundle.loadView(fromNib: "BuyNowView", withType: BuyNowView.self)
            topNowView?.frame = topBuynowView.bounds
            topNowView?.cartDelegate = self
            topBuynowView.addSubview(topNowView!)
            topBuynowView.isHidden = true
            topNowView?.addToCartBtn.setTitle("addToCart".localized, for: .normal)
            topNowView?.buyNowBtn.setTitle("buyNow".localized, for: .normal)
            topNowView?.buyNowBtn.backgroundColor = GlobalData.Credentials.accentColor
            topNowView?.addToCartBtn.backgroundColor = GlobalData.Credentials.accentColor
        }
        
        
    }

    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func cartBarBtnAction(_ sender: Any) {
        if  self.tabBarController?.selectedIndex == 3{
          self.navigationController?.popViewController(animated: true)
        }else{
         self.tabBarController?.selectedIndex = 3
        }
    }
    
    @IBAction func searchAct(_ sender: Any) {
        if  self.tabBarController?.selectedIndex == 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
        self.tabBarController?.selectedIndex = 1
        }
        
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiUse) {

        var params = ""

        switch call {
        case .addToCart:
            params = GlobalData.apiName.addToCart
        case .addToWishlist:
            params = GlobalData.apiName.addToWishlist
        case .buyNow:
             params = GlobalData.apiName.addToCart
        default:
              params = GlobalData.apiName.productPage + templateId!
        }

       
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: RequestType.post, controler: Controllers.product) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {

                if responseObject!["cartCount"] != JSON.null {
                    self.tabBarController?.setCartCount(value: responseObject!["cartCount"].stringValue)
                    self.cartBtn.badgeNumber = responseObject!["cartCount"].intValue
                }

              switch call {

                 case .addToCart:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                  case .addToWishlist:
                    if responseObject!["success"].boolValue {
                          self.AddToWishlistButton.setTitle("addedToWishlist".localized, for: .normal)
                        self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist-Fill")
                        if let id = dict["productId"] as? String {
                             self.wishlistIds.append(id)
                        }
                       
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                        self.productPageViewModalObject.wishlistExists = true
                    } else {
                       ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                 case .buyNow:
                    if responseObject!["success"].boolValue {
                        let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "checkoutDataViewController") as! CheckoutDataViewController
                        let nav  = UINavigationController(rootViewController: view)
                        self.present(nav, animated: true, completion: nil)
                    } else {

                    }

                default:
                    self.productPageViewModalObject.getValue(jsonData: responseObject!) {

                        (data: Bool) in
                        self.wishlistIds = self.productPageViewModalObject.idArray
                        self.ProductImageCollection.reloadData()
                         self.ProductImageListCollection.reloadData()
                         self.moreInfoTableView.reloadData()
                        self.topBuynowView.isHidden = true
                        if self.productid == nil  {
                            self.productid = self.productPageViewModalObject.productData?.productId
                            self.productid = responseObject!["productId"].stringValue
                        }
                        self.navigationItem.title = self.productPageViewModalObject.productData?.name
                        self.setProductData(productData: self.productPageViewModalObject.productData!)
                         self.mainView.isHidden = false

                        if GlobalData.AddOnsEnabled.wishlistEnable == false {
                            self.wishlistView.isHidden = true
                            self.wishlistViewHeight.constant = 0
                        }

                        if GlobalData.AddOnsEnabled.reviewEnable == false {
                            self.ReviewView.isHidden = true
                            self.reviewViewHeight.constant = 0
                        }

                        if self.productPageViewModalObject.wishlistExists  {
                            self.AddToWishlistButton.setTitle("addedToWishlist".localized, for: .normal)
                            self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist-Fill")
                        } else {
                             self.AddToWishlistButton.setTitle("addToWishlist".localized, for: .normal)
                            self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist")
                        }
                        if self.productPageViewModalObject.combArray.count == 0 && self.productPageViewModalObject.wishlistExists {
                            self.wishlistIds.append((self.productPageViewModalObject.productData?.productId)!)
                        }
                        self.setvariants(variants: self.productPageViewModalObject.variantsArray, productId: self.productid!)
                        self.moreInfoTableView.isScrollEnabled = false
                        self.infoTableViewHeight.constant = self.tableViewHeight

//                        if !self.productPageViewModalObject.descExists && !self.productPageViewModalObject.reviewsExists {
//                            if self.productPageViewModalObject.isSellerProduct {
//                                 self.infoTableViewHeight.constant = 182
//                            } else {
//                                 self.infoTableViewHeight.constant = 50
//                            }
//
//                        } else if self.productPageViewModalObject.descExists && self.productPageViewModalObject.reviewsExists {
//                            if self.productPageViewModalObject.isSellerProduct {
//                                self.infoTableViewHeight.constant = 282
//                            } else {
//                                self.infoTableViewHeight.constant = 200
//                            }
//                        } else {
//                            if self.productPageViewModalObject.isSellerProduct {
//                                self.infoTableViewHeight.constant = 232
//                            } else {
//                                self.infoTableViewHeight.constant = 150
//                            }
//                        }

                    }
                }

            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 
    func isVisible(view: UIView) -> Bool {
        func isVisible(view: UIView, inView: UIView?) -> Bool {
            guard let inView = inView else { return true }
            let viewFrame = inView.convert(view.bounds, from: view)
            if viewFrame.intersects(inView.bounds) {
                return isVisible(view: view, inView: inView.superview)
            }
            return false
        }
        return isVisible(view: view, inView: view.superview)
    }

    func setProductData(productData: ProductPageData ) {
        instockLabel.text = productData.stockMsg
         brandIcon.setImage(imageUrl: productData.brandImage, controller: .product)
        if productData.isStock{
            instockView.backgroundColor = UIColor().HexToColor(hexString: "3B9416")
        }else{
            instockView.backgroundColor = UIColor().HexToColor(hexString: "DC143C")
        }
        if productData.userCustomerGrpId{
            addToCartButtonViewHeight.constant = 50
            addToCartButtonView.isHidden = false
        }else{
            addToCartButtonViewHeight.constant = 0
            addToCartButtonView.isHidden = true
        }
        stockQuantityBarView?.setBackgroundColor(level: productData.stockLevel)
        refLabel.text = productData.reftext
        ProductName.text = productData.name

        if productData.reducePrice!.count > 1 {
            price.text = productData.reducePrice
            strikePrice.text = productData.price
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: productData.price!)
            attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
            strikePrice.attributedText = attributeString
        } else {
            price.text = productData.price
            strikePrice.text = productData.reducePrice
        }

        numberViews.text = String.init(format: "%@ %@", productData.totalReviews!, "reviews".localized)
        ReviewCountLabel.text = productData.rating
      //  qtyStepper.minimumValue = 1

    }

    func fetchDefaultValues (productId: String) -> [VariantsCombination] {

        if let index =  self.productPageViewModalObject.combArray.index(where: { $0.productId == productId }) {
            if self.productPageViewModalObject.combArray[index].addedToWishlist {
                self.AddToWishlistButton.setTitle("Added to Wishlist", for: .normal)
                self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist-Fill")
            }
            
            return self.productPageViewModalObject.combArray[index].varCombArray
        } else {
            return []
        }

    }

    var myCombinationDict = [[String : String]]()

    func setvariants (variants: [Variants], productId: String) {

         let varCombArray = fetchDefaultValues (productId : productId)

        if variants.count > 0 {
            var intY: CGFloat = 8
            for i in 0..<variants.count {

                let varLabel: UILabel = UILabel()
                varLabel.frame = CGRect(x: 8, y: intY, width: SCREEN_WIDTH - 16, height: 17)
                varLabel.font = GlobalData.fonts.small
                varLabel.SetDefaultTextColor()
                varLabel.text = variants[i].name
                self.variantView.addSubview(varLabel)
                intY = intY + 25
                if variants[i].type == "color" {
                    let varScroll: UIScrollView = UIScrollView()
                    varScroll.frame = CGRect(x: 8, y: intY, width: SCREEN_WIDTH - 16, height: 44)
                    varScroll.tag = 100 + i
                    self.variantView.addSubview(varScroll)
                    var intX: CGFloat = 0
                     for j in 0..<variants[i].varArray.count {
                        let circleView: UIButton = UIButton(frame: CGRect(x: intX, y: 8, width:36, height: 36))
                        circleView.backgroundColor = UIColor().HexToColor(hexString: variants[i].varArray[j].htmlCode!)
                        circleView.layer.cornerRadius = 18
                        circleView.tag = (i*1000) + j + 1000
                        circleView.addTarget(self, action:#selector(self.buttonClicked(sender:)), for: .touchUpInside)
                        circleView.applyBorder(colours: GlobalData.Credentials.setBorderColor)
                        varScroll.addSubview(circleView)
                        intX = intX + 44

                        if varCombArray.contains(where: { $0.attributeId == variants[i].attributeId &&   $0.valueId == variants[i].varArray[j].valueId }) {
                            circleView.applyBorder(colours: GlobalData.Credentials.accentColor)
                            circleView.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)
                            let dict = ["valueId" : variants[i].varArray[j].valueId!, "attributeId" : variants[i].attributeId!]
                            myCombinationDict.append(dict)
                            if wishlistIds.contains(productid!) {
                                self.AddToWishlistButton.setTitle("addedToWishlist".localized, for: .normal)
                                self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist-Fill")
                            } else {
                                self.AddToWishlistButton.setTitle("addToWishlist".localized, for: .normal)
                                self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist")
                            }
//                            let dict = ["valueId" : variants[i].varArray[j].valueId! , "attributeId" : variants[i].attributeId!]
//                            combintationDict.append(dict)
                        }
                    }
                     intY = intY + 52
                    varScroll.contentSize = CGSize(width: intX, height: 36)
                } else if  variants[i].type == "radio"  ||  variants[i].type == "select"   ||  variants[i].type == "hidden" {
                    let varScroll: UIScrollView = UIScrollView()
                    varScroll.frame = CGRect(x: 8, y: intY, width: SCREEN_WIDTH - 16, height: 44)
                     varScroll.tag = 100 + i
                    self.variantView.addSubview(varScroll)
                    var intX: CGFloat = 0
                    for j in 0..<variants[i].varArray.count {

                        let buttonText = String.init(format: "%@", variants[i].varArray[j].name!)
                        let stringSize = buttonText.widthOfString(usingFont:  UIFont.systemFont(ofSize: 14))
                        let valueView: UIButton = UIButton(frame: CGRect(x: intX, y: 8, width:stringSize + 26, height: 36))
                        valueView.setTitle(buttonText, for: .normal)
                        valueView.tag = (i*1000) + j + 1000
                        valueView.setTitleColor(GlobalData.Credentials.defaultTextColor, for: .normal)
                        valueView.layer.cornerRadius = 18
                        valueView.titleLabel?.font = GlobalData.fonts.medium
                        valueView.addTarget(self, action:#selector(self.buttonClicked(sender:)), for: .touchUpInside)
                        valueView.applyBorder(colours: GlobalData.Credentials.setBorderColor)
                        varScroll.addSubview(valueView)
                        intX = intX + stringSize + 34
                        if varCombArray.contains(where: { $0.attributeId == variants[i].attributeId &&   $0.valueId == variants[i].varArray[j].valueId }) {
                            valueView.applyBorder(colours: GlobalData.Credentials.accentColor)
                            valueView.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)

                            let dict = ["valueId" : variants[i].varArray[j].valueId!, "attributeId" : variants[i].attributeId!]
                            myCombinationDict.append(dict)
                            if wishlistIds.contains(productid!) {
                                self.AddToWishlistButton.setTitle("addedToWishlist".localized, for: .normal)
                                self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist-Fill")
                            } else {
                                self.AddToWishlistButton.setTitle("addToWishlist".localized, for: .normal)
                                self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist")
                            }
                        }
                    }
                    intY = intY + 52
                    varScroll.contentSize = CGSize(width: intX, height: 44)
                }

            }
            self.variantViewHeight.constant = intY
        } else {
            self.variantViewHeight.constant = 0
        }
    }

    func buttonClicked(sender: UIButton) {
        let superTag = ((sender.superview?.tag)! - 100)

        let attID = self.productPageViewModalObject.variantsArray[superTag].attributeId

        for j in 0..<self.productPageViewModalObject.variantsArray[superTag].varArray.count {

            if let temp: UIButton = self.view.viewWithTag(((superTag  * 1000) + j) + 1000) as? UIButton {
                 let subTag = (sender.tag - 1000)%1000

                if sender.tag == temp.tag && myCombinationDict.count >= superTag  && myCombinationDict.count != 0 {
                     let valId =  self.productPageViewModalObject.variantsArray[superTag].varArray[subTag].valueId
                    myCombinationDict.remove(at: superTag)
                    let dict = ["valueId" : valId!, "attributeId" : attID!]
                    myCombinationDict.insert(dict, at: superTag)
                    self.fetchProductId()

                    temp.applyBorder(colours: GlobalData.Credentials.accentColor)
                    temp.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)
                } else {
                    temp.applyBorder(colours: GlobalData.Credentials.setBorderColor)
                    temp.setTitleColor(GlobalData.Credentials.defaultTextColor, for: .normal)
                }
            }
        }
    }

    func fetchProductId(  ) {
         for i in 0..<self.productPageViewModalObject.combArray.count {
            let arr = self.productPageViewModalObject.combArray[i]

              var ExistCombinationDict = [[String : String]]()
            for j in 0..<arr.varCombArray.count {

                let newArary = arr.varCombArray[j]

                let dict = ["valueId" : newArary.valueId, "attributeId" : newArary.attributeId!]
                ExistCombinationDict.append(dict as! [String: String])

            }
                 var value = 0

                for k in 0..<myCombinationDict.count {
                    if ExistCombinationDict.contains(where: { $0 == myCombinationDict[k] }) {
                        value = 1
                    } else {
                        value = 0
                        break
                    }

                }

                if value == 1 {
                    productid = arr.productId
                    if arr.addedToWishlist {
                        if wishlistIds.contains(productid!) {
                            self.AddToWishlistButton.setTitle("addedToWishlist".localized, for: .normal)
                            self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist-Fill")
                        } else {
                            self.AddToWishlistButton.setTitle("addToWishlist".localized, for: .normal)
                            self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist")
                        }
                    } else {
                        if wishlistIds.contains(productid!) {
                            self.AddToWishlistButton.setTitle("addedToWishlist".localized, for: .normal)
                            self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist-Fill")
                        } else {
                            self.AddToWishlistButton.setTitle("addToWishlist".localized, for: .normal)
                            self.WishlistImage.image = #imageLiteral(resourceName: "Icon-Wishlist")
                        }
                    }
                    if arr.priceReduce!.count > 1 {
                        price.text = arr.priceReduce
                        strikePrice.text = arr.priceUnit
                        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string:  arr.priceUnit!)
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                        strikePrice.attributedText = attributeString
                    } else {
                        price.text = arr.priceUnit
                        strikePrice.text = arr.priceReduce
                    }
                    break
                }
        }
    }

    @IBAction func WishlistPressed(_ sender: UIButton) {
        if LoginCustomer.loginCheckCustomer() {
             if wishlistIds.contains(productid!) {
//            if self.productPageViewModalObject.wishlistExists {
                ErrorChecking.warningView(message: "productWishlist".localized)
                
            } else {
                let dict = ["productId" : productid!] as [String: Any]
                self.makeRequest(dict: dict, call: .addToWishlist)
            }
        } else {
            LoginCustomer.showLogginMessage()
        }

    }

    @IBAction func reviewButtonCLicked(_ sender: UIButton) {
        if LoginCustomer.loginCheckCustomer() {
            let view  = viewController(forStoryboardName: productStoryBoard, forViewControllerName: addReviewViewControllerIdentifier) as! AddReviewViewController
            view.PName = name ?? ""
            view.templateId = templateId!
            self.navigationController?.pushViewController(view, animated: true)
        } else {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func stepperPressed(_ sender: UIButton) {
        if sender.tag == 0, var qty = Int(quentitytextField.text ?? "1"){
            qty = qty + 1
            quentitytextField.text = "\(qty)"
        }else if sender.tag == 1, var qty = Int(quentitytextField.text ?? "1"){
            if qty > 1{
                qty = qty - 1
                quentitytextField.text = "\(qty)"
            }
        }

    }
 @IBAction func btnAddToCart(_ sender: UIButton) {
    if self.productPageViewModalObject.productData?.isStock ?? false{
        addToCart(cart: .cart)
    }else{
         ErrorChecking.warningView(message:  self.productPageViewModalObject.productData?.stockMsg ?? "")
       
    }
    }
    @IBAction func ReviewRatingTap(_ sender: UITapGestureRecognizer) {

        if let total = self.productPageViewModalObject.productData?.totalReviews {
            if total != "0" && total != "" {
                let view  = viewController(forStoryboardName: productStoryBoard, forViewControllerName: reviewsViewControllerIdentifier) as! ReviewsViewController
                view.id = templateId!
                self.navigationController?.pushViewController(view, animated: true)
            }
        }

    }
    @IBAction func unwindToProduc(segue: UIStoryboardSegue) {
        //nothing goes here

        self.makeRequest(dict: [:], call: .none)
    }
}

extension ProductDataViewController {
    func layoutSets() {
        numberViews.SetDefaultTextColor()
        strikePrice.SetDefaultTextColor()
        addReviewButton.titleLabel?.SetDefaultTextColor()
        AddToWishlistButton.titleLabel?.SetDefaultTextColor()
        StarView.applyGradientToTopView(colours:  GlobalData.Credentials.reviewGradient)

    }
}

extension ProductDataViewController: AddToCartDelegate {
    func addToCart(cart: AddCart) {
         if LoginCustomer.loginCheckCustomer() {
            switch cart {
            case .cart:
                let dict = ["productId" : productid!, "add_qty" : Int(quentitytextField.text ?? "1") ?? 1] as [String: Any]
                self.makeRequest(dict: dict, call: .addToCart)
            case .buyNow:
                let dict = ["productId" : productid!, "add_qty" : Int(quentitytextField.text ?? "1") ?? 1] as [String: Any]
                self.makeRequest(dict: dict, call: .buyNow)
            }
        } else {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
        }
    }
}

extension ProductDataViewController: NewViewControllerOpen {

    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {

        switch Controller {
        case .pdf:
            let view  = viewController(forStoryboardName: customerStoryboard, forViewControllerName: viewInvoicesViewControllerIdentifier) as! ViewInvoicesViewController
            view.title1 = name
            view.url = id
            self.navigationController?.pushViewController(view, animated: true)
        case .description:
            let view  = viewController(forStoryboardName: productStoryBoard, forViewControllerName: descriptionViewControllerIdentifier) as! DescriptionViewController
           
            view.htmlData = id
            view.title1 = "Details".localized
            self.navigationController?.pushViewController(view, animated: true)
        case .reviews:
            let view  = viewController(forStoryboardName: productStoryBoard, forViewControllerName: reviewsViewControllerIdentifier) as! ReviewsViewController
            view.id = templateId!
            self.navigationController?.pushViewController(view, animated: true)
        case .sellerProfile: break
           
        default:
            break
        }
    }

}

extension ProductDataViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.isVisible(view: cartView) {
            topBuynowView.isHidden = true
        } else {
            topBuynowView.isHidden = false
        }
    }

}

struct myCombinations {
    var valueId: String?
    var attId: String?

    init(valId: String, attID: String) {
        self.valueId = valId
        self.attId = attID
    }
}

extension ProductDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        let view  = viewController(forStoryboardName: productStoryBoard, forViewControllerName: "zoomImageViewController") as! ZoomImageViewController
        view.dict = dict
        view.currentPage = self.productPageViewModalObject.currentPage
        view.transitioningDelegate = self
        view.interactor = interactor
        let nav  = UINavigationController(rootViewController: view)
        self.present(nav, animated: true, completion: nil)
    }
}

extension ProductDataViewController: UIViewControllerTransitioningDelegate {
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}

class DismissAnimator: NSObject {
}

extension DismissAnimator: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.6
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
            let containerView = transitionContext.containerView
//            else {
//                return
//        }

        containerView.insertSubview((toVC?.view)!, belowSubview: (fromVC?.view)!)

        let screenBounds = UIScreen.main.bounds
        let bottomLeftCorner = CGPoint(x: 0, y: screenBounds.height)
        let finalFrame = CGRect(origin: bottomLeftCorner, size: screenBounds.size)

        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            animations: {
                fromVC?.view.frame = finalFrame
            },
                completion: { _ in
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        )
    }
}
extension ProductDataViewController: SFSafariViewControllerDelegate{
    @IBAction func faceBookShare(){
        if UIApplication.shared.canOpenURL(URL(string: "fbauth2://")!) {
            let content = LinkShareContent(url: URL(string: self.productPageViewModalObject.productData?.productUrl ?? "http://35.178.164.144:8169/") ?? URL(string:"http://35.178.164.144:8169/")!, title: self.ProductName.text ?? "", description: self.productPageViewModalObject.productData?.description ?? "", quote: "", imageURL: nil)
                let shareDialog = ShareDialog(content: content)
                shareDialog.mode = .native
                shareDialog.failsOnInvalidData = true
                shareDialog.completion = { result in
                    // Handle share results
                }
                do{
                    try shareDialog.show()
                }catch{
                    
                }

        }else{
            
        
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            guard let composer = SLComposeViewController(forServiceType: SLServiceTypeFacebook) else { return }
            composer.add(URL(string: self.productPageViewModalObject.productData?.productUrl ?? "http://35.178.164.144:8169/"))
            composer.setInitialText(self.ProductName.text)
            present(composer, animated: true, completion: nil)
        }else {
            let postText = self.ProductName.text ?? ""
            let postUrl = self.productPageViewModalObject.productData?.productUrl ?? "http://35.178.164.144:8169/"
            let shareString = "https://facebook.com/intent/post?text=\(postText)&url=\(postUrl)"
            let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: escapedShareString)
            UIApplication.shared.openURL(url!)
        }
        }
    }
    
    @IBAction func twitterShare(){
        let tweetText = self.ProductName.text ?? ""
        let tweetUrl =  self.productPageViewModalObject.productData?.productUrl ?? "http://35.178.164.144:8169/"
        
        let shareString = "https://twitter.com/intent/tweet?text=\(tweetText)&url=\(tweetUrl)"
        
        // encode a space to %20 for example
        let escapedShareString = shareString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        // cast to an url
        let url = URL(string: escapedShareString)
        
        // open in safari
        UIApplication.shared.openURL(url!)
    }
    @IBAction func linkedinShare(){
        if  UIApplication.shared.canOpenURL(URL(string: "linkedin://app")!){

        LISDKSessionManager.createSession(withAuth:
            [LISDK_BASIC_PROFILE_PERMISSION,LISDK_W_SHARE_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: {(sucess) in
                let session = LISDKSessionManager.sharedInstance().session
                print("Session ",session!)
                //let url = "https://api.linkedin.com/v1/people/~"
                if LISDKSessionManager.hasValidSession(){
                    let url: String = "https://api.linkedin.com/v1/people/~/shares"
                    
                    let payloadStr: String = "{\"comment\":\"\(self.ProductName.text ?? "")\",\"submitted-url\":\"\(self.productPageViewModalObject.productData?.productUrl ?? "http://35.178.164.144:8169/")\",\"visibility\":{\"code\":\"anyone\"}}"
                    
                    let payloadData = payloadStr.data(using: String.Encoding.utf8)
                    
                    LISDKAPIHelper.sharedInstance().postRequest(url, body: payloadData, success: { (response) in
                        print(response!.data)
                    }, error: { (error) in
                        print(error!)
                        
                        let alert = UIAlertController(title: "Alert!", message: "aomething went wrong", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    })
                }
        }) {(error) in
            print("Error \(String(describing: error))")
        }
        }else{
            let urlString = "https://www.linkedin.com/shareArticle?mini=true&url=\(self.productPageViewModalObject.productData?.productUrl ?? "http://35.178.164.144:8169/")&title=\(self.ProductName.text ?? "")&summary=&source=LinkedIn"
            let escapedShareString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            if let url = URL(string: escapedShareString){
           UIApplication.shared.openURL(url)
            }

        }
    }
}
