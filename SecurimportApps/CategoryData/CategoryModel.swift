//
//  CategoryModel.swift
//  Odoo application
//
//  Created by vipin sahu on 9/7/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

class CategoryMenuModel {
    var categories = [Category]()
    init?(data: JSON) {
        if let categories = data.array {
            self.categories = categories.map { Category(json: $0 ) }
        }
    }
}

class Category {
    var name: String?
    var CategoryId: String?
    var isCollapsed = true
    var SubcategoryArray = [SubCategoryData]()
    init(json: JSON) {
        self.name = json["name"].string ?? json["categoryName"].stringValue
        self.CategoryId = json["category_id"].string ?? json["categoryId"].stringValue
        if let categories = json["children"].array {
            self.SubcategoryArray = categories.map { SubCategoryData(json: $0 ) }
        }
    }
}

class SubCategoryData {
    var name: String?
    var CategoryId: String?
    var containSubCategory: Bool
    var jsonData: JSON?
    init(json: JSON) {
        self.name = json["name"].string ?? json["categoryName"].stringValue
        self.CategoryId = json["category_id"].stringValue == "" ? json["categoryId"].stringValue : json["category_id"].stringValue
        if json["children"].count > 0 {
            self.containSubCategory = true
            self.jsonData = json["children"]
        } else {
            self.containSubCategory = false
            self.jsonData = JSON.null
        }
    }
}
