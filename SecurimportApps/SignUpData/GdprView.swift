//
/**
Odoo Marketplace
@Category Webkul
@author    Webkul
Created by: jitendra on 03/08/18
FileName: GdprView.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license   https://store.webkul.com/license.html
*/


import UIKit
import SwiftyJSON

class GdprView: UIView {
    var moveDelegate:NewViewControllerOpen!
    @IBOutlet weak var checkUncheckBtn: UISwitch!
    @IBOutlet weak var termsLabel: UILabel!
    var type = ""

    
    override func awakeFromNib() {
        termsLabel.halfTextWithColorChange(fullText: termsLabel.text!, changeText: "Terms & Conditions".localized, color: UIColor.blue)
        termsLabel.isUserInteractionEnabled = true
        termsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAccept)))
    }
    
    @objc private func didTapAccept() {
        //MARK:- delegate call on the review terms and conditions to the cms page
        moveDelegate.moveToController(id: type, name: "gdpr", dict: JSON.null, Controller: Controllers.cmsController)
    }
    
}
