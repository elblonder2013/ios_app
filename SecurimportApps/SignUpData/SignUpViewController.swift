//
//  SignUpViewController.swift
//  Odoo application
//
//  Created by bhavuk.chawla on 17/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class SignUpViewController: UIViewController {

  @IBOutlet weak var signupTableView: UITableView!
    
    lazy var viewModel : SignUpViewModel = {
        let viewModel = SignUpViewModel()
        return viewModel
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
         FormItemCellType.registerCells(for: self.signupTableView)
        signupTableView.delegate = self
        signupTableView.dataSource = self
        signupTableView.tableFooterView = UIView()
        if GlobalData.AddOnsEnabled.allowGdpr  {
          
            
        } else {
            
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.view.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        
        self.navigationItem.title = appName.localized
      
        viewModel.dismissViewController = {[weak self] (result) in
            if result{
                self?.performSegue(withIdentifier: "back", sender: self)
            }
            
        }
        viewModel.reloadTableView = {[weak self] () in
            self?.signupTableView.reloadData()
            
        }
        
       
    }
    
    @objc func didTapAccept(){
       
        let view = viewController(forStoryboardName: GlobalData.Storyboard.homeStoryboard, forViewControllerName: Controllers.cmsController.rawValue) as! CmsDataViewController
            view.title1 = "Terms & Conditions".localized
            view.apiCheck = "gdpr"
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
    }
    
   
   
    
    @IBAction func SignUpClicked(_ sender: UIButton) {
        
        self.dismissKeyboard()
        
       
//        viewModel.makeRequest(dict: dict, call: WhichApiUse.signUp)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
  
  

}

extension  SignUpViewController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: GlobalData.Storyboard.homeStoryboard, forViewControllerName: Controller.rawValue) as! CmsDataViewController
        view.title1 = "Terms & Conditions".localized
        if name == "gdpr" {
            view.apiCheck = "gdpr"
        } else {
            view.apiCheck = ""
        }
        let nav  = UINavigationController(rootViewController: view)
        self.present(nav, animated: true, completion: nil)
    }
}

extension SignUpViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  viewModel.form.formItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.form.formItems[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.form.formItems[indexPath.section][indexPath.row]
        let cell: UITableViewCell
        if let cellType = viewModel.form.formItems[indexPath.section][indexPath.row].uiProperties.cellType {
            cell = cellType.dequeueCell(for: tableView, at: indexPath)
            
        } else {
            cell = UITableViewCell()
        }
        
        viewModel.form.formItems[indexPath.section][indexPath.row].indexPath = indexPath
        if let formUpdatableCell = cell as? FormUpdatable {
            item.indexPath = indexPath
            formUpdatableCell.update(with: item)
        }
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerList[section]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
