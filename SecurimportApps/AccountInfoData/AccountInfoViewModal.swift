//
//  AccountInfoViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//




import Foundation
import SwiftyJSON
import Toaster
class AccountInfoViewModal : NSObject{
    var dismissViewController : ((_ result:Bool) -> Void)?
    var reloadTableView: (() -> Void)?
    var  logout:(() -> Void)?
    var countryArray = [CountryData]()
    var isAdded = false
    var form = Form()
    var userDetails = JSON()
    var headerList = ["Your Personal Details".localized,"Your Address".localized,"Your Contact Information".localized,"Additional Information".localized,"Your Password"]
    var companyType = ["Private / Independent".localized : "person","company".localized : "company"]
    var company = ["person" : "Private / Independent".localized,"company" : "company".localized]
    override init() {
        super.init()
        if let data = UserDefaults.standard.object( forKey: "userDetails") as? [String:Any]{
            userDetails = JSON(data)
        }
        makeRequest(dict: [:], call: .getCountries)
    }
    
    func createDropDown(index:Int,placeholder: String, key: String = "", key2: String = "", countryData: [CountryData], isRequired: Bool = false, heading: String = "", image: UIImage?, right: UIImage?,placeholder2: String, image2: UIImage?,value:String,value2:String) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.dropDown
        fieldItem.countryData = countryData
        fieldItem.keyType = key
        fieldItem.keyType2 = key2
        fieldItem.isRequired = isRequired
        fieldItem.image = image
        fieldItem.rightIcon = right
        fieldItem.heading = heading
        fieldItem.placeholder2 = placeholder2
        fieldItem.image2 = image2
        fieldItem.value = value
        fieldItem.value2 = value2
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
    }
    func creatbutton(index:Int,placeholder: String) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.button
        fieldItem.actionCompletion = { [weak self] value in
            if value == "submit"{
                self?.update()
            }
        }
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
    }
    func createSingleDropDown(index:Int,placeholder: String, key: String = "",  arrayList: [String], isRequired: Bool = false, heading: String = "", image: UIImage?, right: UIImage?,value:String) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.singleDropDown
        fieldItem.dropDownArray = arrayList
        fieldItem.keyType = key
        fieldItem.image = image
        fieldItem.value = value
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
        fieldItem.actionCompletion = { [weak self] value in
            if value == "chnageCompanyType"{
                
            }
        }
        
        fieldItem.isRequired = isRequired
        fieldItem.rightIcon = right
        fieldItem.heading = heading
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
        
    }
    
    
    func createForm() {
        self.form.formItems = []
        
        self.createSingleDropDown(index: 0, placeholder: "Gender",key: "company_type", arrayList: [company[userDetails["gender"].stringValue] ?? ""], image: nil, right: nil, value: company[userDetails["gender"].stringValue] ?? "")
        self.createTextField(index: 0,placeholder: "vat".localized, isRequired: true, key: "vat", value: userDetails["vat"].stringValue, heading: "vat".localized, valiidationType : "validate-alphanum", image: nil, keyboard: .emailAddress)
        self.createTextField(index: 0,placeholder: "First Name".localized, isRequired: true, key: "firstname", value:  userDetails["firstname"].stringValue, heading: "First Name".localized,valiidationType : "validate-alphanum", image: nil)
        self.createTextField(index: 0,placeholder: "Last Name".localized, isRequired: true, pwdType: false, key: "lastname", value:  userDetails["lastname"].stringValue, heading: "Last Name".localized, valiidationType: "validate-alphanum", image: nil)
        self.createTextField(index: 0,placeholder: "E-Mail Address".localized,isRequired: true, key: "login", value: userDetails["customerEmail"].stringValue, heading: "E-Mail Address".localized, valiidationType : "validate-email", image: nil, keyboard: .emailAddress)
        
        self.createTextField(index: 1,placeholder: "Street Address".localized, isRequired: true, key: "street", value: userDetails["street"].stringValue, heading: "Street Address".localized,valiidationType : "validate-alphanum", image: nil)
        
        self.createTextField(index: 1,placeholder: "Post code".localized, isRequired: true, key: "zipcode", value: userDetails["postCode"].stringValue, heading: "Post code".localized, image: nil, keyboard: .numberPad)
        
        if self.countryArray.count > 0 {
            self.createDropDown(index: 1,placeholder:"country".localized, key: "country_id", key2: "state_id", countryData: self.countryArray, isRequired: true, heading: "Country".localized, image: nil, right: nil, placeholder2: "State".localized, image2: nil, value: userDetails["country"].stringValue, value2: userDetails["state"].stringValue)
        }
        self.createTextField(index: 1,placeholder: "City".localized, isRequired: true, key: "city", value: userDetails["city"].stringValue, heading: "City".localized,valiidationType : "validate-alphanum", image: nil)
        
        self.createTextField(index: 2,placeholder: "Telephone Number: 1".localized, isRequired: true, key: "phone", value:  userDetails["telNumber1"].stringValue, heading: "Telephone Number: 1".localized,valiidationType : "validate-number", image: nil, keyboard: .numberPad)
        self.createTextField(index: 2,placeholder: "Telephone Number: 2".localized, isRequired: false, key: "mobile", value:  userDetails["telNumber2"].stringValue, heading:  "Telephone Number: 2".localized,valiidationType : "validate-number", image: nil, keyboard: .numberPad)
        self.createSwitchControl(index: 3, placeholder: "changePassword".localized, isRequired: false, key: "")
        
        
        self.reloadTableView?()
        
    }
    func changePassword(){
        if let index = self.form.formItems[safe:3]?.firstIndex(where: {$0.placeholder == "enterPassword".localized}){
            self.form.formItems[3].remove(at: index+1)
            self.form.formItems[3].remove(at: index)
        }else{
            self.createTextField(index: 3, placeholder: "enterPassword".localized, isRequired: true, key: "password", value: "", heading: "New Password".localized, image: nil,isSecure:true)
            self.createTextField(index: 3, placeholder: "EnterConfirmPassword".localized, isRequired: true, key: "cPassword", value: "", heading: "confirmPassword".localized, image: nil,isSecure:true)
        }
        self.reloadTableView?()
    }
    
    func createSwitchControl(index:Int,placeholder: String, isRequired: Bool = false, key: String = "") {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.switchControl
        fieldItem.isRequired = isRequired
        fieldItem.keyType = key
        fieldItem.actionCompletionWithValue = { [weak self] (action, value) in
            if action == "changePassword"{
                self?.changePassword()
            }
            
        }
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
        
    }
    
    func createBoolCheck(index:Int,placeholder: String, isRequired: Bool = false, key: String = "", valiidationType: String = "") {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.addressCheck
        fieldItem.isRequired = isRequired
        fieldItem.keyType = key
        fieldItem.valiidationType = valiidationType
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
    }
    
    func createTextField(index:Int,position:Int = -1,placeholder: String, isRequired: Bool = false, pwdType: Bool = false, key: String = "",value : String, heading: String, valiidationType: String = "" , image: UIImage?, keyboard : UIKeyboardType = .default,isSecure : Bool = false) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.textField
        fieldItem.isRequired = isRequired
        fieldItem.isSecure = pwdType
        fieldItem.keyType = key
        fieldItem.heading = heading
        fieldItem.uiProperties.keyboardType = keyboard
        fieldItem.valiidationType = valiidationType
        fieldItem.image = image
        fieldItem.isSecure = isSecure
        fieldItem.value = value
        fieldItem.isShowPassword = isSecure
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
        
        if self.form.formItems[safe:index] != nil{
            if position > 0{
                self.form.formItems[index].insert(fieldItem, at: 1)
            }else{
                self.form.formItems[index] += [fieldItem]
            }
        }else{
            
            self.form.formItems.append([fieldItem])
        }
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiUse) {
        
        var params = ""
        var verb = RequestType.post
        
        if call == WhichApiUse.accountInfo {
            verb = .post
            params = GlobalData.apiName.accountInfo
        } else {
            params =  GlobalData.apiName.getCountries
            verb = .post
        }
        
        NetworkManager.shared.fetchData(params: params, login: false, view: nil, dict: dict, verbs: verb, controler: Controllers.accountInfo) { (responseObject: JSON?, error: Error?) in
            
            if (error != nil) {
                print("Error logging you in!")
            } else {
                if call == WhichApiUse.accountInfo {
                    if responseObject!["success"].boolValue {
                        if self.userDetails["customerEmail"].stringValue != dict["login"] as? String{
                            for i in UserDefaults.standard.dictionaryRepresentation() {
                                print(i.key)
                                if i.key != firebaseKey && i.key != "loadLanguage" && i.key != "defaultLanguage"   && i.key != AppLanguageKey   {
                                    UserDefaults.standard.removeObject(forKey: i.key)
                                    UserDefaults.standard.synchronize()
                                }
                            }
                            UserDefaults.standard.set("yes", forKey: "loadLanguage")
                            UserDefaults.standard.synchronize()
                            self.logout?()
                        }else{
                        Toast(text: responseObject!["message"].stringValue).show()
                        ToastView.appearance().backgroundColor = .black
                        UserDefaults.standard.set(dict["name"] as? String, forKey: customerNameKey)
                        UserDefaults.standard.set( responseObject!.dictionaryObject, forKey: "userDetails")
                        UserDefaults.standard.synchronize()
                        }
                    }else{
                        ErrorChecking.ErrorView(message: responseObject!["message"].stringValue)
                    }
                }else{
                if responseObject!["success"].boolValue {
                    self.setCountries(jsonData: responseObject!) { (data) in
                        self.createForm()
                    }
                }else{
                    ErrorChecking.ErrorView(message: responseObject!["message"].stringValue)
                }
                
                }
            }
        }
    }
    
    
    func setCountries( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = NewAddressModal(data : jsonData ) else {
            return
        }
        if !data.countryArray.isEmpty {
            self.countryArray = data.countryArray
            completion(true)
        } else {
            completion(false)
        }
    }
    
    func update(){
        if !form.isValid().0 {
            ErrorChecking.ErrorView(message:form.isValid().1 + " input value is not valid".localized)
        } else {
            var data = form.getFormData()

            data["company_type"] = companyType[(data["company_type"] as? String) ?? ""]
            print(data)
            if let password = data["password"] as? String{
                if password !=  data["cPassword"] as? String{
                    ErrorChecking.ErrorView(message: "PasswordShouldMatch".localized)
                }else{
                    makeRequest(dict: data, call: WhichApiUse.accountInfo)
                }
            }else{
                makeRequest(dict: data, call: WhichApiUse.accountInfo)
            }
        }
    }
}
