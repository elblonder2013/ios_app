//
//  NewAddressModal.swift
//  Odoo iOS
//
//  Created by Bhavuk on 05/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct NewAddressModal {
     var countryArray = [CountryData]()

    init?(data: JSON) {
        Addons.AddonsChecks(data: data)
        if let array = data["countries"].array {
            if array.count > 0 {
                self.countryArray = array.map { CountryData(data : $0) }
            }
        }
    }
}

struct CountryData {

    var countryid: String?
    var countryName: String?
    var stateArray = [StateData]()
    init(data: JSON) {
        self.countryid = data["id"].stringValue
        self.countryName = data["name"].stringValue
        if let array = data["states"].array {
            if array.count > 0 {
                self.stateArray = array.map { StateData(data : $0) }
            }
        }
    }

}

struct StateData {
    var stateid: String?
    var stateName: String?
        init(data: JSON) {
            self.stateid = data["id"].stringValue
            self.stateName = data["name"].stringValue
        }

}
