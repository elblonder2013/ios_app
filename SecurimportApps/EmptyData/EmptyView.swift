//
//  EmptyView.swift
//  Odoo application
//
//  Created by vipin sahu on 9/6/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    @IBOutlet weak var labelText: UILabel!

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    var delegate: EmptyDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        button.clipsToBounds = true
        button.layer.cornerRadius = 7
        button.backgroundColor = GlobalData.Credentials.accentColor
        labelText.textColor =  GlobalData.Credentials.defaultTextColor
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func buttonClicked(_ sender: UIButton) {
        delegate?.buttonPressed()
    }

}

protocol EmptyDelegate {
    func buttonPressed()
}
