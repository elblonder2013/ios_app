//
//  SubmitTableViewCell.swift
//  SecurimportApps
//
//  Created by debabrata on 25/11/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit
import Reusable

class SubmitTableViewCell: UITableViewCell, FormConformity, NibReusable {
    var formItem: FormItem?
    
@IBOutlet weak var submitBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        submitBtn.backgroundColor =  GlobalData.Credentials.accentColor
        submitBtn.layer.cornerRadius = 4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnPress(){
        self.formItem?.actionCompletion?("submit")
    }
    
}
extension SubmitTableViewCell: FormUpdatable {
    func update(with formItem: FormItem) {
        self.formItem = formItem
      submitBtn.setTitle(formItem.placeholder, for: .normal)

    }
}
