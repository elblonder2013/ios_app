//
//  CheckoutAddressTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class CheckoutAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var billingAddressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        addressView.myBorder()
//        addressView.backgroundColor = UIColor.clear
        billingAddressLabel.text = billingAddress.localized
        billingAddressLabel.SetAccentTextColor()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
