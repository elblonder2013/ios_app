//
//  HomeViewModel.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
import SwiftyJSON

protocol SendDataOfTouchToCollection {
    
}

class HomeViewModel: NSObject,CheckTouch {
    func touchScr(section:Int,row:Int) {
        print("hi")
        
    }
    
    var homeVC = HomeController()
    var delegate: NewViewControllerOpen?
    var cameraAction: CameraAction?
    var navBar : UINavigationController!
    var tableview: UITableView!
    var items = [HomeViewModalItem]()
    var categories = [CategorySet]()
    var banner = [BannerSet]()
    var homePageWishListArray = [String]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data =  HomeModel(data: jsonData) else {
            return
        }
        delegateToch = self
        items.removeAll()
        homePageWishListArray.removeAll()
        //MARK: For Homage Page WishList data
        if jsonData["wishlist"].count > 0 {
            for i in 0..<jsonData["wishlist"].count {
                homePageWishListArray.append(jsonData["wishlist"][i].stringValue)
            }
        }
        //         if !data.categories.isEmpty {
        let categoryItem = HomeViewModalCategoryItem(catName: data.categories)
        categories = data.categories
        items.append(categoryItem)
        
        //         }
        if !data.banners.isEmpty {
            let bannerItem  = HomeViewModalBannerItem(banner: data.banners)
            banner = data.banners
            items.append(bannerItem)
        }
        
        if !data.mainProduct.isEmpty {
            for i in 0..<data.mainProduct.count {
                let friendsItem = HomeViewModalProductsItem(product: data.mainProduct[i].products, head: data.mainProduct[i].HeadingName!, sliderUrl:  data.mainProduct[i].sliderUrl!, slider_mode:  data.mainProduct[i].slider_mode!, colorCode: data.mainProduct[i].colorCode!)
                items.append(friendsItem)
            }
        }
        
        completion(true)
        
    }
    
}

extension HomeViewModel: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[indexPath.section]
        
        switch item.type {
        case .categories:
            if let item = item as? HomeViewModalCategoryItem {
                navBar.navigationBar.isHidden = true
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeCategoryCell.identifier) as! HomeCategoryCell
                cell.homeCategoryCollection.tag = indexPath.section
                if item.catName.count == 0 {
                    cell.categoryCollectionHeight.constant = 0
                    cell.homeCategoryCollection.reloadData()
                    cell.gradientView.applyGradient(colours: GlobalData.Credentials.gradientArray)
                }
                cell.categories = item.catName
                cell.delegate = self
                cell.delegate1 = self
                cell.backgroundColor = UIColor.clear
                cell.homeCategoryCollection.backgroundColor = UIColor.white
                cell.layoutIfNeeded()
                cell.homeCategoryCollection.reloadData()
                return cell
            }
            
        case .banner:
            if let item = item as? HomeViewModalBannerItem {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerTableViewCell.identifier) as! HomeBannerTableViewCell
                cell.bannerCollection.tag = indexPath.section
                cell.banner = item.banner
                cell.delegate = self
                cell.collectionType = .banner
                cell.layoutIfNeeded()
                return cell
            }
        case .products:
            if let item = item as? HomeViewModalProductsItem {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeProductTableViewCell.identifier) as! HomeProductTableViewCell
                cell.backgroundColor =  GlobalData.Credentials.defaultBackgroundColor
                print(item.colorCode)
                
                if item.slider_mode == "fixed"{
                    if item.colorCode.count > 0 {
                        cell.backgroundColor = UIColor().HexToColor(hexString: item.colorCode)
                    } else {
                        //                            cell.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
                    }
                    if let layout =  cell.homeProductCollection.collectionViewLayout as? UICollectionViewFlowLayout {
                        layout.scrollDirection = .vertical
                    }
                    cell.backgroundColor = UIColor.clear
                } else {
                    print(item.product)
                    if let layout =  cell.homeProductCollection.collectionViewLayout as? UICollectionViewFlowLayout {
                        layout.scrollDirection = .horizontal
                        cell.collectionHeight.constant = SCREEN_WIDTH/2.5 + 10
                    }
                }
                cell.homeProductCollection.tag = indexPath.section
                cell.slider_mode = item.slider_mode
                cell.products = item.product
                DispatchQueue.main.async {
                    cell.homeProductCollection.reloadData()
                }
                cell.button.tag = indexPath.section
                cell.delegate = self
                cell.heading.text = item.heading
                cell.homeVC = homeVC
                cell.layoutIfNeeded()
                if item.slider_mode == "fixed"{
                    cell.backgroundColor = UIColor.clear
                    cell.homeProductCollection.backgroundColor = UIColor.clear
                }
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.section]
        switch item.type {
        case .banner:
            return SCREEN_WIDTH / 2
        default:
            return UITableView.automaticDimension
        }
    }
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //         return 300
    //    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let  cell = tableview.cellForRow(at: IndexPath(row: 0, section: 0)) as? HomeCategoryCell {
            if tableview.visibleCells.contains(cell) {
                navBar.navigationBar.isHidden = true
            } else {
                navBar.navigationBar.isHidden = false
            }
        } else {
            navBar.navigationBar.isHidden = false
        }
        
    }
    
}



extension HomeViewModel: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        
        if call == WhichApiUse.categories {
            
            delegate?.moveToController(id: id, name: name, dict: JSON.null, Controller: Controllers.productCategory)
        } else if call == WhichApiUse.products {
            delegate?.moveToController(id: id, name: name, dict: JSON.null, Controller: Controllers.product)
            
        } else if call == WhichApiUse.custom {
            delegate?.moveToController(id: id, name: name, dict: JSON.null, Controller: Controllers.custom)
        } else {
            if   let item = items[index] as? HomeViewModalProductsItem {
                delegate?.moveToController(id: item.sliderUrl, name: item.heading, dict: JSON.null, Controller: Controllers.homeSliders)
            }
        }
    }
}

extension HomeViewModel: SearchAction {
    func notificationAction() {
        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.notifications)
    }
    
    func doSearchAction() {
        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.search)
    }
    
    func openCameraAction(sender: UIButton) {
        self.cameraAction?.openCameraOptions(sender: sender)
    }
    
}

protocol CameraAction {
    func openCameraOptions(sender: UIButton)
}

protocol HomeViewModalItem {
    var type: HomeType { get }
    var rowCount: Int { get }
}

class HomeViewModalCategoryItem: HomeViewModalItem {
    var type: HomeType {
        return .categories
    }
    var rowCount: Int {
        return 1
    }
    var catName: [CategorySet]
    
    init(catName: [CategorySet]) {
        self.catName = catName
    }
}
class HomeViewModalBannerItem: HomeViewModalItem {
    var type: HomeType {
        return .banner
    }
    var rowCount: Int {
        return 1
    }
    var banner: [BannerSet]
    
    init(banner: [BannerSet]) {
        self.banner = banner
    }
}

class HomeViewModalProductsItem: HomeViewModalItem {
    var type: HomeType {
        return .products
    }
    var rowCount: Int {
        return 1
    }
    
    var heading: String
    var sliderUrl: String
    var slider_mode: String
    var product: [Product]
    var colorCode: String
    
    init(product: [Product], head: String, sliderUrl: String, slider_mode: String, colorCode: String) {
        self.product = product
        self.heading = head
        self.sliderUrl = sliderUrl
        self.slider_mode = slider_mode
        self.colorCode = colorCode
    }
}

enum HomeType {
    case categories
    case banner
    case products
}
