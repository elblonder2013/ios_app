//
//  ProfileController.swift
//  Odoo application
//
//  Created by vipin sahu on 9/5/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

import SwiftyJSON
import Kingfisher
import SVProgressHUD
@objcMembers
class ProfileController: UIViewController {
 var height = SCREEN_WIDTH / 2
    var headerView: ProfileHeader?
     @IBOutlet weak var profileTableView: UITableView!
     let gradient: CAGradientLayer = CAGradientLayer()
    var profileArray = [String]()
     var profileImageArray = [UIImage]()
    var sellerProfileArray = [String]()
    var sellerProfileImageArray = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        navigationItem.title = profile.localized
        profileTableView.tableFooterView = UIView()

          headerView = (Bundle.main.loadNibNamed("ProfileHeader", owner: self, options: nil)?[0] as? ProfileHeader)!
        headerView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_WIDTH/2)

        profileTableView.estimatedRowHeight = UITableView.automaticDimension
        profileTableView.tableHeaderView = headerView
        profileTableView.tableHeaderView = nil
        profileTableView.contentInset  = UIEdgeInsets(top: SCREEN_WIDTH/2, left: 0, bottom: 0, right: 0)
        headerView?.delegate = self
        headerView?.passDataDelegate = self
        if let value = UserDefaults.standard.value(forKey: CustomerImageUrl) as? String {
            
            let urls =  value + String.init(format: "/%ix%i", Int((64) * UIScreen.main.scale), Int((64) * UIScreen.main.scale))
            print(urls)
            self.headerView?.profileImage.kf.setImage(with: URL(string : urls), options: nil)
            self.headerView?.profileImage.contentMode = .scaleAspectFill
        }
        
        profileTableView.estimatedSectionHeaderHeight = 40.0
        self.blur()
        profileTableView.addSubview(headerView!)

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
       
         NotificationCenter.default.addObserver(self, selector: #selector(EmailVerifyChanged), name: NSNotification.Name(rawValue: "AccountProfileChanged"), object: nil)
         SVProgressHUD.dismiss()
        profileArray.removeAll()
        sellerProfileArray.removeAll()
        sellerProfileImageArray.removeAll()

        if UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
               profileTableView.contentInset  = UIEdgeInsets(top: SCREEN_WIDTH/2, left: 0, bottom: 0, right: 0)
            headerView?.isHidden = false
            self.HeaderDataSet()
            profileArray.append(accountInfo.localized)
            if GlobalData.AddOnsEnabled.isEmailVerified {
                profileImageArray.append(UIImage(named : "Table-Icon-User-Info")!)
                profileArray.append(addressBook.localized)
                profileImageArray.append(UIImage(named : "Table-Icon-Address-Book")!)
                profileArray.append(allOrder.localized)
                profileImageArray.append(UIImage(named : "Table-Icon-Orders")!)
                if GlobalData.AddOnsEnabled.wishlistEnable {
                profileArray.append(myWishlist.localized)
                profileImageArray.append(UIImage(named : "Table-Icon-Wishlist")!)
                }
            } else {
                profileImageArray.append(UIImage(named : "profile-page")!)
                profileArray.append(addressBook.localized)
                profileImageArray.append(UIImage(named : "Table-Icon-Address-Book")!)
                profileArray.append(allOrder.localized)
                profileImageArray.append(UIImage(named : "Table-Icon-Orders")!)
                if GlobalData.AddOnsEnabled.wishlistEnable {
                    profileArray.append(myWishlist.localized)
                    profileImageArray.append(UIImage(named : "Table-Icon-Wishlist")!)
                }

            }
            
            profileArray.append("Invoices and Payments".localized)
            profileImageArray.append(UIImage(named : "Tabe-Icon-User")!)

        } else {

            headerView?.isHidden = true
               profileTableView.contentInset  = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            profileTableView.scrollsToTop = true
            profileArray.append(signInSignUp.localized)
             profileImageArray.append(UIImage(named : "Tabe-Icon-User")!)
            profileArray.append(shareApp.localized)
             profileImageArray.append(UIImage(named : "Table-Icon-Share")!)
            profileArray.append(rateUs.localized)
             profileImageArray.append(UIImage(named : "Table-Icon-Rate-Us")!)
             profileTableView.reloadData()
            let  indexPath1 = IndexPath.init(row: 0, section: 0)
            profileTableView.scrollToRow(at: indexPath1, at: .top, animated: true)
        }
        
        profileArray.append("languages".localized)
        profileImageArray.append(UIImage(named : "Table-Icon-Language")!)
        if GlobalData.Credentials.environment == EnvironmentType.marketplace {
            sellerProfileArray.append("marketplace".localized)
             sellerProfileImageArray.append(UIImage(named : "Icon Copy 4")!)
        }
        if UserDefaults.standard.value(forKey: loggedInUserKey) != nil  &&  GlobalData.AddOnsEnabled.is_seller && GlobalData.Credentials.environment == EnvironmentType.marketplace {
             sellerProfileArray.append("sellerDashboard".localized)
             sellerProfileImageArray.append(UIImage(named : "activity")!)
            sellerProfileArray.append("sellerProfile".localized)
            sellerProfileImageArray.append(UIImage(named : "order")!)
            sellerProfileArray.append("sellerOrder".localized)
            sellerProfileImageArray.append(UIImage(named : "seller-profile")!)
            sellerProfileArray.append("askToAdmin".localized)
            sellerProfileImageArray.append(UIImage(named : "message")!)
        }

        profileTableView.reloadData()

    }
    override func viewWillLayoutSubviews() {
      
    }

    func HeaderDataSet () {
        if UserDefaults.standard.value(forKey: customerEmailKey) != nil {
            headerView?.emailLabel.text = UserDefaults.standard.value(forKey: customerEmailKey) as? String
        }
        if UserDefaults.standard.value(forKey: customerNameKey) != nil {
            headerView?.nameLabel.text = UserDefaults.standard.value(forKey: customerNameKey) as? String
        }
        self.headerView?.profileImage.image = #imageLiteral(resourceName: "User-Profile-Icon")
        print(UserDefaults.standard.value(forKey: ))
        if let value = UserDefaults.standard.value(forKey: CustomerImageUrl) as? String {
            let urls =  value + String.init(format: "/%ix%i", Int((64) * UIScreen.main.scale), Int((64) * UIScreen.main.scale))
            print(urls)
            self.headerView?.profileImage.kf.setImage(with: URL(string : urls), options: nil)
            self.headerView?.profileImage.contentMode = .scaleAspectFill
        }
        
    }

    func EmailVerifyChanged(notification: Notification) {
        if UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
            if GlobalData.AddOnsEnabled.isEmailVerified {

                profileImageArray.remove(at: 1)
                profileImageArray.insert(UIImage(named : "Table-Icon-User-Info")!, at: 1)

            } else {
                profileImageArray.remove(at: 1)
                profileImageArray.insert(UIImage(named : "profile-page")!, at: 1)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        // Dispose of any resources that can be recreated.
    }

    func makeRequest(dict: [String: Any] ) {

        let params = GlobalData.apiName.accountInfo
       
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: .post, controler: Controllers.Profile) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                if responseObject!["success"].boolValue {
                    UserDefaults.standard.set(responseObject!["customerProfileImage"].stringValue, forKey: CustomerImageUrl)
                    UserDefaults.standard.synchronize()
                    ImageCache.default.removeImage(forKey: responseObject!["customerProfileImage"].stringValue)
                    let cache = KingfisherManager.shared.cache
                    cache.clearMemoryCache()
                    cache.clearDiskCache()
                    cache.cleanExpiredDiskCache()
                    let urls =  responseObject!["customerProfileImage"].stringValue + String.init(format: "/%ix%i", Int((64) * UIScreen.main.scale), Int((64) * UIScreen.main.scale))
                    print(urls)
                    self.headerView?.profileImage.image = nil
                     print(self.headerView?.profileImage.image)
                    self.headerView?.profileImage.kf.setImage(with: URL(string : urls), options: nil)
                    self.headerView?.profileImage.contentMode = .scaleAspectFill
                    print(self.headerView?.profileImage.image)
                }

            }
        }
    }



}

extension ProfileController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: accountInfoDataViewControllerIdentifier) as! AccountInfoDataViewController
        self.navigationController?.pushViewController(view, animated: true)
    }
}

enum Customer {
    case dashboard
    case account
    case address
    case order
    case wishlist
}

extension ProfileController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return profileArray.count
        case 1:
            return sellerProfileArray.count
        default:
           return 0
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.imageView?.image = profileImageArray[indexPath.row]
            cell.textLabel?.text = profileArray[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            return cell
        case 1:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.imageView?.image = sellerProfileImageArray[indexPath.row]
            cell.textLabel?.text = sellerProfileArray[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:

            switch profileArray[indexPath.row] {
            case signInSignUp.localized:
                let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
                let nav  = UINavigationController(rootViewController: view)
                self.present(nav, animated: true, completion: nil)
            case accountInfo.localized:
                   let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: dashBoardDatalViewControllerIdentifier) as! DashBoardDatalViewController
                   self.navigationController?.pushViewController(view, animated: true)
            case shareApp.localized:
                let objectsToShare = ["https://itunes.apple.com/us/app/app-name/id1314246215?ls=1&mt=8"]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                activityVC.popoverPresentationController?.sourceView = tableView.cellForRow(at: indexPath)
                self.present(activityVC, animated: true, completion: nil)

            case addressBook.localized :
                    let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: addressDataViewControllerIdentifier) as! AddressDataViewController
                    self.navigationController?.pushViewController(view, animated: true)
                
            case rateUs.localized:
                let url = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/id1314246215?mt=8")!
                UIApplication.shared.openURL(url)

            case "languages".localized:
                    let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: "languageProfileDataViewController") as! LanguageProfileDataViewController
                    self.navigationController?.pushViewController(view, animated: true)
                
            case allOrder.localized:
                let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: orderHistoryViewControllerIdentifier) as! OrderHistoryViewController
                self.navigationController?.pushViewController(view, animated: true)
                

            case myWishlist.localized:
                 if GlobalData.AddOnsEnabled.wishlistEnable {
                let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: "languageProfileDataViewController") as! LanguageProfileDataViewController
                        self.navigationController?.pushViewController(view, animated: true)
                 }
            case "Invoices and Payments".localized:
                let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: "InvoicesViewController") as! InvoicesViewController
                self.navigationController?.pushViewController(view, animated: true)
           
            default:
              break
            }
        case 1:

            break
        default:
            break
        }
    }

//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//         return UITableViewAutomaticDimension
//    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
            return UITableView.automaticDimension
        } else {
            return 44.0
        }
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.profileTableView.reloadData()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
            if headerView != nil {
            let yPos: CGFloat = -scrollView.contentOffset.y

                if yPos > 0 {
                    var imgRect: CGRect? = headerView?.frame
                    imgRect?.origin.y = scrollView.contentOffset.y
                    imgRect?.size.height = SCREEN_WIDTH/2 + yPos  - SCREEN_WIDTH/2
                    headerView?.frame = imgRect!
                    self.profileTableView.sectionHeaderHeight = (imgRect?.size.height)!

                }
            }
        }
    }

     func applyGradientToTopView(colours: [UIColor]) {

        gradient.frame = (headerView?.profileView.frame)!

        gradient.colors = colours.map { $0.withAlphaComponent(0.90).cgColor }
         gradient.removeFromSuperlayer()
        headerView?.profileView.layer.insertSublayer(gradient, at: 0)
    }

}

extension ProfileController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {

        let alert = UIAlertController(title: "UPLOAD".localized, message: nil, preferredStyle: .actionSheet)

        let takeAction = UIAlertAction(title: "Take a photo".localized, style: .default, handler: take)
        let upload = UIAlertAction(title: "Upload from library".localized, style: .default, handler: uploadImage)
        let CancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: cancel)

        alert.addAction(takeAction)
        alert.addAction(upload)
        alert.addAction(CancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width : 1.0, height : 1.0)

        self.present(alert, animated: true, completion: nil)
    }

    func take(alertAction: UIAlertAction!) {

        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    func uploadImage(alertAction: UIAlertAction!) {

        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)

    }
    func cancel(alertAction: UIAlertAction!) {

    }

    func blur() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = (headerView?.bannerImage.bounds)!
        blurEffectView.alpha = 0.9
        blurEffectView.backgroundColor = GlobalData.Credentials.accentColor.withAlphaComponent(0.4)
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        headerView?.bannerImage!.addSubview(blurEffectView)
    }
}


extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {


//
//        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
//
//        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
//            let imageData = image.jpegData(compressionQuality: 0.8) as Data?
//                       let strBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
//                       let dict : [String : Any] = ["image" : strBase64!]
//            self.makeRequest(dict: dict)
//
//        } else {
//            print("Something went wrong")
//        }

//        var selectedImage: UIImage?
//        if let editedImage = info[.editedImage] as? UIImage {
//            selectedImage = editedImage
//            //self.profileImageArray.image = selectedImage!
//            let imageData = selectedImage!.pngData()
//            let strBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
//            let dict: [String: Any] = ["image" : strBase64!]
//
//            //self.makeRequest(dict: dict)
//            picker.dismiss(animated: true, completion: nil)
//        } else if let originalImage = info[.originalImage] as? UIImage {
//            selectedImage = originalImage
//            let imageData = selectedImage!.pngData()
//            let strBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
//            let dict: [String: Any] = ["image" : strBase64!]
//
//            self.makeRequest(dict: dict)
//            picker.dismiss(animated: true, completion: nil)
//        }
//
        

        if let image = info[.editedImage] as? UIImage {
            let newImg = fixOrientation(img: image)
            
            let imageData = newImg.jpegData(compressionQuality: 0.8) as NSData!
            let strBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
            let dict: [String: Any] = ["image" : strBase64!]
           
            

            self.makeRequest(dict: dict)
        } else {
            print("Something went wrong")
        }
//
        self.dismiss(animated: true, completion: nil)
    }
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
    
    
    
    private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
        return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
        return input.rawValue
    }

}
//extension UIImage {
//    var jpegRepresentationData: NSData! {
//        return UIImageJPEGRepresentation(self, 1.0)! as NSData   // QUALITY min = 0 / max = 1
//    }
//    var pngRepresentationData: NSData! {
//        return self.pngData()! as NSData
//    }
//}
