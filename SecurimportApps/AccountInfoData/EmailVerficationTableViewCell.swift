//
//  EmailVerficationTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 18/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON

class EmailVerficationTableViewCell: UITableViewCell {
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var resendVarificationBtn: UIButton!
    
    var delegate: PassData?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        headingLabel.text = emailVerificationHeading.localized
        headingLabel.SetDefaultTextColor()
        if  let email =  UserDefaults.standard.value(forKey: customerEmailKey) as? String {
            emailField.text = email
        }
        emailField.isUserInteractionEnabled = false
        resendVarificationBtn.backgroundColor = GlobalData.Credentials.accentColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    @IBAction func ResendButtonClicked(_ sender: UIButton) {
        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.sendVerificationLink)
    }

}
