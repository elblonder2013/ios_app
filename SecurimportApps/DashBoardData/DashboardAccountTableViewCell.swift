//
//  DashboardAccountTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 13/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class DashboardAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
