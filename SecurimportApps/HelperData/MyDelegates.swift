//
//  MyDelegates.swift
//  Odoo application
//
//  Created by Bhavuk on 28/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

// for passing data
//
protocol PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse)
}
//MARK:- delegate for the review terms and conditions for account info page
protocol AccountDeacticvateAction {
    func accountDeactivate(type:String)
    func downloadAllInfo(type:String)
    func downloadGdprRequest(type:String)
}
@objc protocol cartCount {
    func cartValue(count:String)
    func pushToBuyNow()
    @objc optional func addToWishList()
    @objc optional func homeRefreshTouch()
    
}

// for moving to next controllers

protocol NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers)
}
protocol backFromReview {
    func backFromReviewController()
}
