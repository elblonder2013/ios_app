//
//  CheckoutDataViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toaster

class CheckoutDataViewController: UIViewController,backFromReview {
    
    @IBOutlet weak var ReviewView: UIView!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressViewHeight: NSLayoutConstraint!
    @IBOutlet weak var CheckoutScrollView: UIScrollView!
    @IBOutlet weak var reviewTopLabel: UILabel!
    @IBOutlet weak var paymentTopLabel: UILabel!
    @IBOutlet weak var addressTopLabel: UILabel!
    @IBOutlet weak var AddressTopView: UIView!
    @IBOutlet weak var paymentTopView: UIView!
    @IBOutlet weak var reviewTopView: UIView!
    @IBOutlet weak var addressInternalView: UIImageView!
    @IBOutlet weak var paymentInternalTopView: UIImageView!
    @IBOutlet weak var reviewInternalTopView: UIImageView!
    @IBOutlet weak var addressPaymentReviewBckgndView: UIView!
    
    @IBOutlet weak var shippingTopLabel: UILabel!
    @IBOutlet weak var shippingInternalView: UIImageView!
    @IBOutlet weak var ShippingTopView: UIView!
    @IBOutlet weak var addressTapGestureRecognizer: UITapGestureRecognizer!
    
    @IBOutlet weak var shippingView: UIView!
    @IBOutlet var shippingTapGesture: UITapGestureRecognizer!
    @IBOutlet var paymentTapgestureRecognizer: UITapGestureRecognizer!
    var addressViewXib: CheckoutAddressView?
    var checkoutPaymentXib: CheckoutPaymentView?
    var checkoutOrderReviewXib: CheckoutOrderReviewView?
    var checkoutShippingViewXib: ShippingView?
    
    var addressId: String?
    var apiUse: WhichApiUse?
    var shippingId: String?
    var paymentID : String = ""
    var paymentCode : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        apiUse = WhichApiUse.getAddress
        addressPaymentReviewBckgndView.semanticContentAttribute = .forceLeftToRight
        self.addressPaymentReviewBckgndView.isHidden = true
        self.addressView.isHidden = true
        self.makeRequest(id: "", dict: [:])
        //        self.addressTapGestureRecognizer.isEnabled = false
        //        self.paymentTapgestureRecognizer.isEnabled = false
        // Do any additional setup after loading the view.
        self.stringsLocalized()
        
    }
    
    func stringsLocalized() {
        addressTopLabel.text = "address".localized
        shippingTopLabel.text = "shipping".localized
        paymentTopLabel.text = "payment".localized
        reviewTopLabel.text = "review".localized
        self.navigationItem.title = "checkout".localized
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CheckoutScrollView.contentSize = CGSize(width: 3*SCREEN_WIDTH, height: UIScreen.main.bounds.size.height - 100)
        CheckoutScrollView.isPagingEnabled = true
        CheckoutScrollView.isScrollEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func BackPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func alert1(message: String, title: String ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK".localized, style: .default, handler: { (_ action: UIAlertAction) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //    override func viewDidDisappear(_ animated: Bool) {
    //          self.tabBarController?.tabBar.isHidden = false
    //    }
    //
    
    func setTopView (call: WhichApiUse) {
        AddressTopView.backgroundColor = UIColor.clear
        paymentTopView.backgroundColor = UIColor.clear
        reviewTopView.backgroundColor = UIColor.clear
        ShippingTopView.backgroundColor = UIColor.clear
        
        switch call {
        case .getAddress:
            self.removeAssets()
            
            addressTopLabel.textColor = UIColor.black
            paymentTopLabel.SetDefaultTextColor()
            reviewTopLabel.SetDefaultTextColor()
            shippingTopLabel.SetDefaultTextColor()
            AddressTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            addressInternalView.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
            
            
            ShippingTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
            shippingInternalView.backgroundColor = GlobalData.Credentials.defaultCheckoutColor
            
            paymentTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
            paymentInternalTopView.backgroundColor = GlobalData.Credentials.defaultCheckoutColor
            
            reviewTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
            reviewInternalTopView.backgroundColor = GlobalData.Credentials.defaultCheckoutColor
            
        case .getShipping:
            self.removeAssets()
            
            addressTopLabel.textColor = UIColor.black
            shippingTopLabel.textColor = UIColor.black
            paymentTopLabel.SetDefaultTextColor()
            reviewTopLabel.SetDefaultTextColor()
            
            AddressTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            addressInternalView.image = #imageLiteral(resourceName: "check-icon-16x16")
            addressInternalView.backgroundColor = GlobalData.Credentials.accentColor
            
            
            ShippingTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            shippingInternalView.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
            
            paymentTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
            paymentInternalTopView.backgroundColor = GlobalData.Credentials.defaultCheckoutColor
            
            reviewTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
            reviewInternalTopView.backgroundColor = GlobalData.Credentials.defaultCheckoutColor
            
        case .getPayment:
            
            self.removeAssets()
            
            addressTopLabel.textColor = UIColor.black
            shippingTopLabel.textColor = UIColor.black
            paymentTopLabel.textColor = UIColor.black
            reviewTopLabel.SetDefaultTextColor()
            
            AddressTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            addressInternalView.image = #imageLiteral(resourceName: "check-icon-16x16")
            addressInternalView.backgroundColor = GlobalData.Credentials.accentColor
            
            paymentTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            paymentInternalTopView.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
            
            reviewTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
            reviewInternalTopView.backgroundColor = GlobalData.Credentials.defaultCheckoutColor
            
            if GlobalData.AddOnsEnabled.allowShipping {
                ShippingTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
                shippingInternalView.image = #imageLiteral(resourceName: "check-icon-16x16")
                shippingInternalView.backgroundColor = GlobalData.Credentials.accentColor
            } else {
                ShippingTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
                shippingInternalView.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
                shippingTopLabel.SetDefaultTextColor()
            }
            
            
        case .getOrderReview:
            self.removeAssets()
            
            addressTopLabel.textColor = UIColor.black
            paymentTopLabel.textColor = UIColor.black
            reviewTopLabel.textColor = UIColor.black
            shippingTopLabel.textColor = UIColor.black
            
            AddressTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            addressInternalView.image = #imageLiteral(resourceName: "check-icon-16x16")
            addressInternalView.backgroundColor = GlobalData.Credentials.accentColor
            
            paymentTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            paymentInternalTopView.image = #imageLiteral(resourceName: "check-icon-16x16")
            paymentInternalTopView.backgroundColor = GlobalData.Credentials.accentColor
            if GlobalData.AddOnsEnabled.allowShipping {
                ShippingTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
                shippingInternalView.image = #imageLiteral(resourceName: "check-icon-16x16")
                shippingInternalView.backgroundColor = GlobalData.Credentials.accentColor
            } else {
                ShippingTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
                shippingInternalView.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
                shippingTopLabel.SetDefaultTextColor()
            }
            
            
            reviewTopView.applyBorder(colours: GlobalData.Credentials.accentColor)
            reviewInternalTopView.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
            
        case .placeOrder:
            print()
        default:
            print("")
        }
        
    }
    
    func removeAssets() {
        if let lay = addressInternalView.layer.sublayers {
            for layers in lay {
                layers.removeFromSuperlayer()
            }
        }
        if let lay = paymentInternalTopView.layer.sublayers {
            for layers in lay {
                layers.removeFromSuperlayer()
            }
        }
        
        if let lay = shippingInternalView.layer.sublayers {
            for layers in lay {
                layers.removeFromSuperlayer()
            }
        }
        
        if let lay = reviewInternalTopView.layer.sublayers {
            for layers in lay {
                layers.removeFromSuperlayer()
            }
        }
        
        addressInternalView.backgroundColor = UIColor.clear
        paymentInternalTopView.backgroundColor = UIColor.clear
        reviewInternalTopView.backgroundColor = UIColor.clear
        shippingInternalView.backgroundColor = UIColor.clear
        
        shippingInternalView.image = nil
        addressInternalView.image = nil
        paymentInternalTopView.image = nil
        reviewInternalTopView.image = nil
        
        AddressTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
        paymentTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
        reviewTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
        ShippingTopView.applyBorder(colours: GlobalData.Credentials.defaultCheckoutColor)
        
        addressTopLabel.SetDefaultTextColor()
        paymentTopLabel.SetDefaultTextColor()
        reviewTopLabel.SetDefaultTextColor()
        shippingTopLabel.SetDefaultTextColor()
        
    }
    //MARK:- back from the cms page
    func backFromReviewController() {
        self.setTopView(call: WhichApiUse.getOrderReview)
        self.CheckoutScrollView.setContentOffset(CGPoint(x: 3*SCREEN_WIDTH, y:  0), animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func AddressTap(_ sender: UITapGestureRecognizer) {
        self.setTopView(call: WhichApiUse.getAddress)
        self.CheckoutScrollView.setContentOffset(CGPoint(x: 0, y:  0), animated: true)
    }
    
    @IBAction func shippingTap(_ sender: UITapGestureRecognizer) {
        if GlobalData.AddOnsEnabled.allowShipping {
            if self.CheckoutScrollView.contentOffset.x > SCREEN_WIDTH {
                self.setTopView(call: WhichApiUse.getShipping)
                self.CheckoutScrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH, y:  0), animated: true)
            }
        }
        
    }
    
    @IBAction func paymentTap(_ sender: UITapGestureRecognizer) {
        if self.CheckoutScrollView.contentOffset.x > 2*SCREEN_WIDTH {
            self.setTopView(call: WhichApiUse.getPayment)
            self.CheckoutScrollView.setContentOffset(CGPoint(x: 2*SCREEN_WIDTH, y:  0), animated: true)
        }
    }
    
}

extension CheckoutDataViewController {
    
    func makeRequest(id: String, dict: [String: Any]) {
        var params = ""
        
        var verb: RequestType?
        switch apiUse {
        case .getAddress?:
            params = GlobalData.apiName.getAddress
            verb = .post
        case .getPayment?:
            params = GlobalData.apiName.paymentMethod
            verb = .post
        case .getOrderReview?:
            params = GlobalData.apiName.orderReview
            verb = .post
        case .getShipping?:
            params = GlobalData.apiName.getShipping
            verb = .get
        case .placeOrder?:
            params = GlobalData.apiName.placeOrder
            verb = .post
        default:
            print("")
        }
        
        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: verb!, controler: Controllers.checkout) { (responseObject: JSON?, error: Error?) in
            
            if (error != nil) {
                print("Error logging you in!")
                
            } else {
                switch self.apiUse {
                    
                case .getAddress?:
                    self.addressPaymentReviewBckgndView.isHidden = false
                    self.addressView.isHidden = false
                    self.setTopView(call: self.apiUse!)
                    self.addressViewHeight.constant = UIScreen.main.bounds.size.height - 160
                    if self.addressViewXib == nil {
                        self.addressViewXib = Bundle.loadView(fromNib: "CheckoutAddressView", withType: CheckoutAddressView.self)
                        self.addressViewXib?.frame = self.addressView.bounds
                        self.addressViewXib?.delegate = self
                        self.addressViewXib?.moveDelegate = self
                        self.addressView.addSubview(self.addressViewXib!)
                    }
                    self.addressViewXib?.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                    }
                case .getShipping?:
                    print()
                    self.setTopView(call: self.apiUse!)
                    self.CheckoutScrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH, y:  0), animated: true)
                    print(self.CheckoutScrollView.contentOffset)
                    if self.checkoutShippingViewXib == nil {
                        self.checkoutShippingViewXib = Bundle.loadView(fromNib: "ShippingView", withType: ShippingView.self)
                        self.checkoutShippingViewXib?.frame = self.paymentView.bounds
                        self.checkoutShippingViewXib?.delegate = self
                        self.shippingView.addSubview(self.checkoutShippingViewXib!)
                    }
                    self.checkoutShippingViewXib?.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        
                    }
                case .getPayment?:
                    self.setTopView(call: self.apiUse!)
                    self.CheckoutScrollView.setContentOffset(CGPoint(x: 2*SCREEN_WIDTH, y:  0), animated: true)
                    print(self.CheckoutScrollView.contentOffset)
                    if self.checkoutPaymentXib == nil {
                        self.checkoutPaymentXib = Bundle.loadView(fromNib: "CheckoutPaymentView", withType: CheckoutPaymentView.self)
                        self.checkoutPaymentXib?.frame = self.paymentView.bounds
                        self.checkoutPaymentXib?.delegate = self
                        self.paymentView.addSubview(self.checkoutPaymentXib!)
                    }
                    self.checkoutPaymentXib?.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        
                    }
                case .getOrderReview?:
                    self.setTopView(call: self.apiUse!)
                    self.CheckoutScrollView.setContentOffset(CGPoint(x: 3*SCREEN_WIDTH, y:  0), animated: true)
                    if responseObject!["success"].boolValue {
                        if self.checkoutOrderReviewXib == nil {
                            self.checkoutOrderReviewXib = Bundle.loadView(fromNib: "CheckoutOrderReviewView", withType: CheckoutOrderReviewView.self)
                            self.checkoutOrderReviewXib?.frame = self.ReviewView.bounds
                            self.checkoutOrderReviewXib?.delegate = self
                            self.checkoutOrderReviewXib?.moveDelegate = self
                            
                            self.ReviewView.addSubview(self.checkoutOrderReviewXib!)
                        }
                        self.checkoutOrderReviewXib?.getValue(jsonData: responseObject!) {
                            (data: Bool) in
                            
                        }
                    } else {
                        self.alert1(message: responseObject!["message"].stringValue, title: "Error")
                    }
                case .placeOrder?:
                    if responseObject!["success"].boolValue {
                        let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "orderPlaceDataViewController") as! OrderPlaceDataViewController
                        if let data = responseObject {
                            view.desc = data["message"].stringValue
                            view.id = data["name"].stringValue
                        } else {
                            
                        }
                        
                        self.navigationController?.pushViewController(view, animated: true)
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                default :
                    print("")
                }
                
            }
        }
    }
    
}

extension CheckoutDataViewController: PassData {
    
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        
        switch call {
        case .getPayment:
            shippingId = id
            apiUse = WhichApiUse.getPayment
            self.makeRequest(id: "", dict: [:])
        case .getOrderReview :
            paymentID = id
            paymentCode = name
            if let shipId = addressId {
                var  dict = ["shippingAddressId" : shipId, "acquirerId" : id]
                if GlobalData.AddOnsEnabled.allowShipping {
                    dict["shippingId"] = checkoutShippingViewXib?.shippingId
                }
                
                apiUse = WhichApiUse.getOrderReview
                self.makeRequest(id: "", dict: dict)
            }
        case .placeOrder :
            if paymentCode == "REDSYS"{
                let viewWeb = viewController(forStoryboardName: checkoutStoryBoard, forViewControllerName: "PaymentezPaymentGatewayViewController") as! PaymentezPaymentGatewayViewController
                viewWeb.delegate = self
                viewWeb.url = self.checkoutOrderReviewXib?.paymentUrl ?? ""
                let navPayfort  = UINavigationController(rootViewController: viewWeb)
                self.present(navPayfort, animated: true, completion: nil)
            }else{
            let dict = ["paymentReference" : "", "paymentStatus" : ""]
            apiUse = WhichApiUse.placeOrder
            self.makeRequest(id: "", dict: dict)
            }
        case .getShipping:
            addressId = id
            apiUse = WhichApiUse.getShipping
            self.makeRequest(id: "", dict: [:])
        default:
            print()
        }
        
    }
    
}

extension CheckoutDataViewController: AddressUpdate {
    func addressupdated(addressId: String) {
        apiUse = WhichApiUse.getAddress
        addressViewXib?.shippingAddressId = addressId
        self.makeRequest(id: "", dict: [:])
    }
}

extension CheckoutDataViewController: NewViewControllerOpen {
    
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        
        print("myyyyy", Controller)
        
        if Controller == Controllers.editAddress {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
            view.addressId = id
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }
        
        if Controller == Controllers.addAddress {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }
        
        if Controller == Controllers.changeAddress {
            
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: "changeAddressViewController") as! ChangeAddressViewController
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }
        //MARK:- go to the cms page
        if Controller == Controllers.cmsController{
            let view = viewController(forStoryboardName: GlobalData.Storyboard.homeStoryboard, forViewControllerName: Controller.rawValue) as! CmsDataViewController
            view.title1 = "Terms & Conditions".localized
            if name == "gdpr" {
                view.apiCheck = "gdpr"
            } else {
                view.apiCheck = ""
            }
            view.type = id
            view.delegate = self
            view.orderReviewLongTermData = (self.checkoutOrderReviewXib?.dataForLongTerms)!
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
        }
    }
}
extension CheckoutDataViewController: paymentCheckDelegate{
    func paymentCheck(status: Bool, reference: String) {
        let tile = status ? "successfull".localized : "Order Failed".localized
//        let message = status ? "successfull".localized : "Order Failed".localized
        let alert = UIAlertController(title: tile, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertAction.Style.cancel, handler: { _ in
             self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
