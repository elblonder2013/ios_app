//
//  OrderHistoryModal.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

class OrderHistoryModel {

    var orders = [orderHistoryData]()
    init?(data: JSON) {
        Addons.AddonsChecks(data: data)
        if data["recentOrders"] != JSON.null {
            if let data  = data["recentOrders"].array {
                self.orders = data.map { orderHistoryData( data: $0) }
            }

        }
    }
}

class orderHistoryData {
    var name: String?
    var address: String?
    var total: String?
    var date: String?
    var status: String?
    var orderId: String?
    var subtotal: String?
    var taxes: String?
    var grantTotal: String?
    init(data: JSON) {
        self.name = data["name"].stringValue
        self.address = data["shipping_address"].stringValue
        self.total = data["amount_total"].stringValue
        self.date = data["create_date"].stringValue
        self.status = data["status"].stringValue
        self.orderId = data["id"].stringValue

        // For order Review

        self.subtotal = data["subtotal"]["value"].stringValue
        self.taxes = data["tax"]["value"].string ?? data["amount_tax"].stringValue
        self.grantTotal = data["grandtotal"]["value"].stringValue
    }
}
