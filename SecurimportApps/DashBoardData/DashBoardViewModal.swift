//
//  DashBoardViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

class  DashBoardViewModal: NSObject {
     var moveDelegate: NewViewControllerOpen?
    var items = [DashboardItem]()
    var delegate: NewViewControllerOpen?
    func getValue( jsonData: JSON, cases: Dashboard, completion: ((_ data: Bool) -> Void) ) {

        switch cases {

        case .order:

            if let name = UserDefaults.standard.value(forKey: customerNameKey) as? String, let email =  UserDefaults.standard.value(forKey: customerEmailKey) as? String {
                items.append(DashboardAccountItem(name: name, email: email))
            }
            guard let data = DashBoardOrderModal(data : jsonData) else {
                return
            }
            if !data.orders.isEmpty {
                items.append(DashboardOrderItem(orders: data.orders))
                completion(true)
            } else {
                completion(false)
            }
        case .address:
            guard let data = AddressModal(data : jsonData) else {
                return
            }

            if data.billingAddres != nil {
                items.append(DashboardAddressItem(billing: data.billingAddres!))
                if !data.shippingAddress.isEmpty {

                    items.append(DashboardShippingAddressItem(shipping: data.shippingAddress[0]))
                    completion(true)
                } else {
                    completion(false)
                }

            }

        default:
            print()
        }
    }
}

extension DashBoardViewModal: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]

        switch item.type {

        case  .account:
            if let item = item as? DashboardAccountItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: DashboardAccountTableViewCell.identifier) as! DashboardAccountTableViewCell
                cell.nameLabel.text = item.name
                cell.emailLabel.text = item.email
                return cell
            }

        case .order:
            if let item = item as? DashboardOrderItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: OrderHistoryViewCell.identifier) as! OrderHistoryViewCell
                cell.item = item.orders[indexPath.row]
                cell.selectionStyle = .none
                cell.separatorInset = UIEdgeInsets(top: 0, left: SCREEN_WIDTH/2, bottom: 0, right: SCREEN_WIDTH/2)
                return cell
            }
        case .address :
            if let item = item as? DashboardAddressItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: DashboardBillingAddressTableViewCell.identifier) as! DashboardBillingAddressTableViewCell
                cell.addressLabel.text =  item.billing?.address!
                cell.selectionStyle = .none
                return cell
            }

        case .shipping:
            if let item = item as? DashboardShippingAddressItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: DashBoardShippingTableViewCell.identifier) as! DashBoardShippingTableViewCell
                cell.address.text = item.shipping?.address!
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
            }
        case .signOut:
              return UITableViewCell()
        }

        return UITableViewCell()
    }

//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if items[section].type == Dashboard.order {
//
//
//
////            let view = UITableViewHeaderFooterView()
////            view.textLabel?.text = "X Clear Searches"
////            view.textLabel?.textColor = GlobalData.Credentials.defaultTextColor
////            view.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
////            view.textLabel?.backgroundColor = UIColor.clear
////            view.backgroundColor = UIColor.clear
////            view.tintColor = UIColor.clear
////            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
////            tapRecognizer.numberOfTapsRequired = 1
////            tapRecognizer.numberOfTouchesRequired = 1
////            view.addGestureRecognizer(tapRecognizer)
////            return view
//        }
//        else{
//            return nil
//        }
//    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].heading
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch items[indexPath.section].type {
        case .account:
                delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.accountInfo)
        case .order:
            if let item = items[indexPath.section] as? DashboardOrderItem {
                delegate?.moveToController(id:item.orders[indexPath.row].orderId!, name: item.orders[indexPath.row].name!, dict: JSON.null, Controller: Controllers.orderDetails)
            }
        case .address:
            if let item = items[indexPath.section] as? DashboardAddressItem {
                delegate?.moveToController(id:(item.billing?.addressId!)!, name: "", dict: JSON.null, Controller: Controllers.address)
            }
        case .shipping:
            if let item = items[indexPath.section] as? DashboardShippingAddressItem {
                delegate?.moveToController(id:(item.shipping?.addressId!)!, name: "", dict: JSON.null, Controller: Controllers.address)
            }

        case .signOut:
            print()
        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class DashboardAccountItem: DashboardItem {

    var type: Dashboard {
        return .account
    }

    var heading: String {
        return "accountInformation".localized
    }

    var rowCount: Int {
        return 1
    }
    var name: String?
    var email: String?

    init(name: String, email: String) {
        self.name = name
        self.email = email
    }

}

class DashboardOrderItem: DashboardItem {

    var type: Dashboard {
        return .order
    }

    var heading: String {
        return recentOrders.localized
    }

    var rowCount: Int {
        return orders.count
    }
    var orders: [orderHistoryData]

    init(orders: [orderHistoryData]) {
        self.orders = orders
    }

}

class DashboardAddressItem: DashboardItem {

    var type: Dashboard {
        return .address
    }

    var heading: String {
        return billingAddress.localized
    }

    var rowCount: Int {
        return 1
    }

    var billing: AddressData?

    init(billing: AddressData ) {
        self.billing = billing
    }
}

class DashboardShippingAddressItem: DashboardItem {

    var type: Dashboard {
        return .shipping
    }

    var heading: String {
        return shippingAddress.localized
    }

    var rowCount: Int {
        return 1
    }

    var shipping: AddressData?

    init(shipping: AddressData ) {
        self.shipping = shipping
    }

}

protocol DashboardItem {
    var type: Dashboard { get }
    var rowCount: Int { get }
    var heading: String { get }
}

extension DashBoardViewModal: NewViewControllerOpen {

    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        moveDelegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controller)
    }
}

enum Dashboard {
    case account
    case order
    case address
    case shipping
    case signOut
}
