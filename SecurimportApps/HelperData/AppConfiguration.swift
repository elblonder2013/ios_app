//
//  GlobalData.swift
//  CS-Cart MVVM
//
//  Created by vipin sahu on 6/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON
import Toaster
import SwiftMessages
var delegateToch:CheckTouch!
var typeOfShortcut = ""
let SCREEN_WIDTH = ((UIApplication.shared.statusBarOrientation == .portrait) || (UIApplication.shared.statusBarOrientation == .portraitUpsideDown) ? UIScreen.main.bounds.size.width : UIScreen.main.bounds.size.height)
let screenHeight = UIScreen.main.bounds.size.height

var CategoryArray: JSON?


enum EnvironmentType {
    case mobikul, marketplace
}
struct SortData {
    var list = [[String]]()
    init(data: JSON) {
        list = data.arrayValue.map{
            $0.arrayValue.map{$0.stringValue}
        }
    }
}
struct GlobalData {
    
    struct Credentials {
        static let environment:EnvironmentType = .mobikul
        static let BASEURL : String =  "https://www.securimport.com/"
//        static let BASEURL : String = "http://35.176.118.59:8169/en_US/"
        static let BASEDATA : String = "Acf5Df35QWE:Acf5Df35QWE"
        static let defaultCheckoutColor  = UIColor().HexToColor(hexString: "#C2C5CC")
        static let textColor: String = "#83BF3A"
        static let defaultBackgroundColor  = UIColor().HexToColor(hexString: "#F5F5F5")
        static let fontName: String = ".SFUIText-Medium"
        static let boldFontName: String = "HelveticaNeue-Bold"
        static let accentColor  = UIColor().HexToColor(hexString: "#000000")
        static let gradientArray = [UIColor().HexToColor(hexString: "316195"), UIColor().HexToColor(hexString: "316195")]
        static let setBorderColor = UIColor(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0)
        static let defaultTextColor = UIColor().HexToColor(hexString: "#ADADB0")
        static let textFieldBackgroundColor = UIColor().HexToColor(hexString: "#316195")
        static let reviewGradient =  [UIColor().HexToColor(hexString: "93BC4B"), UIColor().HexToColor(hexString: "9ED836")]
        static let textFieldBorder = UIColor().HexToColor(hexString: "C8C7CC")

    }
    struct AddOnsEnabled {
        static var wishlistEnable = true
        static var reviewEnable = true
        static var emailVerification = true
        static var isEmailVerified = true
        static var allowResetPassword = true
        static var allowSignUp = true
        static var allowGuest = true
        static var is_seller = false
        static var allowShipping = false
        static var allowGdpr = false
        static var productsPriceAvailable = false
        static var sortData : SortData?
        
    }
    
    struct apiName {
        
        //home
        
        static let getHomePage = "mobikul/homepage"
        static let getSplashData = "mobikul/splashPageData"
        
        // cart
        static let getCartApi = "mobikul/mycart/"
        
        static let cartToWishlist = "my/cartToWishlist"
        static let emptyCart = "mobikul/mycart/setToEmpty"
        static let addToCart = "mobikul/mycart/addToCart"
        
        // order
        static let orders = "mobikul/my/orders"
        static let orderDetail = "mobikul/my/order/"
        
        // notification
        static let notifications = "mobikul/notificationMessages"
        
        // login
        static let login = "mobikul/customer/login"
        static let signUp = "mobikul/customer/signUp"
        static let forget  = "mobikul/customer/resetPassword"
        static let registerFcm = "mobikul/registerFcmToken"
        static let signOut = "mobikul/customer/signOut"
        static let gdprDataOnsignup = "mobikul/signup/terms"
        static let getcustomerform = "mobikul/getcustomerform"
        
        // wishlist
        static let wishlist = "mobikul/my/wishlists"
        static let removeWishlist = "my/removeWishlist/"
        static let wishlistToCart = "my/wishlistToCart"
        static let addToWishlist = "my/addToWishlist"
        
        // address
        static let getAddress = "mobikul/my/addresses"
        static let getCountries = "mobikul/localizationData"
        static let addAddress = "mobikul/my/address/new"
        static let editAddress = "mobikul/my/address/"
        static let defaultAddress = "mobikul/my/address/default/"
        
        //search
        
        static let searchSuggestion = "mobikul/search"
        
        // Product Category
        
        static let productCategory = "mobikul/sliderProducts/"
        
        // Product Page
        
        static let productPage = "mobikul/template/"
        
        // Reviews
        
        static let reviews = "product/reviews"
        static let saveReview = "my/saveReview"
        static let guestSaveReview  = "guest/saveReview"
        static let likeDislike = "review/likeDislike"
        
        // checkout
        
        static let paymentMethod = "mobikul/paymentAcquirers"
        static let orderReview = "mobikul/orderReviewData"
        static let placeOrder = "mobikul/placeMyOrder"
        static let getShipping = "mobikul/ShippingMethods"
        
        // account infoaccount
        static let accountInfo = "mobikul/saveMyDetails"
        static let verifyEmail = "send/verifyEmail"
        static let gdprDeactivate = "mobikul/gdpr/deactivate"
        static let dowaloadGdpr = "mobikul/gdpr/download"
        static let dowaloadGdprRequest = "mobikul/gdpr/downloadRequest"
        // markeplace api's
        
        static let sellerProfile = "my/Template/seller/"
        static let sellerReviews = "my/review/seller/"
        static let allSeller = "mobikul/marketplace"
        static let sellerDashBoard = "mobikul/marketplace/seller/dashboard"
        static let sellerProduct = "mobikul/marketplace/seller/product"
        static let sellerOrders = "mobikul/marketplace/seller/orderlines"
        static let termsCondition = "mobikul/marketplace/seller/terms"
        static let askToAdmin = "mobikul/marketplace/seller/ask"
        static let becomeSeller = "mobikul/marketplace/become/seller"
        
        static let invoices = "mobikul/my/invoices-and-payments"
        
    }
    
    struct QueryArray {
        static var values = [String]()
    }
    
    struct Storyboard {
        static  let homeStoryboard = "Home"
        static  let customerStoryboard = "Customer"
        static let productStoryBoard = "Product"
        static let marketplaceStoryBoard = "MarketPlace"
        static let loginStoryboard = "Login"
        
    }
    
    struct fonts {
        static let small = UIFont(name: "SFUIText-Regular", size: 12.0)
        static let medium = UIFont(name: "SFUIText-Regular", size: 14.0)
        static let natural = UIFont(name: "SFUIText-Regular", size: 17.0)
        static let semiBoldLarge = UIFont(name: "SFUIText-Semibold", size: 20.0)
        static let semiBoldnormal = UIFont(name: "SFUIText-Semibold", size: 17.0)
        
    }
    
    struct StoreData {
        static var guestWishlistData = [String]()
        static var languages = [AllLanguages]()
        static var defaultLanguage = ""
        static var check = false
        static var firstLaunch = false
    }
}

extension UIColor {
    func HexToColor(hexString: String, alpha: CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}

struct  Addons {
    static func AddonsChecks(data: JSON) {
        
        if data["addons"]["wishlist"] != JSON.null {
            if data["addons"]["wishlist"].boolValue {
                GlobalData.AddOnsEnabled.wishlistEnable = true
            } else {
                GlobalData.AddOnsEnabled.wishlistEnable = false
            }
        }
        
        //MARK:- gdpr allowship
        
//        if data["addons"]["odoo_gdpr"] != JSON.null {
//            if data["addons"]["odoo_gdpr"].boolValue {
//                GlobalData.AddOnsEnabled.allowGdpr = true
//            } else {
//                GlobalData.AddOnsEnabled.allowGdpr = false
//            }
//        }
        
        if data["addons"]["review"] != JSON.null {
            if data["addons"]["review"].boolValue {
                GlobalData.AddOnsEnabled.reviewEnable = true
            } else {
                GlobalData.AddOnsEnabled.reviewEnable = false
            }
        }
        
        if data["addons"]["email_verification"] != JSON.null {
            print(data["addons"]["email_verification"].boolValue)
            if data["addons"]["email_verification"].boolValue {
                GlobalData.AddOnsEnabled.emailVerification = true
            } else {
                GlobalData.AddOnsEnabled.emailVerification = false
            }
            
        }
        if data["allowShipping"] != JSON.null {
            if data["allowShipping"].boolValue {
                GlobalData.AddOnsEnabled.allowShipping = true
            } else {
                GlobalData.AddOnsEnabled.allowShipping = false
            }
        }
        if (data["sortData"].array != nil) {
            GlobalData.AddOnsEnabled.sortData = SortData(data:data["sortData"])
           
        }
        
        
        if data["is_email_verified"] != JSON.null {
            if data["is_email_verified"].boolValue {
                GlobalData.AddOnsEnabled.isEmailVerified = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ProfileChanged"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AccountProfileChanged"), object: nil)
            } else {
                GlobalData.AddOnsEnabled.isEmailVerified = false
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ProfileChanged"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AccountProfileChanged"), object: nil)
            }
        }
        
        if data["allow_guestCheckout"] != JSON.null {
            if data["allow_guestCheckout"].boolValue {
                GlobalData.AddOnsEnabled.allowGuest = true
            } else {
                GlobalData.AddOnsEnabled.allowGuest = false
            }
        }
        
        if data["allow_signup"] != JSON.null {
            if data["allow_signup"].boolValue {
                GlobalData.AddOnsEnabled.allowSignUp = true
            } else {
                GlobalData.AddOnsEnabled.allowSignUp = false
            }
        }
        
        if data["allow_resetPwd"] != JSON.null {
            if data["allow_resetPwd"].boolValue {
                GlobalData.AddOnsEnabled.allowResetPassword = true
            } else {
                GlobalData.AddOnsEnabled.allowResetPassword = false
            }
        }
        
    }
    
}

struct ErrorChecking {
    
    static func warningView(message: String) {
        let success = MessageView.viewFromNib(layout: .messageView)
        success.configureTheme(.warning)
        success.configureDropShadow()
        success.configureContent(title: "warning".localized, body: message)
        //        success.configureContent(title: "warning".localized, body: message, iconText: "😳")
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.duration = .seconds(seconds: 0.5)
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    static func successView(message: String) {
        let success = MessageView.viewFromNib(layout: .messageView)
        success.configureTheme(.success)
        success.configureDropShadow()
        //                success.backgroundColor = UIColor.black
        success.configureContent(title: "success".localized, body: message)
        //        success.configureContent(title: "success".localized, body:message, iconText: "👍")
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.duration = .seconds(seconds: 0.5)
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    static func ErrorView(message: String?) {
        let success = MessageView.viewFromNib(layout: .messageView)
        success.configureTheme(.error)
        success.configureDropShadow()
        //            success.backgroundColor = UIColor.black
        //        success.configureContent(title: "error".localized, body: message ?? "someErrorOccur".localized, iconText: "😯")
        success.configureContent(title: "error".localized, body: message ?? "someErrorOccur".localized)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.duration = .seconds(seconds: 0.5)
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
}

struct  LoginCustomer {
    
    static func loginCheckCustomer() -> Bool {
        if UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
            return true
        } else {
            return false
        }
    }
    
    static func showLogginMessage() {
        ErrorChecking.warningView(message: "Please login first")
    }
    
}

extension Character {
    fileprivate func isEmoji() -> Bool {
        return Character(UnicodeScalar(UInt32(0x1d000))!) <= self && self <= Character(UnicodeScalar(UInt32(0x1f77f))!)
            || Character(UnicodeScalar(UInt32(0x2100))!) <= self && self <= Character(UnicodeScalar(UInt32(0x26ff))!)
    }
}

extension String {
    func stringByRemovingEmoji() -> String {
        return String(self.filter { !$0.isEmoji() })
    }
}

extension String {
    func replace(string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
}

extension NSAttributedString {
    
    /** Will Trim space and new line from start and end of the text */
    public func trimWhiteSpace() -> NSAttributedString {
        let invertedSet = CharacterSet.whitespacesAndNewlines.inverted
        let startRange = string.utf16.description.rangeOfCharacter(from: invertedSet)
        let endRange = string.utf16.description.rangeOfCharacter(from: invertedSet, options: .backwards)
        guard let startLocation = startRange?.upperBound, let endLocation = endRange?.lowerBound else {
            return NSAttributedString(string: string)
        }
        
        let location = string.utf16.distance(from: string.startIndex, to: startLocation) - 1
        let length = string.utf16.distance(from: startLocation, to: endLocation) + 2
        let range = NSRange(location: location, length: length)
        return attributedSubstring(from: range)
    }
    
}
