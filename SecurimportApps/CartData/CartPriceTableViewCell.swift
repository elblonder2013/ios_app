//
//  CartPriceTableViewCell.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class CartPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var subtotaltext: UILabel!
    @IBOutlet weak var subtotalprice: UILabel!
    @IBOutlet weak var taxtext: UILabel!
    @IBOutlet weak var taxprice: UILabel!
    @IBOutlet weak var totaltext: UILabel!
    @IBOutlet weak var totalprice: UILabel!
     @IBOutlet weak var emptyButton: UIButton!

    var delegate: PassData?

    override func awakeFromNib() {
        super.awakeFromNib()
        subtotaltext.text = subtotal.localized
        totaltext.text = total.localized
        taxtext.text = tax.localized
        emptyButton.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)
        emptyButton.setTitle("emptyCart".localized, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
    @IBAction func emptyPressed(_ sender: Any) {
        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 1, call: WhichApiUse.cartEmpty)
    }

    var item: CartData? {
        didSet {
           subtotalprice.text = item?.subTotal
            taxprice.text = item?.tax
            totalprice.text = item?.total

        }
    }

}
