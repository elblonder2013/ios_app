//
//  WishlistDataViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toaster

class WishlistDataViewController: UIViewController {
  var apiUse: WhichApiUse?
    var emptyView: EmptyView?
     var wishlistViewModalObject  = WishlistViewModal()
    @IBOutlet weak var wishlistTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = myWishlist.localized
        wishlistViewModalObject.delegate = self
        // Do any additional setup after loading the view.
        wishlistTableView.delegate = wishlistViewModalObject
        wishlistTableView.dataSource = wishlistViewModalObject
        wishlistTableView.estimatedRowHeight = 100
        wishlistTableView.rowHeight = UITableView.automaticDimension
        wishlistTableView.separatorStyle = .none
        wishlistViewModalObject.passdataDelegate = self
        wishlistTableView.register(WishlistProductTableViewCell.nib, forCellReuseIdentifier: WishlistProductTableViewCell.identifier)
        self.makeRequest(id: "", dict: [:])
    }

    func makeRequest(id: String, dict: [String: Any]) {
        var params = ""

        var verb: RequestType?
        switch apiUse {
        case .removeFromWishlist?:
             params = GlobalData.apiName.removeWishlist + id
            verb = .delete
        case .moveToCart?:
            params = GlobalData.apiName.wishlistToCart
            verb = .post
        default:
             params = GlobalData.apiName.wishlist
             verb = .post
        }

         
        NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: verb!, controler: Controllers.Profile) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                if responseObject!["cartCount"] != JSON.null {
                    self.tabBarController?.setCartCount(value: responseObject!["cartCount"].stringValue)
                }
                switch self.apiUse {

                case .removeFromWishlist?:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                        self.apiUse = nil
                        self.makeRequest(id: "", dict: [:])
                    } else {

                    }
                case .moveToCart?:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message:  responseObject!["message"].stringValue)
                        self.apiUse = nil
                        self.makeRequest(id: "", dict: [:])
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }

                default:
                    self.wishlistViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        if data {
                            self.emptyView?.isHidden = true
                            self.wishlistTableView.isHidden = false
                            self.wishlistTableView.reloadData()
                        } else {
                            self.emptyView?.isHidden = false
                            self.wishlistTableView.isHidden = true

                        }
                    }
                }

            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = cartEmpty.localized
            emptyView?.button.setTitle(browseCategories.localized, for: .normal)
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-wishlist")
            emptyView?.delegate = self
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WishlistDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
          apiUse = call
        print(dict)
        switch call {
        case .removeFromWishlist:
            self.makeRequest(id: id, dict: [:])
        case .moveToCart:
            self.makeRequest(id: id, dict: dict)
        default:
            print("")
        }
    }
}

extension WishlistDataViewController: EmptyDelegate {
    func buttonPressed() {
        self.tabBarController?.selectedIndex = 2
    }
}

extension WishlistDataViewController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productDataViewControllerIdentifier) as! ProductDataViewController
        view.templateId = id
        view.productid = name
        self.navigationController?.pushViewController(view, animated: true)
    }
}
