//
//  InvoicesModel.swift
//  SecurimportApps
//
//  Created by debabrata on 07/10/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON
struct InvoicesModel {
    var id : String!
    var name : String!
    var dateDue : String!
    var dateInvoice : String!
    var residual : String!
    var state : String!
    var pdf : String!
    
    init(data:JSON) {
        id = data["id"].stringValue
        name = data["name"].stringValue
        dateDue = data["dateDue"].stringValue
        dateInvoice = data["dateInvoice"].stringValue
        residual = data["residual"].stringValue
        state = data["state"].stringValue
        pdf = data["pdf"].stringValue
    }
}
