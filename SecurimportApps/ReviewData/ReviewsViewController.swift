//
//  ReviewsViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 04/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import  SwiftyJSON

class ReviewsViewController: UIViewController {

    var id: String?
    fileprivate let reviewViewModalObject = ReviewViewModal()

    @IBOutlet weak var reviewViewController: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Reviews"
        reviewViewController.register(ReviewsTableViewCell.nib, forCellReuseIdentifier: ReviewsTableViewCell.identifier)
        reviewViewController.delegate = reviewViewModalObject
        reviewViewController.dataSource = reviewViewModalObject
        reviewViewController.estimatedRowHeight = 100
        reviewViewController.rowHeight = UITableView.automaticDimension
        reviewViewModalObject.passData = self
      
        self.makeRequest(call: WhichApiUse.none, dict: [:])
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    func makeRequest( call : WhichApiUse , dict: [String: Any]) {
        var newDict = [String: Any]()
        var params = ""
        var verb = RequestType.post
        switch call {
        case .likeDislike:
             params =  GlobalData.apiName.likeDislike
            newDict = dict
            verb = RequestType.post
            
        default:
           params =  GlobalData.apiName.reviews
             let dict = ["template_id" : Int(id!) ?? 0]
            newDict = dict
            verb = RequestType.post
        }
        
      
       
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: newDict, verbs: verb, controler: Controllers.reviews) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                switch call {
                case .likeDislike:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                    
                default:
                    self.reviewViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        print(data)
                        if data {
                            //                        self.emptyView?.isHidden = true
                            self.reviewViewController.isHidden = false
                            self.reviewViewController.reloadData()
                        } else {
                            //                        self.emptyView?.isHidden = false
                            self.reviewViewController.isHidden = true
                        }
                    }
                }
                
                
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReviewsViewController: PassData {
    func passData(id: String, name: String, dict: [String : Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        self.makeRequest(call: WhichApiUse.likeDislike, dict: dict)
    }
}
