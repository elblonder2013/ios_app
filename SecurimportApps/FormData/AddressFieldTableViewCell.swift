//
//  AddressFieldTableViewCell.swift
//  MobikulOpencartMp
//
//  Created by bhavuk.chawla on 05/11/18.
//  Copyright © 2018 yogesh. All rights reserved.
//

import UIKit
import Reusable

class AddressFieldTableViewCell: UITableViewCell, FormConformity, NibReusable {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var hedingLabel: UILabel!
    @IBOutlet weak var passwordShowHide: UIButton!
    var formItem: FormItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let languageCode =  UserDefaults.standard.object(forKey: "language") as? String{
            
            if languageCode == "ar" {
                  textField.textAlignment = .right
                if #available(iOS 9.0, *) {
                   textField.semanticContentAttribute = .forceRightToLeft
                } else {
                    // Fallback on earlier versions
                }
            }else {
                L102Language.setAppleLAnguageTo(lang: "en")
                 textField.textAlignment = .left
                if #available(iOS 9.0, *) {
                   textField.semanticContentAttribute = .forceLeftToRight
                    //                                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        
        self.textField.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
    }
    @IBAction func showPassword(_ sender : UIButton){
         formItem?.isShowPassword = !(formItem?.isShowPassword ?? false)
        self.textField.isSecureTextEntry = formItem?.isShowPassword ?? false
        if self.formItem?.isShowPassword ?? false{
            sender.setImage(UIImage(named: "passwordHide"), for: .normal)
        }else{
             sender.setImage(UIImage(named: "passwordShow"), for: .normal)
        }
        
    }
    @objc func textFieldDidChanged(_ textField: UITextField) {
        self.formItem?.valueCompletion?(textField.text)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}

extension AddressFieldTableViewCell: FormUpdatable {
    func update(with formItem: FormItem) {
        self.formItem = formItem
        self.hedingLabel.text = formItem.heading
         self.textField.isSecureTextEntry = false
         passwordShowHide.isHidden = true
        if formItem.isSecure{
            passwordShowHide.isHidden = false
            self.textField.clearButtonMode = .never
            self.textField.isSecureTextEntry = formItem.isShowPassword
            if self.formItem?.isShowPassword ?? false{
                passwordShowHide.setImage(UIImage(named: "passwordHide"), for: .normal)
            }else{
                passwordShowHide.setImage(UIImage(named: "passwordShow"), for: .normal)
            }
        }
        self.textField.text = self.formItem?.value as? String
        self.textField.accessibilityHint = self.formItem?.keyType
        if let image = formItem.image {
            self.textField.setLeftIcon(image)
        }
        self.textField.placeholder = self.formItem?.placeholder
        self.textField.keyboardType = self.formItem?.uiProperties.keyboardType ?? .default
        self.textField.tintColor = self.formItem?.uiProperties.tintColor
    }
}
extension UITextField {
        func setLeftIcon(_ icon: UIImage) {
        let padding = 8
        let size = 16
        let outerView = UIView(frame: CGRect(x: 8, y: 0, width: size+padding + 16, height: size) )
        let iconView  = UIImageView(frame: CGRect(x: padding + 8, y: 0, width: size, height: size))
        iconView.image = icon
        outerView.addSubview(iconView)
        leftView = outerView
        leftViewMode = .always
    }
}
