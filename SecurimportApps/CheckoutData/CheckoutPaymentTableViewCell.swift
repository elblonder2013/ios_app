//
//  CheckoutPaymentTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class CheckoutPaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentInformationName: UILabel!
    @IBOutlet weak var paymentMethodName: UILabel!
    @IBOutlet weak var CheckboxImage: UIImageView!
    var paymentID: String?
    override func awakeFromNib() {
        super.awakeFromNib()
        CheckboxImage.applyBorder(colours: GlobalData.Credentials.accentColor)
//        CheckboxImage.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
        paymentInformationName.SetDefaultTextColor()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
    var item: PaymentData? {
        didSet {
            paymentMethodName.text = item?.name
            paymentInformationName.text = item?.description

            if paymentID == item?.paymentId {
                CheckboxImage.backgroundColor = GlobalData.Credentials.accentColor
                CheckboxImage.image = #imageLiteral(resourceName: "check-icon24x24")
            } else {
                 CheckboxImage.backgroundColor = UIColor.clear
                CheckboxImage.image = nil
            }
        }
    }

}
