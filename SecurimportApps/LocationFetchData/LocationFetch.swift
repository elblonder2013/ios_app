//
//  LocationFetch.swift
//  Odoo iOS
//
//  Created by bhavuk.chawla on 02/12/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import CoreLocation

class LocationFetch: NSObject, CLLocationManagerDelegate {
    var locationManager = CLLocationManager()
    var country: String?

    override init() {
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
//        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
         print("Inside")
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         print("Insiwdwdde")
        let latestLocation = locations.last
        let g = CLGeocoder()
        var p: CLPlacemark?
            g.reverseGeocodeLocation(latestLocation!, completionHandler: {
                (placemarks, error) in
                let pm = placemarks
                if ((pm != nil) && (pm?.count)! > 0) {
                    // p = CLPlacemark()
                    p = CLPlacemark(placemark: (pm?[0])!)
                    self.country = p?.country
//                    print("Inside what is in p: \(p?.country )")
//                    print(p?.addressDictionary)
//                    print(p?.subAdministrativeArea)
//                    print(p?.subThoroughfare)
//                    print(p?.thoroughfare)
//                    print(p?.locality)
//                    print(p?.postalCode)
//                    print(p?.administrativeArea)
//                    print(p?.country)
//                    print(p?.isoCountryCode)
                }
            })
        }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print(status)
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }

    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        print("Insiwdwdde")
        let latestLocation = newLocation
        let g = CLGeocoder()
        var p: CLPlacemark?
        g.reverseGeocodeLocation(latestLocation!, completionHandler: {
            (placemarks, error) in
            let pm = placemarks
            if ((pm != nil) && (pm?.count)! > 0) {
                // p = CLPlacemark()
                p = CLPlacemark(placemark: (pm?[0])!)
                self.country = p?.country
//                print("Inside what is in p: \(p?.country )")
//                print(p?.addressDictionary)
//                print(p?.subAdministrativeArea)
//                print(p?.subThoroughfare)
//                print(p?.thoroughfare)
//                print(p?.locality)
//                print(p?.postalCode)
//                print(p?.administrativeArea)
//                print(p?.country)
//                print(p?.isoCountryCode)
            }
        })
    }

}
