//
//  OptionTableViewCell.swift
//  SecurimportApps
//
//  Created by debabrata on 04/10/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

class OptionTableViewCell: UITableViewCell {
    @IBOutlet weak var label : UILabel!
     @IBOutlet weak var txtValue : UITextField!
     @IBOutlet weak var dropIcon : UIImageView!
    var selectIndex = -1
    var delegate : PassData?
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    var item : ProductAttributes?{
         didSet {
        guard let item = item else{
            return
         }
            txtValue.delegate = self
            txtValue.text = ""
            label.text = item.name
            if item.value != -1 && item.value < item.values.count{
                txtValue.text = item.values[item.value].name
            }
        }
    }
}
extension OptionTableViewCell : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  item?.type == "select"{
        didTap()
        return false
    }
        return true
    }
    func didTap() {
        if item?.values.count ?? 0 > 0 {
            ActionSheetStringPicker.show(withTitle: "Select".localized, rows: item?.values.map{$0.name}, initialSelection: 0, doneBlock: {
                picker, indexes, values in
                if   GlobalData.AddOnsEnabled.sortData?.list.count ?? 0 > 0 {
                    self.txtValue.text = self.item?.values[indexes].name
                    self.delegate?.passData(id: "", name: "", dict: ["tag":self.tag], jsonData: JSON.null, index: indexes, call: .none)
                }
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: self.txtValue)
        }
    }
}

