//
//  ChangeAddressViewModal.swift
//  Odoo iOS
//
//  Created by Bhavuk on 07/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChangeAddressViewModal: NSObject {
    var controller: Controllers?
    var delegate: AddressUpdate?
    var whichApiCallDelegate: PassData?

    var shippingAddress = [AddressData]()

    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = CheckoutAddressModal(data : jsonData) else {
            return
        }

        if !data.shippingAddress.isEmpty {
            self.shippingAddress = data.shippingAddress
            completion(true)
        } else {
            completion(false)
        }
    }
}

extension ChangeAddressViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shippingAddress.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = shippingAddress[indexPath.row].address
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.accessoryType = .checkmark
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if controller != nil {
            whichApiCallDelegate?.passData(id: shippingAddress[indexPath.row].addressId!, name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.defaultAddress)
        } else {
            delegate?.addressupdated(addressId: shippingAddress[indexPath.row].addressId!)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
