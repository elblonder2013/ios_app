//
//  GradientExtension.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import Toaster

extension UIView {
    func applyGradient(colours: [UIColor]) {
        self.applyGradient(colours: colours, locations: nil)
    }

    func applyGradient(colours: [UIColor], locations: [NSNumber]?) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.2, y: 1)
        gradient.endPoint = CGPoint(x: 0.8, y: 0)
        self.layer.addSublayer(gradient)
    }

    func applyWithAlphaGradient(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.withAlphaComponent(0.8).cgColor }
        gradient.startPoint = CGPoint(x: 0.2, y: 1)
        gradient.endPoint = CGPoint(x: 0.8, y: 0)
       self.layer.insertSublayer(gradient, at: 0)
    }
}

extension UIView {
    func applyBorder(colours: UIColor) {
        self.layer.borderColor = colours.cgColor
        self.layer.borderWidth = 1.5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }

    func applyTextFieldBorder() {
        self.layer.borderColor = GlobalData.Credentials.textFieldBorder.cgColor
        self.layer.borderWidth = 0.1
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
    }
}

extension UIView {

    func applyGradientToTopView(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.startPoint = CGPoint(x: 0.2, y: 1)
        gradient.endPoint = CGPoint(x: 0.8, y: 0)
        gradient.colors = colours.map { $0.cgColor }
        self.layer.insertSublayer(gradient, at: 0)
    }
}

extension UIViewController {

    func alert(message: String, title: String ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

}

extension UIView {
    func myBorder() {
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.25).cgColor
//        self.layer.shadowOpacity = 2
//        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 1.0
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 5
        self.layer.masksToBounds = false
    }

}

extension UINavigationBar {
    /// Applies a background gradient with the given colors
    func applyNavigationGradient( colors: [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar

        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }

    /// Creates a gradient image with the given settings
    static func gradient(size: CGSize, colors: [UIColor]) -> UIImage? {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }

        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)

        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }

        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }

        // Create the Coregraphics gradient
        var locations: [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }

        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])

        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension UIView {
    func applyBorderShadow(colours: UIColor) {
        self.applyShadowWithoutBorder(colours: colours, locations: nil)
    }

    func applyShadowWithoutBorder(colours: UIColor, locations: [NSNumber]?) {
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0, height: 4.0)
        self.layer.shadowRadius = 14
    }
}

extension Bundle {

    static func loadView<T>(fromNib name: String, withType type: T.Type) -> T {
        if let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T {
            return view
        }

        fatalError("Could not load view with type " + String(describing: type))
    }
}

func viewController(forStoryboardName: String, forViewControllerName: String) -> UIViewController {
    return UIStoryboard(name: forStoryboardName, bundle: nil).instantiateViewController(withIdentifier: forViewControllerName)
}
func viewControllerLogin(forStoryboardName: String, forViewControllerName: String) -> UIViewController {
    return UIStoryboard(name: forStoryboardName, bundle: nil).instantiateViewController(withIdentifier: forViewControllerName)
}

let AppLanguageKey = "AppLanguage"
var AppLanguageDefaultValue = "en"

var appLanguage: String {

    get {
        if let language = UserDefaults.standard.string(forKey: AppLanguageKey) {
            return language
        } else {
            UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
            return AppLanguageDefaultValue
        }
    }

    set(value) {
        UserDefaults.standard.setValue((value), forKey: AppLanguageKey)
    }

}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}
extension String {
    var html2AttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public func getHtml2AttributedString(font: UIFont?) -> NSAttributedString? {
        guard let font = font else {
            return html2AttributedString
        }
        
        let modifiedString = "<style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px;}</style>\(self)";
        
        guard let data = modifiedString.data(using: .utf8) else {
            return nil
        }
        
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error)
            return nil
        }
    }
}
extension String {

    var localized: String {
        return localized(lang: appLanguage)
    }

    var localizeStringUsingSystemLang: String {
        return NSLocalizedString(self, comment: "")
    }

    func localized(lang: String?) -> String {

        if let lang = lang {
            if let path = Bundle.main.path(forResource: lang, ofType: "lproj") {
                let bundle = Bundle(path: path)
                return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
            }
        }

        return localizeStringUsingSystemLang
    }
}

// RTL

let APPLE_LANGUAGE_KEY = "AppleLanguages"

/// L102Language

class L102Language {
    
    /// get current Apple language
    
    class func currentAppleLanguage() -> String {
        
        let userdef = UserDefaults.standard
        
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        
        let current = langArray.firstObject as! String
        
        return current
        
    }
    
    /// set @lang to be the first in Applelanguages list
    
    class func setAppleLAnguageTo(lang: String) {
        
        let userdef = UserDefaults.standard
        
        userdef.set([lang, currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        
        userdef.synchronize()
        
    }
}


extension String {
    func removeString(completeString: String, remove: String) -> String {
        let aString = completeString
        let newString = aString.replacingOccurrences(of: "", with: remove, options: .literal, range: nil)
        return newString
    }

}

extension UILabel {
    func halfTextColorChange (fullText: String, changeText: String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalData.Credentials.defaultTextColor, range: range)
        self.attributedText = attribute
    }

    func halfTextWithColorChange(fullText: String, changeText: String, color: UIColor) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        self.attributedText = attribute
    }
}

extension UIImageView {

    func setImage(imageUrl: String, controller: Controllers) {

        var urls = imageUrl

        switch controller {
        case .productCategory:
            urls.append(String.init(format: "/%ix%i", Int(SCREEN_WIDTH/2 * UIScreen.main.scale), Int(SCREEN_WIDTH/2 * UIScreen.main.scale)))
         case .category:
             urls.append(String.init(format: "/%ix%i", Int((50 ) * UIScreen.main.scale), Int((50) * UIScreen.main.scale)))
            print(urls)
        case .cart:
            urls.append(String.init(format: "/%ix%i", Int(100 * UIScreen.main.scale), Int(100 * UIScreen.main.scale)))
        case .notifications:
            urls.append(String.init(format: "/%ix%i", Int((SCREEN_WIDTH - 32) * UIScreen.main.scale), Int((SCREEN_WIDTH/2 - 32) * UIScreen.main.scale)))
        case .homeBanners, .sellerBanner:
              urls.append(String.init(format: "/%ix%i", Int((SCREEN_WIDTH) ), Int((SCREEN_WIDTH/2 ))))
            print(urls)
        case .homeProduct:
            urls.append(String.init(format: "/%ix%i", Int((SCREEN_WIDTH/2 - 8 ) * UIScreen.main.scale), Int((SCREEN_WIDTH/2 - 24) * UIScreen.main.scale)))
        case .product:
            urls.append(String.init(format: "/%ix%i", Int((SCREEN_WIDTH) * UIScreen.main.scale), Int((SCREEN_WIDTH) * UIScreen.main.scale)))
        case .Profile:
            urls.append(String.init(format: "/%ix%i", Int((64) * UIScreen.main.scale), Int((64) * UIScreen.main.scale)))
            
        case.zoom:
            urls.append(String.init(format: "/%ix%i", Int((SCREEN_WIDTH) * UIScreen.main.scale), Int((UIScreen.main.bounds.size.height) * UIScreen.main.scale)))
        case.sellerProducts:
            urls.append(String.init(format: "/%ix%i", Int((140) * UIScreen.main.scale), Int(140 * UIScreen.main.scale)))
            
        case .touch:
             urls.append(String.init(format: "/%ix%i", Int(self.frame.size.width), Int(self.frame.size.height)))
        default:
           print("")
        }
        print(urls)
        if URL(string: urls) != nil && urls.count > 0 {
            let resource = ImageResource(downloadURL: URL(string: urls)!, cacheKey: urls)
            if controller == Controllers.homeBanners ||  controller == Controllers.zoom ||  controller == Controllers.sellerBanner {
                if  controller == Controllers.zoom{
                     self.contentMode = .scaleAspectFit
                } else {
                    self.contentMode = .scaleToFill
                }
                
            } else {
                self.contentMode = .scaleAspectFit
            }
            self.kf.setImage(with: resource, placeholder: #imageLiteral(resourceName: "placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
        } else {
            self.image = #imageLiteral(resourceName: "placeholder")
        }
    }
}

extension String {

    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
}
extension String {
   
    
    var isValidPhone: Bool{
        let phoneRegex = "^((0091)|(\\+91)|0?)[6789]{1}\\d{9}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
    
}
extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
public extension UIView {

    public class func instantiateFromNib<T: UIView>(viewType: T.Type) -> T {
        return Bundle.main.loadNibNamed(String(describing: viewType), owner: nil, options: nil)?.first as! T
    }

    public class func instantiateFromNib() -> Self {
        return instantiateFromNib(viewType: self)
    }

}
extension UIImageView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
extension UILabel {
    func SetDefaultTextColor() {
        self.textColor = GlobalData.Credentials.defaultTextColor
    }
      func SetAccentTextColor() {
          self.textColor = GlobalData.Credentials.accentColor
        }

}

func checkValidEmail(data:String) -> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
    return (emailTest.evaluate(with: data))
}


extension UIView {
    func SetAccentBackColor() {
        self.backgroundColor = GlobalData.Credentials.accentColor
    }
}

extension UITabBarController {

    func setCartCount (value: String) {

        if  value != "0" && !value.isEmpty {
             self.tabBar.items![3].badgeValue = value
        } else {
             self.tabBar.items![3].badgeValue = nil
        }

    }

}

extension UIAlertController {
     func alertWithTitle(title: String, message: String, buttonTitle: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
        alertController.addAction(action)
        return alertController
    }
}

extension UITableView {
    func reloadDataWithAutoSizingCellWorkAround() {
        self.reloadData()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.reloadData()
    }
}

extension UIView {
    func shadowBorder1() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 4
        self.layer.shadowOffset =  CGSize(width: 0, height: 2)
        
    }
}
extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
