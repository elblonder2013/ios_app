//
//  ZoomImageViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 18/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class ZoomImageViewController: UIViewController {

    var dict = [String: Any]()
    var imageArray = [String]()

    @IBOutlet weak var zoomCollectionVFiew: UICollectionView!
    @IBOutlet weak var croosbtn: UIBarButtonItem!
    @IBOutlet weak var imageController: UIImageView!
     var currentPage = 0
    var interactor: Interactor?

    override func viewDidLoad() {
        super.viewDidLoad()
        print(dict)
//         navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.view.backgroundColor = UIColor.clear
        zoomCollectionVFiew.isPagingEnabled = true
//        let image : UIImage? = UIImage(named: "Icon-Close")?.withRenderingMode(.alwaysOriginal)
//        croosbtn.setBackgroundImage(image, for: .normal, barMetrics: .default)
        // Do any additional setup after loading the view.
        imageArray = dict["image"] as! [String]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        let  indexPath1 = IndexPath.init(row: currentPage, section: 0)
        zoomCollectionVFiew.scrollToItem(at: indexPath1, at: .right, animated: true)
          showHelperCircle()
    }

    @IBAction func handleGesture(sender: UIPanGestureRecognizer) {
        print("dswswdkdkwkdw")
        let percentThreshold: CGFloat = 0.3

        // convert y-position to downward pull progress (percentage)
        let translation = sender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)

        guard let interactor = interactor else { return }

        switch sender.state {
        case .began:
            interactor.hasStarted = true
            dismiss(animated: true, completion: nil)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish
                ? interactor.finish()
                : interactor.cancel()
        default:
            break
        }
    }

    func showHelperCircle() {
        let center = CGPoint(x: view.bounds.width * 0.5, y: 100)
        let small = CGSize(width: 30, height: 30)
        let circle = UIView(frame: CGRect(origin: center, size: small))
        circle.layer.cornerRadius = circle.frame.width/2
        circle.backgroundColor = UIColor.white
        circle.layer.shadowOpacity = 0.8
        circle.layer.shadowOffset = CGSize.zero
        view.addSubview(circle)
        UIView.animate(
            withDuration: 0.5,
            delay: 0.25,
            options: [],
            animations: {
                circle.frame.origin.y += 200
                circle.layer.opacity = 0
        },
            completion: { _ in
                circle.removeFromSuperview()
        }
        )
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func barButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension ZoomImageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(imageArray.count)
        return imageArray.count

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CustomCellCollectionViewCell
        cell.imagee.isUserInteractionEnabled = true
        cell.imagee.setImage(imageUrl: imageArray[indexPath.row], controller: .zoom)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH, height: (UIScreen.main.bounds.size.height + 20) )

    }

}

class Interactor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}
