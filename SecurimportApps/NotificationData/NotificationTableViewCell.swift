//
//  NotificationTableViewCell.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var imagHeight: NSLayoutConstraint!
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        notificationView.myBorder()
        notificationView.backgroundColor = UIColor.white
        subTitleLabel.textColor = GlobalData.Credentials.defaultTextColor
        timeLabel.textColor = GlobalData.Credentials.defaultTextColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: NotificationData? {
        didSet {
           subTitleLabel.text = item?.body
            titleLabel.text = item?.title
            timeLabel.text = item?.time
            if !(item?.bannerUrl?.isEmpty)! {
                imagHeight.constant = SCREEN_WIDTH / 2 - 32
                let url = (item?.bannerUrl)!
                notificationImage.setImage(imageUrl: url, controller: .notifications)
            } else {
               imagHeight.constant = 0
            }

        }
    }
}
