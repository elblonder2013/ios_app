//
//  HomeCategoryCollectionViewCell.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class HomeCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        categoryImage.clipsToBounds = true
        categoryImage.layer.cornerRadius = 25
        categoryImage.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        categoryImage.layer.borderWidth = 1
    }

    var item: CategorySet? {
        didSet {

            guard let item = item else {
                return
            }
            categoryLabel.text = item.categoryName
            categoryImage.setImage(imageUrl: item.CategoryUrl!, controller: .category)
          //  categoryLabel.font = UIFont(name: GlobalData.Credentials.fontName, size: 15.0)
//            categoryImage.image = #imageLiteral(resourceName: "placeholder")
//            categoryImage.contentMode = .scaleAspectFit
//            categoryImage.kf.setImage(with: URL(string: item.CategoryUrl!), placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
            //            categoryImage.sd_setImage(with: URL(string: item.CategoryUrl!), placeholderImage: placeholder)
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
