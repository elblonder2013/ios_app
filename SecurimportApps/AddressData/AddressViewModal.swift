//
//  AddressViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

class AddressViewModal: NSObject {

    var delegate: NewViewControllerOpen?
    var items = [AddressViewModalItem]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = AddressModal(data : jsonData) else {
            return
        }
        items.removeAll()
      
        if data.billingAddres != nil {
            items.append(AddressViewModalBillingItem(billing: data.billingAddres!))
            if !data.shippingAddress.isEmpty {
                 items.append(AddressViewModalShippingItem(shipping: data.shippingAddress))
            }
            completion(true)
        } else {
            completion(false)
        }

    }

}

extension AddressViewModal: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]

        switch item.type {
        case .billing:
            if let item = item as? AddressViewModalBillingItem {
               let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.billing?.address
                 cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                 cell.accessoryType = .disclosureIndicator
                cell.selectionStyle = .none
                return cell
            }
        case .shipping :
            if let item = item as? AddressViewModalShippingItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.shipping[indexPath.row].address
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                cell.accessoryType = .disclosureIndicator
                cell.selectionStyle = .none
                return cell
            }

        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let item = items[section]
        switch item.type {
        case .billing:
            return billingAddress.localized
        case .shipping:
            return shippingAddress.localized
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if let item =  items[indexPath.section] as? AddressViewModalBillingItem {
                delegate?.moveToController(id: (item.billing?.addressId!)!, name: "", dict: JSON.null, Controller: Controllers.editAddress)
            }
        } else {
            if let item =  items[indexPath.section] as? AddressViewModalShippingItem {
                delegate?.moveToController(id: item.shipping[indexPath.row].addressId!, name: "yes", dict: JSON.null, Controller: Controllers.editAddress)
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

enum addressType {
    case billing
    case shipping
}

protocol AddressViewModalItem {
    var type: addressType { get }
    var rowCount: Int { get }
}

struct AddressViewModalBillingItem: AddressViewModalItem {
    var type: addressType {
        return .billing
    }

    var rowCount: Int {
        return 1
    }

    var billing: AddressData?

    init(billing: AddressData ) {
        self.billing = billing
    }
}

struct AddressViewModalShippingItem: AddressViewModalItem {
    var type: addressType {
        return .shipping
    }

    var rowCount: Int {
        return shipping.count
    }

    var shipping: [AddressData]

    init(shipping: [AddressData] ) {
        self.shipping = shipping
    }

}
