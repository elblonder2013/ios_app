//
//  ViewInvoicesViewController.swift
//  SecurimportApps
//
//  Created by debabrata on 07/10/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit
import Foundation
import WebKit
import SVProgressHUD
import SwiftyJSON
import PDFKit

class ViewInvoicesViewController: UIViewController {
     var pdfView = PDFView()
    var url = ""
    var title1 = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = title1
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pdfView)
        pdfView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
       
         SVProgressHUD.show(withStatus: "Loading....".localized)
        downloadPDF()
        
    }
   
    func downloadPDF(){
        SVProgressHUD.show(withStatus: "Loading....".localized)
        let baseURLWithAPIKeyString = url
        var requestURL: String = baseURLWithAPIKeyString
        requestURL = requestURL.removeWhitespace()
        requestURL = requestURL.stringByRemovingEmoji()
        print(requestURL)
        var request1 = URLRequest(url: URL(string : requestURL)!)
        request1.httpMethod = "GET"
        request1.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let encodedData = GlobalData.Credentials.BASEDATA.data(using: String.Encoding.utf8)!
        let base64String = encodedData.base64EncodedString()
        print(base64String)
        request1.setValue("Basic \(base64String)", forHTTPHeaderField: "Authorization")
        if UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
            if let dic  =  UserDefaults.standard.value(forKey: "loginDict") {
                let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                let loginbase64String = jsonData.base64EncodedString()
                request1.setValue(loginbase64String, forHTTPHeaderField: "Login")
                print(loginbase64String)
            }
            if let dic = UserDefaults.standard.value(forKey: "socialDict") {
                let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                let loginbase64String = jsonData.base64EncodedString()
                request1.setValue(loginbase64String, forHTTPHeaderField: "SocialLogin")
                print(loginbase64String)
            }
        }
    let config = URLSessionConfiguration.default
    let session =  URLSession(configuration: config)
    let task = session.dataTask(with: request1, completionHandler: {(data, response, error) in
        if error == nil{
             SVProgressHUD.dismiss()
            if let pdfData = data {
                DispatchQueue.main.async {
                    if let document = PDFDocument(data: pdfData) {
                        self.pdfView.document = document
                    }
                }
            }
        }else{
             SVProgressHUD.dismiss()
            print(error?.localizedDescription ?? "")
        }
    })
    task.resume()
    }
   
}
