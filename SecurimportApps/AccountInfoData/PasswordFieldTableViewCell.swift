//
//  PasswordFieldTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 01/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class PasswordFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!

    @IBOutlet weak var newPassLbl: UILabel!
    @IBOutlet weak var confirmPassLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        newPassLbl.text = "New Password".localized
        confirmPassLbl.text = "Confirm Password".localized
        // Initialization code
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
