    //
//  LaunchController.swift
//  Odoo iOS
//
//  Created by bhavuk.chawla on 16/12/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class LaunchController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if GlobalData.Credentials.environment == EnvironmentType.marketplace {
            imageView.image = UIImage(named: "launchMp")
        } else {
          imageView.image = UIImage(named: "launchMp")
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.makeRequest(call: WhichApiUse.splash)
    }
    func makeRequest(call: WhichApiUse) {
        var params = ""
        
        switch call {
        case .splash:
            
            params = GlobalData.apiName.getSplashData
        default:
            print()
        }
        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: RequestType.post, controler: Controllers.home  ) { (responseObject: JSON?, error: Error?) in
            SVProgressHUD.show(withStatus: "Loading....".localized)
            if (error != nil) {
                SVProgressHUD.dismiss()
                print("Error logging you in!")
                
            } else {
                switch call {
                case .splash:
                    self.tabBarController?.tabBar.items?[4].selectedImage = UIImage(named: "Icon-Accoun-Fill")?.withRenderingMode(.alwaysOriginal)
                    self.tabBarController?.tabBar.items?[4].image = UIImage(named: "Icon-Account")?.withRenderingMode(.alwaysOriginal)
            
                    if let data = responseObject {
                        Addons.AddonsChecks(data: data)
                        let data = splashModel(data: data)
                        GlobalData.StoreData.languages = data.languages
                    }
        
                    if GlobalData.AddOnsEnabled.allowGuest || UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
                        
                    } else {
                        if UserDefaults.standard.value(forKey: loggedInUserKey) == nil {
                            self.tabBarController?.setCartCount(value: "")
                        }
                    }
                     SVProgressHUD.dismiss()
                    print(UserDefaults.standard.value(forKey: "loadLanguage"))
                    if UserDefaults.standard.value(forKey: "loadLanguage") != nil {
                        
                        self.performSegue(withIdentifier: "move", sender: self)
                    } else {
                        self.launchViewController()
                    }
                    
                default:
                    print()
                }
                
            }
        }
    }
    
    func launchViewController() {
        print(GlobalData.StoreData.languages)
        if GlobalData.StoreData.languages.count > 0 {
            if GlobalData.StoreData.languages.count == 1 {
                GlobalData.StoreData.check = true
                GlobalData.StoreData.defaultLanguage = GlobalData.StoreData.languages[0].languageCode
                UserDefaults.standard.set(GlobalData.StoreData.defaultLanguage, forKey: "defaultLanguage")
                UserDefaults.standard.synchronize()
                
                let val =  GlobalData.StoreData.defaultLanguage.split(separator: "_")[0]
                print(val)
                AppLanguageDefaultValue = String(val)
                UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
                GlobalData.StoreData.check = false
                 self.performSegue(withIdentifier: "move", sender: self)
            }
            else{
                 // if GlobalData.AddOnsEnabled.allowGuest || UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
                    let view = viewController(forStoryboardName:GlobalData.Storyboard.homeStoryboard, forViewControllerName: Controllers.languageChooser.rawValue) as! LanguageChooserViewController
                    
                    self.present(view, animated: true, completion: nil)
//                  } else {
//                    let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
//                    let nav  = UINavigationController(rootViewController: view)
//                    self.navigationController?.present(nav, animated: true, completion: nil)
//                }
//                let view = viewController(forStoryboardName:GlobalData.Storyboard.homeStoryboard, forViewControllerName: Controllers.languageChooser.rawValue) as! LanguageChooserViewController
//
//               self.present(view, animated: true, completion: nil)
                
            }
        }
        else{
            GlobalData.StoreData.check = false
            self.performSegue(withIdentifier: "move", sender: self)
        }
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
//        DispatchQueue.main.async {
//        self.makeRequest(call: WhichApiUse.splash)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
