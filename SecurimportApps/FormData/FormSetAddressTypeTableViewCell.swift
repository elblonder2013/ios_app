//
/**
MobikulOpencart
@Category Webkul
@author Webkul <support@webkul.com>
FileName: FormSetAddressTypeTableViewCell.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license https://store.webkul.com/license.html ASL Licence
@link https://store.webkul.com/license.html

*/


import UIKit
import Reusable

class FormSetAddressTypeTableViewCell: UITableViewCell , FormConformity, NibReusable {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var yesImage: UIImageView!
    @IBOutlet weak var yesBrn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var noImage: UIImageView!
    var formItem: FormItem?
    override func awakeFromNib() {
        super.awakeFromNib()
        yesImage.applyBorder(colours: GlobalData.Credentials.accentColor)
        noImage.applyBorder(colours: GlobalData.Credentials.accentColor)
        noImage.layer.cornerRadius = 2
        yesImage.layer.cornerRadius = 2
        yesBrn.setTitle("Yes".localized, for: .normal)
        noBtn.setTitle("No".localized, for: .normal)
       
        
        yesImage.isUserInteractionEnabled = true
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
        yesImage.addGestureRecognizer(doubleTap)
        
        noImage.isUserInteractionEnabled = true
        let doubleTap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap1))
        noImage.addGestureRecognizer(doubleTap1)
        
       
        
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @objc func handleDoubleTap() {
        noImage.backgroundColor = UIColor.white
        self.noImage.image = nil
        if let form = formItem {
            form.value = "1"
            self.yesImage.image = UIImage(named: "ic_check")
            yesImage.backgroundColor = GlobalData.Credentials.accentColor
            yesImage.clipsToBounds = true
        }
    }
    
    @objc func handleDoubleTap1() {
        yesImage.backgroundColor = UIColor.white
        self.yesImage.image = nil
        if let form = formItem {
            form.value = "0"
            self.noImage.image = UIImage(named: "ic_check")
            noImage.backgroundColor = GlobalData.Credentials.accentColor
            noImage.clipsToBounds = true
        }
    }
    
    @IBAction func yesClicked(_ sender: Any) {
        self.handleDoubleTap()
    }
    @IBAction func noClicked(_ sender: Any) {
        self.handleDoubleTap1()
        
    }
    
}

extension FormSetAddressTypeTableViewCell: FormUpdatable {
    func update(with formItem: FormItem) {
        self.formItem = formItem
        self.headingLabel.text = formItem.placeholder
        if formItem.value as? String == "0" {
            self.noImage.image = UIImage(named: "ic_check")
            noImage.backgroundColor = GlobalData.Credentials.accentColor
            noImage.clipsToBounds = true
            yesImage.backgroundColor = UIColor.white
            self.yesImage.image = nil
        }
        
        if formItem.value as? String == "1" {
            self.yesImage.image = UIImage(named: "ic_check")
            yesImage.backgroundColor = GlobalData.Credentials.accentColor
            yesImage.clipsToBounds = true
            noImage.backgroundColor = UIColor.white
            self.noImage.image = nil
        }
    }
}
