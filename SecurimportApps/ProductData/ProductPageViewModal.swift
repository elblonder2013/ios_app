//
//  ProductPageViewModal.swift
//  Odoo iOS
//
//  Created by Bhavuk on 03/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProductPageViewModal: NSObject {
    var productImages = [String]()
    var tableArray = [String]()
     var productData: ProductPageData?
    var variantsArray = [Variants]()
    var delegate: NewViewControllerOpen?
    //var pageController: CHIPageControlFresno?
      var combArray = [Combinations]()
     var wishlistExists = false
    var descExists = false
    var reviewsExists = false
    var passDelegate: PassData?
      var currentPage = 0
    var isSellerProduct = false
    var sellerProductData: SellerProductData!
    var sectionCount = 0
     var idArray = [String]()
    var selectIndex = 0
    var ProductImageCollection: UICollectionView?
     var ProductImageListCollection: UICollectionView?
    var fileAttachments = [FileAttachments]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = ProductPageModal(data : jsonData ) else {
                return
        }
 
        if !data.productImages.isEmpty {
            self.productImages = data.productImages
        }
        // pageController?.numberOfPages = productImages.count

        idArray = data.idArray
//        tableArray.append("More Information")

        self.productData = data.productData!
        self.variantsArray = data.variantsArray
        self.combArray = data.combArray
        self.wishlistExists = data.wishlistExists

        if let descData =  productData?.description {
            if descData.count > 0 {
                self.descExists = true
                tableArray.append("details".localized)
            }
        }

        if data.isSellerProduct  && data.sellerProductData != nil {
             print("data.isSellerProduct ", data.isSellerProduct )
            isSellerProduct = data.isSellerProduct
            sellerProductData = data.sellerProductData
            sectionCount = 2
        } else {
            sectionCount = 1
        }
        if productData?.fileAttachments.count ?? 0 > 0 {
            sectionCount += 1
            fileAttachments = productData?.fileAttachments ?? []
        }
        
        if let reviewData = productData?.totalReviews {

            if reviewData.count > 0 && reviewData != "0"{
                 self.reviewsExists = true
                 tableArray.append("reviews".localized)
            }

        }

        completion(true)
    }

}

extension ProductPageViewModal: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return productImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBannerCollectionViewCell", for: indexPath as IndexPath) as! HomeBannerCollectionViewCell
        cell.backgroundColor =  GlobalData.Credentials.defaultBackgroundColor
        cell.bannerImage.setImage(imageUrl: productImages[indexPath.row], controller: .product)
         if collectionView.tag == 99{
            
        cell.bannerImage.layer.borderColor = UIColor.lightGray.cgColor
            if currentPage == indexPath.row{
                cell.bannerImage.layer.borderColor = GlobalData.Credentials.accentColor.cgColor
            }
        cell.bannerImage.layer.borderWidth = 2
        }
        cell.setNeedsLayout()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 99{
            return CGSize(width: 60, height: 60 )
        }
        return CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH )

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 99{
           currentPage = indexPath.row
            collectionView.reloadData()
            ProductImageCollection?.scrollToItem(at: indexPath, at: .right, animated: false)
        }else{
        var dict = [String: Any]()
            dict = ["image" : productImages]
          passDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiUse.zoom)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
         if collectionView.tag == 99{
        
        let totalCellWidth = 60 * productImages.count
        let totalSpacingWidth = 10 * (productImages.count - 1)
        
        let leftInset = (SCREEN_WIDTH - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
extension ProductPageViewModal: UIScrollViewDelegate {
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
         currentPage = Int(scrollView.contentOffset.x / SCREEN_WIDTH)
        ProductImageListCollection?.reloadData()
    }

}

extension ProductPageViewModal: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 && isSellerProduct {
            return  1
        }else if fileAttachments.count>0 && section == sectionCount - 1{
            return fileAttachments.count
        }else {
            return tableArray.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if indexPath.section == 0 && isSellerProduct {
            let cell = tableView.dequeueReusableCell(withIdentifier: SellerProductTableViewCell.identifier) as! SellerProductTableViewCell
            cell.item = sellerProductData
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = (fileAttachments.count>0 && indexPath.section == sectionCount - 1) ? fileAttachments[indexPath.row].name : tableArray[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            cell.selectionStyle = .none
            return cell
        }

    }
    

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if fileAttachments.count>0 && section == sectionCount - 1{
            return "download".localized
        }else {
            return " "
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if fileAttachments.count>0 && section == sectionCount - 1{
            return 30
        }else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if indexPath.section == 0 && isSellerProduct {
             delegate?.moveToController(id: sellerProductData.marketplace_seller_id, name: sellerProductData.seller_name, dict: JSON.null, Controller: Controllers.sellerProfile)
        }else if fileAttachments.count>0 && indexPath.section == sectionCount - 1{
              delegate?.moveToController(id: fileAttachments[indexPath.row].url, name: fileAttachments[indexPath.row].name, dict: JSON.null, Controller: Controllers.pdf)
         }else {
            if indexPath.row == 0  &&  self.descExists == true {
                delegate?.moveToController(id: (productData?.description!)!, name: "", dict: JSON.null, Controller: Controllers.description)
            } else {
                delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.reviews)
            }
        }
    }

}
