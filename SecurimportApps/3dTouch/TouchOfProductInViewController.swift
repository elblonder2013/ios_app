//
/**
 Odoo Marketplace
 @Category Webkul
 @author    Webkul
 Created by: jitendra on 28/08/18
 FileName: TouchOfProductInViewController.swift
 Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 @license   https://store.webkul.com/license.html
 */


//

import Foundation
import UIKit
import SwiftyJSON

protocol wishListArrayUpdate {
    func wishListArrayLoaclyUpdate(id:String)
}
class TouchOfProductInViewController: UIViewController {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameData: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    var productImgUrl = ""
    var productName = ""
    var wishListCheck = ""
    var productId = ""
    var dataInJson:JSON = ""
    var apiUse: WhichApiUse?
    var delegate:cartCount?
    var controller:Controllers!
    var wishListTrue = Bool()
    var delegateOfWishList:wishListArrayUpdate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.controller == .home {
            controller = Controllers.home
            productId = self.dataInJson["productId"].stringValue
            productNameData.text = self.dataInJson["name"].stringValue
            productImage?.setImage(imageUrl: self.dataInJson["thumbNail"].stringValue, controller: Controllers.touch)
        } else if self.controller == .sellerProfile {
            controller = Controllers.sellerProfile
            productId = self.dataInJson["productId"].stringValue
            productNameData.text = self.dataInJson["name"].stringValue
            productImage?.setImage(imageUrl: self.dataInJson["thumbNail"].stringValue, controller: Controllers.touch)
        }  else if self.controller == .allSeller {
            controller = Controllers.allSeller
            productNameData.text = productName
            productImage?.setImage(imageUrl: productImgUrl, controller: Controllers.touch)
        }
        else {
            controller = Controllers.productCategory
            productNameData.text = productName
            productImage?.setImage(imageUrl: productImgUrl, controller: Controllers.touch)
        }
        
        print(dataInJson,"val")
        setupView()
    }
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- button action
    override var previewActionItems: [UIPreviewActionItem] {
        
        let AddToCartBtn = UIPreviewAction(title: "addToCart".localized, style: false ? .destructive : .default) {
            (action, viewController) -> Void in
            if LoginCustomer.loginCheckCustomer() {
            self.apiUse = WhichApiUse.addToCart
            let dict = ["productId": Int(self.productId), "add_qty": 1] as [String : Any]
            self.makeRequest(dict1: dict, page: 0, index: 0, id: self.productId)
            } else {
                 ErrorChecking.warningView(message: "Please login first")
        }
        }
        let BuyNowBtn = UIPreviewAction(title: "buyNow".localized, style: false ? .destructive : .default) {
            (action, viewController) -> Void in
             if LoginCustomer.loginCheckCustomer() {
            self.apiUse = WhichApiUse.buyNow
            let dict = ["productId": Int(self.productId), "add_qty": 1] as [String : Any]
            self.makeRequest(dict1: dict, page: 0, index: 0, id: self.productId)
             }else {
                ErrorChecking.warningView(message: "Please login first")
            }
        }
        
        let wishListBtn = UIPreviewAction(title: wishListTrue ? "addedToWishlist".localized : "addToWishlist".localized, style: wishListTrue ? .destructive : .default) {
            (action, viewController) -> Void in
            if LoginCustomer.loginCheckCustomer() {
                if self.wishListTrue == true {
                     ErrorChecking.warningView(message: "productWishlist".localized)
                } else {
            self.apiUse = WhichApiUse.addToWishlist
            let dict = ["productId": Int(self.productId), "add_qty": 1] as [String : Any]
            self.makeRequest(dict1: dict, page: 0, index: 0, id: self.productId)
                }
        }else {
             ErrorChecking.warningView(message: "Please login first")
        }
        }
        return [AddToCartBtn,BuyNowBtn,wishListBtn]
    }
    
    
    
    //MARK:- Api hit function
    
    func makeRequest(dict1: [String: Any], page: Int, index: Int, id: String) {
        var params = ""
        var dict = [String: Any]()
        var verb: RequestType?
        switch apiUse {
            
        case .addToCart?:
            params = GlobalData.apiName.addToCart
            dict = dict1
            verb = .post
        case .addToWishlist?:
            params = GlobalData.apiName.addToWishlist
            dict = dict1
            verb = .post
        case .removeFromWishlist?:
            params = GlobalData.apiName.removeWishlist + id
            verb = .delete
        case .buyNow?:
            params = GlobalData.apiName.addToCart
            dict = dict1
            verb = .post
        default:
            print("hi")
        }
        
          NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: verb ?? .post, controler: controller) { (responseObject: JSON?, error: Error?) in
            
            if (error != nil) {
                print("Error logging you in!")
                
            } else {
                switch self.apiUse {
                    
                case .addToCart?:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                        
                        if responseObject!["cartCount"] != JSON.null {
                            
                            self.tabBarController?.setCartCount(value: responseObject!["cartCount"].stringValue)
                            
                            self.delegate?.cartValue(count: responseObject!["cartCount"].stringValue)
                        }
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                    
                case .addToWishlist?:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                        if self.controller !=  Controllers.productCategory {
                        self.delegateOfWishList.wishListArrayLoaclyUpdate(id: self.productId)
                        }
                        if self.controller == .productCategory {
                            self.delegate?.addToWishList!()
                        } else
                        {
                            
                        }
                        
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                case .removeFromWishlist?:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                        
                        
                        
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                case .buyNow?:
                    if responseObject!["success"].boolValue {
                        self.delegate?.pushToBuyNow()
                        
                        
                    } else {
                        
                    }
                default:
                    print("hi")
                }
                
            }
        }
    }
    
    private func setupView() {
        if controller == .home {
            delegate?.homeRefreshTouch!()
        }
        self.navigationController?.popToRootViewController(animated: true)
        return
    }
    
    @IBAction func toggleLikeState(_ sender: UIButton) {
        sender.pushSoft {
            //self.animal?.toggleLikeState()
            self.setupView()
        }
    }
}
extension UIButton {
    func pushSoft(completion:@escaping (() -> Void)) {
        self.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: .curveEaseOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: { (finished) in
            if finished {
                DispatchQueue.main.async {
                    completion()
                }
            }
        })
    }
}
