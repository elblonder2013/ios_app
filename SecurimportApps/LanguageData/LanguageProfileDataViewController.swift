//
//  LanguageProfileDataViewController.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 25/04/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit

class LanguageProfileDataViewController: UIViewController {
//
    @IBOutlet weak var languageTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Languages"
        // Do any additional setup after loading the view.
    }
//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        languageTableView.delegate = self
        languageTableView.dataSource = self
        languageTableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LanguageProfileDataViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GlobalData.StoreData.languages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = GlobalData.StoreData.languages[indexPath.row].languageName
        cell.selectionStyle = .none
        if let code = UserDefaults.standard.value(forKey: "defaultLanguage") as? String, code ==  GlobalData.StoreData.languages[indexPath.row].languageCode {
            cell.accessoryType = .checkmark
        }
        if (UserDefaults.standard.value(forKey: "defaultLanguage") as! String).split(separator: "_")[0] == "ar"{
             cell.textLabel?.textAlignment = .right
        }else{
             cell.textLabel?.textAlignment = .left
        }
        return cell
    }
    //
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GlobalData.StoreData.defaultLanguage = GlobalData.StoreData.languages[indexPath.row].languageCode
        UserDefaults.standard.set(GlobalData.StoreData.defaultLanguage, forKey: "defaultLanguage")
        UserDefaults.standard.synchronize()
        let val =  GlobalData.StoreData.defaultLanguage.split(separator: "_")[0]
        print(val)
        AppLanguageDefaultValue = String(val)
        UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
        if val == "ar" {
            L102Language.setAppleLAnguageTo(lang: "ar")
            if #available(iOS 9.0, *) {
                UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
                UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
                self.tabBarController?.tabBar.semanticContentAttribute = .forceRightToLeft
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            } else {
                // Fallback on earlier versions
            }
            
        } else {
            
            L102Language.setAppleLAnguageTo(lang: "en")
            if #available(iOS 9.0, *) {
                UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                UITabBar.appearance().semanticContentAttribute =  .forceLeftToRight
                self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                UITableView.appearance().semanticContentAttribute = .forceLeftToRight
                UITableView.appearance().semanticContentAttribute = .forceLeftToRight
            } else {
                // Fallback on earlier versions
            }
            
        }
       self.performSegue(withIdentifier: "home", sender: self)
    }
}
