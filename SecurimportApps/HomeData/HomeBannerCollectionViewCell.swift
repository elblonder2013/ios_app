//
//  HomeBannerCollectionViewCell.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class HomeBannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bannerImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: BannerSet? {
        didSet {

            guard let item = item else {
                return
            }
            bannerImage.contentMode = .scaleAspectFit
            bannerImage.setImage(imageUrl: item.pictureUrl!, controller: .homeBanners)

        }
    }
}
