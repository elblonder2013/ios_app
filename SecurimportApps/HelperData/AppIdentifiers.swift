//
//  AppIdentifiers.swift
//  Odoo application
//
//  Created by bhavuk.chawla on 14/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation

// user defaults keys

let loggedInUserKey = "isLoggedIn"
let loginStoredataKey = "loginStoredata"
let customerIdKey = "customerId"
let customerNameKey = "customerName"
let customerEmailKey = "customerEmail"
let CustomerImageUrl = "customerImageUrl"
let firebaseKey = "firebaseToken"
let gdprCheck = "False"

//  Story Boards

let homeStoryboard = "Home"
let customerStoryboard = "Customer"
let productStoryBoard = "Product"
let checkoutStoryBoard = "Checkout"

// View Controller identifiers

let signcontrollerIdentifier  = "signInViewController"
let signUpViewControllerIdentifier = "signUpViewController"
let notificationViewControllerIdentifier = "notificationViewController"
let orderHistoryViewControllerIdentifier = "orderHistoryViewController"
let orderDetailViewControllerIdentifier = "orderDetailViewController"
let addressDataViewControllerIdentifier = "addressDataViewController"
let wishlistDataViewControllerIdentifier = "wishlistDataViewController"
let accountInfoDataViewControllerIdentifier = "accountInfoDataViewController"
let dashBoardDatalViewControllerIdentifier = "dashBoardDatalViewController"
let newAddressDataViewControllerIdentifier = "newAddressDataViewController"
let productCategoryViewControllerIdentifier = "productCategoryViewController"
let productDataViewControllerIdentifier = "productDataViewController"
let addReviewViewControllerIdentifier = "addReviewViewController"
let descriptionViewControllerIdentifier = "descriptionViewController"
let reviewsViewControllerIdentifier = "reviewsViewController"
let viewInvoicesViewControllerIdentifier = "ViewInvoicesViewController"

