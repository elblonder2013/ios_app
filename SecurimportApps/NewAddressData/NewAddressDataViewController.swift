//
//  NewAddressDataViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift
import ActionSheetPicker_3_0
import CoreLocation

protocol AddressUpdate {
    func addressupdated(addressId: String)
}

class NewAddressDataViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var deleteHeight: NSLayoutConstraint!
    @IBOutlet weak var DeleteButton: UIButton!
    @IBOutlet weak var stateFieldLabel: UILabel!
    @IBOutlet weak var CountryFieldLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
     @IBOutlet weak var nameLabel: UILabel!
     @IBOutlet weak var phoneLabel: UILabel!
     @IBOutlet weak var addressLabel: UILabel!
     @IBOutlet weak var streetLabel: UILabel!
     @IBOutlet weak var zipLabel: UILabel!
     @IBOutlet weak var countryLabel: UILabel!
     @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var stateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameField: UITextField!
     @IBOutlet weak var phoneField: UITextField!
     @IBOutlet weak var streetField: UITextField!
     @IBOutlet weak var zipField: UITextField!
     @IBOutlet weak var countryView: UIView!
     @IBOutlet weak var stateFieldView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cityField: UITextField!
    
    var delete = false

    var countryArray = [CountryData]()
     var countryRow = 0
    var countries = [JSON]()
    var countryId: String?
    var StateId: String?
    var stateRow  = 0
    var addressId: String?
    var delegate: AddressUpdate?
    var locationManager = CLLocationManager()
    var locationCheck = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.stateView.isHidden = true
         contactLabel.SetDefaultTextColor()
         nameLabel.SetDefaultTextColor()
         phoneLabel.SetDefaultTextColor()
         addressLabel.SetDefaultTextColor()
         streetLabel.SetDefaultTextColor()
         zipLabel.SetDefaultTextColor()
         countryLabel.SetDefaultTextColor()
        cityLabel.SetDefaultTextColor()
         stateLabel.SetDefaultTextColor()
        saveButton.SetAccentBackColor()
        saveButton.setTitle("saveAddress".localized, for: .normal)
        self.navigationItem.title = address.localized
        contactLabel.text = contactAddress.localized
        nameLabel.text = yourName.localized
        phoneLabel.text = phone.localized
        addressLabel.text = address.localized
        streetLabel.text = streetAddress.localized
        zipLabel.text = zip1.localized
        cityLabel.text = city.localized
        countryLabel.text = country.localized
        stateLabel.text = state.localized
        zipField.keyboardType = .numberPad
        phoneField.keyboardType = .numberPad
        DeleteButton.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)

        let borderColor: UIColor? = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
        countryView.layer.borderColor = borderColor?.cgColor
        countryView.layer.borderWidth = 1.0
        countryView.layer.cornerRadius = 5.0
        stateFieldView.layer.borderColor = borderColor?.cgColor
        stateFieldView.layer.borderWidth = 1.0
        stateFieldView.layer.cornerRadius = 5.0
        stateViewHeight.constant = 0
       
//        countryView.applyBorder(colours: GlobalData.Credentials.textFieldBorder)
//        stateFieldView.applyBorder(colours: GlobalData.Credentials.textFieldBorder)

        if addressId == nil || delete == false {
            deleteHeight.constant = 0
            DeleteButton.isHidden = true
        } else {
             deleteHeight.constant = 50
             DeleteButton.isHidden = false
        }
       
        if let name = UserDefaults.standard.value(forKey: customerNameKey) as? String {
            self.nameField.text = name
        }
        
        // Do any additional setup after loading the view.
       
            self.makeRequest(dict: [:], call: .none)
        
//        }
//        else{
//            self.makeRequest(dict: [:], call: .editAddress)
//        }
        
    }

    func makeRequest(dict: [String: Any], call: WhichApiUse) {
        var params = ""

         var verb: RequestType = .post

        switch call {
        case .addAddress:
            params = GlobalData.apiName.addAddress
            verb = .post
        case .editAddress:
            params = GlobalData.apiName.editAddress + addressId!
            verb = .post
        case .editSave:
              params = GlobalData.apiName.editAddress + addressId!
              verb = .put
        case .deleteAddress:
            params = GlobalData.apiName.editAddress + addressId!
            verb = .delete
        default:
             params =  GlobalData.apiName.getCountries
             verb = .post
        }

        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: verb, controler: Controllers.addAddress) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {

                switch call {
                case .addAddress:
                    print("ddfbdss")
                    self.delegate?.addressupdated(addressId: "")
                    self.navigationController?.popViewController(animated: true)
                case .editAddress:

                    if let  data: JSON =  responseObject {

                        self.nameField.text = data["name"].stringValue
                        self.phoneField.text = data["phone"].stringValue
                        self.zipField.text = data["zip"].stringValue
                        self.cityField.text = data["city"].stringValue
                        self.streetField.text = data["street"].stringValue

                        let contId = data["country_id"].stringValue
                        if let index = self.countryArray.index( where: { $0.countryid == contId }) {

                           print(index)

                            self.CountryFieldLabel.text = self.countryArray[index].countryName!
                            self.countryId = self.countryArray[index].countryid!
                            self.countryRow = index

                            let staId = data["state_id"].stringValue

                            if staId.count > 0 {
                                if let index = self.countryArray[index].stateArray.index( where: { $0.stateid == staId }) {
                                    self.stateFieldLabel.text = self.countryArray[self.countryRow].stateArray[index].stateName!
                                    self.StateId = self.countryArray[self.countryRow].stateArray[index].stateid!
                                    self.stateRow = index
                                    self.stateView.isHidden = false
                                    self.stateViewHeight.constant = 72
                                }
                            }

                        }

                    }
                case .editSave:
                    print("hello~")
                     self.delegate?.addressupdated(addressId: "")
                    self.navigationController?.popViewController(animated: true)
                case .deleteAddress:
                    if responseObject!["success"].boolValue {
                        self.delegate?.addressupdated(addressId: "")
                        self.navigationController?.popViewController(animated: true)
                    }
                default:
                    self.setCountries(jsonData: responseObject!) {

                        (data: Bool) in

                    }
                    if self.addressId == nil {
                        self.fetchLocation()
                    }
                    
                    if self.addressId != nil {
                        self.makeRequest(dict: [:], call: .editAddress)
                    }
                }

            }
        }
    }

     func setCountries( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = NewAddressModal(data : jsonData ) else {
            return
        }

        if !data.countryArray.isEmpty {
            self.countryArray = data.countryArray
              self.CountryFieldLabel.text = self.countryArray[0].countryName!
                self.countryId = self.countryArray[0].countryid!
              completion(true)
        } else {
            completion(false)
        }

            if self.countries.isEmpty {

            } else {

            }

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func CountryTap(_ sender: UITapGestureRecognizer) {
        ActionSheetStringPicker.show(withTitle: "Countries", rows: countryArray.map {$0.countryName! }, initialSelection: countryRow, doneBlock: {
            picker, indexes, values in

//            print("values = \(values)")
//            print("indexes = \(indexes)")
//            print("picker = \(picker)")
             self.countryId = self.countryArray[indexes].countryid!
            self.countryRow = indexes
            self.CountryFieldLabel.text = values! as? String
            self.stateRow = 0
            if self.countryArray[indexes].stateArray.count > 0 {
                self.stateView.isHidden = false
                self.stateViewHeight.constant = 72
                  self.stateFieldLabel.text = self.countryArray[indexes].stateArray[0].stateName!
                self.StateId =  self.countryArray[indexes].stateArray[0].stateid!
            } else {
                self.StateId = nil
                self.stateFieldLabel.text = ""
                self.stateView.isHidden = true
                self.stateViewHeight.constant = 0
            }

            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.CountryFieldLabel)

    }

    @IBAction func StateTap(_ sender: UITapGestureRecognizer) {
        ActionSheetStringPicker.show(withTitle: "States", rows: countryArray[countryRow].stateArray.map {$0.stateName! }, initialSelection: stateRow, doneBlock: {
            picker, indexes, values in

//            print("values = \(values)")
//            print("indexes = \(indexes)")
//            print("picker = \(picker)")
            self.stateFieldLabel.text = values! as? String
            self.stateRow = indexes
            self.StateId =  self.countryArray[self.countryRow].stateArray[indexes].stateid!

            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.stateFieldLabel)
    }

    
   
    @IBAction func saveClicked(_ sender: UIButton) {

        var value = 0
        var errorMessage = ""

        if (nameField.text?.isEmpty)! {
            value = 1
            errorMessage = "Name is required field"
        } else if (phoneField.text?.isEmpty)! {
            value = 1
            errorMessage = "Phone is required field"
        }
        
//        else if (phoneField.text ?? "").isValidPhone == false {
//            value = 1
//            errorMessage = "phoneNumberNotValid".localized
//        }
        else if (streetField.text?.isEmpty)! {
            value = 1
            errorMessage = "Street Address is required field"
        }
        else if (cityField.text?.isEmpty)! {
            value = 1
            errorMessage = "City is required field"
        }
        else if (zipField.text?.isEmpty)! {
            value = 1
            errorMessage = "Zip/Postal Code is required field"
        } else if (zipField.text ?? "").isNumeric == false {
            value = 1
            errorMessage = "Zip/Postal Code only numeric"
        }
        

        if value == 1 {
            self.alert(message: errorMessage, title: "Error")
        } else {
            var dict = [String: Any]()

            dict["name"] = nameField.text
            dict["city"] = cityField.text
            dict["zip"] = zipField.text
            dict["street"] = streetField.text
            dict["phone"] = phoneField.text
            dict["country_id"] = countryId

            if StateId != nil {
                if !(StateId?.isEmpty)! {
                    dict["state_id"] = StateId!
                }
            }
            if addressId == nil {
                self.makeRequest(dict: dict, call: .addAddress)
            } else {
                self.makeRequest(dict: dict, call: .editSave)
            }
        }

    }
    
    @IBAction func locationBtnAct(_ sender: Any) {
        locationCheck = false
        self.fetchLocation()
    }
    

    @IBAction func DeleteClicked(_ sender: UIButton) {
        self.makeRequest(dict: [:], call: .deleteAddress)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func fetchLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

}
extension NewAddressDataViewController: CLLocationManagerDelegate {
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locationCheck == false{
        locationCheck = true
            locationManager.stopUpdatingHeading()
        let latestLocation = locations.last
        let g = CLGeocoder()
        var p:CLPlacemark?
        g.reverseGeocodeLocation(latestLocation!, completionHandler: {
            (placemarks, error) in
            let pm = placemarks
            if ((pm != nil) && (pm?.count)! > 0){
                // p = CLPlacemark()
                p = CLPlacemark(placemark: (pm?[0])!)
                
                self.streetField.text = (p?.subAdministrativeArea ?? "") + "," + (p?.thoroughfare ?? "")
                self.zipField.text = p?.postalCode
                self.cityField.text = p?.locality
                if self.countryArray.count > 0 {
                    if let index = self.countryArray.index(where: { $0.countryName == (p?.country ?? "")}) {
                        self.CountryFieldLabel.text = self.countryArray[index].countryName!
                        self.countryId = self.countryArray[index].countryid!
                         self.countryRow = index
                        if self.countryArray[index].stateArray.count > 0 {
                            self.stateView.isHidden = false
                            self.stateViewHeight.constant = 72
                             if let index1 = self.self.countryArray[index].stateArray.index(where: { $0.stateName == (p?.administrativeArea ?? "")}) {
                                self.stateFieldLabel.text = self.countryArray[index].stateArray[index1].stateName!
                                self.StateId =  self.countryArray[index].stateArray[index1].stateid!
                                  self.stateRow = index1
                             } else {
                                self.stateFieldLabel.text = self.countryArray[index].stateArray[0].stateName!
                                self.StateId =  self.countryArray[index].stateArray[0].stateid!
                            }
                            
                           
                        } else {
                            self.StateId = nil
                            self.stateFieldLabel.text = ""
                            self.stateView.isHidden = true
                            self.stateViewHeight.constant = 0
                        }
                    }
                }
            }
        })
        }
    }
}
