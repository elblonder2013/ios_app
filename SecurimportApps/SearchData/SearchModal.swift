//
//  SearchModal.swift
//  Odoo application
//
//  Created by Bhavuk on 31/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SearchModal {
    var products = [Product]()
      var query = [Query]()
    init?(data: JSON) {
        Addons.AddonsChecks(data: data)
        if let array  = data["products"].array {
            if array.count > 0 {
                self.products = array.map {
                    Product(json: $0)
                }
            }
        }
        if let queries = UserDefaults.standard.value(forKey: "Queries") as? [String] {

            if queries.count > 0 {
                query = queries.map {
                    Query(value: $0)
                }

            }
        }
    }
}

struct Query {
    var value: String?
    init(value: String) {
        self.value = value
    }
}
