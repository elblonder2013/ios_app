//
//  ProfileHeader.swift
//  Odoo application
//
//  Created by bhavuk.chawla on 14/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileHeader: UIView {

    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editProfileView: UIButton!
    var passDataDelegate: PassData?
    var delegate: NewViewControllerOpen?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.isUserInteractionEnabled = true
        editProfileView.layer.borderWidth = 2
        editProfileView.layer.borderColor = UIColor.white.cgColor
        profileView.backgroundColor = UIColor.clear
        editProfileView.setTitle("editProfile".localized, for: .normal)
//        profileView.alpha = 0.1
//      mainView.backgroundColor = UIColor.clear

    }

    @IBAction func changeImageTapped(_ sender: UITapGestureRecognizer) {
        passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.uploadImage)

    }

    @IBAction func EditProfileClicked(_ sender: UIButton) {

        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.accountInfo)
    }

}
