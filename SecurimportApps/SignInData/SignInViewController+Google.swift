//
//  SignInViewController+Google.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 25/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import Foundation
import GoogleSignIn
import FirebaseAuth

extension SignInViewController: SocialLoginGoogle {
    func googleLogin() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
         GIDSignIn.sharedInstance().signIn()
    }
}

extension SignInViewController : GIDSignInDelegate , GIDSignInUIDelegate {
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if error != nil {
            // ...
            return
        }
        
        guard let authentication = user.authentication else { return }
        _ = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                          accessToken: authentication.accessToken)
        
        print("aaaa", user.profile.email , user.profile.name)
        
        // var fullNameArr = user.profile.name.components(separatedBy: " ")
        
        var loginDict = [String : Any]()
        
        loginDict["login"] =  user.profile.email
        loginDict["name"] = user.profile.name
        loginDict["authUserId"] =  user.userID
        loginDict["authProvider"] = "GMAIL"
        loginDict["isSocialLogin"] = true
        loginDict["password"] = ""
        self.makeRequest(dict: loginDict, call: WhichApiUse.social)
        //        self.makeRequest(call: WhichApiCall.social, email: "", newDict: loginDict)
        
        GIDSignIn.sharedInstance().signOut()
    }
    
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
