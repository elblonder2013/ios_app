//
//  ProductOrderTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 05/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class ProductOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        productLabel.SetDefaultTextColor()
        productLabel.text = "Product".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: Product? {
        didSet {
            productImage.setImage(imageUrl: (item?.pictureUrl!)!, controller: .cart)
            nameLabel.text = item?.name
            priceLabel.text = String.init(format: "%@: %@", price.localized, (item?.priceUnit)!)
            qtyLabel.text = String.init(format: "%@: %@", qty.localized, (item?.qty)!)
            totalLabel.text = String.init(format: "%@: %@", subtotal.localized, (item?.priceTotal)!)
            priceLabel.halfTextColorChange(fullText: priceLabel.text!, changeText: price.localized)
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: qty.localized)
            totalLabel.halfTextColorChange(fullText: totalLabel.text!, changeText: subtotal.localized)
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: qty.localized)

        }
    }

    var item1: Product? {
        didSet {
            productImage.setImage(imageUrl: (item1?.pictureUrl )!, controller: .cart)
            nameLabel.text = item?.name
            priceLabel.text = String.init(format: "%@: %@", price.localized, (item1?.price)!)
            qtyLabel.text = String.init(format: "%@: %@", qty.localized, (item1?.qty)!)
            totalLabel.text = String.init(format: "%@: %@", subtotal.localized, (item1?.totalPrice)!)
            priceLabel.halfTextColorChange(fullText: priceLabel.text!, changeText: price.localized)
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: qty.localized)
            totalLabel.halfTextColorChange(fullText: totalLabel.text!, changeText: subtotal.localized)
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: qty.localized)

        }
    }

}
