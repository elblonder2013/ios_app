//
//  OptionViewController.swift
//  SecurimportApps
//
//  Created by debabrata on 04/10/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class OptionsViewController:UIViewController {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var titlelabel: UILabel!
    var productAttributes = [ProductAttributes]()
    var delegate : OptionsDelegate?
    var tableViewHeight: CGFloat {
        tableview.layoutIfNeeded()
        return tableview.contentSize.height
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableview.register(OptionTableViewCell.nib, forCellReuseIdentifier: OptionTableViewCell.identifier)
        tableview.delegate = self
        tableview.dataSource = self
        self.view.backgroundColor = UIColor.clear
         self.view.isOpaque = false
        viewHeight.constant = tableViewHeight + 100 > screenHeight - 120 ? screenHeight - 140 : tableViewHeight + 100
        UIView.animate(withDuration: 1.5, animations: {
            self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        }, completion: {res in
        })

    }
    
    @IBAction func cancelPress(sender:UIButton){
        self.tabBarController?.tabBar.isHidden = false
        delegate?.optionsDataPass(data: nil)
        self.dismiss(animated: true, completion: {
        })
    }
    @IBAction func donePress(sender:UIButton){
        delegate?.optionsDataPass(data: productAttributes)
        self.dismiss(animated: true, completion: {
            self.tabBarController?.tabBar.isHidden = false
        })
    }
            
    
}
extension OptionsViewController : UITableViewDataSource,UITableViewDelegate,PassData{
    func passData(id: String, name: String, dict: [String : Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        productAttributes[(dict["tag"] as? Int ?? 0)].value = index
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productAttributes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: OptionTableViewCell.identifier) as? OptionTableViewCell{
            cell.item = productAttributes[indexPath.row]
            cell.delegate = self
            cell.selectionStyle = .none
            cell.tag = indexPath.row
            return cell
        }
        return UITableViewCell()
    }
}
protocol OptionsDelegate {
    func optionsDataPass(data:[ProductAttributes]?)
}
