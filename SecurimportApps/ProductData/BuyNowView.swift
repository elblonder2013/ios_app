//
//  BuyNowView.swift
//  Odoo iOS
//
//  Created by Bhavuk on 03/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

protocol AddToCartDelegate {

    func addToCart(cart: AddCart)
}

class BuyNowView: UIView {

    @IBOutlet weak var buyNowBtn: UIButton!
    @IBOutlet weak var addToCartBtn: UIButton!
    var cartDelegate: AddToCartDelegate?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        
    }
    
    @IBAction func addToCartClicked(_ sender: UIButton) {
//        if LoginCustomer.loginCheckCustomer() {
             cartDelegate?.addToCart(cart: AddCart.cart)
//        }
//        else{
//            LoginCustomer.showLogginMessage()
//        }

    }
    @IBAction func buyNowClicked(_ sender: UIButton) {
//        if LoginCustomer.loginCheckCustomer() {
            cartDelegate?.addToCart(cart: AddCart.buyNow)
//        }
//        else{
//            LoginCustomer.showLogginMessage()
//        }

    }
}

enum AddCart {
    case cart
    case buyNow
}
