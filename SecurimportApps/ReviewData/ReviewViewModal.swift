//
//  ReviewViewModal.swift
//  Odoo iOS
//
//  Created by Bhavuk on 04/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import SwiftyJSON

class ReviewViewModal: NSObject {

     var passData: PassData?
    var reviews = [ReviewData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = ReviewModal(data : jsonData) else {
            return
        }

        if !data.reviews.isEmpty {
            reviews = data.reviews
            completion(true)
        } else {
            completion(false)
        }
    }
}

extension ReviewViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewsTableViewCell.identifier) as! ReviewsTableViewCell
        cell.item = reviews[indexPath.row]
        cell.selectionStyle = .none
        cell.passData = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Product Reviews"
    }

}

extension ReviewViewModal: PassData {
    func passData(id: String, name: String, dict: [String : Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        passData?.passData(id: id, name: name, dict: dict, jsonData: jsonData, index: index, call: call)
    }
}
