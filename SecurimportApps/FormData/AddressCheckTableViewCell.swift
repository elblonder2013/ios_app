//
//  AddressCheckTableViewCell.swift
//  MobikulOpencart
//
//  Created by bhavuk.chawla on 20/11/18.
//  Copyright © 2018 yogesh. All rights reserved.
//

import UIKit
import Reusable

class AddressCheckTableViewCell: UITableViewCell  , FormConformity, NibReusable {

    @IBOutlet weak var imagev: UIImageView!
    @IBOutlet weak var label: UILabel!
      var formItem: FormItem?
    override func awakeFromNib() {
        super.awakeFromNib()
        label.text = "My delivery and billing addresses are same".localized
        imagev.applyBorder(colours: GlobalData.Credentials.accentColor)
        imagev.layer.cornerRadius = 2
        label.isUserInteractionEnabled = true
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
        label.addGestureRecognizer(doubleTap)
        
        imagev.isUserInteractionEnabled = true
        let doubleTap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleDoubleTap))
        imagev.addGestureRecognizer(doubleTap1)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
     @objc func handleDoubleTap(_ gestureRecognizer: UIGestureRecognizer) {
        if let form = formItem {
            if form.value as? String == "1" {
                form.value = "0"
                imagev.backgroundColor = UIColor.white
                self.imagev.image = nil
            } else {
                form.value = "1"
                self.imagev.image = UIImage(named: "ic_check")
                imagev.backgroundColor = GlobalData.Credentials.accentColor
                imagev.clipsToBounds = true
            }
        }
           
    }
    
}

extension AddressCheckTableViewCell: FormUpdatable {
    func update(with formItem: FormItem) {
        self.formItem = formItem
        
        if formItem.value as? String == "1" {
            self.imagev.image = UIImage(named: "ic_check")
            imagev.backgroundColor = GlobalData.Credentials.accentColor
            imagev.clipsToBounds = true
        } else {
            imagev.backgroundColor = UIColor.white
            self.imagev.image = nil
           
        }
    }
}

