//
//  CheckoutOrderReviewView.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON

class CheckoutOrderReviewView: UIView{

@IBOutlet weak var continueButton: UIButton!
@IBOutlet weak var checkoutOrderReview: UITableView!
var delegate: PassData?
var dataForShortTerms = ""
var dataForLongTerms = ""
var moveDelegate:NewViewControllerOpen!
let xibView = Bundle.main.loadNibNamed("GdprView", owner: self, options: nil)?[0] as! GdprView
/*
 // Only override draw() if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 override func draw(_ rect: CGRect) {
 // Drawing code
 }
 */

override func awakeFromNib() {
    super.awakeFromNib()
    if GlobalData.AddOnsEnabled.allowGdpr  {
        xibView.isHidden = false
    } else {
        xibView.isHidden = true
    }
    checkoutOrderReview.register(CheckoutOrderDetailReviewTableViewCell.nib, forCellReuseIdentifier: CheckoutOrderDetailReviewTableViewCell.identifier)
    checkoutOrderReview.register(ProductOrderTableViewCell.nib, forCellReuseIdentifier: ProductOrderTableViewCell.identifier)
    checkoutOrderReview.register(OrderShippingTableViewCell.nib, forCellReuseIdentifier: OrderShippingTableViewCell.identifier)
    
    checkoutOrderReview.delegate = self
    checkoutOrderReview.delegate = self
    checkoutOrderReview.estimatedRowHeight = 100
    checkoutOrderReview.rowHeight = UITableView.automaticDimension
    continueButton.setTitle("continue".localized, for: .normal)
    continueButton.backgroundColor = GlobalData.Credentials.accentColor

    //xibView.frame = self.bounds
    //xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    xibView.moveDelegate = self
    //        xibView.termsLabel.textColor = UIColor.blue
    xibView.type = "orderReview"
    //checkoutOrderReview.tableFooterView = xibView
    // checkoutOrderReview.tableFooterView?.frame.size.height = 80
    
}

@IBAction func ContinuePressed(_ sender: UIButton) {
    if GlobalData.AddOnsEnabled.allowGdpr  {
        if xibView.checkUncheckBtn.isOn {
            delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.placeOrder)
        } else {
            ErrorChecking.warningView(message: "Please select Terms & Conditions".localized)
        }
    }
    else
    {
        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.placeOrder)
    }
}

var items = [OrderDetailViewItem]()
var orderId: String?
    var paymentUrl = ""
func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
    guard let data = OrderDetailModal(data : jsonData
        ) else {
            return
    }
    paymentUrl = data.paymentUrl
    dataForShortTerms = data.shortTerm!
    dataForLongTerms = data.longTerm!
    let GdprShortTermData = data.shortTerm!.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    dataForShortTerms = GdprShortTermData
    xibView.termsLabel.textColor = UIColor.black
    xibView.termsLabel.text = "I Accept Terms & Conditions".localized//GdprShortTermData
    xibView.termsLabel.halfTextWithColorChange(fullText: xibView.termsLabel.text!, changeText: "Terms & Conditions", color: UIColor.blue)
    items.removeAll()
    if !data.products.isEmpty {
        items.append(OrderViewModalProductItem(products: data.products))
        items.append(OrderViewModalOrdersItem(orderData: data.orders!))
        items.append(OrderViewModalShippingItem(shipping: data.shippingAddress!))
        items.append(OrderViewModalBillingItem(billing: data.billingAddress!))
        if data.shippingData != nil {
            items.append(OrderViewModalShippingDetailsItem(shipping: data.shippingData!))
        }
        checkoutOrderReview.reloadData()
        completion(true)
        
    } else {
        completion(false)
    }
    }
}



extension CheckoutOrderReviewView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        
        switch item.type {
        case .orderData :
            if let item = item as? OrderViewModalOrdersItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutOrderDetailReviewTableViewCell.identifier) as! CheckoutOrderDetailReviewTableViewCell
                cell.item1 = item.orderData
                cell.selectionStyle = .none
                return cell
            }
            
        case .products:
            if let item = item as? OrderViewModalProductItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: ProductOrderTableViewCell.identifier) as! ProductOrderTableViewCell
                cell.item1 = item.products[indexPath.row]
                cell.selectionStyle = .none
                return cell
            }
            
        case .shiiping:
            if let item = item as? OrderViewModalShippingItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.shipping
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                cell.selectionStyle = .none
                if (UserDefaults.standard.value(forKey: "defaultLanguage") as! String).split(separator: "_")[0] == "ar"{
                    cell.textLabel?.textAlignment = .right
                }else{
                    cell.textLabel?.textAlignment = .left
                }
                return cell
            }
            
        case .billing:
            
            if let item = item as? OrderViewModalBillingItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.billing
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                cell.selectionStyle = .none
                if (UserDefaults.standard.value(forKey: "defaultLanguage") as! String).split(separator: "_")[0] == "ar"{
                    cell.textLabel?.textAlignment = .right
                }else{
                    cell.textLabel?.textAlignment = .left
                }
                return cell
            }
        case .shippingDetails:
            if let item = item as? OrderViewModalShippingDetailsItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: OrderShippingTableViewCell.identifier) as! OrderShippingTableViewCell
                cell.shippingMethodName.text = item.shipping?.name
                cell.shippingPrice.text = item.shipping?.price
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let item = items[section]
        
        switch item.type {
        case .orderData :
            return "amountDetail".localized
            
        case .products:
            if let item = item as? OrderViewModalProductItem {
                return item.heading
            }
            
        case .shiiping:
            if let item = item as? OrderViewModalShippingItem {
                return item.heading
            }
            
        case .billing:
            
            if let item = item as? OrderViewModalBillingItem {
                return item.heading
            }
        case .shippingDetails:
            
            if let item = item as? OrderViewModalShippingDetailsItem {
                return item.heading
            }
        }
        return ""
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == items.count - 1{
            return xibView
        }else{
             return nil
        }
       
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == items.count - 1{
            return 80
        }else{
            return 0
        }
    }
}

//MARK:- delegate call on the review terms and conditions to the cms page
extension CheckoutOrderReviewView: NewViewControllerOpen{
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        if Controller == Controllers.cmsController{
            moveDelegate.moveToController(id: id, name: name, dict: dict, Controller: Controller)
        }
    }
}

