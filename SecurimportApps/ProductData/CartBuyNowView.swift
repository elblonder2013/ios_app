//
//  CartBuyNowView.swift
//  Odoo iOS
//
//  Created by Bhavuk on 03/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class CartBuyNowView: UIView {
    @IBOutlet weak var contentView: UIView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        self.commonInit()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//                super.init(coder: aDecoder)
//        self.commonInit()
//    }

//    private func commonInit() {
//        Bundle.main.loadNibNamed("CartBuyNowView", owner: self, options: nil)
//        addSubview(contentView)
//        contentView.frame = self.bounds
//         contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let xibView = Bundle.main.loadNibNamed("CartBuyNowView", owner: CartBuyNowView.self, options: nil)!.first as! UIView
        print(Bundle.main.loadNibNamed("CartBuyNowView", owner: self, options: nil)?[0])
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(xibView)
    }

}
