//
//  CategoryController.swift
//  Odoo application
//
//  Created by vipin sahu on 9/5/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class CategoryController: UIViewController {
    @IBOutlet weak var categoryTableView: UITableView!

    fileprivate let viewModel = CategoryMenuViewModel()
    var categoryData: JSON?
    var name: String?

    override func viewDidLoad() {
        super.viewDidLoad()
          self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        navigationController?.navigationBar.tintColor = UIColor.white
           categoryTableView?.register(CategoryHeadeViewCell.nib, forHeaderFooterViewReuseIdentifier: CategoryHeadeViewCell.identifier)
        viewModel.getValue(categoryData: categoryData ?? JSON.null)
        viewModel.reloadSections = { [weak self] (section: Int) in
            self?.categoryTableView?.beginUpdates()
            self?.categoryTableView?.reloadSections([section], with: .fade)
            self?.categoryTableView?.endUpdates()
        }
        viewModel.delegate = self
        categoryTableView?.estimatedRowHeight = 100
        categoryTableView?.rowHeight = UITableView.automaticDimension
        categoryTableView?.sectionHeaderHeight = 70
        categoryTableView?.separatorStyle = .none
        categoryTableView?.dataSource = viewModel
        categoryTableView?.delegate = viewModel
     
        self.categoryTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        if((name?.count) ?? 0 > 0) {
            self.navigationItem.title = name
        } else {
            self.navigationItem.title = categories.localized
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
         SVProgressHUD.dismiss()
        
    }


}
extension CategoryController: SubCategoryDelegate {
    func passSubCategoryData(id: String, name: String, categoryData: JSON) {
        if categoryData != JSON.null {
            let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName : "categoryController") as! CategoryController
            view.categoryData = categoryData
            view.name = name
            self.navigationController?.pushViewController(view, animated: true)
        } else {
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
            view.categoryId = id
            view.categoryName = name
            view.parentController = Controllers.productCategory
            self.navigationController?.pushViewController(view, animated: true)
        }
    }

}
