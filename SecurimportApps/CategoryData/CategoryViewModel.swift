//
//  CategoryViewModel.swift
//  Odoo application
//
//  Created by vipin sahu on 9/7/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

protocol SubCategoryDelegate {
    func passSubCategoryData(id: String, name: String, categoryData: JSON)
}

class CategoryMenuViewModel: NSObject {
    var reloadSections: ((_ section: Int) -> Void)?

    var items = [CategoryMenuViewModelItem]()

    var delegate: SubCategoryDelegate?

    func getValue(categoryData: JSON) {
        items.removeAll()
        if(categoryData != JSON.null) {
            guard let profile = CategoryMenuModel(data : categoryData) else {
                return
            }
            if !profile.categories.isEmpty {
                let categoriesItem = ProfileViewModecategoriesItem(categories: profile.categories)
                items.append(categoriesItem)
            }
        } else {
            guard let data = CategoryArray, let profile = CategoryMenuModel(data : data) else {
                return
            }
            if !profile.categories.isEmpty {
                let categoriesItem = ProfileViewModecategoriesItem(categories: profile.categories)
                items.append(categoriesItem)
            }

        }
    }
}

extension CategoryMenuViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {

        if items.count > 0, let item = items[0] as? ProfileViewModecategoriesItem {
            return item.rowCount
        }

        return 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if items.count > 0, let item = items[0] as? ProfileViewModecategoriesItem {
            if item.categories[section].isCollapsed {
                return 0
            } else {
                return item.categories[section].SubcategoryArray.count
            }
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        if items.count > 0, let item = items[0] as? ProfileViewModecategoriesItem {
            cell.textLabel?.text = item.categories[indexPath.section].SubcategoryArray[indexPath.row].name
            cell.textLabel?.textColor = UIColor.gray
            cell.accessoryType = .disclosureIndicator
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if  items.count > 0, let item = items[0] as? ProfileViewModecategoriesItem {
            if item.categories[indexPath.section].SubcategoryArray[indexPath.row].containSubCategory {
                delegate?.passSubCategoryData(id: item.categories[indexPath.section].SubcategoryArray[indexPath.row].CategoryId!, name: item.categories[indexPath.section].SubcategoryArray[indexPath.row].name!, categoryData: item.categories[indexPath.section].SubcategoryArray[indexPath.row].jsonData!)
            } else {
                delegate?.passSubCategoryData(id: item.categories[indexPath.section].SubcategoryArray[indexPath.row].CategoryId!, name: item.categories[indexPath.section].SubcategoryArray[indexPath.row].name!, categoryData: JSON.null)
            }
        }
    }

}

extension CategoryMenuViewModel: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CategoryHeadeViewCell.identifier) as? CategoryHeadeViewCell {
            if let item = items[0] as? ProfileViewModecategoriesItem {
                headerView.item = item.categories[section]
            }
            headerView.section = section
            headerView.delegate = self
            headerView.contentView.backgroundColor = UIColor.white
            return headerView
        }
        return UIView()
    }
}

extension CategoryMenuViewModel: HeaderViewDelegate {
    func toggleSection(header: CategoryHeadeViewCell, section: Int) {
        if let item = items[0] as? ProfileViewModecategoriesItem {
            if(item.categories[section].SubcategoryArray.count > 0) {
                let collapsed = !item.categories[section].isCollapsed
                item.categories[section].isCollapsed = collapsed
                header.setCollapsed(collapsed: collapsed)
                reloadSections?(section)
            } else {
                  delegate?.passSubCategoryData(id: item.categories[section].CategoryId!, name: item.categories[section].name!, categoryData: JSON.null)
            }
        }
    }
}

protocol CategoryMenuViewModelItem {
    var rowCount: Int { get }
}

class ProfileViewModecategoriesItem: CategoryMenuViewModelItem {

    var rowCount: Int {
        return categories.count
    }
    var categories: [Category]
    init(categories: [Category]) {
        self.categories = categories
    }
}
