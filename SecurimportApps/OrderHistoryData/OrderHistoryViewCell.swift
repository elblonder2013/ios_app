//
//  OrderHistoryViewCell.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class OrderHistoryViewCell: UITableViewCell {

    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var placedOnLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var shipToLabel: UILabel!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        shipToLabel.text = shipTo.localized
        placedOnLabel.text = placedOn.localized
        placedOnLabel.textColor = GlobalData.Credentials.defaultTextColor
        shipToLabel.text = shipTo.localized
        shipToLabel.textColor = GlobalData.Credentials.defaultTextColor
        statusLabel.textColor = GlobalData.Credentials.defaultTextColor
        orderView.backgroundColor = UIColor.white
        orderView.myBorder()
//        orderView.layer.shadowRadius = 10
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: orderHistoryData? {
        didSet {
            orderIdLabel.text = String.init(format: "%@ #%@", order.localized, (item?.name)!)
            statusLabel.text = item?.status
            placedOnLabel.text = placedOn.localized
            dateLabel.text = item?.date
            addressLabel.text = item?.address
            shipToLabel.text = shipTo.localized
            totalLabel.text = String.init(format: "%@: %@", orderTotal.localized, (item?.total)!)

            totalLabel.halfTextColorChange(fullText: totalLabel.text!, changeText: orderTotal.localized)
            orderIdLabel.halfTextColorChange(fullText: orderIdLabel.text!, changeText:  order.localized)

            if item?.status == "sale"{
                statusIcon.image = #imageLiteral(resourceName: "Status-Complete")
            } else {
                statusIcon.image = #imageLiteral(resourceName: "Status-Pending")
            }

        }
    }
}
