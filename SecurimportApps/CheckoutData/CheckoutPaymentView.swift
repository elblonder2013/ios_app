//
//  CheckoutPaymentView.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toaster

class CheckoutPaymentView: UIView {

    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var paymentTABLEVIEW: UITableView!

    var delegate: PassData?
    var paymentId: String?
    var paymentCode = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        paymentTABLEVIEW.register(CheckoutPaymentTableViewCell.nib, forCellReuseIdentifier: CheckoutPaymentTableViewCell.identifier)
        paymentTABLEVIEW.delegate = self
        paymentTABLEVIEW.delegate = self
        paymentTABLEVIEW.estimatedRowHeight = 100
        paymentTABLEVIEW.rowHeight = UITableView.automaticDimension
        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.backgroundColor = GlobalData.Credentials.accentColor
    }

    @IBAction func continuePressed(_ sender: UIButton) {
        if let id = paymentId {
            delegate?.passData(id: id, name: paymentCode, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.getOrderReview)
        } else {
            ErrorChecking.warningView(message: "selectPaymentMethod".localized)
          
        }
    }

    var paymentMethods = [PaymentData]()

    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = PaymentModal(data : jsonData) else {
            return
        }

        if !data.paymentMethods.isEmpty {
            paymentMethods = data.paymentMethods
            completion(true)
            paymentTABLEVIEW.reloadData()
        } else {
            completion(false)
        }

    }
}

extension CheckoutPaymentView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethods.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutPaymentTableViewCell.identifier) as! CheckoutPaymentTableViewCell
            cell.selectionStyle = .none
            if let id = paymentId {
                cell.paymentID = id
            }
            cell.item = paymentMethods[indexPath.row]
            return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Payment Methods".localized
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        paymentId = paymentMethods[indexPath.row].paymentId
        paymentCode = paymentMethods[indexPath.row].code
        self.paymentTABLEVIEW.reloadData()
    }

}

struct PaymentModal {
    var paymentMethods = [PaymentData]()
    init?(data: JSON) {
        if let array  = data["acquirers"].array {
            if array.count > 0 {
                self.paymentMethods = array.map { PaymentData(data: $0) }
            }
        }
    }
}

struct PaymentData {
    var paymentId: String
    var description: String
    var name: String
    var paymentUrl : String
    var code : String
    init(data: JSON) {
        self.paymentId = data["id"].stringValue
        self.description = data["description"].stringValue
        self.name = data["name"].stringValue
        self.paymentUrl = data["paymentUrl"].stringValue
        self.code = data["code"].stringValue
    }
}
