//
//  OrderDetailViewModal.swift
//  Odoo iOS
//
//  Created by Bhavuk on 05/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import SwiftyJSON

class OrderDetailViewModal: NSObject {
//    var passdataDelegate : PassData?
    var delegate: NewViewControllerOpen?
    var items = [OrderDetailViewItem]()
    var orderId: String?

    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = OrderDetailModal(data : jsonData
            ) else {
                return
        }

        if !data.products.isEmpty {

            items.append(OrderViewModalOrdersItem(orderData: data.orders!))
            items.append(OrderViewModalProductItem(products: data.products))
            items.append(OrderViewModalShippingItem(shipping: data.shippingAddress!))
            items.append(OrderViewModalBillingItem(billing: data.billingAddress!))
            if data.shippingData != nil {
                 items.append(OrderViewModalShippingDetailsItem(shipping: data.shippingData!))
            }

            completion(true)

        } else {
            completion(false)
        }

    }
}

extension OrderDetailViewModal: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]

        switch item.type {
        case .orderData :
            if let item = item as? OrderViewModalOrdersItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: OrderDetailTableViewCell.identifier) as! OrderDetailTableViewCell
                cell.item = item.orderData
                return cell
            }

        case .products:
            if let item = item as? OrderViewModalProductItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: ProductOrderTableViewCell.identifier) as! ProductOrderTableViewCell
                cell.item = item.products[indexPath.row]
                cell.selectionStyle = .none
                return cell
            }

        case .shiiping:
            if let item = item as? OrderViewModalShippingItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.shipping
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                return cell
            }

        case .billing:

            if let item = item as? OrderViewModalBillingItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.billing
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.lineBreakMode = .byWordWrapping
                return cell
            }
        case .shippingDetails:
            if let item = item as? OrderViewModalShippingDetailsItem {
                 let cell = tableView.dequeueReusableCell(withIdentifier: OrderShippingTableViewCell.identifier) as! OrderShippingTableViewCell
                cell.shippingMethodName.text = item.shipping?.name
                 cell.shippingPrice.text = item.shipping?.price
                return cell
            }
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let item = items[section]

        switch item.type {
        case .orderData :
            if let item = item as? OrderViewModalOrdersItem {
                let heading  = item.heading + orderId!
                return heading
            }

        case .products:
            if let item = item as? OrderViewModalProductItem {
                return item.heading
            }

        case .shiiping:
            if let item = item as? OrderViewModalShippingItem {
                return item.heading
            }

        case .billing:

            if let item = item as? OrderViewModalBillingItem {
                 return item.heading
            }
        case .shippingDetails:
            
            if let item = item as? OrderViewModalShippingDetailsItem {
                return item.heading
            }
        }
         return ""
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let item = items[indexPath.section]
        if let item = item as? OrderViewModalProductItem {
            print(item.products[indexPath.row])
            delegate?.moveToController(id: item.products[indexPath.row].templateId!, name: item.products[indexPath.row].name!, dict: JSON.null, Controller: Controllers.product)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

enum OrderCases {
    case orderData
    case products
    case billing
    case shiiping
    case shippingDetails
}

protocol OrderDetailViewItem {
    var type: OrderCases { get }
    var rowCount: Int { get }
    var heading: String { get }
}

class OrderViewModalOrdersItem: OrderDetailViewItem {

    var type: OrderCases {
        return .orderData
    }

    var rowCount: Int {
        return 1
    }

    var heading: String {
        return String.init(format: "%@ #", order.localized)
    }
    var orderData: orderHistoryData?

    init(orderData: orderHistoryData ) {
        self.orderData = orderData
    }

}

class OrderViewModalProductItem: OrderDetailViewItem {

    var type: OrderCases {
        return .products
    }

    var rowCount: Int {
        return products.count
    }

    var heading: String {
        return "Products".localized
    }
    var products: [Product]

    init(products: [Product] ) {
        self.products = products
    }

}

class OrderViewModalBillingItem: OrderDetailViewItem {

    var type: OrderCases {
        return .billing
    }

    var rowCount: Int {
        return 1
    }

    var heading: String {
        return billingAddress.localized
    }
    var billing: String?

    init(billing: String ) {
        self.billing = billing
    }

}

class OrderViewModalShippingItem: OrderDetailViewItem {

    var type: OrderCases {
        return .shiiping
    }

    var rowCount: Int {
        return 1
    }

    var heading: String {
        return shippingAddress.localized
    }
    var shipping: String?

    init(shipping: String ) {
        self.shipping = shipping
    }

}

class OrderViewModalShippingDetailsItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .shippingDetails
    }
    
    var rowCount: Int {
        return 1
    }
    
    var heading: String {
        return "shippingDetails".localized
    }
    var shipping: ShippingData?
    
    init(shipping: ShippingData ) {
        self.shipping = shipping
    }
    
}
