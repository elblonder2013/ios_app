//
//  AccountInfoDataViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toaster
import Alamofire
import Kingfisher
class AccountInfoDataViewController: UIViewController,AccountDeacticvateAction {
    //MARK:- gdpr download request
    func downloadGdprRequest(type: String) {
        AccountDeativationType = type
        self.makeRequest(dict: [:], call: WhichApiUse.gdprDownladRequest)
        
    }
    
    func downloadAllInfo(type: String) {
        
        AccountDeativationType = type
        if downloadBtncheck == 0 {
            self.makeRequest(dict: [:], call: WhichApiUse.gdprDownladRequest)
        }
    }
    
    //MARK:- accont deactivate delegate function
    func accountDeactivate(type: String) {
        AccountDeativationType = type
        //"type": "temporary/permanent"
        var dict = [String:Any]()
        if AccountDeativationType == "Temporary"{
            dict = ["type":"temporary"]
        } else {
            dict = ["type":"permanent"]
        }
        let alert = UIAlertController(title: "deactivate".localized, message: "\("doYouwantToDeativate".localized) " + "\(AccountDeativationType)", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "continue".localized, style: UIAlertAction.Style.destructive, handler:
            { (action: UIAlertAction!) in
                self.makeRequest(dict: dict, call: WhichApiUse.gdprDeactivateAccount)
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localized, style: UIAlertAction.Style.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    
    var downloadGdprData:JSON = ""
    @IBOutlet weak var accountTableView: UITableView!
    var downloadBtncheck = 0
    var sectionCount = 2
    @IBOutlet weak var updateButton: UIButton!
    var delegate: AddressUpdate?
    var AccountDeativationType = String()
    var reloadSections: ((_ section: Int) -> Void)?
    lazy var viewModel : AccountInfoViewModal = {
        let viewModel = AccountInfoViewModal()
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
       FormItemCellType.registerCells(for: self.accountTableView)

        accountTableView.estimatedRowHeight = 100
        accountTableView.rowHeight = UITableView.automaticDimension
        accountTableView.delegate = self
        accountTableView.dataSource = self
        self.navigationItem.title = accountInfo.localized
     
        print(GlobalData.AddOnsEnabled.emailVerification)
        print(GlobalData.AddOnsEnabled.isEmailVerified)
        
        if GlobalData.AddOnsEnabled.emailVerification == true  && GlobalData.AddOnsEnabled.isEmailVerified == false {
            sectionCount = 3
        } else {
            if GlobalData.AddOnsEnabled.allowGdpr == true {
                sectionCount = 4
            }
        }
        
        
        reloadSections = { [weak self] (section: Int) in
            self?.accountTableView?.beginUpdates()
            //              self?.accountTableView?.reloadRows(at: [section], with: .fade)
            self?.accountTableView?.reloadSections([section], with: .automatic)
            
            self?.accountTableView?.endUpdates()
        }
        updateButton.SetAccentBackColor()
        updateButton.setTitle("updateInformation".localized, for: .normal)
        accountTableView.tableFooterView = UIView()
        viewModel.reloadTableView = {[weak self] () in
            self?.accountTableView.reloadData()
            
        }
        viewModel.logout = {[weak self] () in
            self?.performSegue(withIdentifier: "home", sender: self)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateClicked(_ sender: Any) {
        self.dismissKeyboard()
        if !viewModel.form.isValid().0 {
            ErrorChecking.ErrorView(message:viewModel.form.isValid().1 + " input value is not valid".localized)
        } else {
            var data = viewModel.form.getFormData()
            print(data)
            if data["password"] as? String !=  data["cPassword"] as? String{
                ErrorChecking.ErrorView(message: "PasswordShouldMatch".localized)
            }else{
                data["name"] = (data["firstname"] as? String ?? "") + " " + (data["lastname"] as? String ?? "")
                viewModel.makeRequest(dict: data, call: WhichApiUse.accountInfo)
            }
        }
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiUse) {
        
        var params = ""
        var verb = RequestType.post
        if AccountDeativationType == "Temporary" {
            verb = .post
            params = GlobalData.apiName.gdprDeactivate
        } else if AccountDeativationType == "Permanent" {
            verb = .post
            params = GlobalData.apiName.gdprDeactivate
        } else if AccountDeativationType == "downloadAllInfo" {
            verb = .get
            params = GlobalData.apiName.dowaloadGdpr
            //dowaloadGdprRequest
        } else if self.AccountDeativationType == "downloadGdprRequest" {
            verb = .post
            params = GlobalData.apiName.dowaloadGdprRequest
        } else {
            if call == WhichApiUse.sendVerificationLink {
                verb = .post
                params = GlobalData.apiName.verifyEmail
            } else {
                verb = .post
                params = GlobalData.apiName.accountInfo
            }
        }
        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: verb, controler: Controllers.accountInfo) { (responseObject: JSON?, error: Error?) in
            
            if (error != nil) {
                print("Error logging you in!")
                
            } else {
                
                if self.AccountDeativationType == "Temporary" {
                    if responseObject!["success"].boolValue {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                        for i in UserDefaults.standard.dictionaryRepresentation() {
                            print(i.key)
                            if i.key != firebaseKey || i.key != "loadLanguage" || i.key != "defaultLanguage"   || i.key != AppLanguageKey   {
                                UserDefaults.standard.removeObject(forKey: i.key)
                                UserDefaults.standard.synchronize()
                            }
                        }
                        UserDefaults.standard.set("yes", forKey: "loadLanguage")
                        UserDefaults.standard.synchronize()
                        self.performSegue(withIdentifier: "backFromAccount", sender: self)
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                } else if self.AccountDeativationType == "Permanent" {
                    if responseObject!["success"].boolValue {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                        for i in UserDefaults.standard.dictionaryRepresentation() {
                            print(i.key)
                            if i.key != firebaseKey || i.key != "loadLanguage" || i.key != "defaultLanguage"   || i.key != AppLanguageKey   {
                                UserDefaults.standard.removeObject(forKey: i.key)
                                UserDefaults.standard.synchronize()
                            }
                        }
                        UserDefaults.standard.set("yes", forKey: "loadLanguage")
                        UserDefaults.standard.synchronize()
                        self.performSegue(withIdentifier: "backFromAccount", sender: self)
                    } else {
                        
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                } else if self.AccountDeativationType == "downloadAllInfo" {
                    if responseObject!["success"].boolValue {
                        self.downloadGdprData = responseObject!
                        if responseObject!["success"].boolValue {
                            self.downloadBtncheck = 1
                            self.accountTableView.reloadData()
                            if responseObject!["downloadUrl"].stringValue != "" {
                                
                                
                                
                                
                                
                                if  let cico = URL(string: responseObject!["downloadUrl"].stringValue) {
                                    
                                    print("The Url is : /(cico)", cico)
                                    
                                    self.pdfCheck(url: responseObject!["downloadUrl"].stringValue, id: "0")
                                    
                                }
                            }
                            ErrorChecking.warningView(message: responseObject!["downloadMessage"].stringValue)
                        } else {
                            self.downloadBtncheck = 1
                            self.accountTableView.reloadData()
                            ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                            
                        }
                    }else {
                        self.downloadBtncheck = 1
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                } else if self.AccountDeativationType == "downloadGdprRequest"{
                    ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                } else {
                    
                    if responseObject!["success"].boolValue {
                        Toast(text: responseObject!["message"].stringValue).show()
                        ToastView.appearance().backgroundColor = .black
                       
                        UserDefaults.standard.synchronize()
                        if self.rowValue != 0 {
                            if let password = dict["password"] as? String , let dic  =  UserDefaults.standard.value(forKey: "loginDict") as? [String: Any] {
                                var dict = dic
                                dict["pwd"] = password
                                UserDefaults.standard.set(dict, forKey: "loginDict")
                                UserDefaults.standard.synchronize()
                            }
                        }
                    }
                    
                    self.delegate?.addressupdated(addressId: "")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    func getMimeType(type:String)-> String{
        switch type {
        case "txt":
            return "text/plain";
        case "htm":
            return "text/html";
        case "html":
            return "text/html";
        case "php":
            return "text/html";
        case "css":
            return "text/css";
        case "js":
            return "application/javascript";
        case "json":
            return "application/json";
        case "xml":
            return "application/xml";
        case "swf":
            return "application/x-shockwave-flash";
        case "flv":
            return "video/x-flv";
        case "png":
            return "image/png";
        case "jpe":
            return "image/jpeg";
        case "jpeg":
            return "image/jpeg";
        case "gif":
            return "image/gif";
        case "bmp":
            return "image/bmp";
        case "ico":
            return "image/vnd.microsoft.icon";
        case "tiff":
            return "image/tiff";
        case "tif":
            return "image/tiff";
        case "svg":
            return "image/svg+xml";
        case "svgz":
            return "image/svg+xml";
        case "zip":
            return "application/zip";
        case "rar":
            return "application/x-rar-compressed";
        case "exe":
            return "application/x-msdownload";
        case "msi":
            return "application/x-msdownload";
        case "mp3":
            return "audio/mpeg";
        case "qt":
            return "video/quicktime";
        case "mov":
            return "video/quicktime";
        case "pdf":
            return "application/pdf";
        case "psd":
            return "image/vnd.adobe.photoshop";
        case "ai":
            return "application/postscript";
        case "eps":
            return "application/postscript";
        case "ps":
            return "application/postscript";
        case "doc":
            return "application/msword";
        case "rtf":
            return "application/rtf";
        case "xls":
            return "application/vnd.ms-excel";
        case "ppt":
            return "application/vnd.ms-powerpoint";
        case "odt":
            return "application/vnd.oasis.opendocument.text";
        case "ods":
            return "application/vnd.oasis.opendocument.spreadsheet";
            
        default:
            return ""
        }
    }
    func pdfCheck(url: String, id: String) {
        // Create destination URL
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as! URL
        print(documentsUrl)
        let destinationFileUrl = documentsUrl.appendingPathComponent(id + "webkul.pdf")
        print(documentsUrl)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destinationFileUrl.path) {
            print("FILE AVAILABLE")
            
            let view = viewController(forStoryboardName: GlobalData.Storyboard.homeStoryboard, forViewControllerName: Controllers.cmsController.rawValue) as! CmsDataViewController
            view.title1 = "Gdpr Downloed Data"
            view.type = "DownloedData"
            let nav  = UINavigationController(rootViewController: view)
            view.urlString = destinationFileUrl.absoluteString
            self.present(nav, animated: true, completion: nil)
        } else {
            //Create URL to the source file you want to download
            let stringUrl = url
            
            
            let fileURL = URL(string: stringUrl)
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    
                    print(tempLocalUrl)
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                        self.move(url: destinationFileUrl)
                    }
                } else {
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any);
                }
            }
            task.resume()
        }
    }
    
    func move(url: URL) {
        let view = viewController(forStoryboardName: GlobalData.Storyboard.homeStoryboard, forViewControllerName: Controllers.cmsController.rawValue) as! CmsDataViewController
        view.title1 = "Gdpr Downloed Data"
        view.type = "DownloedData"
        let nav  = UINavigationController(rootViewController: view)
        view.url = url
        self.present(nav, animated: true, completion: nil)
    }
    var rowValue = 0
}

extension AccountInfoDataViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  viewModel.form.formItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.form.formItems[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.form.formItems[indexPath.section][indexPath.row]
        let cell: UITableViewCell
        if let cellType = viewModel.form.formItems[indexPath.section][indexPath.row].uiProperties.cellType {
            cell = cellType.dequeueCell(for: tableView, at: indexPath)
        } else {
            cell = UITableViewCell()
        }
        viewModel.form.formItems[indexPath.section][indexPath.row].indexPath = indexPath
        if let formUpdatableCell = cell as? FormUpdatable {
            item.indexPath = indexPath
            formUpdatableCell.update(with: item)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerList[section]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


extension AccountInfoDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        if call == WhichApiUse.sendVerificationLink {
            self.makeRequest(dict: [:], call: call)
        } else {
            
            if index == 0 {
                rowValue  = 0
                reloadSections?(1)
            } else {
                rowValue  = 1
                reloadSections?(1)
            }
            
        }
    }
}


