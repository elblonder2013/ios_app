//
//  SearchViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 31/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

@objcMembers
class SearchViewModal: NSObject {

    var items = [SearchViewModalItem]()
    var table = UITableView()
    var delegate: NewViewControllerOpen?

    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = SearchModal(data : jsonData) else {
            return
        }
        items.removeAll()
        if !data.query.isEmpty {
            items.append(SearchViewModalQueryItem(query :data.query))
        }
        if !data.products.isEmpty {
            items.append(SearchViewModalProductsItem(products : data.products))
        }

        if !data.query.isEmpty ||  !data.products.isEmpty {
            completion(true)
        } else {
            completion(false)
        }

    }
}

extension SearchViewModal: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
         return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return items[section].rowCount
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            let item = items[section]

            switch item.type {
                case .queries:
                    return recentSearches.localized
                 case .products:
                    return popularKeywords.localized
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]

        switch item.type {
        case .queries:
            if let item = item as? SearchViewModalQueryItem {
                let cell  = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.query[indexPath.row].value
                return cell
            }
        case .products:
            if let item = item as? SearchViewModalProductsItem {
                let cell  = UITableViewCell(style: .default, reuseIdentifier: "cell")
                cell.textLabel?.text = item.products[indexPath.row].name
                return cell
            }

        }

       return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.section]

        switch item.type {
            case .queries:
                if let item = item as? SearchViewModalQueryItem {
                    delegate?.moveToController(id: "", name: item.query[indexPath.row].value!, dict: JSON.null, Controller: Controllers.search)
                }
             case .products:
                 if let item = item as? SearchViewModalProductsItem {
                     delegate?.moveToController(id: item.products[indexPath.row].templateId!, name: item.products[indexPath.row].name!, dict: JSON.null, Controller: Controllers.product)
                }
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
         if items[section].type == SearchType.queries {
            let view = UITableViewHeaderFooterView()
            view.textLabel?.text = "X Clear Searches"
            view.textLabel?.textColor = GlobalData.Credentials.defaultTextColor
            view.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
             view.textLabel?.backgroundColor = UIColor.clear
            view.backgroundColor = UIColor.clear
            view.tintColor = UIColor.clear
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            tapRecognizer.numberOfTapsRequired = 1
            tapRecognizer.numberOfTouchesRequired = 1
            view.addGestureRecognizer(tapRecognizer)
            return view
        } else {
            return nil
        }
    }

    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.backgroundColor = UIColor.clear
        view.tintColor = UIColor.clear
    }

    func handleTap(gestureRecognizer: UITapGestureRecognizer) {
        UserDefaults.standard.removeObject(forKey: "Queries")
        UserDefaults.standard.synchronize()
        GlobalData.QueryArray.values.removeAll()

        if items[0].type == SearchType.queries {
            items.remove(at: 0)
        }

       table.reloadData()
    }

}

protocol SearchViewModalItem {
    var type: SearchType { get }
    var rowCount: Int { get }
}

enum SearchType {
    case queries
    case products
}

class SearchViewModalProductsItem: SearchViewModalItem {
    var type: SearchType {
        return .products
    }

    var rowCount: Int {
        return products.count
    }

    var products: [Product]

    init(products: [Product] ) {
        self.products = products
    }
}

class SearchViewModalQueryItem: SearchViewModalItem {
    var type: SearchType {
        return .queries
    }

    var rowCount: Int {
        return query.count
    }

    var query: [Query]

    init(query: [Query] ) {
        self.query = query
    }
}
