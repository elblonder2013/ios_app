//
//  ViewController.swift
//  Odoo application
//
//  Created by vipin sahu on 8/30/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Toaster
import RealmSwift
import PermissionScope
import Firebase
import UserNotifications
import Alamofire
import IQKeyboardManagerSwift
import FirebaseMessaging
protocol CheckTouch {
    func touchScr(section:Int,row:Int)
}


@objcMembers
class HomeController: UIViewController, UIScrollViewDelegate {
    var homeViewModelObject : HomeViewModel!
    var launchView: LaunchController?
    @IBOutlet weak var homeTableView: UITableView!
    var refreshControl: UIRefreshControl!
    var selectedIndex:IndexPath?
    var responseObjectData:JSON = ""
    var indexDataValue = ""
    var counterOfLocation = Int()
    var indexGlobalRowOfCollection = ""
    private let kAppGroupName = "group.com.webkul.OdooMarketplace"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data = UserDefaults(suiteName: kAppGroupName)
        data?.set("noDataText".localized, forKey: "noDataText")
        data?.synchronize()
        
        homeViewModelObject = HomeViewModel()
        NotificationCenter.default.addObserver(self, selector: #selector(self.DTouch), name: NSNotification.Name(rawValue: "3DTouch"), object: nil)
      
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        homeViewModelObject.delegate = self
        homeViewModelObject.cameraAction = self
        self.removeTabbarItemsText()
        tabBarController?.tabBar.tintColor = GlobalData.Credentials.accentColor
        tabBarController?.delegate = self
        homeViewModelObject.navBar = self.navigationController
        homeTableView.register(HomeCategoryCell.nib, forCellReuseIdentifier: HomeCategoryCell.identifier)
        homeTableView.register(HomeBannerTableViewCell.nib, forCellReuseIdentifier: HomeBannerTableViewCell.identifier)
        homeTableView.register(HomeProductTableViewCell.nib, forCellReuseIdentifier: HomeProductTableViewCell.identifier)
        
        homeTableView?.dataSource = homeViewModelObject
        homeTableView?.delegate = homeViewModelObject
        homeViewModelObject.tableview = homeTableView
        homeViewModelObject.homeVC = self
        navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        //        navigationController?.navigationBar.isHidden = true
        if GlobalData.Credentials.environment == EnvironmentType.marketplace {
            self.navigationItem.title = "appNameMP".localized
        } else {
            self.navigationItem.title = "appName".localized
        }
        print("hello")
        homeTableView.backgroundColor = GlobalData.Credentials.defaultBackgroundColor
        homeTableView.separatorStyle = .none
        homeTableView.backgroundColor = UIColor.white
        self.automaticallyAdjustsScrollViewInsets = false
        //        self.homeTableView.contentInset = UIEdgeInsets(top: -45, left: 0, bottom: 0, right: 0)
        self.homeTableView.estimatedRowHeight = 85
        self.homeTableView.rowHeight = UITableView.automaticDimension
        self.view.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
        NotificationCenter.default.addObserver(self, selector: #selector(EmailVerifyChanged), name: NSNotification.Name(rawValue: "ProfileChanged"), object: nil)
        
        SVProgressHUD.show(withStatus: "Loading....".localized)
        if GlobalData.AddOnsEnabled.allowGuest || UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
            self.makeRequest(call: WhichApiUse.home)
        } else {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
           let nav  = UINavigationController(rootViewController: view)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }
        
        self.addRefreshController()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func addRefreshController(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:  #selector(self.refresh), for: UIControl.Event.valueChanged)
        homeTableView.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        self.makeRequest(call: WhichApiUse.home)
    }
    
    @IBAction func notificationClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.navigationBar.isHidden = false
        let vc = self.storyboard?.instantiateViewController(withIdentifier: notificationViewControllerIdentifier) as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tabBarController?.tabBar.invalidateIntrinsicContentSize()
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
       self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
       self.navigationController?.navigationBar.isHidden = true
         
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func removeTabbarItemsText() {
        if let items = tabBarController?.tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            }
        }
    }
    
    func makeRequest(call: WhichApiUse) {
        var params = ""
        
        switch call {
        case .splash:
            
            params = GlobalData.apiName.getSplashData
        case .home:
            params = GlobalData.apiName.getHomePage
        default:
            print()
        }
        
      
        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: RequestType.post, controler: Controllers.home  ) { (responseObject: JSON?, error: Error?) in
            SVProgressHUD.show(withStatus: "Loading....".localized)
            if (error != nil) {
                SVProgressHUD.dismiss()
                print("Error logging you in!")
                
            } else {
                switch call {
                case .splash:
                    self.tabBarController?.tabBar.items?[4].selectedImage = UIImage(named: "Icon-Accoun-Fill")?.withRenderingMode(.alwaysOriginal)
                    self.tabBarController?.tabBar.items?[4].image = UIImage(named: "Icon-Account")?.withRenderingMode(.alwaysOriginal)
                    
                    
                    if let data = responseObject {
                        Addons.AddonsChecks(data: data)
                        let data = splashModel(data: data)
                        GlobalData.StoreData.languages = data.languages
                    }
                    
                    
                    
                    
                    if GlobalData.AddOnsEnabled.allowGuest || UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
                        self.makeRequest(call: WhichApiUse.home)
                    } else {
                        print("hello")
                        SVProgressHUD.dismiss()
                        if UserDefaults.standard.value(forKey: loggedInUserKey) == nil {
                            self.tabBarController?.setCartCount(value: "")
                        }
                    }
                    
                case .home:
                    self.refreshControl.endRefreshing()
                    SVProgressHUD.dismiss()
                    
                    self.presentSimplePermissionsAlert()
                    if responseObject!["cartCount"] != JSON.null {
                        self.tabBarController?.setCartCount(value: responseObject!["cartCount"].stringValue)
                    } else {
                        self.tabBarController?.setCartCount(value: "")
                    }
                    CategoryArray = (responseObject?["categories"])!
                    //                    self.startMonitoring()
                    self.responseObjectData = responseObject!
                    self.homeViewModelObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        
                        //                        responseObject!.save()
                        self.homeTableView.reloadDataWithAutoSizingCellWorkAround()
                        if  typeOfShortcut != "" {
                            self.pushForShortcutWhenAppIsKillState(type: typeOfShortcut)
                            typeOfShortcut = ""
                        }
                        //                        reloadDataWithAutoSizingCellWorkAround
                        //                        self.homeTableView.reloadData()
                        SVProgressHUD.dismiss()
                        
                    }
                default:
                    print()
                }
                
            }
        }
    }
    
    func presentSimplePermissionsAlert() {
        let permissionScope = PermissionScope()
        
        // Set up permissions
        permissionScope.addPermission(LocationWhileInUsePermission(),
                                      message: "We use this to track\r\nwhere you live"
        )
        permissionScope.addPermission(NotificationsPermission(notificationCategories: nil),
                                      message: "So that we can send you the latests updates straightaway"
        )
        //        permissionScope.addPermission(CameraPermission(),
        //                                      message: "So that we can capture images from Camera"
        //        )
        
        permissionScope.addPermission(PhotosPermission(),
                                      message: "So that we can access to your Photos from Camera roll"
        )
        
        // Show permission dialog with callbacks
        permissionScope.show({ finished, results in
            print("Permissions state changed, results: \(results)")
            if PermissionScope().statusLocationInUse() == .authorized {
                //                   let data = LocationFetch()
                //                    print(data.country)
            }
            if PermissionScope().statusNotifications() == .authorized {
                if #available(iOS 10.0, *) {
                    UNUserNotificationCenter.current().delegate = self
                } else {
                    // Fallback on earlier versions
                }
                Messaging.messaging().delegate = self
            }
            
        },
                             cancelled: { results in
                                print("User closed the permission alert, results: \(results)")
                                
                                if PermissionScope().statusLocationInUse() == .authorized {
                                    //                    let data = LocationFetch()
                                    //                    print(data.country)
                                }
                                
                                if PermissionScope().statusNotifications() == .authorized {
                                    if #available(iOS 10.0, *) {
                                        UNUserNotificationCenter.current().delegate = self
                                    } else {
                                        // Fallback on earlier versions
                                    }
                                    Messaging.messaging().delegate = self
                                }
        }
        )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    func EmailVerifyChanged(notification: Notification) {
        
        if GlobalData.AddOnsEnabled.isEmailVerified  ||  UserDefaults.standard.value(forKey: loggedInUserKey) == nil {
            
            self.tabBarController?.tabBar.items?[4].selectedImage = UIImage(named: "Icon-Accoun-Fill")?.withRenderingMode(.alwaysOriginal)
            self.tabBarController?.tabBar.items?[4].image = UIImage(named: "Icon-Account")?.withRenderingMode(.alwaysOriginal)
        } else {
            self.tabBarController?.tabBar.items?[4].selectedImage = UIImage(named: "Icon-Account-Fill-Verify")?.withRenderingMode(.alwaysOriginal)
            self.tabBarController?.tabBar.items?[4].image = UIImage(named: "Icon-Account-Verify")?.withRenderingMode(.alwaysOriginal)
        }
        
    }
    
}

extension HomeController: NewViewControllerOpen {
    
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        
        switch Controller {
            
        case .search:
            self.tabBarController?.selectedIndex = 1
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "searchController") as? SearchController
            //            let navSearchViewController: UINavigationController = UINavigationController(rootViewController: vc!)
            //            self.present(navSearchViewController, animated: false, completion: nil)
            
        case .notifications:
            self.navigationController?.navigationBar.isHidden = false
            let vc = self.storyboard?.instantiateViewController(withIdentifier: notificationViewControllerIdentifier) as! NotificationViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .homeSliders:
            self.navigationController?.navigationBar.isHidden = false
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
            let outputString = String(id.dropFirst())
            view.categoryId = outputString
            view.categoryName = name
            view.parentController = Controller
            self.navigationController?.pushViewController(view, animated: true)
            
        case .productCategory:
            self.navigationController?.navigationBar.isHidden = false
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
            view.categoryId = id
            view.categoryName = name
            view.parentController = Controller
            self.navigationController?.pushViewController(view, animated: true)
           
        case .product:
            self.navigationController?.navigationBar.isHidden = false
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productDataViewControllerIdentifier) as! ProductDataViewController
            view.templateId = id
            view.name = name
            self.navigationController?.pushViewController(view, animated: true)
        case .custom:
            print("idid",id)
            self.navigationController?.navigationBar.isHidden = false
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
            view.categoryId = id
            view.categoryName = name
            view.parentController = Controller
            self.navigationController?.pushViewController(view, animated: true)
            
            
        default:
            print()
        }
        
    }
    
    //    func searchOpen() {
    //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "searchController") as? SearchController
    //        let navSearchViewController: UINavigationController = UINavigationController(rootViewController: vc!)
    //        self.present(navSearchViewController, animated: false, completion: nil)
    //    }
}

extension HomeController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController,
                          didSelect viewController: UIViewController) {
        if let vc = viewController as? UINavigationController {
            //            if tabBarController.selectedIndex == 4 {
            vc.popViewController(animated: false)
            //            }
        }
    }
}

extension HomeController: CameraAction, SuggestionDataHandlerDelegate {
    
    func openCameraOptions(sender: UIButton) {
        let alert = UIAlertController(title: "chooseaction".localized, message: nil, preferredStyle: .actionSheet)
        let TextDetection = UIAlertAction(title: "detecttext".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.navigationController?.navigationBar.isHidden = false
            if let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: Controllers.Detector.rawValue) as? DetectorViewController {
                view.detectorType = .text
                view.delegate = self
                view.title = "detecttext".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        let ImageDetection = UIAlertAction(title:  "detectimage".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.navigationController?.navigationBar.isHidden = false
            if let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: Controllers.Detector.rawValue) as? DetectorViewController {
                view.detectorType = .image
                view.delegate = self
                view.title = "detectimage".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        alert.addAction(TextDetection)
        alert.addAction(ImageDetection)
        let cancel = UIAlertAction(title: "cancel".localized, style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(cancel)
        alert.popoverPresentationController?.sourceView = sender
        alert.popoverPresentationController?.sourceRect = CGRect(x: sender.frame.size.width/2, y: sender.frame.size.height, width : 1.0, height : 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func suggestedData(data: String) {
        let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
        view.categoryName = data
        view.parentController = Controllers.search
        self.navigationController?.pushViewController(view, animated: true)
    }
}
extension HomeController:UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        //(previewingContext.sourceView as? UICollectionView)?.tag
        
        print(location)
        
        var heightOffset = UIApplication.shared.statusBarFrame.height
        let newLocation = CGPoint(x: location.x, y: location.y - heightOffset )
        var checxk = 0
        var index = 0
        for cells in homeTableView.visibleCells {
            
            if let cell = cells as? HomeProductTableViewCell {
                for cellsq in cell.homeProductCollection.visibleCells {
                    if cellsq.frame.contains(newLocation) && checxk == 0 {
                        checxk = 1
                        index = (cell.homeProductCollection.indexPathForItem(at: newLocation)?.row)!
                        indexGlobalRowOfCollection = "\(index)"
                    }
                    
                }
                
            }
          }
        //print((previewingContext.sourceView as? UITableView)?.indexPathForRow(at: location)?.section,"section")
        var indexData = (previewingContext.sourceView as? UITableView)?.indexPathForRow(at: location)?.section
        if indexData != nil{
            indexDataValue = "\(indexData!)"
        }
 
        print(index,"row")
        let story = UIStoryboard(name: "Home", bundle: nil)
        let touchDetailVC = story.instantiateViewController(withIdentifier: "TouchOfProductInViewController")as! TouchOfProductInViewController
        touchDetailVC.dataInJson = (responseObjectData["productSliders"][((previewingContext.sourceView as? UICollectionView)?.tag)! - 2]["products"][Int(indexGlobalRowOfCollection)!])
        touchDetailVC.delegate = self
        if homeViewModelObject.homePageWishListArray.contains((responseObjectData["productSliders"][((previewingContext.sourceView as? UICollectionView)?.tag)! - 2]["products"][Int(indexGlobalRowOfCollection)!]["productId"].stringValue)) {
        touchDetailVC.wishListTrue = true
        }
        touchDetailVC.delegateOfWishList = self
        touchDetailVC.preferredContentSize = CGSize(width: SCREEN_WIDTH, height: screenHeight - 300)
        var previewFrame = view.frame
        touchDetailVC.controller = Controllers.home
        previewFrame.origin.y += heightOffset
        previewingContext.sourceRect = previewFrame
        if  touchDetailVC.dataInJson.count > 0 {
            return touchDetailVC
        } else {
            return nil
        }
        
    }
    
    func pushForShortcutWhenAppIsKillState(type:String) {
        let value  =  type
        
        switch value {
        case "Search":
            self.tabBarController?.selectedIndex = 1
        case "MarketPlace": break
            
           
        case "Profile":
            self.tabBarController?.selectedIndex = 4
        case "Notification":
            
            self.navigationController?.navigationBar.isHidden = false
            self.tabBarController?.selectedIndex = 0
            let vc = self.storyboard?.instantiateViewController(withIdentifier: notificationViewControllerIdentifier) as! NotificationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case "Cart":
            self.tabBarController?.selectedIndex = 3
        default:
            break
        }
        
        
        }
 
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        
    }
}
extension HomeController {
    func DTouch (_ note: Notification) {
        let value  =  note.object as? String
        
        switch value {
        case "Search"?:
            self.tabBarController?.selectedIndex = 1
        case "MarketPlace"?: break
            
        case "Profile"?:
            self.tabBarController?.selectedIndex = 4
        case "Notification"?:
            
            self.navigationController?.navigationBar.isHidden = false
             self.tabBarController?.selectedIndex = 0
            let vc = self.storyboard?.instantiateViewController(withIdentifier: notificationViewControllerIdentifier) as! NotificationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case "Cart"?:
            self.tabBarController?.selectedIndex = 3
        default:
            break
        }
        
        
    }
}
extension HomeController:cartCount,wishListArrayUpdate {
    func wishListArrayLoaclyUpdate(id: String) {
        if homeViewModelObject.homePageWishListArray.contains(id) {
            
        } else {
            homeViewModelObject.homePageWishListArray.append(id)
        }
        
    }
    
    
    func cartValue(count: String) {
        indexDataValue = ""
        indexGlobalRowOfCollection = ""
        self.tabBarController?.setCartCount(value: count)
    }
    func pushToBuyNow() {
        indexDataValue = ""
        indexGlobalRowOfCollection = ""
        let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "checkoutDataViewController") as! CheckoutDataViewController
        let nav  = UINavigationController(rootViewController: view)
        self.present(nav, animated: true, completion: nil)
    }
    func homeRefreshTouch() {
        indexDataValue = ""
        indexGlobalRowOfCollection = ""
}
}
