//
//  AddressDataViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddressDataViewController: UIViewController {

    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var newAddressButton: UIButton!
    var emptyView: EmptyView?

    fileprivate var addressViewModalObject  = AddressViewModal()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        newAddressButton.backgroundColor = GlobalData.Credentials.accentColor
        newAddressButton.setTitle(addNewAddress.localized, for: .normal)
        self.navigationItem.title = addressBook.localized
        addressTableView.delegate = addressViewModalObject
        addressTableView.dataSource = addressViewModalObject
        addressTableView.tableFooterView = UIView()
        addressViewModalObject.delegate = self
//        addressTableView.separatorStyle = .none
        addressTableView.estimatedRowHeight = 100
        addressTableView.rowHeight = UITableView.automaticDimension
        addressTableView.showsVerticalScrollIndicator = false
        // Do any additional setup after loading the view.
        self.addressTableView.isHidden = true
        self.newAddressButton.isHidden = true
        self.makeRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeRequest() {
        let params = GlobalData.apiName.getAddress
        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: RequestType.post, controler: Controllers.address ) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                self.addressViewModalObject.getValue(jsonData: responseObject!) {
                 (data: Bool) in
                    print(data)
                    if data {
                        self.emptyView?.isHidden = true
                        self.addressTableView.isHidden = false
                        self.newAddressButton.isHidden = false
                        self.addressTableView.reloadData()
                    } else {
                        print("sfjjs")
                        self.emptyView?.isHidden = false
                        self.addressTableView.isHidden = true
                        self.newAddressButton.isHidden = true
                    }
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = addressEmpty.localized
            emptyView?.button.setTitle(addNewAddress.localized, for: .normal)
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-address")
            emptyView?.delegate = self
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func newAddressClicked(_ sender: UIButton) {
        let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
        view.delegate = self
        self.navigationController?.pushViewController(view, animated: true)
    }

}

extension AddressDataViewController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
        view.addressId = id
        if name == "yes" {
          view.delete = true
        } else {
          view.delete = false
        }
        view.delegate = self
        self.navigationController?.pushViewController(view, animated: true)
    }

}

extension AddressDataViewController: AddressUpdate {
    func addressupdated(addressId: String) {
         self.makeRequest()
    }

}

extension AddressDataViewController: EmptyDelegate {
    func buttonPressed() {
        let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
        view.delegate = self
        self.navigationController?.pushViewController(view, animated: true)
    }
}
