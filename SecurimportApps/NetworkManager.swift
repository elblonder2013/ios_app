//
//  NetworkManager.swift
//  swift-mvvm
//
//  Created by Taylor Guidon on 11/30/16.
//  Copyright © 2016 ISL. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire
import Toaster
import Reachability
import Realm
import RealmSwift

protocol NetworkManagerDelegate {
    func dataReceived(data: JSON?, error: NSError?)
}

typealias ServiceResponse = (JSON?, Error?) -> Void

enum RequestType: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

class NetworkManager: NSObject {
    var baseURLString: String = ""
//    var baseURLWithAPIKeyString: String = "" 
    var delegate: NetworkManagerDelegate?

//    init(apiKey: String) {
//        self.baseURLWithAPIKeyString = "\(self.baseURLString)\(apiKey)/"
//        
//        super.init()
//    }

    static let shared = NetworkManager(baseURL: GlobalData.Credentials.BASEURL)
    
    private init(baseURL:String) {
      baseURLString = baseURL
         super.init()
    }
    
    func fetchData(params: String, login: Bool, view: UIView?, dict: [String: Any], verbs: RequestType, controler: Controllers, _ completion: @escaping ServiceResponse) {
        if self.checkNetworkStatus() {
            SVProgressHUD.show(withStatus: "Loading....".localized)
            view?.isUserInteractionEnabled = false
            var baseURLWithAPIKeyString = "\(self.baseURLString)\(params)"

            if(UserDefaults.standard.value(forKey: "language") != nil) {
                baseURLWithAPIKeyString += "&lang_code="
                baseURLWithAPIKeyString += UserDefaults.standard.value(forKey: "language") as! String
            }

            if(UserDefaults.standard.value(forKey: "currency") != nil) {
                baseURLWithAPIKeyString += "&currency_code="
                baseURLWithAPIKeyString += UserDefaults.standard.value(forKey: "currency") as! String
            }
             let manager = Alamofire.SessionManager.default
            if !baseURLWithAPIKeyString.contains(GlobalData.apiName.placeOrder) {
                manager.session.configuration.timeoutIntervalForRequest = 120
            }

            var requestURL: String = baseURLWithAPIKeyString
            requestURL = requestURL.removeWhitespace()
            requestURL = requestURL.stringByRemovingEmoji()
            print(requestURL)

            var request1 = URLRequest(url: URL(string : requestURL)!)
           // print(verbs.rawValue)
            request1.httpMethod = verbs.rawValue
            request1.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            let encodedData = GlobalData.Credentials.BASEDATA.data(using: String.Encoding.utf8)!
            let base64String = encodedData.base64EncodedString()
            request1.addValue("application/text", forHTTPHeaderField: "Content-Type")
             print(base64String)
            request1.setValue("Basic \(base64String)", forHTTPHeaderField: "Authorization")

            if verbs == .put {
              let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                request1.httpBody = jsonData
            }

            if verbs == .post && !dict.isEmpty && !baseURLWithAPIKeyString.contains("login") {
                let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                request1.httpBody = jsonData
                let jsonSortString: String = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                 print(jsonSortString)
              print(dict)
            }

            if UserDefaults.standard.value(forKey: loggedInUserKey) != nil {
                if let dic  =  UserDefaults.standard.value(forKey: "loginDict") {
                    let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                    let loginbase64String = jsonData.base64EncodedString()
                    request1.setValue(loginbase64String, forHTTPHeaderField: "Login")
                   print(loginbase64String)

                }
                
                if let dic = UserDefaults.standard.value(forKey: "socialDict") {
                    let jsonData = try! JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
                    let loginbase64String = jsonData.base64EncodedString()
                    request1.setValue(loginbase64String, forHTTPHeaderField: "SocialLogin")
                     print(loginbase64String)
                    
                }
            }

            if login == true {
                let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                let loginbase64String = jsonData.base64EncodedString()
                request1.setValue(loginbase64String, forHTTPHeaderField: "Login")
                print(loginbase64String)
            }
            
            if let code = UserDefaults.standard.value(forKey: "defaultLanguage") as? String {
                request1.setValue(code, forHTTPHeaderField: "lang")
            }
            
          //  print(request1.allHTTPHeaderFields ?? "")

            manager.request(request1).responseJSON { (response) in
                switch response.result {
                case .success:
                    view?.isUserInteractionEnabled = true
                    SVProgressHUD.dismiss()
                    self.storeData(controller: controler, data: response.data!)
                    let jsonData  = try! JSON.init(data: response.data!)
                       print(jsonData)
                      GlobalData.AddOnsEnabled.productsPriceAvailable = jsonData["productsPriceAvailable"].boolValue
                    completion(jsonData, nil)

                case .failure(let error):
                    view?.isUserInteractionEnabled = true
                    SVProgressHUD.dismiss()
                    if error._code == NSURLErrorTimedOut {
                        //HANDLE TIMEOUT HERE

                    } else {
                        print(error)
                        print(response.description)
                        print(response.response?.statusCode)
//                        let jsonData  = try! JSON.init(data: response.data!)
//                        print(jsonData)
                        DispatchQueue.main.async {
                            if controler != .search {
                            ErrorChecking.warningView(message: "Some error occur,try again!!")
                            }
                        }
                    }
                    completion( nil, error as NSError)
                }
            }
        } else {
            }
    }

    static let reachability = Reachability()!

    func checkNetworkStatus() -> Bool {
      if NetworkManager.reachability.isReachable {
            return true
      } else {
            return false
        }
    }

    func storeData (controller: Controllers, data: Data) {
        print("controller", controller)
         let dataString = String(data: data, encoding: String.Encoding.utf8)

        let item = Item()
        item.data = dataString!
        item.className = "controller"

    }
}
/*
 class NetworkManager {
 
 // MARK: - Properties
 
 static let shared = NetworkManager(baseURL: API.baseURL)
 
 // MARK: -
 
 let baseURL: URL
 
 // Initialization
 
 private init(baseURL: URL) {
 self.baseURL = baseURL
 }
 
 }
 */
