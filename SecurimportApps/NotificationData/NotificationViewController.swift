//
//  NotificationViewController.swift
//  Odoo application
//
//  Created by bhavuk.chawla on 17/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON


class NotificationViewController: UIViewController {
    var emptyView: EmptyView?
    fileprivate let notificationViewModalObject = NotificationViewModal()
    private let kAppGroupName = "group.com.webkul.OdooMarketplace"
    
    @IBOutlet weak var notificationTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        self.navigationItem.title = notifications.localized
        // Do any additional setup after loading the view.
        notificationTableView.delegate = notificationViewModalObject
        notificationTableView.dataSource = notificationViewModalObject
        notificationTableView.register(NotificationTableViewCell.nib, forCellReuseIdentifier: NotificationTableViewCell.identifier)
        notificationTableView.tableFooterView = UIView()
        notificationTableView.separatorStyle = .none
        notificationTableView.estimatedRowHeight = 100
        notificationTableView.rowHeight = UITableView.automaticDimension
        notificationTableView.showsVerticalScrollIndicator = false
        self.makeRequest()
        
//        let dict: JSON = ["period": "test","body": "kdfsjvkldfkldfsjfhgjksfdhghdfsjghdfhgjhsdfghjkkhjdfsgjdkfgjkhdfsgjh", "title": "test", "banner": "http://mpdemo.webkul.com/web/image/mobikul.banners/1/image/375x187"]
//        let dict1: JSON = ["all_notification_messages": [dict, dict, dict]]
//        
//        let data = UserDefaults(suiteName: self.kAppGroupName)
//        data?.set(dict1.rawString(), forKey: "notificationDataController")
//        data?.synchronize()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func makeRequest() {
        let params = GlobalData.apiName.notifications
        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: RequestType.post, controler: Controllers.notifications ) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                self.notificationViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    print(data)
                    if data {

                        let data = UserDefaults(suiteName: self.kAppGroupName)
                        data?.set(responseObject!.rawString(), forKey: "notificationDataController")
                        data?.synchronize()
                        
                        self.emptyView?.isHidden = true
                        self.notificationTableView.isHidden = false
                        self.notificationTableView.reloadData()
                    } else {
                        self.emptyView?.isHidden = false
                        self.notificationTableView.isHidden = true
                    }
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
         if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = notificationEmpty.localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-notification")
            emptyView?.button.isHidden = true
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NotificationViewController: PassData {
    func passData(id: String, name: String, dict: [String : Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        
    }
}
