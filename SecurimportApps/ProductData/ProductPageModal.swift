//
//  ProductPageModal.swift
//  Odoo iOS
//
//  Created by Bhavuk on 03/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ProductPageModal {
    var productImages = [String]()

    var productData: ProductPageData?
    var variantsArray = [Variants]()
     var combArray = [Combinations]()
    var wishlistExists = false
     var isSellerProduct = false
    var sellerProductData: SellerProductData!
    var idArray = [String]()

    init?(data: JSON) {

      Addons.AddonsChecks(data: data)

        if let array = data["images"].array {

            self.productImages = array.map { $0.stringValue }
        }
        self.productData = ProductPageData(data: data)
        if let array = data["attributes"].array {
            if array.count > 0 {
                self.variantsArray = array.map { Variants(data : $0) }
            }
        }
        if let array = data["variants"].array {
            if array.count > 0 {
                self.combArray = array.map { Combinations(data : $0) }
            }
        }
        
        if self.combArray.count > 0 {
            
            let array = self.combArray.filter({ $0.addedToWishlist == true })
            self.idArray = array.compactMap({$0.productId})
            print("self.idArray",self.idArray)
        }

        
        if  data["addedToWishlist"].bool != nil {
            self.wishlistExists = data["addedToWishlist"].boolValue
        }

        if  data["seller_info"] != JSON.null {
           isSellerProduct = true
            sellerProductData = SellerProductData(data: data["seller_info"])
        } else {
            isSellerProduct = false
        }
    }

}

struct  Combinations {
    var priceReduce: String?
    var productId: String?
    var priceUnit: String?
    var imageUrl: String
    var addedToWishlist: Bool!
    var varCombArray = [VariantsCombination]()

    init(data: JSON) {
        priceReduce = data["priceReduce"].stringValue
        productId = data["productId"].stringValue
        priceUnit = data["priceUnit"].stringValue
          imageUrl = data["images"][0].stringValue
        addedToWishlist = data["addedToWishlist"].boolValue
        if let array = data["combinations"].array {
            self.varCombArray = array.map { VariantsCombination(data : $0) }
        }

    }
}

struct VariantsCombination {
    var valueId: String?
    var attributeId: String?
    init(data: JSON) {
        valueId = data["valueId"].stringValue
        attributeId = data["attributeId"].stringValue
    }
}

struct Variants {
    var type: String?
    var name: String?
    var attributeId: String?
    var varArray = [VariantsValues]()

    init(data: JSON) {
        type = data["type"].stringValue
        name = data["name"].stringValue
        attributeId = data["attributeId"].stringValue
        if let array = data["values"].array {
            self.varArray = array.map { VariantsValues(data : $0) }
        }

    }
}

struct VariantsValues {
    var valueId: String?
    var htmlCode: String?
    var  name: String?

    init(data: JSON) {
         valueId = data["valueId"].stringValue
         htmlCode = data["htmlCode"].stringValue
         name = data["name"].stringValue

    }

}

struct ProductPageData {
    var name: String?
    var totalReviews: String?
    var rating: String?
    var price: String?
    var reducePrice: String?
    var description: String?
    var productId : String!
    var stockLevel : String!
    var brandName : String!
    var brandImage : String!
    var reftext : String!
    var stockMsg : String
    var isStock : Bool!
    var userCustomerGrpId : Bool!
    var productUrl : String!
    var fileAttachments  = [FileAttachments]()
    init(data: JSON) {
        self.name = data["name"].stringValue
        self.totalReviews = data["total_review"].stringValue
        self.rating = data["avg_rating"].stringValue
        self.price = GlobalData.AddOnsEnabled.productsPriceAvailable ? data["priceUnit"].stringValue : ""
        self.reducePrice = data["priceReduce"].stringValue
        self.description = data["description"].stringValue
        self.productId = data["productId"].stringValue
        self.reftext = (data["refCode"].stringValue == "" ? "" : "Ref: \(data["refCode"].stringValue)  ") + (data["modelBarcode"].stringValue == "" ? "" : "Model: \(data["modelBarcode"].stringValue)  ")
        self.brandName = data["brand"]["name"].stringValue
        self.brandImage = data["brand"]["image"].stringValue
        self.stockMsg = data["stockMsg"].stringValue
         self.stockLevel = data["stockLevel"].stringValue
        self.productUrl = data["productUrl"].stringValue
        self.isStock = data["isStock"].boolValue
        self.userCustomerGrpId = data["userCustomerGrpId"].boolValue
        fileAttachments = data["fileAttachments"].arrayValue.map{FileAttachments(data: $0)}
    }
}
struct FileAttachments{
    var url: String
    var name: String
    init(data: JSON) {
        self.url = data["url"].stringValue
        self.name = data["name"].stringValue
       
    }
}
struct SellerProductData {
    var total_reviews: String!
    var seller_profile_image: String!
    var message: String!
    var seller_name: String!
    var marketplace_seller_id: String!
    var average_rating: String!
    init(data: JSON) {
         self.total_reviews = data["total_reviews"].stringValue
         self.seller_profile_image = data["seller_profile_image"].stringValue
         self.message = data["message"].stringValue
         self.seller_name = data["seller_name"].stringValue
         self.marketplace_seller_id = data["marketplace_seller_id"].stringValue
         self.average_rating = data["average_rating"].stringValue
    }

}
