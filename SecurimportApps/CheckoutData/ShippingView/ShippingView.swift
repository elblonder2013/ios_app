//
//  ShippingView.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 20/04/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit
import Toaster
import SwiftyJSON

class ShippingView: UIView {
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var shippingTableView: UITableView!
    
    var delegate: PassData?
    var shippingId: String?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shippingTableView.register(ShippingMethodsTableViewCell.nib, forCellReuseIdentifier: ShippingMethodsTableViewCell.identifier)
        shippingTableView.delegate = self
        shippingTableView.delegate = self
        shippingTableView.estimatedRowHeight = 100
        shippingTableView.rowHeight = UITableView.automaticDimension
        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.backgroundColor = GlobalData.Credentials.accentColor
    }
    
    @IBAction func continuePressed(_ sender: UIButton) {
        if let id = shippingId {
            delegate?.passData(id: id, name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.getPayment)
        } else {
            ErrorChecking.warningView(message: "selectShippingMethod".localized)
        }
    }
    
    var shippingMethods = [ShippingData]()
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = ShippingModal(data : jsonData) else {
            return
        }
        
        if !data.shippingMethods.isEmpty {
            shippingMethods = data.shippingMethods
            completion(true)
            shippingTableView.reloadData()
        } else {
            completion(false)
        }
        
    }
}

extension ShippingView: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shippingMethods.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: ShippingMethodsTableViewCell.identifier) as! ShippingMethodsTableViewCell
        cell.selectionStyle = .none
        if let id = shippingId {
            cell.shippingId = id
        }
        cell.item = shippingMethods[indexPath.row]
        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "shippingMethods".localized
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        shippingId = shippingMethods[indexPath.row].id
        self.shippingTableView.reloadData()
    }

}

struct ShippingModal {
    var shippingMethods = [ShippingData]()
    init?(data: JSON) {
        if let array  = data["ShippingMethods"].array {
            if array.count > 0 {
                self.shippingMethods = array.map { ShippingData(data: $0) }
            }
        }
    }
}

struct ShippingData {
    var id: String!
    var description: String!
    var name: String!
    var price: String!
    init(data: JSON) {
        self.id = data["id"].stringValue
        self.description = data["description"].stringValue
        self.name = data["name"].stringValue
        self.price = data["price"].string ?? data["total"].stringValue
    }
}
