//
//  HomeModel.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct HomeModel {
    var banners = [BannerSet]()
    var categories = [CategorySet]()
    var mainProduct = [MainProduct]()

    init?(data: JSON) {
        Addons.AddonsChecks(data: data)

        if data["is_seller"].boolValue {
            GlobalData.AddOnsEnabled.is_seller = true
        } else {
            GlobalData.AddOnsEnabled.is_seller = false
        }

        if let array  = data["featuredCategories"].array {
            self.categories = array.map { CategorySet(json: $0) }
        }
        GlobalData.AddOnsEnabled.allowGdpr = data["TermsAndConditions"].boolValue
        if let array  = data["bannerImages"].array {
            self.banners = array.map { BannerSet(json: $0) }
        }
//        print(data["productSliders"][0]["products"][0]["name"])

        if let data  = data["productSliders"].array {
            self.mainProduct = data.map { MainProduct( json: $0) }
        }

    }

}

struct CategorySet {
    var categoryName: String?
    var CategoryUrl: String?
    var CategoryId: String?
    var subCategories : JSON?
    init(json: JSON) {
        self.categoryName = json["categoryName"].stringValue
        self.CategoryUrl = json["url"].stringValue
        self.CategoryId = json["categoryId"].stringValue
        self.subCategories = json["children"]
    }
}

struct BannerSet {
    var pictureUrl: String?
    var bannerId: String?
    var bannerName: String?
    var bannerType: String?
    var domain: String!
    init(json: JSON) {
        self.pictureUrl = json["url"].stringValue
        self.bannerId = json["id"].stringValue
        self.bannerName = json["bannerName"].stringValue
        self.bannerType = json["bannerType"].stringValue
        self.domain = json["domain"].stringValue
    }

}

struct MainProduct {
    var HeadingName: String?
    var sliderUrl: String?
     var products = [Product]()
    var slider_mode: String?
    var colorCode: String?
     init(json: JSON) {
        self.products = json["products"].arrayValue.map {
            Product(json: $0)
        }
        self.HeadingName = json["title"].stringValue
        self.sliderUrl = json["url"].stringValue
        self.slider_mode = json["slider_mode"].stringValue
        self.colorCode = json["color"].stringValue
    }

}

struct Product {
    var name: String?
    var pictureUrl: String?
    var price: String?
    var productId: String?
    var qty: String?
    var totalPrice: String?
    var lineId: String?
    var wishlistId: String?
    var templateId: String?
    var priceUnit: String?
    var priceTotal: String?
    var reducePrice: String?
    var isStock: Bool?
    var modelBarcode:String!
    var refCode: String!
    var stockLevel : String!
    var shortDescription:NSAttributedString!
    var isCustomizableProduct : Bool!
    var userCustomerGrpId : Bool!
    init(json: JSON) {
        self.stockLevel = json["stockLevel"].stringValue
         self.isStock = json["isStock"].boolValue
        self.userCustomerGrpId = json["userCustomerGrpId"].boolValue
        self.isCustomizableProduct = json["isCustomizableProduct"].boolValue
        self.name = json["name"].stringValue
        self.pictureUrl = json["thumbNail"].stringValue
        self.price = GlobalData.AddOnsEnabled.productsPriceAvailable ? json["priceUnit"].stringValue : ""
        self.productId =  json["productId"].stringValue
        self.qty = json["qty"].stringValue
        self.totalPrice =  json["total"].stringValue
        self.lineId = json["lineId"].stringValue
        self.wishlistId = json["id"].stringValue
        self.templateId = json["templateId"].stringValue
        self.priceUnit = json["price_unit"].stringValue
        self.priceTotal = json["price_total"].stringValue
        self.reducePrice = GlobalData.AddOnsEnabled.productsPriceAvailable ? json["priceReduce"].stringValue : ""
        self.refCode = json["refCode"].stringValue != "" ? "Ref: \(json["refCode"].stringValue)" : ""
        self.modelBarcode = json["modelBarcode"].stringValue != "" ? "Model: \(json["modelBarcode"].stringValue)" : ""
        self.shortDescription = json["shortDescription"].stringValue.html2AttributedString?.trimWhiteSpace()
    }
}


struct splashModel {
    var languages = [AllLanguages]()
    var defaultLanguage : AllLanguages!
    init(data: JSON) {
        if let array  = data["allLanguages"].array {
            self.languages = array.map { AllLanguages(data: $0) }
            defaultLanguage = AllLanguages(data: data["defaultLanguage"])
            if  (UserDefaults.standard.value(forKey: "defaultLanguage") == nil) {
                GlobalData.StoreData.defaultLanguage = defaultLanguage.languageCode
                UserDefaults.standard.set(GlobalData.StoreData.defaultLanguage, forKey: "defaultLanguage")
                UserDefaults.standard.synchronize()
            }
            
            
        }
    }
}

struct AllLanguages {
    var languageCode : String!
    var languageName : String!
    init(data : JSON) {
        languageCode = data[0].stringValue
        languageName = data[1].stringValue
    }
    
}
