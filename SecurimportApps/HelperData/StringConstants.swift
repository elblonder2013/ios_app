//
//  StringConstants.swift
//  Odoo application
//
//  Created by vipin sahu on 9/7/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import UIKit


// Empty states

let cartEmpty = "cartEmpty";
let searchEmpty = "searchEmpty"
let notificationEmpty = "notificationEmpty"
let addressEmpty = "addressEmpty"
let orderEmpty = "orderEmpty"
let productEmpty = "productEmpty"


let categories = "categories"
let searchProducts = "searchProducts"
let appName = "appName"
let browseCategories = "browseCategories"

let viewAll = "viewAll"




// Profile Data

let profile = "profile";
let signIn = "signIn";
let shareApp = "shareApp";
let rateUs = "rateUs";
let dashboard = "dashboard";
let accountInfo = "accountInfo";
let addressBook = "addressBook";
let allOrder = "allOrder";
let myWishlist = "myWishlist";
let signInSignUp = "signInSignUp";



// Sign in Data

let emailAddress  = "emailAddress";
let login = "login";
let wantsignUp = "wantsignUp";
let password = "password";
let forgotPassword = "forgotPassword";
let loginFailed = "loginFailed"

// Notification

let notifications = "notifications";


// order History Data

let order = "order";
let placedOn = "placedOn";
let shipTo = "shipTo";
let orderTotal = "orderTotal";
let viewOrder = "viewOrder";
let reorder = "reorder";

// Cart Data

let cart = "cart";
let price = "price";
let qty = "qty";
let subtotal = "subtotal";
let total = "total";
let tax = "tax";
let proceedCheckout = "proceedCheckout";


// Address data

let addNewAddress = "addNewAddress"
let billingAddress = "billingAddress";
let shippingAddress = "shippingAddress";


// Search Data

let recentSearches = "recentSearches";
let popularKeywords = "popularKeywords";


// Dashboard

let recentOrders = "recentOrders";
let accountInformation  = "accountInformation";


// Acouninfo

let yourName = "yourName";
let changePassword = "changePassword";
let updateInformation = "updateInformation";
let emailVerificationHeading = "emailVerificationHeading";


// new Address

let contactAddress = "contactAddress";
let phone = "phone";
let address = "address";
let streetAddress = "streetAddress";
let city = "city";
let state = "state";
let zip1 = "zip";
let country = "country";
let saveAddress = "saveAddress";



