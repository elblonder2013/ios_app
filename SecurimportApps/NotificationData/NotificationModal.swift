//
//  NotificationModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct NotificationModal {

   var notification = [NotificationData]()
     init?(data: JSON) {
        Addons.AddonsChecks(data: data)
        if let notifications = data["all_notification_messages"].array {
            if notifications.count > 0 {
                self.notification = notifications.map {
                    NotificationData(data: $0)!
                }
            }
        }
    }
}

struct NotificationData {

    var bannerUrl: String?
    var title: String?
    var body: String?
    var time: String?
    init?(data: JSON) {
        self.time = data["period"].stringValue
        self.body = data["body"].stringValue
        self.title = data["title"].stringValue
        self.bannerUrl = data["banner"].stringValue
    }
}
