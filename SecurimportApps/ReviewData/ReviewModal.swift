//
//  ReviewModal.swift
//  Odoo iOS
//
//  Created by Bhavuk on 04/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ReviewModal {

    var reviews = [ReviewData]()
    init?(data: JSON) {
        if let array  = data["product_reviews"].array {
            self.reviews = array.map { ReviewData(data: $0) }
        }

    }
}

struct ReviewData {
    var rating: String?
     var date: String?
     var title: String?
     var id: String?
     var email: String?
     var name: String?
    var msg: String?
    var likes: String!
    var dislikes: String!
    var like: Int!
    var dislike: Int!
    init(data: JSON) {
        rating = data["rating"].stringValue
        date = data["create_date"].stringValue
        title = data["title"].stringValue
        id = data["id"].stringValue
        email = data["email"].stringValue
        name = data["customer"].stringValue
        msg = data["msg"].stringValue
        likes = data["likes"].stringValue
        dislikes = data["dislikes"].stringValue
        like = data["likes"].intValue
        dislike = data["dislikes"].intValue
        
    }
}
