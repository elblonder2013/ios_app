//
//  SearchController.swift
//  Odoo application
//
//  Created by vipin sahu on 9/5/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire
import IQKeyboardManagerSwift
class SearchController: UIViewController,SuggestionDataHandlerDelegate,UIGestureRecognizerDelegate {
    
    func suggestedData(data: String) {
//        searchBar.text = data
//        controller = "1"
//        searchBar.becomeFirstResponder()
//        if let text = searchBar.text {
//            if text.count > 2 {
//                let sessionManager = Alamofire.SessionManager.default
//                sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
//                    dataTasks.forEach { $0.cancel() }
//                    uploadTasks.forEach { $0.cancel() }
//                    downloadTasks.forEach { $0.cancel() }
//                }
//                var dict = [String: Any]()
//                dict["search"] = text
//                dict["offset"] = 0
//                dict["limit"] = 25
//                self.makeRequest(dict: dict)
//            } else {
//                self.makeRequest(dict: [:])
//            }
//        }
        searchBar.text = data
        let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
        view.categoryName = data
        view.parentController = Controllers.search
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    fileprivate let searchViewModalObject = SearchViewModal()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchTableview: UITableView!
    var controller = ""
    var emptyView: EmptyView?
    var templateImage: UIImage!
    let cameraBtn = UIButton()
    
    @IBOutlet weak var cancelBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        tabBarController?.tabBar.isHidden = false
        searchViewModalObject.delegate = self
        navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        self.navigationItem.titleView = self.searchBar
        searchBar.showsCancelButton = true
        searchBar.tintColor = UIColor.white
        searchBar.subviews[0].subviews.compactMap() { $0 as? UITextField }.first?.tintColor = UIColor.black
        setDoneOnKeyboard()
        searchBar.placeholder = searchProducts.localized
        searchTableview.delegate = searchViewModalObject
        searchTableview.dataSource = searchViewModalObject
        searchTableview.tableFooterView = UIView()
        searchBar.showsCancelButton = false
        cancelBtn.title = "Cancel".localized
        
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let clearButton = searchTextField.value(forKey: "clearButton") as? UIButton {
            // Create a template copy of the original button image
            //templateImage =  clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
            // Set the template image copy as the button image
            //clearButton.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
            // Finally, set the image color
            //clearButton.tintColor = .lightGray
            if let code = UserDefaults.standard.value(forKey: AppLanguageKey) as? String{
                if code == "en"{

            cameraBtn.frame = CGRect(origin: CGPoint(x: searchBar.frame.size.width - 105, y: (searchBar.frame.size.height - clearButton.frame.size.height)/2), size: clearButton.frame.size)
            cameraBtn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
            cameraBtn.tintColor = .lightGray
            searchBar.addSubview(cameraBtn)
            cameraBtn.addTarget(self, action: #selector(OpenCamera), for: .touchUpInside)
//            searchTextField.clearButtonMode = .always
                } else {
                    cameraBtn.frame = CGRect(origin: CGPoint(x: searchBar.frame.size.width - 160, y: (searchBar.frame.size.height - clearButton.frame.size.height)/2), size: clearButton.frame.size)
                    cameraBtn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
                    cameraBtn.tintColor = .lightGray
                    searchBar.addSubview(cameraBtn)
                    cameraBtn.addTarget(self, action: #selector(OpenCamera), for: .touchUpInside)
                }
        }
        }
//        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
//        searchTextField.clearButtonMode = .always
    }
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.searchBar.inputAccessoryView = keyboardToolbar
    }
    
    @objc func dismissKeyboard() {
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func OpenCamera(sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "chooseaction".localized, message: nil, preferredStyle: .actionSheet)
        let TextDetection = UIAlertAction(title: "detecttext".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: Controllers.Detector.rawValue) as? DetectorViewController {
                view.detectorType = .text
                view.delegate = self
                view.title = "detecttext".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        let ImageDetection = UIAlertAction(title:  "detectimage".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: Controllers.Detector.rawValue) as? DetectorViewController {
                view.detectorType = .image
                view.delegate = self
                view.title = "detectimage".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        alert.addAction(TextDetection)
        alert.addAction(ImageDetection)
        let cancel = UIAlertAction(title: "cancel".localized, style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(cancel)
        alert.popoverPresentationController?.sourceView = sender//self.view
        //alert.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width - 110, y: 64, width : 1.0, height : 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func cameraBtnAction(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "chooseaction".localized, message: nil, preferredStyle: .actionSheet)
        let TextDetection = UIAlertAction(title: "detecttext".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: Controllers.Detector.rawValue) as? DetectorViewController {
                view.detectorType = .text
                view.delegate = self
                view.title = "detecttext".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        let ImageDetection = UIAlertAction(title:  "detectimage".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName: Controllers.Detector.rawValue) as? DetectorViewController {
                view.detectorType = .image
                view.delegate = self
                view.title = "detectimage".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        alert.addAction(TextDetection)
        alert.addAction(ImageDetection)
        let cancel = UIAlertAction(title: "cancel".localized, style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(cancel)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width - 110, y: 64, width : 1.0, height : 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func endKeyboardAct(_ sender: Any) {
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
        self.tabBarController?.selectedIndex = 0
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.dismiss()
        if controller == "" {
            searchBar.text = ""
            if emptyView == nil {
                emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
                emptyView?.frame = self.view.bounds
                emptyView?.labelText.text = searchEmpty.localized
                emptyView?.button.setTitle(browseCategories.localized, for: .normal)
                emptyView?.isHidden = true
                self.view.addSubview(emptyView!)
            }
            self.makeRequest(dict: [:])
        } else {
            controller = ""
        }
    }
    
    func makeRequest(dict: [String: Any]) {
        let params = GlobalData.apiName.searchSuggestion
       
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: RequestType.post, controler: Controllers.search) { (responseObject: JSON?, error: Error?) in
            
            if (error != nil) {
                print("Error logging you in!")
                
            } else {
                self.searchViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    if data {
                        
                        self.emptyView?.isHidden = true
                        self.searchTableview.isHidden = false
                        self.searchTableview.reloadData()
                        self.searchViewModalObject.table = self.searchTableview
                    } else {
                        self.searchViewModalObject.items.removeAll()
                        self.emptyView?.isHidden = true
                        self.searchTableview.isHidden = true
                    }
                    
                }
                
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SearchController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}

extension SearchController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.tabBarController?.selectedIndex = 0
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !(searchBar.text?.isEmpty)! {
            if !GlobalData.QueryArray.values.contains(searchBar.text!) {
                GlobalData.QueryArray.values.append(searchBar.text!)
                UserDefaults.standard.set(GlobalData.QueryArray.values, forKey: "Queries")
                UserDefaults.standard.synchronize()
            }
        }
        
        if !(searchBar.text?.isEmpty)! {
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
            view.categoryName = searchBar.text!
            view.parentController = Controllers.search
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == "" {
            cameraBtn.isHidden = false
//            if let searchTextField = searchBar.value(forKey: "_searchField") as? UITextField, let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
//                clearButton.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
//                clearButton.tintColor = .lightGray
//                clearButton.addTarget(self, action: #selector(OpenCamera), for: .touchUpInside)
//                searchTextField.clearButtonMode = .always
//                clearButton.isHidden = false
//            }
        } else {
            cameraBtn.isHidden = true
//            if let searchTextField = searchBar.value(forKey: "_searchField") as? UITextField, let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
//                clearButton.setImage(templateImage, for: .normal)
//                clearButton.tintColor = .lightGray
//                clearButton.removeTarget(self, action: #selector(OpenCamera), for: .touchUpInside)
//                searchTextField.clearButtonMode = .always
//                clearButton.isHidden = false
//            }
        }
        
        if let text = searchBar.text {
            if text.count > 2 {
                let sessionManager = Alamofire.SessionManager.default
                sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
                    dataTasks.forEach { $0.cancel() }
                    uploadTasks.forEach { $0.cancel() }
                    downloadTasks.forEach { $0.cancel() }
                }
                var dict = [String: Any]()
                dict["search"] = text
                dict["offset"] = 0
                dict["limit"] = 25
                self.makeRequest(dict: dict)
            }
            else{
                self.makeRequest(dict: [:])
                //                self.searchViewModalObject.searchSuggestion.removeAll()
                //                self.seaechTableView.reloadData()
            }
        } else {
            
        }
    }
    
}

extension SearchController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        
        if Controller == Controllers.product {
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productDataViewControllerIdentifier) as! ProductDataViewController
            view.templateId = id
            view.name = name
            self.navigationController?.pushViewController(view, animated: true)
        }
        
        if Controller == Controllers.search {
            let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productCategoryViewControllerIdentifier) as! ProductCategoryViewController
            view.categoryName = name
            view.parentController = Controllers.search
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
}
