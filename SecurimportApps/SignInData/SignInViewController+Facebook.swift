//
//  SignIn+Fb.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 24/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import Foundation
import FBSDKLoginKit


//
extension SignInViewController: SocialLoginFacebook {
    func fbLogin() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = .web
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
            if (error == nil){
                if let fbloginresult : FBSDKLoginManagerLoginResult = (result)
                {
                    if(fbloginresult.isCancelled){

                    }
                    else{
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            self.getFBUserData()
                        }
                    }
                }
            }
        }
    }

    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    //                    print("wd",result)

                    let Dict  = result as? [String : Any]
                    var loginDict = [String : Any]()

                    loginDict["login"] = (Dict?["email"] as? String)
                    loginDict["name"] = (Dict?["first_name"] as? String)
                    loginDict["authUserId"] = (Dict?["id"] as? String)
                    loginDict["authProvider"] = "FACEBOOK"
                    loginDict["isSocialLogin"] = true
                    loginDict["password"] = ""

                    self.makeRequest(dict: loginDict, call: WhichApiUse.social)
                }

            })
        }
    }


}




