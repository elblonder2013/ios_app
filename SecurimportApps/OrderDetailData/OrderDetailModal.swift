//
//  OrderDetailViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 31/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct OrderDetailModal {
    var billingAddress: String?
    var shippingAddress: String?
    var shortTerm:String?
    var longTerm:String?
    var products = [Product]()
    var orders: orderHistoryData?
    var shippingData: ShippingData?
    var paymentUrl : String
    init?(data: JSON) {
        paymentUrl = data["paymentData"]["paymentUrl"].stringValue
        shortTerm = data["paymentTerms"]["paymentShortTerms"].stringValue
        longTerm =  data["paymentTerms"]["paymentLongTerms"].stringValue
        Addons.AddonsChecks(data: data)
        billingAddress = data["billing_address"].string ?? data["billingAddress"].stringValue
        shippingAddress = data["shipping_address"].string ?? data["shippingAddress"].stringValue
        orders = orderHistoryData(data: data)
        if data["delivery"] != JSON.null {
            shippingData = ShippingData(data:data["delivery"])
            
        }

        if let products = data["items"].array {
            self.products = products.map {
                Product(json: $0)
            }
        }
        
    }

}
