//
//  WishlistProductTableViewCell.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class WishlistProductTableViewCell: UITableViewCell {
    @IBOutlet weak var wishlistView: UIView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var cartbutton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!

    var delegate: PassData?
    override func awakeFromNib() {
        super.awakeFromNib()
        removeButton.setTitle("remove".localized, for: .normal)
        cartbutton.setTitle("moveToCart".localized, for: .normal)
        wishlistView.layer.masksToBounds =  false
        wishlistView.backgroundColor = UIColor.white
        wishlistView.myBorder()
        // Initialization code
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
    var item: Product? {
        didSet {
            productImage.setImage(imageUrl: (item?.pictureUrl!)!, controller: .cart)
            productNameLabel.text = item?.name
            priceLabel.text = String.init(format: "%@: %@", price.localized, (item?.price)!)
            priceLabel.halfTextColorChange(fullText: priceLabel.text!, changeText: price.localized)

        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func removeAction(_ sender: UIButton) {
          delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call:  WhichApiUse.removeFromWishlist)
    }
    @IBAction func MoveToCartAction(_ sender: UIButton) {
          delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call:  WhichApiUse.moveToCart)
    }

}
