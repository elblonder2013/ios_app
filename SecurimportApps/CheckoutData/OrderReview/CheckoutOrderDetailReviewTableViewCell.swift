//
//  CheckoutOrderDetailReviewTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class CheckoutOrderDetailReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var subtotaltext: UILabel!
    @IBOutlet weak var subtotalprice: UILabel!
    @IBOutlet weak var taxtext: UILabel!
    @IBOutlet weak var taxprice: UILabel!
    @IBOutlet weak var totaltext: UILabel!
    @IBOutlet weak var totalprice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        subtotaltext.text = subtotal.localized
        totaltext.text = total.localized
        taxtext.text = tax.localized
        subtotaltext.SetDefaultTextColor()
        totaltext.SetDefaultTextColor()
        taxtext.SetDefaultTextColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item1: orderHistoryData? {
        didSet {
            subtotalprice.text = item1?.subtotal
            taxprice.text = item1?.taxes
            totalprice.text = item1?.grantTotal

        }
    }

}
