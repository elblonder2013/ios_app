import Foundation
import UIKit
import Reusable
struct FormItemUIProperties {
    var tintColor = UIColor.black
    var keyboardType = UIKeyboardType.default
    var cellType: FormItemCellType?
}
protocol FormValidable {
    var isValid: Bool {get set}
    var isRequired: Bool {get set}
    var isSecure: Bool {get set}
    func checkValidity()
    var keyType: String {get set}
    var emailType: Bool {get set}
    var valiidationType: String {get set}
}
class Form {
    var formItems = [[FormItem]]()
    @discardableResult
    func isValid() -> (Bool, String) {
        var isValid = true
        var errorString = ""
        for item1 in self.formItems {
            for item in item1 {
                item.checkValidity()
                if !item.isValid {
                    isValid = false
                    errorString = item.placeholder
                    break;
                }
                if item.isRequired, item.value == nil || item.value as? String == nil || (item.value as? String)?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
                    isValid = false
                    errorString = item.placeholder
                    break;
                }
            }
            if isValid == false{
               break;
            }
        }
        return (isValid, errorString)
    }
    func getFormData() -> [String: Any] {
        var dict = [String: Any]()
        for item1 in self.formItems {
            for item in item1 {
                if item.uiProperties.cellType != .button{
                    if item.keyType.count > 0 , let value = item.value {
                        dict[item.keyType] = value
                    }
                    if item.keyType2.count > 0 , let value = item.value2 {
                        dict[item.keyType2] = value
                    }
                    if item.keyType3.count > 0 , let value = item.value3 {
                        dict[item.keyType3] = value
                    }
                    if item.keyType4.count > 0 , let value = item.value4 {
                        dict[item.keyType4] = value
                    }
                }
            }
        }
        return dict
    }
}
enum FormItemCellType {
    case textField
    case textView
    case dropDown
    case addressCheck
    case label
    case forward
    case set
    case switchControl
    case singleDropDown
    case button
    static func registerCells(for tableView: UITableView) {
        tableView.register(cellType: AddressFieldTableViewCell.self)
        tableView.register(cellType: FormDropDownTableViewCell.self)
        tableView.register(cellType: AddressCheckTableViewCell.self)
        tableView.register(cellType: FormLabelTableViewCell.self)
        tableView.register(cellType: FormSetAddressTypeTableViewCell.self)
        tableView.register(cellType: ChangeTableViewCell.self)
        tableView.register(cellType: DorpDownTableViewCell.self)
        tableView.register(cellType: SubmitTableViewCell.self)
    }
    func dequeueCell(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        switch self {
        case .textView:
            cell = UITableViewCell()
        case .textField:
            cell = tableView.dequeueReusableCell(for: indexPath,cellType: AddressFieldTableViewCell.self)
            if let languageCode =  UserDefaults.standard.object(forKey: "language") as? String{
                if languageCode == "ar" {
                    cell.contentView.semanticContentAttribute = .forceRightToLeft
                }else {
                    L102Language.setAppleLAnguageTo(lang: "en")
                    cell.contentView.semanticContentAttribute = .forceLeftToRight
                }
            }
        case .dropDown:
            cell = tableView.dequeueReusableCell(for: indexPath, cellType: FormDropDownTableViewCell.self)
            if let cell = cell as? FormDropDownTableViewCell {
                cell.tableView = tableView
            }
        case .label: cell = tableView.dequeueReusableCell(for: indexPath,cellType: FormLabelTableViewCell.self)
        case .forward: cell = UITableViewCell()
        case .addressCheck: cell = tableView.dequeueReusableCell(for: indexPath,cellType: AddressCheckTableViewCell.self)
        case .set: cell = tableView.dequeueReusableCell(for: indexPath, cellType: FormSetAddressTypeTableViewCell.self)
        case .switchControl: cell = tableView.dequeueReusableCell(for: indexPath, cellType: ChangeTableViewCell.self)
        case .singleDropDown: cell = tableView.dequeueReusableCell(for: indexPath,cellType: DorpDownTableViewCell.self)
        case .button: cell = tableView.dequeueReusableCell(for: indexPath,cellType: SubmitTableViewCell.self)
        }
        cell.selectionStyle = .none
        return cell
    }
}
protocol FormUpdatable {
    func update(with formItem: FormItem)
}
protocol FormConformity {
    var formItem: FormItem? {get set}
}

