//
//  DescriptionViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 04/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SVProgressHUD
import WebKit
class DescriptionViewController: UIViewController, WKNavigationDelegate {
    
    var wKWebView: WKWebView!
    var title1: String?
    var url : URL?
    var htmlData: String?
    
    override func loadView() {
        wKWebView = WKWebView()
        wKWebView.navigationDelegate = self
        view = wKWebView
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = self.url{
             wKWebView.load(URLRequest(url: url))
        } else{
            wKWebView.loadHTMLString(htmlData!, baseURL: nil)
        }
        self.navigationItem.title = title1
        self.view.backgroundColor = UIColor.white
        wKWebView.backgroundColor = UIColor.white
        wKWebView.isOpaque = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
         SVProgressHUD.show(withStatus: "Loading....".localized)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
           SVProgressHUD.dismiss()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
