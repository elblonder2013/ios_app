//
//  OrderHistoryViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class OrderHistoryViewModal: NSObject {

    var delegate: NewViewControllerOpen?

    var orders = [orderHistoryData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = OrderHistoryModel(data : jsonData) else {
            return
        }

        if !data.orders.isEmpty {
            orders = data.orders
             completion(true)
        } else {
             completion(false)
        }

    }
}

extension OrderHistoryViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderHistoryViewCell.identifier) as! OrderHistoryViewCell
        cell.item = orders[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.moveToController(id: orders[indexPath.row].orderId!, name: orders[indexPath.row].name!, dict: JSON.null, Controller: Controllers.orderDetails)
    }

}
