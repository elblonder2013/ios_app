//
/**
Odoo Marketplace
@Category Webkul
@author    Webkul
Created by: jitendra on 10/08/18
FileName: PermanetDeactivateTableViewCell.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license   https://store.webkul.com/license.html
*/


import UIKit

class PermanetDeactivateTableViewCell: UITableViewCell {
    var delegate:AccountDeacticvateAction!

    @IBOutlet weak var deactiveLbl: UILabel!
    @IBOutlet weak var deactivateBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        deactivateBtn.setTitle("deactivate".localized, for: .normal)
        deactiveLbl.text = "deactivatePermanent".localized
        deactivateBtn.layer.cornerRadius = 5
        deactivateBtn.layer.masksToBounds = true
    }
     //MARK:- Permanent Btn Act
    @IBAction func permanentAccountAct(_ sender: Any) {
        delegate.accountDeactivate(type: "Permanent")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
