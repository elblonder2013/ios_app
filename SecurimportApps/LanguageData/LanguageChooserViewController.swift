//
//  LanguageChooserViewController.swift
//  Ermin_Trevisan
//
//  Created by bhavuk.chawla on 15/02/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Toaster

class LanguageChooserViewController: UIViewController {

    @IBOutlet weak var LanguageTableView: UITableView!
    @IBOutlet weak var selectLabel: UILabel!
    @IBOutlet weak var appName: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var continueBtn: UIButton!
    var row = 0
    override func viewDidLoad() {
        super.viewDidLoad()
//        chooseBtn.backgroundColor = GlobalData.Credentials.accentColor
        // Do any additional setup after loading the view.
         if GlobalData.Credentials.environment == EnvironmentType.marketplace {
                appName.text = "appNameMP".localized
         } else {
            appName.text = "appName".localized
        }
        UserDefaults.standard.set("yes", forKey: "loadLanguage")
        UserDefaults.standard.synchronize()
        
        if let index =  GlobalData.StoreData.languages.index(where: { $0.languageCode == GlobalData.StoreData.defaultLanguage}) {
            row = index
        }
       
      
        
        LanguageTableView.delegate = self
        LanguageTableView.dataSource = self
        continueBtn.backgroundColor = GlobalData.Credentials.accentColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if GlobalData.StoreData.languages.count == 1 {
//            GlobalData.StoreData.check = true
//            GlobalData.StoreData.defaultLanguage = GlobalData.StoreData.languages[0].languageCode
//            UserDefaults.standard.set(GlobalData.StoreData.defaultLanguage, forKey: "defaultLanguage")
//            UserDefaults.standard.synchronize()
//            
//
//            self.call()
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var chooseBtn: UIButton!
    @IBAction func chooseClicked(_ sender: Any) {
        
        if GlobalData.StoreData.languages.count > 0 {
        
        ActionSheetStringPicker.show(withTitle: "languages".localized, rows: GlobalData.StoreData.languages.map {$0.languageName }, initialSelection: row, doneBlock: {
            picker, indexes, values in
          
            GlobalData.StoreData.check = true
            GlobalData.StoreData.defaultLanguage = GlobalData.StoreData.languages[indexes].languageCode
            UserDefaults.standard.set(GlobalData.StoreData.defaultLanguage, forKey: "defaultLanguage")
            UserDefaults.standard.synchronize()
            
            let val =  GlobalData.StoreData.defaultLanguage.split(separator: "_")[0]
            print(val)
            AppLanguageDefaultValue = String(val)
            UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
            self.call()
           return
            
            
        }, cancel: { ActionStringCancelBlock in return }, origin: chooseBtn)
        }
        else{
            ErrorChecking.warningView(message: "Nolanguages".localized)
//            Toast(text: "No languages").show()
//            ToastView.appearance().backgroundColor = .black
        }
    }
    
    func call() {
        self.performSegue(withIdentifier: "back", sender: self)
    }
    @IBAction func continueClicked(_ sender: UIButton) {
        var val = ""
        if !GlobalData.StoreData.defaultLanguage.isEmpty {
            val = String(GlobalData.StoreData.defaultLanguage.split(separator: "_")[0])
        }
         AppLanguageDefaultValue = String(val)
        UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
        if val == "ar" {
            L102Language.setAppleLAnguageTo(lang: "ar")
            if #available(iOS 9.0, *) {
                UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
                UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
                self.tabBarController?.tabBar.semanticContentAttribute = .forceRightToLeft
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                
            } else {
                // Fallback on earlier versions
            }
            
        } else {
            
            L102Language.setAppleLAnguageTo(lang: "en")
            if #available(iOS 9.0, *) {
                UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                UITabBar.appearance().semanticContentAttribute =  .forceLeftToRight
                self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                
            } else {
                // Fallback on earlier versions
            }
            
        }
        self.performSegue(withIdentifier: "move", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LanguageChooserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        topView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_WIDTH/2 + 50)
         topView.applyGradientToTopView(colours: GlobalData.Credentials.gradientArray)
        return topView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return SCREEN_WIDTH/2 + 50
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GlobalData.StoreData.languages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = GlobalData.StoreData.languages[indexPath.row].languageName
        cell.selectionStyle = .none
        if let code = UserDefaults.standard.value(forKey: "defaultLanguage") as? String, code ==  GlobalData.StoreData.languages[indexPath.row].languageCode {
           cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GlobalData.StoreData.defaultLanguage = GlobalData.StoreData.languages[indexPath.row].languageCode
        UserDefaults.standard.set(GlobalData.StoreData.defaultLanguage, forKey: "defaultLanguage")
        UserDefaults.standard.synchronize()
        let val =  GlobalData.StoreData.defaultLanguage.split(separator: "_")[indexPath.row]
        print(val)
        AppLanguageDefaultValue = String(val)
        UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
        LanguageTableView.reloadData()
    }
}
