//
//  OrderPlaceDataViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 07/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class OrderPlaceDataViewController: UIViewController {
    @IBOutlet weak var orderDescriptionLabel: UILabel!
    @IBOutlet weak var orderSummaryView: UIView!
    @IBOutlet weak var orderIDlabel: UILabel!
    @IBOutlet weak var continueShoppingBtn: UIButton!
    
    @IBOutlet weak var orderPlacedlbl: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    var desc: String?
    var id: String?
    var isSuccess = true

    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().barTintColor = UIColor.white
          self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        orderIDlabel.text = isSuccess ? String.init(format: "Your order is: #".localized + "%@", id!) : ""
          orderDescriptionLabel.text = desc!
        orderSummaryView.myBorder()
        continueShoppingBtn.backgroundColor = GlobalData.Credentials.accentColor
        continueShoppingBtn.setTitle("Continue Shopping".localized, for: .normal)
        orderPlacedlbl.text = isSuccess ? "Order Placed".localized : "Order Failed".localized
        statusImage.image = UIImage(named: isSuccess ? "Status-CompleteLarge" : "Icon-Close-Large")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func orderSummaryPressed(_ sender: UIButton) {

    }

    @IBAction func ContinueShoppingPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "back", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
