//
//  SignInViewController.swift
//  Odoo application
//
//  Created by bhavuk.chawla on 14/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toaster
import FBSDKLoginKit
import GoogleSignIn
import FirebaseAuth
//import TwitterKit
class SignInViewController: UIViewController {

    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    @IBOutlet weak var pwdBtn: UIButton!
    @IBOutlet weak var cross: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var appNameLabel: UILabel!
     @IBOutlet weak var emailLabel: UILabel!
     @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailfield: UITextField!
    @IBOutlet weak var passwordfield: UITextField!
    var isShow = false

    var controller: Controllers?
    weak var socialLoginFacebookDelegate: SocialLoginFacebook?
    weak var socialLoginGoogleDelegate: SocialLoginGoogle?
    override func viewDidLoad() {
        super.viewDidLoad()
       if GlobalData.AddOnsEnabled.allowGuest {
            
        }
         if GlobalData.Credentials.environment == EnvironmentType.marketplace {
         } else {
         }
        if let obj = self as? SocialLoginFacebook {
            self.socialLoginFacebookDelegate = obj
        }
        
        if let obj = self as? SocialLoginGoogle {
            self.socialLoginGoogleDelegate = obj
        }
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.view.backgroundColor = UIColor.clear
          self.applyGradient(colours: GlobalData.Credentials.gradientArray)
        // Do any additional setup after loading the view.

        passwordfield.isSecureTextEntry = true
        signUpButton.layer.borderColor = UIColor.white.cgColor
        signUpButton.layer.borderWidth = 2.0

        if controller == Controllers.home {
            cross.isHidden = true
        }

        if GlobalData.AddOnsEnabled.allowSignUp {
            signUpButton.isHidden = false
        } else {
            signUpButton.isHidden = true
        }

        if GlobalData.AddOnsEnabled.allowResetPassword {
            forgotButton.isHidden = false
        } else {
            forgotButton.isHidden = true
        }
        
        if GlobalData.AddOnsEnabled.allowGuest {
            cross.isHidden = false
        } else {
            cross.isHidden = true
        }

        
        
        self.stringsLocalized()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true

    }

    func stringsLocalized() {
        if GlobalData.Credentials.environment == EnvironmentType.marketplace {
            appNameLabel.text = "appNameMP".localized
        } else {
            appNameLabel.text = "appName".localized
        }
        emailLabel.text = emailAddress.localized
        passwordLabel.text = password.localized
        loginButton.setTitle(login.localized, for: .normal)
        signUpButton.setTitle(wantsignUp.localized, for: .normal)
         forgotButton.setTitle(forgotPassword.localized, for: .normal)

    }

    @IBAction func CrossBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func applyGradient(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = colours.map { $0.cgColor }
        self.view.layer.insertSublayer(gradient, at: 0)
    }

    @IBAction func loginTapped(_ sender: Any) {
        self.dismissKeyboard()
        if (emailfield.text?.isEmpty)! {
            ErrorChecking.warningView(message: "Enter Email".localized)
        } else
        if (passwordfield.text?.isEmpty)! {
            ErrorChecking.warningView(message: "Enter Password".localized)
        } else {
            UserDefaults.standard.removeObject(forKey: "loginDict")
            UserDefaults.standard.synchronize()
            let dict: [String: Any] = ["login" : emailfield.text!, "pwd" : passwordfield.text!]
            print(dict)
            self.makeRequest(dict: dict, call: .none)
        }
    }

    func makeRequest(dict: [String: Any], call: WhichApiUse) {

        var params = ""
        switch call {
        case .forgotPassword:
             params = GlobalData.apiName.forget
        case .registerFcm:
            params = GlobalData.apiName.registerFcm
        case .social:
            params = GlobalData.apiName.signUp
        default:
             params = GlobalData.apiName.login
        }

       
         NetworkManager.shared.fetchData(params: params, login: true, view: self.view, dict: dict, verbs: RequestType.post, controler: Controllers.signIn) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {

                switch call {
                case .forgotPassword:
                   if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                   } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                  case .social:
                  
                     if responseObject!["success"].boolValue {
                        UserDefaults.standard.set(dict["login"] as? String, forKey: customerEmailKey)
                        UserDefaults.standard.set(dict["name"] as? String, forKey: customerNameKey)
                            let dict: [String: Any] = ["authProvider" : responseObject!["cred"]["authProvider"].stringValue, "authUserId" : responseObject!["cred"]["authUserId"].stringValue]
                            UserDefaults.standard.set(true, forKey: loggedInUserKey)
                            UserDefaults.standard.set(dict, forKey: "socialDict")
                            print(dict)
                            UserDefaults.standard.synchronize()
                            if let key = UserDefaults.standard.value(forKey: firebaseKey) as? String {
                                
                                let device_id = UIDevice.current.identifierForVendor?.uuidString
                                let dict: [String: Any] = ["customerId" :responseObject![customerIdKey].stringValue, "fcmToken" : key, "fcmDeviceId" :  device_id!]
                                
                                self.makeRequest(dict: dict, call: WhichApiUse.registerFcm)
                            } else {
                                self.performSegue(withIdentifier: "goBack", sender: self)
                            }
                     } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                case .registerFcm:
                    self.performSegue(withIdentifier: "goBack", sender: self)
                default:
                    if responseObject!["success"].boolValue {
                        UserDefaults.standard.set(true, forKey: loggedInUserKey)
                        UserDefaults.standard.set(dict, forKey: "loginDict")
                        UserDefaults.standard.set( responseObject!.dictionaryObject, forKey: "userDetails")
                        UserDefaults.standard.set(responseObject![customerEmailKey].stringValue, forKey: customerEmailKey)
                        UserDefaults.standard.set(responseObject![customerIdKey].stringValue, forKey: customerIdKey)
                        UserDefaults.standard.set(responseObject![customerNameKey].stringValue, forKey: customerNameKey)
                        UserDefaults.standard.set(responseObject!["customerProfileImage"].stringValue, forKey: CustomerImageUrl)

                        if responseObject!["is_seller"].boolValue {
                           GlobalData.AddOnsEnabled.is_seller = true
                        } else {
                             GlobalData.AddOnsEnabled.is_seller = false
                        }

                        UserDefaults.standard.synchronize()
                        if let key = UserDefaults.standard.value(forKey: firebaseKey) as? String {

                            let device_id = UIDevice.current.identifierForVendor?.uuidString
                            let dict: [String: Any] = ["customerId" :responseObject![customerIdKey].stringValue, "fcmToken" : key, "fcmDeviceId" :  device_id!]

                            self.makeRequest(dict: dict, call: WhichApiUse.registerFcm)
                        } else {
                            self.performSegue(withIdentifier: "goBack", sender: self)
                        }
                        //                    self.dismiss(animated: true, completion: nil)
                    } else {
                        ErrorChecking.ErrorView(message: loginFailed.localized)
                     
                    }
                }

            }
        }
    }
    @IBAction func twitterClicked(_ sender: Any) {
        
//        TWTRTwitter.sharedInstance().logIn { (session, error) in
//            if (session != nil) {
//                //                self.firstName = session?.userName ?? ""
//                //                self.lastName = session?.userName ?? ""
//                let client = TWTRAPIClient.withCurrentUser()
//                client.requestEmail { email, error in
//                    if (session != nil) {
//                        print("signed in as \(String(describing: session?.userName))");
//                        let firstName = session?.userName ?? ""   // received first name
//                        let lastName = session?.userName ?? ""  // received last name
//                        let recivedEmailID = email ?? ""   // received email
//
//                        var loginDict = [String: Any]()
//                        if email == nil {
//                            loginDict["login"] = session?.userID
//                        }else {
//                            loginDict["login"] = email
//                        }
//                        loginDict["name"] = session?.userName ?? ""
//                        loginDict["authUserId"] = session?.authToken ?? ""
//
//                        loginDict["authProvider"] = "TWITTER"
//                        loginDict["isSocialLogin"] = true
//                        loginDict["password"] = ""
//
//                        if email == nil{
//                            //self.loginDict["email"]
//                            let AC = UIAlertController(title:"Enter Email", message: "This account not attached with any email", preferredStyle: .alert)
//                            AC.addTextField { (textField) in
//                                textField.placeholder = "Enter Email"
//                            }
//                            let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
//                                let textField = AC.textFields![0]
//                                if((textField.text?.characters.count)! < 1){
//
//                                    ErrorChecking.warningView(message: "pleasefillemailid".localized)
//                                }else if checkValidEmail(data: textField.text!){
//                                   ErrorChecking.warningView(message: "pleaseentervalidemail".localized);
//
//                                }
//
//                                else{
//                                    loginDict["login"] = textField.text!;
//                                     self.makeRequest(dict: loginDict, call: WhichApiUse.social)
//                                }
//                            })
//                            let noBtn = UIAlertAction(title:"cancel".localized, style:.destructive, handler: {(_ action: UIAlertAction) -> Void in
//                            })
//                            AC.addAction(okBtn)
//                            AC.addAction(noBtn)
//                            self.present(AC, animated: true, completion: {  })
//
//
//                        }else{
//                            self.makeRequest(dict: loginDict, call: WhichApiUse.social)
//
//                        }
//
//
//
//
//
//                    }else {
//                        print("error: \(String(describing: error?.localizedDescription))");
//                    }
//                }
//            }else {
//                print("error: \(String(describing: error?.localizedDescription))");
//            }
//        }
        
    }

    @IBAction func SignUpTapped(_ sender: Any) {
           self.navigationController?.navigationBar.isHidden = false
        let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signUpViewControllerIdentifier)
        self.navigationController?.pushViewController(view, animated: true)
        
//            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signUpViewControllerIdentifier) as! SignUpViewController
//            let nav  = UINavigationController(rootViewController: view)
//            self.present(nav, animated: true, completion: nil)
        
    }

    @IBAction func ForgotPasswordClicked(_ sender: UIButton) {
        let AC = UIAlertController(title: "Forgot Your Password".localized, message: "Please enter the email address you used to register.We will then send you a new Password".localized, preferredStyle: .alert)
        AC.addTextField { (textField) in
            textField.placeholder = "Enter Email Address".localized
            textField.text = self.emailfield.text
        }
        let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            let textField = AC.textFields![0] // Force unwrapping because we know it exists.

            var isValid = 1
            var errorMessage = ""
            if(textField.text?.isEqual(""))! {
                isValid = 0
                errorMessage = "Enter Your Email".localized
            }

            if isValid == 0 {
                print("hdjf")
                self.alert(message: errorMessage, title: "Validation Error".localized)
//              let ac = UIAlertController.alertWithTitle(title: "Validation Error", message: errorMessage, buttonTitle: "ok")
//              self.parent!.present(ac, animated: true, completion: { _ in })

            } else {
                 let dict: [String: Any] = ["login" : textField.text! ]
             self.makeRequest(dict: dict, call: .forgotPassword)

            }

        })
        let noBtn = UIAlertAction(title:"Cancel".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
        })
        AC.addAction(okBtn)
        AC.addAction(noBtn)
        self.parent!.present(AC, animated: true, completion: nil)
    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    @IBAction func passwordShow(_ sender: UIButton) {
        if isShow == false {
            isShow = true
            passwordfield.isSecureTextEntry = false
            pwdBtn.setImage(#imageLiteral(resourceName: "show"), for: .normal)
        } else {
            isShow = false
            passwordfield.isSecureTextEntry = true
            pwdBtn.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
        }

    }
    
    @IBAction func fbClicked(_ sender: UIButton) {
        if let myclass = stringClassFromString("MobikulFBSignIn") as? NSObject.Type {
            //let instance = myclass.init()
            //print(instance)
            //print(myclass)
            _ = myclass.init()
            NotificationCenter.default.addObserver(self, selector: #selector(SignInViewController.responseFromSocialLogin), name: NSNotification.Name(rawValue: "MobikulFBSignIn"), object: nil)
        }
    }

    @IBAction func googleClicked(_ sender: UIButton) {
        socialLoginGoogleDelegate?.googleLogin()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

     @objc func responseFromSocialLogin(data: Any) {
        if let x = data as? NSNotification {
            let userInfo = JSON(x.userInfo as Any)
            if userInfo != JSON.null {
                print(userInfo)
                var loginDict = [String: Any]()
                loginDict["login"] = userInfo["email"].string ?? userInfo["login"].stringValue ?? userInfo["id"].stringValue
                loginDict["name"] = userInfo["first_name"].string ?? userInfo["name"].stringValue
                loginDict["authUserId"] = userInfo["id"].stringValue
                loginDict["authProvider"] = "FACEBOOK"
                loginDict["isSocialLogin"] = true
                loginDict["password"] = ""
               self.makeRequest(dict: loginDict, call: WhichApiUse.social)
            }
        }
    }
    
    func stringClassFromString(_ className: String) -> AnyClass? {
        var namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
        namespace = "MobikulFBSignI".replacingOccurrences(of: " ", with: "_")
        return NSClassFromString("\(namespace).\(className)");
    }

}
//
//{
//    "authUserId" : "435460533572306",
//    "authProvider" : "FACEBOOK",
//    "password" : "",
//    "login" : "mobikul@webkul.com",
//    "name" : "Mobikul",
//    "isSocialLogin" : true
//}

