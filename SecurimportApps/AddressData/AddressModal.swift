//
//  AddressModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct AddressModal {
    var billingAddres: AddressData?
    var shippingAddress = [AddressData]()
    init?(data: JSON) {
        Addons.AddonsChecks(data: data)
        if var address = data["addresses"].array {
            if address.count == 1 {
                 if(address[0]["display_name"] != "\n\n  \n") {
                    self.billingAddres = AddressData(data: address[0])
                }
            } else {
               self.billingAddres = AddressData(data: address[0])
                address.remove(at: 0)
                if address.count > 0 {
                    self.shippingAddress = address.map {
                        AddressData(data: $0)
                    }
                }
        }
        }
    }
}

struct CheckoutAddressModal {
    var billingAddres: AddressData?
    var shippingAddress = [AddressData]()
    init?(data: JSON) {
        if var address = data["addresses"].array {
             if address.count > 0 {
                if address.count == 1 {
                    if(address[0]["display_name"] != "\n\n  \n") {
                        self.billingAddres = AddressData(data: address[0])
                        self.shippingAddress = address.map {
                            AddressData(data: $0)
                        }
                    }
                } else {
                    self.billingAddres = AddressData(data: address[0])
                    self.shippingAddress = address.map {
                        AddressData(data: $0)
                    }
                }
            }
        }
    }
}

//"\n\n  \n"
struct AddressData {
    var addressId: String?
    var address: String?
    init(data: JSON) {
        self.addressId = data["addressId"].stringValue
        self.address = data["display_name"].stringValue
    }

}
