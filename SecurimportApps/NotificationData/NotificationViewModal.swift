//
//  NotificationViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class NotificationViewModal: NSObject {

    var notification = [NotificationData]()
    var passData: PassData?
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = NotificationModal(data : jsonData) else {
            return
        }
        if !data.notification.isEmpty {
            self.notification = data.notification
             completion(true)
        } else {
            completion(false)
        }
    }
}

extension NotificationViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notification.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.identifier) as! NotificationTableViewCell
        cell.item = notification[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            passData?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.deleteNotification)
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
}
