//
//  ChangeAddressViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 07/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChangeAddressViewController: UIViewController {

    var delegate: AddressUpdate?
    var controller: Controllers?
        fileprivate var changeaddressViewModalObject  = ChangeAddressViewModal()
    @IBOutlet weak var changeTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        changeaddressViewModalObject.delegate = self
        changeaddressViewModalObject.whichApiCallDelegate = self
        changeTableview.delegate = changeaddressViewModalObject
        changeTableview.dataSource = changeaddressViewModalObject
        changeTableview.tableFooterView = UIView()
        self.changeaddressViewModalObject.controller = controller
        // Do any additional setup after loading the view.
        self.navigationItem.title = "ChangeAddress".localized
        self.makeRequest(id: "", whichApi: .none)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeRequest( id: String, whichApi: WhichApiUse ) {
        var params = ""
        var verb: RequestType = .post
        switch whichApi {
        case .defaultAddress:
            params =  GlobalData.apiName.defaultAddress + id
            verb = .put
        default:
            params =  GlobalData.apiName.getAddress
        }

        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: verb, controler: Controllers.changeAddress) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                self.changeaddressViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    print(data)
                    switch whichApi {
                    case .defaultAddress:
                        print()
                        self.delegate?.addressupdated(addressId: "")
                        self.navigationController?.popViewController(animated: true)
                    default:
                        if data {
                            self.changeTableview.isHidden = false
                            self.changeTableview.reloadData()
                        } else {
                            self.changeTableview.isHidden = true
                        }
                    }

                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
     
    */

}

extension ChangeAddressViewController: AddressUpdate {

    func addressupdated(addressId: String) {
        delegate?.addressupdated(addressId: addressId)
        self.navigationController?.popViewController(animated: true)
    }

}

extension ChangeAddressViewController: PassData {

    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {

        self.makeRequest(id: id, whichApi: call)
    }
}
