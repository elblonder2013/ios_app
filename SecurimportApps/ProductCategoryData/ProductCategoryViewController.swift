//
//  ProductCategoryViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 31/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toaster
import ActionSheetPicker_3_0
class ProductCategoryViewController: UIViewController,cartCount,UIPopoverPresentationControllerDelegate {
    func pushToBuyNow() {
        let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "checkoutDataViewController") as! CheckoutDataViewController
        let nav  = UINavigationController(rootViewController: view)
        self.present(nav, animated: true, completion: nil)
    }
    
    func cartValue(count: String) {
        self.tabBarController?.setCartCount(value: count)
    }
    
    func addToWishList() {
        productCategoryViewModalObject.products.removeAll()
     self.makeRequest(dict1: [:], page: 0, index: 0, id: "")
    }
    
    
    var categoryId: String?
    var categoryName: String?
    var parentController: Controllers?
     var emptyView: EmptyView?
    var selectedIndex:IndexPath!
    private let offset = "offset"
    private let limit = "limit"
    private let cid = "cid"
    private let search = "search"
    var sortData = [String]()
    var apiUse: WhichApiUse?
    
    var dict1 = [String: Any](), index: Int = 0, id: String = ""
    
    fileprivate let productCategoryViewModalObject = ProductCategoryViewModal()

    @IBOutlet weak var productCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if  GlobalData.AddOnsEnabled.sortData != nil &&  GlobalData.AddOnsEnabled.sortData?.list.count ?? 0 > 0 {
            sortData = GlobalData.AddOnsEnabled.sortData?.list[0] ?? []
        }else{
            self.navigationItem.rightBarButtonItem = nil
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        productCategoryViewModalObject.navigationItem = self.navigationItem
        productCategoryViewModalObject.categoryName = self.categoryName!
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: self.view)
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        productCategoryViewModalObject.passDataDelegate = self
        productCategoryViewModalObject.delegate = self
        productCategoryViewModalObject.collection = productCollectionView
        productCollectionView.register(ProductCategoryCollectionViewCell.nib, forCellWithReuseIdentifier: ProductCategoryCollectionViewCell.identifier)
         productCollectionView.register(ProductCategoryListCollectionViewCell.nib, forCellWithReuseIdentifier: ProductCategoryListCollectionViewCell.identifier)
        productCollectionView.delegate = productCategoryViewModalObject
        productCollectionView.dataSource = productCategoryViewModalObject
        self.makeRequest(dict1: [:], page: 0, index: 0, id: "")
        // Do any additional setup after loading the view.
    }
    @IBAction func buttonTap(sender: UIBarButtonItem) {
       if productCategoryViewModalObject.productAttributes.count>0{
        if let popController = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "OptionsViewController") as? OptionsViewController{
        popController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popController.productAttributes = productCategoryViewModalObject.productAttributes
            self.tabBarController?.tabBar.isHidden = true
            popController.delegate = self
        self.present(popController, animated: true, completion: nil)
         }
       }else{
            ErrorChecking.warningView(message: "No filter Available".localized)
        }
    }

    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func makeRequest(dict1: [String: Any], page: Int, index: Int, id: String) {
        self.dict1 = dict1
        self.index = index
        self.id = id
        var params = ""
        var dict = [String: Any]()
            var verb: RequestType?
        switch apiUse {

        case .addToWishlist?:
            params = GlobalData.apiName.addToWishlist
            dict = dict1
            verb = .post
            
        case .removeFromWishlist?:
            params = GlobalData.apiName.removeWishlist + id
            verb = .delete

        case .buyNow?:
            params = GlobalData.apiName.addToCart
            dict = dict1
            verb = .post
            
        default:
            switch parentController {
            case .productCategory?:
                params = GlobalData.apiName.searchSuggestion
                dict[offset] = page
                dict[limit] = 25
                dict[cid] = Int(categoryId!)
               
                verb = .post
            case .homeSliders?:
                params = categoryId!
                dict[offset] = page
                dict[limit] = 25
                verb = .post
            case .search?:
                params = GlobalData.apiName.searchSuggestion
                dict[offset] = page
                dict[limit] = 25
                dict[search] = categoryName!
                verb = .post
            case .sellerProfile?:
                params = GlobalData.apiName.searchSuggestion
                dict[offset] = page
                dict[limit] = 25
                dict["domain"] = "[('marketplace_seller_id','=',\(self.categoryId ?? ""))]"
                verb = .post
            case .custom?:
                params = GlobalData.apiName.searchSuggestion
                dict[offset] = page
                dict[limit] = 25
                dict["domain"] = (self.categoryId ?? "")
                verb = .post
                
             default:
                print("hs")
            }
            if sortData.count > 1{
                dict["order"] = sortData[1]
            }
            var filterArray = [[Int]]()
            for obj in productCategoryViewModalObject.productAttributes{
                if obj.value != -1{
                filterArray.append(obj.values[obj.value].attribValueID)
                }
            }
                if filterArray.count>0{
                     dict["filter"] = filterArray
                }
            
        }

        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: verb ?? .post, controler: Controllers.productCategory) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                switch self.apiUse {

                case .addToWishlist?:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)

                        let indexPath = IndexPath(row: index, section: 0 )
                        if let cell  = self.productCollectionView.cellForItem(at: indexPath) as? ProductCategoryCollectionViewCell {
                            cell.wishlistICOn.setImage(#imageLiteral(resourceName: "Icon-Wishlist-Fill"), for: .normal)
                        }
                         self.productCategoryViewModalObject.wishlistIds.append(WishlistIds(data: (dict["productId"] as? String)  ?? ""))
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                case .removeFromWishlist?:
                    if responseObject!["success"].boolValue {
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)

                        let indexPath = IndexPath(row: index, section: 0)
                        if let cell  = self.productCollectionView.cellForItem(at: indexPath) as? ProductCategoryCollectionViewCell {
                            let kk =  WishlistIds(data: id)
                            if let index =   self.productCategoryViewModalObject.wishlistIds.index(where: { $0.wishlistId == kk.wishlistId}) {
                                self.productCategoryViewModalObject.wishlistIds.remove(at: index)
                                 cell.wishlistICOn.setImage(#imageLiteral(resourceName: "Icon-Wishlist"), for: .normal)
                            }

                        }

                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }

                case .buyNow?:
                    if responseObject!["success"].boolValue {
                        if responseObject!["cartCount"] != JSON.null {
                            self.tabBarController?.setCartCount(value: responseObject!["cartCount"].stringValue)
                        }
                        ErrorChecking.successView(message: responseObject!["message"].stringValue)
                        let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "checkoutDataViewController") as! CheckoutDataViewController
                        let nav  = UINavigationController(rootViewController: view)
                        self.present(nav, animated: true, completion: nil)
                    } else {
                        ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                    }
                default:
                    self.productCategoryViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in

                        if data {
                            self.productCollectionView?.isHidden = false
                            self.productCollectionView.reloadData()
                            if page == 0{
                                self.productCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                            }
                            self.navigationItem.title = String.init(format: "%@", self.categoryName!  )
                            self.emptyView?.isHidden = true
                        } else {
                            self.productCollectionView?.isHidden = true
                            self.navigationItem.title = String.init(format: "%@", self.categoryName!  )
                            self.emptyView?.isHidden = false
                        }

                    }
                }

            }
        }
    }
    @IBAction func didTapSort() {
        
        if  GlobalData.AddOnsEnabled.sortData != nil &&  GlobalData.AddOnsEnabled.sortData?.list.count ?? 0 > 0 {
            let list = GlobalData.AddOnsEnabled.sortData?.list.map{
                return $0.count>0 ? $0[0] : ""
            }
        ActionSheetStringPicker.show(withTitle: "Sort".localized, rows: list, initialSelection: 0, doneBlock: {
            picker, indexes, values in
             if   GlobalData.AddOnsEnabled.sortData?.list.count ?? 0 > 0 {
                self.productCategoryViewModalObject.products.removeAll()
                self.productCategoryViewModalObject.page = 0
                self.sortData =  GlobalData.AddOnsEnabled.sortData?.list[indexes] ?? []
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.makeRequest(dict1: self.dict1, page: 0, index: self.index, id: self.id)
                }
            }
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.delegate = self
            if parentController == Controllers.search {
                emptyView?.labelText.text = searchEmpty.localized
                emptyView?.button.setTitle(browseCategories.localized, for: .normal)
            } else {
                emptyView?.image.image = #imageLiteral(resourceName: "empty-state-category")
                emptyView?.labelText.text = productEmpty.localized
                 emptyView?.button.setTitle(browseCategories.localized, for: .normal)
            }
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }



}

extension ProductCategoryViewController: NewViewControllerOpen,OptionsDelegate{
    func optionsDataPass(data: [ProductAttributes]?) {
        tabBarController?.tabBar.isHidden = false
        if data != nil{
        productCategoryViewModalObject.productAttributes = data!
        productCategoryViewModalObject.page = 0
        productCategoryViewModalObject.products.removeAll()
        makeRequest(dict1: dict1, page: 0, index: index, id: id)
        }
    }
    
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productDataViewControllerIdentifier) as! ProductDataViewController
        view.templateId = id
        view.productid = name
        self.navigationController?.pushViewController(view, animated: true)
    }
}

extension ProductCategoryViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {

        switch call {
        case .addToWishlist:
            apiUse = call
            self.makeRequest(dict1: dict, page : 0, index: index, id:id )
        case .removeFromWishlist:
            apiUse = call
            self.makeRequest(dict1: [:], page : 0, index: index, id:id )
        case .buyNow:
            apiUse = call
            self.makeRequest(dict1: dict, page : 0, index: index, id:id )
        default:
            apiUse = call
            self.makeRequest(dict1: dict, page : index, index: index, id:id)
        }

        }

}

extension ProductCategoryViewController: EmptyDelegate {
    func buttonPressed() {

        if self.tabBarController?.selectedIndex != 2 {
            self.tabBarController?.selectedIndex = 2
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
extension ProductCategoryViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        var heightOffset = UIApplication.shared.statusBarFrame.height
        //if let navigationController = navigationController {
        heightOffset += 44
        //}
        let newLocation = CGPoint(x: location.x, y: location.y + productCollectionView.contentOffset.y - heightOffset )
        guard let indexPath = productCollectionView.indexPathForItem(at: newLocation) else {
            return nil
        }
        
        guard let cell = productCollectionView.cellForItem(at: indexPath) else {
            return nil
        }
        let story = UIStoryboard(name: "Home", bundle: nil)
        let touchDetailVC = story.instantiateViewController(withIdentifier: "TouchOfProductInViewController")as! TouchOfProductInViewController
        let selectedIndex = indexPath
        touchDetailVC.productImgUrl = productCategoryViewModalObject.products[(selectedIndex.row)].pictureUrl!
        touchDetailVC.productName = productCategoryViewModalObject.products[(selectedIndex.row)].name!
        touchDetailVC.productId = productCategoryViewModalObject.products[(selectedIndex.row)].productId!
        
        if productCategoryViewModalObject.wishlistIds.contains(where: {$0.wishlistId == productCategoryViewModalObject.products[(selectedIndex.row)].productId!}) {
            touchDetailVC.wishListTrue = true
        }
        touchDetailVC.delegate = self
        let screenWidth = UIScreen.main.bounds.width
        touchDetailVC.controller = Controllers.productCategory
        let screenHeight = UIScreen.main.bounds.height
        touchDetailVC.preferredContentSize = CGSize(width: screenWidth, height: screenHeight - 300)
        var previewFrame = cell.frame
        previewFrame.origin.y += heightOffset
        previewingContext.sourceRect = previewFrame
        
        return touchDetailVC
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        //performSegue(withIdentifier: segueIdentifier, sender: nil)
       // let story = UIStoryboard(name: "Main", bundle: nil)
        // let animalDetailVC = story.instantiateViewController(withIdentifier: "AnimalDetailVC")as! AnimalDetailViewController
        
    }
    
}
