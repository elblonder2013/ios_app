//
//  DashBoardModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DashBoardOrderModal {
    var orders = [orderHistoryData]()
    init?(data: JSON) {
        Addons.AddonsChecks(data: data)
        if let array  = data["recentOrders"].array {
            self.orders = array.map { orderHistoryData(data: $0) }
        }
    }

}

struct DashBoardAddressModal {
    var address = [AddressData]()
    init?(data: JSON) {
        if let array  = data["addresses"].array {
            self.address = array.map { AddressData(data: $0) }
        }
    }

}
