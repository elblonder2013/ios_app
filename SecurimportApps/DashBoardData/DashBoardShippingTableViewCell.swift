//
//  DashBoardShippingTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 13/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON

class DashBoardShippingTableViewCell: UITableViewCell {

    @IBOutlet weak var changeAddressButton: UIButton!
    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var address: UILabel!
    var delegate: NewViewControllerOpen?
    @IBOutlet weak var shippingView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        shippingView.myBorder()
        addAddressButton.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)
        changeAddressButton.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)
        addAddressButton.setTitle("Add New Address".localized, for: .normal)
        changeAddressButton.setTitle("Change Address".localized, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    @IBAction func AddAddressClicked(_ sender: UIButton) {
        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.addAddress)
    }

    @IBAction func ChangeAddressClicked(_ sender: UIButton) {
        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.changeAddress)
    }

}
