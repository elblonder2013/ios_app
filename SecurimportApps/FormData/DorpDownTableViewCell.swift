//
//  DorpDownTableViewCell.swift
//  Pharmacist Place
//
//  Created by debabrata on 06/03/19.
//  Copyright © 2019 webkul. All rights reserved.
//

import UIKit
import Reusable
import ActionSheetPicker_3_0

class DorpDownTableViewCell: UITableViewCell, FormConformity, NibReusable {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var hedingLabel: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    var tableView: UITableView?
    var formItem: FormItem?
    override func awakeFromNib() {
        super.awakeFromNib()
        if let languageCode =  UserDefaults.standard.object(forKey: "language") as? String{
            if languageCode == "ar" {
                if #available(iOS 9.0, *) {
                    textField.semanticContentAttribute = .forceRightToLeft
                    textField.textAlignment = .right
                }
            }else {
                L102Language.setAppleLAnguageTo(lang: "en")
                
                if #available(iOS 9.0, *) {
                    textField.semanticContentAttribute = .forceLeftToRight
                    textField.textAlignment = .left
                }
            }
        }
         arrow.roundCorners(corners: [.bottomRight,.topRight], radius: 5)
        textField.isUserInteractionEnabled = true
        textField.delegate = self
        textField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapDropView)))
    }
    
    func textFieldDidChanged(_ textField: UITextField) {
        self.formItem?.valueCompletion?(textField.text)
        
    }

    @objc private func didTapDropView() {
        if let formItem = formItem {
            var rowValue = 0
            if let val = self.formItem?.value as? String, let index = formItem.dropDownArray.index(where: {$0 == val }) {
                rowValue = index
            }
            UIBarButtonItem.appearance().tintColor = UIColor.black
            let gg =   ActionSheetStringPicker(title: formItem.placeholder3, rows: formItem.dropDownArray.map { $0 }, initialSelection: rowValue, doneBlock: {
                picker, indexes, values in
                self.textField.text = formItem.dropDownArray[safe:indexes]
                formItem.value = formItem.dropDownArray[safe:indexes]
                 self.formItem?.actionCompletion?("chnageCompanyType")
                return
            }, cancel: { ActionStringCancelBlock in
                UIBarButtonItem.appearance().tintColor = UIColor.white
                return }, origin: self.textField)
            gg?.setCancelButton(UIBarButtonItem.init(title: "Cancel".localized, style: .done, target: self, action: nil))
            gg?.setDoneButton(UIBarButtonItem.init(title: "Done".localized, style: .done, target: self, action: nil))
            gg?.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]//this is actually the title of toolbar
            gg?.toolbarButtonsColor = UIColor.blue
            gg?.show()
        }
    }
}

extension DorpDownTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.formItem?.actionCompletion?("chnageCompanyType")
    }
}

extension DorpDownTableViewCell: FormUpdatable {
    func update(with formItem: FormItem) {
        self.formItem = formItem
        self.hedingLabel.text = formItem.heading
        self.textField.isSecureTextEntry = formItem.isSecure
        if let val = self.formItem?.value as? String, let index = formItem.dropDownArray.index(where: {$0 == val }) {
            self.textField.text = formItem.dropDownArray[safe:index]
        } else {
           self.textField.text = ""
        }
        self.hedingLabel.text = self.formItem?.placeholder
        self.textField.accessibilityHint = self.formItem?.keyType
        self.textField.placeholder = self.formItem?.placeholder
        self.textField.keyboardType = self.formItem?.uiProperties.keyboardType ?? .default
        self.textField.tintColor = self.formItem?.uiProperties.tintColor
    }
}
