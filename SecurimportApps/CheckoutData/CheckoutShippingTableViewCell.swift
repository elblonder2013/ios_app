//
//  CheckoutShippingTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 06/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON

class CheckoutShippingTableViewCell: UITableViewCell {

    @IBOutlet weak var changeAddressButton: UIButton!
    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var shippingAddressLabel: UILabel!
    @IBOutlet weak var shippingView: UIView!
    var delegate: NewViewControllerOpen?

    override func awakeFromNib() {
        super.awakeFromNib()
        shippingView.myBorder()
//        shippingView.backgroundColor = UIColor.clear
        shippingAddressLabel.text = shippingAddress.localized
        shippingAddressLabel.SetAccentTextColor()
        addAddressButton.setTitle("addNewAddress".localized, for: .normal)
        changeAddressButton.setTitle("ChangeAddress".localized, for: .normal)
        addAddressButton.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)
        changeAddressButton.setTitleColor(GlobalData.Credentials.accentColor, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    @IBAction func AddAddressClicked(_ sender: UIButton) {
        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.addAddress)
    }

    @IBAction func ChangeAddressClicked(_ sender: UIButton) {
        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.changeAddress)
    }

}
