//
//  ProductCategoryViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 31/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
import Toaster

class ProductCategoryViewModal: NSObject {

    var delegate: NewViewControllerOpen?

    var products = [Product]()
    var passDataDelegate: PassData?
    var page = 0
    var total = 0
    var collection: UICollectionView?
    var loadData = false
    var wishlistIds = [WishlistIds]()
    var navigationItem: UINavigationItem?
    var categoryName: String = "" 
   var productAttributes = [ProductAttributes]()
    var collectionLayoutType : layoutLype = .list
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = ProductCategoryModal(data : jsonData) else {
            return
        }

       loadData = true
        total = jsonData["tcount"].intValue
        if !data.products.isEmpty {

            for i in 0..<data.products.count {
                products.append(data.products[i])
            }
            if !data.wishlistIds.isEmpty {
                wishlistIds = data.wishlistIds
            }
            if page==0 && productAttributes.count == 0 {
                productAttributes = data.productAttributes
            }

            completion(true)
        } else {
            completion(false)
        }
    }
}

extension ProductCategoryViewModal: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionLayoutType == .grid{
            
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCategoryCollectionViewCell.identifier, for: indexPath as IndexPath) as! ProductCategoryCollectionViewCell
         cell.wishlistIds = wishlistIds
        cell.item = products[indexPath.row]
        cell.wishlistICOn.tag = indexPath.row
        cell.delgate = self
        return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCategoryListCollectionViewCell.identifier, for: indexPath as IndexPath) as! ProductCategoryListCollectionViewCell
            cell.wishlistIds = wishlistIds
            cell.item = products[indexPath.row]
            cell.wishlistICOn.tag = indexPath.row
             cell.buyNowBtn.tag = indexPath.row
            cell.delgate = self
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          if collectionLayoutType == .grid{
            return CGSize(width: (SCREEN_WIDTH / 2 - 1 ), height: (SCREEN_WIDTH / 2) + 80)
          }else{
            var height = products[indexPath.row].shortDescription.height(withConstrainedWidth: SCREEN_WIDTH - 160)
            height = height + 140
            if height < 230{
                height = 240
            }
            print(height)
            return CGSize(width: SCREEN_WIDTH, height: height)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.moveToController(id: products[indexPath.row].templateId!, name: products[indexPath.row].productId!, dict: JSON.null, Controller: Controllers.product)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

}

extension ProductCategoryViewModal: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let col = collection {
           let currentCellCount = col.numberOfItems(inSection: 0)

            for cell: UICollectionViewCell in col.visibleCells {
             let   indexPathValue = col.indexPath(for: cell)!
                
                self.navigationItem?.title = String.init(format: "%@ (%i) - (%i)", self.categoryName, indexPathValue.row + 1, total)

                if indexPathValue.row == col.numberOfItems(inSection: 0) - 1  && total > currentCellCount && loadData {

                    loadData = false
                    page = page + 1
                    passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: indexPathValue.row + 1, call: .none)
                }
            }
        }
    }
}

extension ProductCategoryViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {

        switch call {
        case .addToWishlist:
            if wishlistIds.contains(where: {$0.wishlistId ==  products[index].productId!}) {
                ErrorChecking.warningView(message: "Already in wishlist")
            } else {
                let dict = ["productId": products[index].productId!]
                passDataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: index, call: call)
            }
        case .buyNow:
            let dict = ["productId": products[index].productId!,"add_qty":1] as [String : Any]
            passDataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: index, call: call)
        case .products:
           delegate?.moveToController(id: products[index].templateId!, name: products[index].productId!, dict: JSON.null, Controller: Controllers.product)
        default:
            break
        }
        

    }
}
enum layoutLype{
    case grid
    case list
}
