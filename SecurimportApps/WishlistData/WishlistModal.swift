//
//  WishlistModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WishlistModal {
     var products = [Product]()

    init?(data: JSON) {
        Addons.AddonsChecks(data: data)
        if let products = data["wishLists"].array {
            self.products = products.map {
                Product(json: $0)
            }
        }
    }
}
