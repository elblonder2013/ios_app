//
//  DashboardBillingAddressTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 13/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class DashboardBillingAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
         addressView.myBorder()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
