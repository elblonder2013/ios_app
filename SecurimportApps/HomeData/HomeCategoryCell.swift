//
//  HomeCategoryCell.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SearchAction {
    func doSearchAction()
    func notificationAction()
    func openCameraAction(sender: UIButton)
}

class HomeCategoryCell: UITableViewCell {

    var delegate: SearchAction?
    var viewModel  = HomeViewModel()
     var categories = [CategorySet]()
    var delegate1: PassData?
    @IBOutlet weak var homeCategoryCollection: UICollectionView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var searchProductsLabel: UILabel!
    @IBOutlet weak var categoryCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var camera_btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        searchProductsLabel.textColor = GlobalData.Credentials.defaultTextColor
        camera_btn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
        camera_btn.tintColor = .lightGray
        homeCategoryCollection.register(HomeCategoryCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCategoryCollectionViewCell.identifier)

        homeCategoryCollection.setNeedsLayout()
        homeCategoryCollection.sizeToFit()
        homeCategoryCollection.applyBorder(colours: GlobalData.Credentials.setBorderColor)

         selectionStyle = .none
        if GlobalData.Credentials.environment == EnvironmentType.marketplace {
            appNameLabel.text = "appNameMP".localized
        } else {
            appNameLabel.text = "appName".localized
        }
       

//        gradientView.applyGradient(colours: GlobalData.Credentials.gradientArray)
        homeCategoryCollection.delegate = self
        homeCategoryCollection.dataSource = self
        searchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
        searchProductsLabel.text = searchProducts.localized
    }

//    @objc private func didTapHeader() {
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

     @objc private func didTapHeader() {
        delegate?.doSearchAction()
    }

    @IBAction func notificationTap(_ sender: Any) {
        delegate?.notificationAction()
    }
    
    @IBAction func tapCamera_btn(_ sender: UIButton) {
        delegate?.openCameraAction(sender: sender)
    }
    
}

extension HomeCategoryCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCategoryCollectionViewCell", for: indexPath as IndexPath) as! HomeCategoryCollectionViewCell
               cell.backgroundColor = UIColor.clear
        cell.item = categories[indexPath.row]
        if let lay = gradientView.layer.sublayers {
            for layers in lay {
                layers.removeFromSuperlayer()
            }
        }
        gradientView.applyGradient(colours: GlobalData.Credentials.gradientArray)
        cell.setNeedsLayout()

        return cell
        //        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 90)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if categories[indexPath.row].subCategories == JSON.null{
         delegate1?.passData(id: categories[indexPath.row].CategoryId ?? "", name: categories[indexPath.row].categoryName ?? "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.categories)
        }else{
            let view = viewController(forStoryboardName: homeStoryboard, forViewControllerName : "categoryController") as! CategoryController
            view.categoryData = categories[indexPath.row].subCategories
            view.name = categories[indexPath.row].categoryName ?? ""
            UIApplication.topViewController()?.navigationController?.pushViewController(view, animated: true)
        }
    }

}
