//
//  ShippingMethodsTableViewCell.swift
//  PrestashopHyperlocalMVVM
//
//  Created by bhavuk.chawla on 06/12/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class ShippingMethodsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
     @IBOutlet weak var priceLabel: UILabel!
     @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var CheckboxImage: UIImageView!
    var shippingId: String?

    override func awakeFromNib() {
        super.awakeFromNib()
         priceLabel.SetDefaultTextColor()
         messageLabel.SetDefaultTextColor()
         CheckboxImage.applyBorder(colours: GlobalData.Credentials.accentColor)
//         CheckboxImage.applyBorder(colours: GlobalConstants.Colors.accentColor)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: ShippingData? {
        didSet {
            nameLabel.text = item?.name
            priceLabel.text = item?.price
            messageLabel.text = item?.description
            if shippingId == item?.id {
                CheckboxImage.backgroundColor = GlobalData.Credentials.accentColor
                CheckboxImage.image = #imageLiteral(resourceName: "check-icon24x24")
            } else {
                CheckboxImage.backgroundColor = UIColor.clear
                CheckboxImage.image = nil
            }
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
