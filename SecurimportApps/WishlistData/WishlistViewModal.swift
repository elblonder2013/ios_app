//
//  WishlistViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class WishlistViewModal: NSObject {

    var delegate: NewViewControllerOpen?
   var products = [Product]()
      var passdataDelegate: PassData?
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = WishlistModal(data : jsonData) else {
            return
        }
            if !data.products.isEmpty {
               products = data.products
                completion(true)
            } else {
                completion(false)
            }
    }
}

extension WishlistViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: WishlistProductTableViewCell.identifier) as! WishlistProductTableViewCell
        cell.item = products[indexPath.row]
        cell.selectionStyle = .none
        cell.delegate = self
        cell.removeButton.tag = indexPath.row
        cell.cartbutton.tag = indexPath.row
        return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.moveToController(id: products[indexPath.row].templateId!, name: products[indexPath.row].productId!, dict: JSON.null, Controller: Controllers.product)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension WishlistViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        if call == WhichApiUse.moveToCart {
                 var dict = [String: Any]()
                 dict["wishlistId"] = products[index].wishlistId!
               passdataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: call)
        } else {
            passdataDelegate?.passData(id: products[index].wishlistId!, name: "", dict: [:], jsonData: JSON.null, index: 0, call: call)
        }
    }
}
