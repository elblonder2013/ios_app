//
//  HomeCollectionViewCell.swift
//  CS-Cart MVVM
//
//  Created by vipin sahu on 6/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productOldPrice: UILabel!

    @IBOutlet weak var checkForPrice:UIButton!
     @IBOutlet weak var checkForPriceButtonHeight:NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.productOldPrice.SetDefaultTextColor()
        cellView.myBorder()
        checkForPrice.setTitle("Check for price".localized, for: .normal)
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: Product? {
        didSet {
            checkForPriceButtonHeight.constant = 0
            checkForPrice.isHidden = true
            productPrice.isHidden = true
            guard let item = item else {
                return
            }

            productName.textColor = UIColor().HexToColor(hexString: GlobalData.Credentials.textColor)
            productName.text = item.name
            productName.font = UIFont(name: GlobalData.Credentials.fontName, size: 14.0)
            productPrice.text = item.reducePrice
            productPrice.font = UIFont(name: GlobalData.Credentials.fontName, size: 14.0)
            productImage.contentMode = .scaleAspectFit

            if item.reducePrice!.count > 1 {
                productPrice.text = item.reducePrice
                productOldPrice.text = item.price
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: item.price!)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                productOldPrice.attributedText = attributeString
            } else {
                productPrice.text = item.price
                productOldPrice.text = item.reducePrice
            }

            if !GlobalData.AddOnsEnabled.productsPriceAvailable {
                checkForPriceButtonHeight.constant = 30
                checkForPrice.isHidden = false
                productOldPrice.isHidden = true
                productPrice.isHidden = true
            }else{
                checkForPriceButtonHeight.constant = 0
                checkForPrice.isHidden = true
                productOldPrice.isHidden = false
                productPrice.isHidden = false
            }
            productImage.setImage(imageUrl: item.pictureUrl!, controller: .homeProduct)

        }
    }

    @IBAction func checkForPriceButtonPrice(){
        if let topview = UIApplication.topViewController(){
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: signcontrollerIdentifier) as! SignInViewController
            let nav  = UINavigationController(rootViewController: view)
            topview.present(nav, animated: true, completion: nil)
        }
       
    }

}
