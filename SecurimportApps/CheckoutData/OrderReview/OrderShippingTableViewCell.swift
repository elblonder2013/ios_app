//
//  OrderShippingTableViewCell.swift
//  NewTheme V4
//
//  Created by bhavuk.chawla on 06/01/18.
//  Copyright © 2018 Bhavuk. All rights reserved.
//

import UIKit

class OrderShippingTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var shippingMethodName: UILabel!
    @IBOutlet weak var shippingPrice: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
}
