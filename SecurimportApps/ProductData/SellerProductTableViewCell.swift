//
//  SellerProductTableViewCell.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 12/02/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit

class SellerProductTableViewCell: UITableViewCell {
    @IBOutlet weak var starView: UIView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var sellerImage: UIImageView!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var soldByLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        soldByLabel.SetDefaultTextColor()
        starView.applyGradientToTopView(colours:  GlobalData.Credentials.reviewGradient)
        sellerImage.layer.masksToBounds = true
        sellerImage.layer.cornerRadius = 4
        soldByLabel.text = "soldBy".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: SellerProductData! {
        didSet {
            ratingLabel.text = item.average_rating
            sellerImage.setImage(imageUrl: item.seller_profile_image, controller: Controllers.Profile)
            sellerName.text = item.seller_name
        }
    }

}
