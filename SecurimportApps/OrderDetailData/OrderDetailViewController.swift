//
//  OrderDetailViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderDetailViewController: UIViewController {

    var id: String?
    var name: String?

    @IBOutlet weak var orderDetailTableView: UITableView!
    fileprivate let orderDetailViewModalObject = OrderDetailViewModal()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeRequest()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "orderSummary".localized
        orderDetailTableView.register(OrderDetailTableViewCell.nib, forCellReuseIdentifier: OrderDetailTableViewCell.identifier)
        orderDetailTableView.register(ProductOrderTableViewCell.nib, forCellReuseIdentifier: ProductOrderTableViewCell.identifier)
         orderDetailTableView.register(OrderShippingTableViewCell.nib, forCellReuseIdentifier: OrderShippingTableViewCell.identifier)
        orderDetailTableView.dataSource = orderDetailViewModalObject
        orderDetailViewModalObject.delegate = self
        orderDetailTableView.delegate = orderDetailViewModalObject
        orderDetailViewModalObject.orderId = name
        // Do any additional setup after loading the view.
    }

    func makeRequest() {

        let params = GlobalData.apiName.orderDetail + id!
        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: RequestType.post, controler: Controllers.orderDetails) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                self.orderDetailViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    self.orderDetailTableView.reloadData()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrderDetailViewController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productDataViewControllerIdentifier) as! ProductDataViewController
        view.templateId = id
        view.name = name
        self.navigationController?.pushViewController(view, animated: true)
    }
}
