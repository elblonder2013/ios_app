//
//  OrderDetailTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 05/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit

class OrderDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var placedOnLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var totalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        placedOnLabel.SetDefaultTextColor()
        // Initialization code
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: orderHistoryData? {
        didSet {
            statusLabel.text = item?.status
            placedOnLabel.text = placedOn.localized
            dateLabel.text = item?.date
            taxLabel.text = String.init(format: "Tax - %@", (item?.taxes!)!)
            taxLabel.halfTextColorChange(fullText: taxLabel.text!, changeText: "Tax")
            totalLabel.text = String.init(format: "%@ - %@", orderTotal.localized, (item?.total)!)
            totalLabel.halfTextColorChange(fullText: totalLabel.text!, changeText: orderTotal.localized)

            if item?.status == "sale"{
                statusIcon.image = #imageLiteral(resourceName: "Status-Complete")
            } else {
                statusIcon.image = #imageLiteral(resourceName: "Status-Pending")
            }

        }
    }

}
