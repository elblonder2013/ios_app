//
//  ProductCategoryModal.swift
//  Odoo application
//
//  Created by Bhavuk on 31/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ProductCategoryModal {

    var productAttributes = [ProductAttributes]()
     var products = [Product]()
     var wishlistIds = [WishlistIds]()
    init?(data: JSON) {

       Addons.AddonsChecks(data: data)

        if let array  = data["products"].array {
            self.products = array.map { Product(json: $0) }
        }
        if let array  = data["wishlist"].array {
            if array.count > 0 {
                self.wishlistIds = array.map { WishlistIds(data: $0.stringValue) }
            }
        }
        if (data["productAttributes"].array != nil){
            productAttributes = data["productAttributes"].arrayValue.map{ProductAttributes(data: $0) }
        }
    }
}

struct WishlistIds {
    var wishlistId: String
    init(data: String) {
        self.wishlistId = data
    }
}
struct ProductAttributes {
    var attributeID: Int!
    var values = [Value]()
    var name: String!
    var newVariant: Bool!
    var type: String!
    var value : Int = -1
    init(data:JSON) {
        attributeID = data["attributeId"].intValue
        newVariant = data["newVariant"].boolValue
        type = data["type"].stringValue
        name = data["name"].stringValue
        values = data["values"].arrayValue.map{Value(data:$0) }
    }
    
}

// MARK: - Value
struct Value  {
    var name, htmlCode: String!
    var attribValueID = [Int]()
    var valueID: Int!
    var newVariant: Bool!
    init(data:JSON) {
        valueID = data["valueId"].intValue
        newVariant = data["newVariant"].boolValue
        attribValueID = data["attrib_value_id"].arrayValue.map{$0.intValue}
        name = data["name"].stringValue
        htmlCode = data["htmlCode"].stringValue
        
    }
}
