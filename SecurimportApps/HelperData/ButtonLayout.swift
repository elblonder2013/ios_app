//
//  ButtonLayout.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 26/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CustomAccentButton: UIButton {
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        sharedInit()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        sharedInit()
//    }
//
//
//    override func layoutSubviews() {
//       sharedInit()
//    }
    
    @IBInspectable var buttonColor: UIColor = GlobalData.Credentials.accentColor {
        didSet {
            backgroundColor = buttonColor
        }
    }
    
    func sharedInit() {
        // Common logic goes here
        self.layer.cornerRadius = 7
        self.backgroundColor = buttonColor
    }
}
