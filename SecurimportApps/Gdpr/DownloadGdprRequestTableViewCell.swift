//
/**
Odoo Marketplace
@Category Webkul
@author    Webkul
Created by: jitendra on 06/08/18
FileName: DownloadGdprRequestTableViewCell.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license   https://store.webkul.com/license.html
*/


import UIKit

class DownloadGdprRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var downloadViewHeight: NSLayoutConstraint!
    @IBOutlet weak var downloadAllInfo: UIButton!
    
    @IBOutlet weak var topHeughtOfDownloadView: NSLayoutConstraint!
    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var downloadBtnforRequest: UIButton!
    @IBOutlet weak var downlaodDataLbl: UILabel!
    var delegate:AccountDeacticvateAction!
    override func awakeFromNib() {
        super.awakeFromNib()
        downloadAllInfo.setTitle("downloadAllInformation".localized, for: .normal)
        // Initialization code
    }
    
    @IBAction func downloadBtnAct(_ sender: Any) {
         delegate.downloadGdprRequest(type: "downloadGdprRequest")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func DowloadRequestAct(_ sender: Any) {
       
        delegate.downloadAllInfo(type: "downloadAllInfo")
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
