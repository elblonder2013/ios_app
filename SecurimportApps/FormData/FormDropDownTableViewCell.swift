//
//  FormDropDownTableViewCell.swift
//  MobikulOpencartMp
//
//  Created by bhavuk.chawla on 05/11/18.
//  Copyright © 2018 yogesh. All rights reserved.
//

import UIKit
import Reusable
import ActionSheetPicker_3_0
    
class FormDropDownTableViewCell: UITableViewCell, FormConformity, NibReusable {
        
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var stateHeadingLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var hedingLabel: UILabel!
     @IBOutlet weak var countryArrow: UIImageView!
    @IBOutlet weak var stateArrow: UIImageView!

    var tableView: UITableView?
        var formItem: FormItem?
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
            if let languageCode =  UserDefaults.standard.object(forKey: "language") as? String{
                
                if languageCode == "ar" {
                    
                    if #available(iOS 9.0, *) {
                        textField.semanticContentAttribute = .forceRightToLeft
                        textField.textAlignment = .right
                        stateTextField.textAlignment = .right
                        stateTextField.semanticContentAttribute = .forceRightToLeft
                    }
                }else {
                    L102Language.setAppleLAnguageTo(lang: "en")
                    
                    if #available(iOS 9.0, *) {
                        textField.semanticContentAttribute = .forceLeftToRight
                        stateTextField.semanticContentAttribute = .forceLeftToRight
                        textField.textAlignment = .left
                        stateTextField.textAlignment = .left
                    }
                }
            }
            textField.isUserInteractionEnabled = true
            textField.delegate = self
            stateTextField.delegate = self
            textField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapCountryView)))
            stateTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapStateView)))
           countryArrow.roundCorners(corners: [.bottomRight,.topRight], radius: 5)
             stateArrow.roundCorners(corners: [.bottomRight,.topRight], radius: 5)
        }
        
        @objc func textFieldDidChanged(_ textField: UITextField) {
            self.formItem?.valueCompletion?(textField.text)
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
        }
    
    @objc private func didTapStateView() {
        if let formItem = formItem ,
            let val1 = self.formItem?.value as? String,
            let index = formItem.countryData.index(where: {$0.countryid == val1 }) {
              let countryData = formItem.countryData[index]
             var rowValue = 0
            if let val = self.formItem?.value2 as? String, let index = countryData.stateArray.index(where: {$0.stateid == val }) {
                rowValue = index
            }
              UIBarButtonItem.appearance().tintColor = UIColor.black
          let gg =   ActionSheetStringPicker(title: formItem.placeholder2, rows: countryData.stateArray.map {$0.stateName }, initialSelection: rowValue, doneBlock: {
                picker, indexes, values in
            
            self.stateTextField.text = countryData.stateArray[safe:indexes]?.stateName
            formItem.value2 = countryData.stateArray[safe:indexes]?.stateid
          
                return
            }, cancel: { ActionStringCancelBlock in
                 UIBarButtonItem.appearance().tintColor = UIColor.white
                return }, origin: self.textField)
            gg?.setCancelButton(UIBarButtonItem.init(title: "Cancel".localized, style: .done, target: self, action: nil))
            gg?.setDoneButton(UIBarButtonItem.init(title: "Done".localized, style: .done, target: self, action: nil))
            gg?.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]//this is actually the title of toolbar
            gg?.toolbarButtonsColor = UIColor.blue
            gg?.show()
        }
    }
    
    @objc private func didTapCountryView() {
         if let formItem = formItem , let val = formItem.countryData as? [CountryData]{
            var rowValue = 0
            if  let val1 = self.formItem?.value as? String,
                let index = formItem.countryData.index(where: {$0.countryid == val1 })  {
                rowValue = index
            }
             UIBarButtonItem.appearance().tintColor = UIColor.black
            
            let gg =  ActionSheetStringPicker(title: formItem.placeholder, rows: val.map { $0.countryName ?? "" }, initialSelection: rowValue, doneBlock: {
            picker, indexes, values in
            self.textField.text = val[safe:indexes]?.countryName
            formItem.value = val[safe:indexes]?.countryid
            if  val[safe:indexes]?.stateArray.count ?? 0 > 0 {
                self.stateTextField.text = val[safe:indexes]?.stateArray[safe:0]?.stateName
                formItem.value2 = val[safe:indexes]?.stateArray[safe:0]?.stateid
                
                } else {
                     formItem.value2 = nil
                formItem.value3 = nil
                 formItem.value4 = nil
                    
                }
            self.tableView?.reloadData()
        }, cancel: { ActionStringCancelBlock in
              UIBarButtonItem.appearance().tintColor = UIColor.white
            return }, origin: self.textField)
            
            gg?.setCancelButton(UIBarButtonItem.init(title: "Cancel".localized, style: .done, target: self, action: nil))
            gg?.setDoneButton(UIBarButtonItem.init(title: "Done".localized, style: .done, target: self, action: nil))
            gg?.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]//this is actually the title of toolbar
            gg?.toolbarButtonsColor = UIColor.blue
        
           
            gg?.show()
        }
    }
}

extension FormDropDownTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
         textField.endEditing(true)
         return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
    }
}
    
extension FormDropDownTableViewCell: FormUpdatable {
    func update(with formItem: FormItem) {
        self.formItem = formItem
        self.hedingLabel.text = formItem.heading
        self.textField.isSecureTextEntry = formItem.isSecure
        if let val = self.formItem?.value as? String, let index = formItem.countryData.index(where: {$0.countryid == val }) {
            self.textField.text =  formItem.countryData[safe:index]?.countryName
            let countryData = formItem.countryData[safe:index]
             if let val = self.formItem?.value2 as? String, let index = countryData?.stateArray.index(where: {$0.stateid == val }) {
                self.stateTextField.text = countryData?.stateArray[safe:index]?.stateName
                let stateData = countryData?.stateArray[safe:index]
             } else {
                if countryData?.stateArray.count ?? 0 > 0 {
                    formItem.value2 = countryData?.stateArray[safe:0]?.stateid
                    self.stateTextField.text = countryData?.stateArray[safe:0]?.stateName
                } else {
                    formItem.value2 = ""
                    self.stateTextField.text = ""
                    formItem.value3 = ""
                    formItem.value4 = ""
                }
            }
        } else {
            if formItem.countryData.count > 0 {
                self.textField.text =  formItem.countryData[safe:0]?.countryName
                formItem.value = formItem.countryData[safe:0]?.countryid
                let countryData = formItem.countryData[safe:0]
                if countryData?.stateArray.count ?? 0 > 0 {
                    formItem.value2 = countryData?.stateArray[safe:0]?.stateid
                    self.stateTextField.text = countryData?.stateArray[safe:0]?.stateName
                } else {
                    formItem.value2 = ""
                    self.stateTextField.text = ""
                   
                }
            }
        }
        self.textField.accessibilityHint = self.formItem?.keyType
        if let image = formItem.image {
            self.textField.setLeftIcon(image)
        }

        if let image = formItem.image2 {
            self.stateTextField.setLeftIcon(image)
        }
        self.stateHeadingLabel.text = self.formItem?.placeholder2
        self.textField.placeholder = self.formItem?.placeholder
        self.textField.keyboardType = self.formItem?.uiProperties.keyboardType ?? .default
        self.textField.tintColor = self.formItem?.uiProperties.tintColor
    }
}
    

extension UITextField {
    
    /// set icon of 20x20 with left padding of 8px
    func setRightIcon(_ icon: UIImage) {
        
        let padding = 8
        let size = 16
        
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
        let iconView  = UIImageView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        iconView.image = icon
        outerView.addSubview(iconView)
        
        rightView = outerView
        rightViewMode = .always
        
    }
}
