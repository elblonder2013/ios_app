//
//  DashBoardDatalViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 30/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class DashBoardDatalViewController: UIViewController {

    @IBOutlet weak var DashBoardTableView: UITableView!
    private let dashBoardViewModalObject = DashBoardViewModal()
    
    @IBOutlet weak var logOutBtn: UIButton!
    private let offset = "offset"
    private let limit = "limit"
    
    let group = DispatchGroup()
    let queue = DispatchQueue.global(qos: .background)

    var requestType: Dashboard = .order
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = dashboard.localized
        dashBoardViewModalObject.moveDelegate = self
        // Do any additional setup after loading the view.
       DashBoardTableView.register(OrderHistoryViewCell.nib, forCellReuseIdentifier: OrderHistoryViewCell.identifier)
        DashBoardTableView.register(DashboardAccountTableViewCell.nib, forCellReuseIdentifier: DashboardAccountTableViewCell.identifier)
        DashBoardTableView.register(DashboardBillingAddressTableViewCell.nib, forCellReuseIdentifier: DashboardBillingAddressTableViewCell.identifier)
        DashBoardTableView.register(DashBoardShippingTableViewCell.nib, forCellReuseIdentifier: DashBoardShippingTableViewCell.identifier)
        DashBoardTableView.delegate = dashBoardViewModalObject
        DashBoardTableView.dataSource = dashBoardViewModalObject
        DashBoardTableView.estimatedRowHeight = 100
        DashBoardTableView.rowHeight = UITableView.automaticDimension
        dashBoardViewModalObject.delegate = self
//        DashBoardTableView.separatorStyle = .none
        logOutBtn.setTitle("logout".localized, for: .normal)
        let queue = DispatchQueue(label: "com.webkul.OdooMarketplace")
        logOutBtn.backgroundColor = GlobalData.Credentials.accentColor
        
        
        self.makeRequest()
    }

    func makeRequest( ) {
        var params = ""
        var dict = [String: Any]()

        print(requestType)
        
        switch requestType {
        case .order:
               params = GlobalData.apiName.orders
               dict[offset] = 0
               dict[limit] = 5
        case .address:
              params = GlobalData.apiName.getAddress
              dict[offset] = 0
              dict[limit] = 5
        case .signOut:
             params = GlobalData.apiName.signOut
                let device_id = UIDevice.current.identifierForVendor?.uuidString
             dict["fcmDeviceId"] = device_id!
            default:
            print()
        }

        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: RequestType.post, controler: Controllers.address) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {

                switch self.requestType {
                case .order:
                    self.dashBoardViewModalObject.items.removeAll()
                    self.dashBoardViewModalObject.getValue(jsonData: responseObject!, cases: self.requestType) {
                        (data: Bool) in

                        if data {
                           self.DashBoardTableView.reloadData()
                            self.requestType = .address
                            self.makeRequest()
                        } else {
                             self.DashBoardTableView.reloadData()
                            self.requestType = .address
                            self.makeRequest()
                        }
                    }

                case .address:
                    self.dashBoardViewModalObject.getValue(jsonData: responseObject!, cases: self.requestType) {
                        (data: Bool) in

                        if data {
                             self.DashBoardTableView.reloadData()
                        }
                    }
                case .signOut:

                    for i in UserDefaults.standard.dictionaryRepresentation() {
                        print(i.key)
                        if i.key != firebaseKey && i.key != "loadLanguage" && i.key != "defaultLanguage"   && i.key != AppLanguageKey   {
                            UserDefaults.standard.removeObject(forKey: i.key)
                            UserDefaults.standard.synchronize()
                        }
                    }
                    UserDefaults.standard.set("yes", forKey: "loadLanguage")
                    UserDefaults.standard.synchronize()
                    self.performSegue(withIdentifier: "home", sender: self)
                case .shipping:
                    print()
                case .account:
                     print()
                }
            }
        }
    }

    @IBAction func LogoutPressed(_ sender: UIButton) {
         self.requestType = .signOut
       self.makeRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DashBoardDatalViewController: NewViewControllerOpen {

    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {

        if Controller == Controllers.address {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
            view.addressId = id
             view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }
        if Controller == Controllers.orderDetails {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: orderDetailViewControllerIdentifier) as! OrderDetailViewController
            view.id = id
            view.name = name
            self.navigationController?.pushViewController(view, animated: true)
        }

        if Controller ==  Controllers.accountInfo {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: accountInfoDataViewControllerIdentifier) as! AccountInfoDataViewController
            self.navigationController?.pushViewController(view, animated: true)
        }

        if Controller == Controllers.editAddress {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
            view.addressId = id
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }

        if Controller == Controllers.addAddress {
            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: newAddressDataViewControllerIdentifier) as! NewAddressDataViewController
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }

        if Controller == Controllers.changeAddress {

            let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: "changeAddressViewController") as! ChangeAddressViewController
            view.delegate = self
            view.controller = Controllers.accountInfo
            self.navigationController?.pushViewController(view, animated: true)
        }
    }

}

extension DashBoardDatalViewController: AddressUpdate {
    func addressupdated(addressId: String) {
        requestType = .order
        self.makeRequest()
    }

}
