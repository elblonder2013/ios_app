//
//  CartViewModal.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class CartViewModal: NSObject {
    var passdataDelegate: PassData?
    var delegate: NewViewControllerOpen?
    var items = [CartViewModalItem]()

    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = CartModal(data : jsonData
   ) else {
            return
        }
        items.removeAll()
        print(data.products)
        if !data.products.isEmpty {
            items.append(CartViewModalProductsItem(products: data.products))
            items.append(CartViewModalPriceItem(cartPrice: data.cartPrice!))
             completion(true)
        } else {
             completion(false)
        }

    }
}

extension CartViewModal: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]

        switch item.type {
        case .products:
            if let item = item as? CartViewModalProductsItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: CartProductTableViewCell.identifier) as! CartProductTableViewCell
                cell.item = item.products[indexPath.row]
                cell.stepperView.tag = indexPath.row
                cell.oldValue = Double(item.products[indexPath.row].qty ?? "0")!
                cell.wishlistbutton.tag = indexPath.row
                cell.removeButton.tag = indexPath.row
                cell.selectionStyle = .none
                cell.delegate = self
                return cell
            }
        case .price :
            if let item = item as? CartViewModalPriceItem {
                let cell = tableView.dequeueReusableCell(withIdentifier: CartPriceTableViewCell.identifier) as! CartPriceTableViewCell
                cell.item = item.cartPrice
                 cell.selectionStyle = .none
                cell.delegate = self
                return cell
            }

        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.section]
        if let item = item as? CartViewModalProductsItem {
            print(item.products[indexPath.row])
            delegate?.moveToController(id: item.products[indexPath.row].templateId!, name: item.products[indexPath.row].name!, dict: JSON.null, Controller: Controllers.product)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

//    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]?
}

extension CartViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        if let item = items[0] as? CartViewModalProductsItem {
            var dict = [String: Any]()
            switch call {
            case.updateCartPlus:
                dict["set_qty"] = String(Int(item.products[index].qty ?? "0")! + 1)
                passdataDelegate?.passData(id: item.products[index].lineId!, name: "", dict: dict, jsonData: JSON.null, index: 0, call: call)
            case .RemoveFromCart:
                dict.removeAll()
                passdataDelegate?.passData(id: item.products[index].lineId!, name: "", dict: dict, jsonData: JSON.null, index: 0, call: call)
            case .moveWishlist:
               dict["line_id"] = item.products[index].lineId
                passdataDelegate?.passData(id: item.products[index].lineId!, name: "", dict: dict, jsonData: JSON.null, index: 0, call: call)
            case .updateCartMinus:
                if item.products[index].qty != "1" {
                    dict["set_qty"] = String(Int(item.products[index].qty ?? "0")! - 1)
                    passdataDelegate?.passData(id: item.products[index].lineId!, name: "", dict: dict, jsonData: JSON.null, index: 0, call: call)
                } else {
                    print("chhch")
                }
            default:
                print()
        }
        if call == WhichApiUse.cartEmpty {
             passdataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: call)
        }
    }
    }

}

enum cartType {
    case products
    case price
}

protocol CartViewModalItem {
    var type: cartType { get }
    var rowCount: Int { get }
}

class CartViewModalProductsItem: CartViewModalItem {
    var type: cartType {
        return .products
    }

    var rowCount: Int {
        return products.count
    }

    var products: [Product]

    init(products: [Product] ) {
        self.products = products
    }
}

class CartViewModalPriceItem: CartViewModalItem {
    var type: cartType {
        return .price
    }

    var rowCount: Int {
        return 1
    }

    var cartPrice: CartData?

    init(cartPrice: CartData ) {
        self.cartPrice = cartPrice
    }
}
