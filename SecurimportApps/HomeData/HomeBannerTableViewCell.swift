//
//  HomeBannerTableViewCell.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

@objcMembers
class HomeBannerTableViewCell: UITableViewCell {

     var banner = [BannerSet]()
     var collectionType = HomeType.products
    var newView = CHIPageControlFresno()

    var delegate: PassData?
    @IBOutlet weak var page: UIView!
    @IBOutlet weak var bannerCollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()

        bannerCollection.register(HomeBannerCollectionViewCell.nib, forCellWithReuseIdentifier: HomeBannerCollectionViewCell.identifier)
         Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        bannerCollection.showsHorizontalScrollIndicator = false
        newView =  CHIPageControlFresno(frame: page.frame)
        newView.radius = 4
        newView.tintColor = GlobalData.Credentials.accentColor
        newView.currentPageTintColor = .black
        newView.center = CGPoint(x: SCREEN_WIDTH/2, y:  20/2)
        page.addSubview(newView)
        bannerCollection.delegate = self
        bannerCollection.dataSource = self
        selectionStyle = .none
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension HomeBannerTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            newView.numberOfPages = banner.count
             return banner.count

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBannerCollectionViewCell", for: indexPath as IndexPath) as! HomeBannerCollectionViewCell
        cell.backgroundColor =  GlobalData.Credentials.defaultBackgroundColor
        cell.item = banner[indexPath.row]
        cell.setNeedsLayout()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH / 2)

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if banner[indexPath.row].bannerType == "product" {
            delegate?.passData(id: banner[indexPath.row].bannerId!, name: banner[indexPath.row].bannerName!, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.products)
        }

        if banner[indexPath.row].bannerType == "category" {
             delegate?.passData(id: banner[indexPath.row].bannerId!, name: banner[indexPath.row].bannerName!, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.categories)
        }
        if banner[indexPath.row].bannerType == "custom" {
            delegate?.passData(id: banner[indexPath.row].domain, name: banner[indexPath.row].bannerName!, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.custom)
        }
        
        

    }

}

extension HomeBannerTableViewCell: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        newView.set(progress: Int(scrollView.contentOffset.x / SCREEN_WIDTH), animated: true)
    }

    func scrollAutomatically(_ timer1: Timer) {

        if let coll  = bannerCollection {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < banner.count - 1) {
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)

                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                } else {
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }

            }
        }

    }
}
