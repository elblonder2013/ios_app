//
//  StockQuantityBarView.swift
//  SecurimportApps
//
//  Created by debabrata on 01/08/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit

class StockQuantityBarView: UIView {
    
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View3: UIView!
    
  func reSetViewColor(){
        View1.backgroundColor = .groupTableViewBackground
        View2.backgroundColor = .groupTableViewBackground
        View3.backgroundColor = .groupTableViewBackground
    }
    
    func setBackgroundColor(level: String){
        reSetViewColor()
        switch level {
        case "no_stock":
            break
        case "low":
            View1.backgroundColor = UIColor().HexToColor(hexString: "#ef4870")
        case "medium":
             View1.backgroundColor = UIColor().HexToColor(hexString: "#f4ef66")
             View2.backgroundColor = UIColor().HexToColor(hexString: "#f4ef66")
        case "high":
             View1.backgroundColor = UIColor().HexToColor(hexString: "#9ec849")
             View2.backgroundColor = UIColor().HexToColor(hexString: "#9ec849")
             View3.backgroundColor = UIColor().HexToColor(hexString: "#9ec849")
        default:
            break
        }
    }
}
