//
//  AppDelegate.swift
//  Odoo application
//
//  Created by vipin sahu on 8/30/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FBSDKLoginKit
import GoogleSignIn
import UserNotifications

import FirebaseMessaging
//import TwitterKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let backImage = UIImage(named: "Back-Chevron-White")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -80.0), for: .default)
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        if let queries = UserDefaults.standard.value(forKey: "Queries") as? [String] {
            GlobalData.QueryArray.values = queries
            print()
        }
        if #available(iOS 11.0, *) {
            UIImageView.appearance().accessibilityIgnoresInvertColors = true
        }

//        BarButtonItemAppearance.tintColor = UIColor.white

        IQKeyboardManager.shared.enable = true

        IQKeyboardManager.shared.enableAutoToolbar = true
        switch GlobalData.Credentials.environment {
        case .marketplace:
            let filePath = Bundle.main.path(forResource: "GoogleService-InfoMarket", ofType: "plist")!
            print(filePath)
            if let options = FirebaseOptions(contentsOfFile: filePath) {
                FirebaseApp.configure(options: options)
            }
            print("")
        case .mobikul:
            let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
            if  let options = FirebaseOptions(contentsOfFile: filePath) {
                FirebaseApp.configure(options: options)
            }
            print("")
        }

        application.delegate = self
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        self.setView()
//         TWTRTwitter.sharedInstance().start(withConsumerKey:"9V0zLej6oqIjx9cZsgx82brG6", consumerSecret:"AZgqtwSt6zvnRv8b6lO9QjCKNDgqy3pttK7mQY453yO1GIO7kK")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
         DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            if let shortcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
           
                 if shortcutItem.type == "com.webkul.OdooMarketplace.notification-black"{
                    typeOfShortcut = shortcutItem.localizedTitle
                    NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
 
                }else  if shortcutItem.type == "com.webkul.OdooMarketplace.user-black"{
                    NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
                    
                }else  if shortcutItem.type == "com.webkul.OdooMarketplace.cart-Black"{
                    NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
                    
                } else {
                    NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
                    
                }
           
        }
  })
       
        
        return true
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        var handle:Bool = true
        handle = GIDSignIn.sharedInstance().handle(url as URL?,
                                                   sourceApplication: sourceApplication,
                                                   annotation: annotation)
        
        handle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//
        
        
        
        let options: [String: AnyObject] = [UIApplication.OpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject, UIApplication.OpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
        
//        handle = TWTRTwitter.sharedInstance().application(application, open: url, options: options)
        if  LISDKCallbackHandler.shouldHandle(url)
        {
            return LISDKCallbackHandler.application(application, open: url, sourceApplication: sourceApplication, annotation: nil)
        }
        else
        {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        
        if shortcutItem.type == "com.webkul.OdooMarketplace.notification-black"{
            NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
            
        }else  if shortcutItem.type == "com.webkul.OdooMarketplace.user-black"{
            NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
            
        }else  if shortcutItem.type == "com.webkul.OdooMarketplace.cart-Black"{
            NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
            
        } else {
          NotificationCenter.default.post(name:  NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)

        }
         
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        var valueTwitter: Bool = true
        
 
        //valueTwitter =  TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        
        return valueTwitter
    }

    
    func setView() {
        if let code = UserDefaults.standard.value(forKey: "defaultLanguage") as? String {
            let val =  code.split(separator: "_")[0]
            print(val)
            AppLanguageDefaultValue = String(val)
            UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
            if val == "ar" {
                L102Language.setAppleLAnguageTo(lang: "ar")
                if #available(iOS 9.0, *) {
                    UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
                    UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
           
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    
                } else {
                    // Fallback on earlier versions
                }
                
            } else {
                
                L102Language.setAppleLAnguageTo(lang: "en")
                if #available(iOS 9.0, *) {
                    UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                    UITabBar.appearance().semanticContentAttribute =  .forceLeftToRight
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    
                } else {
                    // Fallback on earlier versions
                }
                
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var tokenq = ""
        for i in 0..<deviceToken.count {
            tokenq = tokenq + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("token", tokenq)
        
        Messaging.messaging().apnsToken = deviceToken as Data
        Messaging.messaging().subscribe(toTopic: "/topics/global")
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().subscribe(toTopic: "/topics/global")
        UserDefaults.standard.set(fcmToken, forKey: firebaseKey)
         UserDefaults.standard.synchronize()
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

    

    let  gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)

        completionHandler(UIBackgroundFetchResult.newData)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

 
    func applicationWillTerminate(_ application: UIApplication) {
        UIApplication .shared.shortcutItems?.removeAll()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}
@available(iOS 10, *)
extension HomeController : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       willPresent notification: UNNotification,
                                       withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        
        
        // Print full message.
        print("hello",userInfo)
        
        // Change this to your preferred presentation option
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       didReceive response: UNNotificationResponse,
                                       withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        
        // Print full message.
        print("tap on on forground app",userInfo)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pushNotification"), object: nil, userInfo: userInfo)
        completionHandler()
    }
}

extension HomeController: MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().subscribe(toTopic: "topics/global")
        UserDefaults.standard.set(fcmToken, forKey: firebaseKey)
        UserDefaults.standard.synchronize()
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
}

