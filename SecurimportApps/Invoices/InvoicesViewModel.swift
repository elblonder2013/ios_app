//
//  InvoicesViewModel.swift
//  SecurimportApps
//
//  Created by debabrata on 07/10/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
class InvoicesViewModel: NSObject {
    var delegate: NewViewControllerOpen?
     var delegatePass: PassData?
    var page = 0
    var isLoad = false
    var tcount : Int = 0
    var tableView : UITableView!
    var  invoices = [InvoicesModel]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {

        if let invoicesAndPayments = jsonData["invoicesAndPayments"].array {
            invoices += invoicesAndPayments.map{InvoicesModel(data: $0)}
            tcount = jsonData["tcount"].intValue
            isLoad = true
            completion(true)
        } else {
            completion(false)
        }
        
    }
}
extension InvoicesViewModel: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InvoicesTableViewCell.identifier) as! InvoicesTableViewCell
        cell.item = invoices[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.moveToController(id: invoices[indexPath.row].pdf, name: invoices[indexPath.row].name, dict: JSON.null, Controller: Controllers.description)
    }
    
}
extension InvoicesViewModel: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell: UITableViewCell in (tableView.visibleCells) {
            if let indexPath: IndexPath = tableView.indexPath(for: cell) {
                if indexPath.row + 1 <  tcount && indexPath.row + 1 == invoices.count && isLoad {
                    page = page + 1
                    isLoad =  false
                    delegatePass?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: page, call: WhichApiUse.invoices)
                }
                
                
            }
            
        }
        
    }
}
