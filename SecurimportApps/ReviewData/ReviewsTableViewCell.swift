//
//  ReviewsTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 04/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SwiftyJSON

class ReviewsTableViewCell: UITableViewCell {

        @IBOutlet weak var title: UILabel!
        @IBOutlet weak var reviewByLabel: UILabel!
        @IBOutlet weak var nameLabel: UILabel!
        @IBOutlet weak var ratingLabel: UILabel!
        @IBOutlet weak var ratingView: HCSStarRatingView!
        @IBOutlet weak var msgLabel: UILabel!
        @IBOutlet weak var likeBtn: UIButton!
        @IBOutlet weak var likeLabel: UILabel!
        @IBOutlet weak var unlikeBtn: UIButton!
        @IBOutlet weak var unlikeLabel: UILabel!
        var passData: PassData?

    override func awakeFromNib() {
        super.awakeFromNib()
        reviewByLabel.SetDefaultTextColor()
        ratingLabel.SetDefaultTextColor()
        ratingView.tintColor = UIColor().HexToColor(hexString: "FFB926")
        ratingView.isUserInteractionEnabled = false
        reviewByLabel.text = "Reviewed By".localized
        ratingLabel.text = "Rating".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
    
    @IBAction func likeClicked(_ sender: Any) {
        let dict = ["review_id": item.id! , "ishelpful" : true] as [String : Any]
        passData?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiUse.likeDislike)
        
    }
    
    @IBAction func disLikeClicked(_ sender: Any) {
        let dict = ["review_id": item.id! , "ishelpful" : false] as [String : Any]
        passData?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiUse.likeDislike)
    }
    
    var item: ReviewData! {
        didSet {
           title.text = item?.title
            nameLabel.text = String.init(format: "%@ - %@", (item?.name!)!, (item?.date)!)
            msgLabel.text = item?.msg
            ratingView.value = CGFloat(Int(item?.rating ?? "0")!)
            likeLabel.text = item.likes
            unlikeLabel.text = item.dislikes
            if item.like == 0 {
                likeBtn.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            } else {
                likeBtn.setImage(#imageLiteral(resourceName: "like-active"), for: .normal)
            }
            if item.dislike == 0 {
                unlikeBtn.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)
            } else {
                unlikeBtn.setImage(#imageLiteral(resourceName: "dislike-active"), for: .normal)
            }
        }
    }


}
