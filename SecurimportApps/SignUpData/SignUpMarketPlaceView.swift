//
//  SignUpMarketPlaceView.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 23/02/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SellerAction {
    func sellerEvent (value: Int)
}

class SignUpMarketPlaceView: UIView {
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var termsSwitch: UISwitch!
    @IBOutlet weak var profileUrlField: UITextField!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var sellerVendorSwitch: UISwitch!

    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var selectionViewHeight: NSLayoutConstraint!

    @IBOutlet weak var countryTopView: UIView!
    @IBOutlet weak var countryView: UIView!
    
    @IBOutlet weak var wantToBecomeSellerLbl: UILabel!
    @IBOutlet weak var country: UILabel!
    
    @IBOutlet weak var profileUrlLbl: UILabel!
    var delegate: SellerAction!
    var moveDelegate: NewViewControllerOpen!
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        let borderColor: UIColor? = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
        countryView.layer.borderColor = borderColor?.cgColor
        countryView.layer.borderWidth = 1.5
        countryView.layer.cornerRadius = 5.0
        termsLabel.text = "I Accept Terms & Conditions".localized

        termsLabel.halfTextWithColorChange(fullText: termsLabel.text!, changeText: "Terms & Conditions".localized, color: UIColor.blue)
        termsLabel.isUserInteractionEnabled = true
        termsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAccept)))
        
        wantToBecomeSellerLbl.text = "Do you want to become seller/vendor?".localized
        country.text = "Country".localized
        profileUrlLbl.text = "Profile URL".localized
    }

    @objc private func didTapAccept() {
        moveDelegate.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.cmsController)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let xibView = Bundle.main.loadNibNamed("SignUpMarketPlaceView", owner: self, options: nil)?[0] as! UIView
        xibView.frame = self.bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        self.addSubview(xibView)
    }

    @IBAction func sellerAction(_ sender: Any) {
        if sellerVendorSwitch.isOn {
            delegate.sellerEvent(value: 1)
        } else {
            delegate.sellerEvent(value: 0)
        }
    }

}
