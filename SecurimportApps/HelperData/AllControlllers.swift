//
//  AllFiles.swift
//  Odoo application
//
//  Created by bhavuk.chawla on 17/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation

enum Controllers: String {
    case notifications = "notificationDataController"
    case signUp
    case signIn
    case search
    case touch
    case orderDetails
    case address
    case productCategory
    case product
    case category
    case homeBanners
    case home
    case homeSliders
    case cart
    case homeProduct
    case description
    case addReview
    case reviews
    case editAddress
    case addAddress
    case changeAddress
    case accountInfo
    case Profile
    case checkout
    case zoom
    case sellerProfile = "sellerProfileDataViewController"
    case sellerBanner
    case sellerReview = "sellerReviewDataViewController"
    case allSeller = "allSellerDataViewController"
    case sellerDashboard = "sellerDashboardDataViewController"
    case sellerProducts = "sellerProductViewController"
    case sellerOrder = "sellerOrderViewController"
    case cmsController = "cmsDataViewController"
    case askToAdminController = "askToAdminViewController"
    case becomeSeller = "becomeSellerViewController"
    case custom
    case languageChooser = "languageChooserViewController"
    case languageProfileConroller = "languageProfileDataViewController"
    case Detector = "DetectorViewController"
    case invoicesViewController = "InvoicesViewController"
    case pdf
}

enum WhichApiUse: String {
    case custom
    case getCart
    case updateCartPlus
    case updateCartMinus
    case RemoveFromCart
    case moveWishlist
    case cartEmpty
    case removeFromWishlist
    case moveToCart
    case searchQuery
    case none
    case categories
    case products
    case addToWishlist
    case addToCart
    case addAddress
    case editAddress
    case getAddress
    case getPayment
    case getOrderReview
    case getShipping
    case placeOrder
    case checkoutAddressContinue
    case editSave
    case buyNow
    case forgotPassword
    case deleteAddress
    case defaultAddress
    case uploadImage
    case sendVerificationLink
    case home
    case splash
    case registerFcm
    case signOut
    case zoom
    case productPending = "pending"
    case productApproved = "approved"
    case productRejected = "rejected"
    case becomeSeller
    case signUp
    case social
    case orderShipped = "shipped"
    case orderApproved
    case orderNew = "new"
    case likeDislike
    case deleteNotification
    case gdpr
    case gdprDeactivateAccount
    case gdprDownladRequest
    case getcustomerform
     case invoices
    case getCountries
    case accountInfo
}
