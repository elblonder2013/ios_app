//
//  CartModal.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

class CartModal {

    var products = [Product]()
    var cartPrice: CartData?
    init?(data: JSON) {

        Addons.AddonsChecks(data: data)

        if let products = data["items"].array {
            self.products = products.map {
                Product(json: $0)
            }
        }

        self.cartPrice = CartData(data: data)

    }
}

struct CartData {

    var total: String?
    var tax: String?
    var cartCount: String?
    var subTotal: String?
    init(data: JSON) {
        self.total = data["grandtotal"]["value"].stringValue
         self.tax = data["tax"]["value"].stringValue
        self.cartCount = data["cartCount"].stringValue
        self.subTotal = data["subtotal"]["value"].stringValue
    }
}
