//
//  HomeProductTableViewCell.swift
//  Odoo application
//
//  Created by vipin sahu on 9/1/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation



class HomeProductTableViewCell: UITableViewCell {
    
    func touchScr(x: Int, y: Int){
        
    }
    
    
    var homeVC = HomeController()
    var products = [Product]()
    var isSeller  = false
    @IBOutlet weak var homeProductCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var button: UIButton!
    var selectedIndex: IndexPath?
    var view = UIView()
    var delegate: PassData?
    var slider_mode: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
        button.clipsToBounds = true
        //        button.applyShadow(colours: GlobalData.Credentials.setBorderColor, locations: nil)
        button.applyGradient(colours: GlobalData.Credentials.gradientArray)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.applyShadowWithoutBorder(colours: GlobalData.Credentials.setBorderColor, locations: nil)
        button.setTitle(viewAll.localized, for: .normal)
        homeProductCollection.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        homeProductCollection.backgroundColor = UIColor.clear
        homeProductCollection.delegate = self
        homeProductCollection.dataSource = self
        
        homeProductCollection.reloadData()
        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        if isSeller {
            if traitCollection.forceTouchCapability == .available {
               
            }
        } else {
            if traitCollection.forceTouchCapability == .available {
                homeVC.registerForPreviewing(with: homeVC, sourceView: homeProductCollection)
            }
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    @IBAction func ViewAllPress(_ sender: UIButton) {
        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiUse.none)
    }
    
}

extension HomeProductTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSeller {
            return 0
        } else {
            return products.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath as IndexPath) as! HomeCollectionViewCell
        
        cell.setNeedsLayout()
        if isSeller {
           
            
        } else {
            cell.categoryImage.image = nil
            cell.item = products[indexPath.row]
            cell.cellView.layer.cornerRadius = 7
            
            cell.setNeedsLayout()
            if slider_mode == "fixed" {
                self.collectionHeight.constant = self.homeProductCollection.contentSize.height
                cell.backgroundColor = UIColor.clear
                if traitCollection.forceTouchCapability == .available {
                    homeVC.registerForPreviewing(with: homeVC, sourceView: self.homeProductCollection)
                }
            } else {
                if traitCollection.forceTouchCapability == .available {
                    homeVC.registerForPreviewing(with: homeVC, sourceView: self.homeProductCollection)
                }
                cell.backgroundColor = GlobalData.Credentials.defaultBackgroundColor
            }
        }
//         cell.layoutIfNeeded()
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if slider_mode == "fixed" {
            return CGSize(width: SCREEN_WIDTH / 2 - 8, height: SCREEN_WIDTH / 2)
        } else {
            return CGSize(width: SCREEN_WIDTH / 2.5 - 8, height: SCREEN_WIDTH / 2.5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isSeller {
           
        } else {
            delegate?.passData(id: products[indexPath.row].templateId!, name:  products[indexPath.row].name!, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.products)
        }
        
    }
    
    func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        //        super.viewWillTransition(to: size, with: coordinator)
        homeProductCollection?.collectionViewLayout.invalidateLayout()
    }
}
//extension HomeProductTableViewCell:UIViewControllerPreviewingDelegate {
//    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
//        var heightOffset = UIApplication.shared.statusBarFrame.height
//        //if let navigationController = navigationController {
//        heightOffset += 44
//        //}
//        let newLocation = CGPoint(x: location.x, y: location.y - heightOffset)
//        guard let indexPath = homeProductCollection.indexPathForItem(at: newLocation) else {
//            return nil
//        }
//
//        guard let cell = homeProductCollection.cellForItem(at: indexPath) else {
//            return nil
//        }
//        let story = UIStoryboard(name: "Home", bundle: nil)
//        let animalDetailVC = story.instantiateViewController(withIdentifier: "TouchOfProductInViewController")as! TouchOfProductInViewController
//        selectedIndex = indexPath
//        //        animalDetailVC.productImgUrl = productCategoryViewModalObject.products[(selectedIndex?.row)!].pictureUrl!
//        //        animalDetailVC.productName = productCategoryViewModalObject.products[(selectedIndex?.row)!].name!
//        let screenWidth = UIScreen.main.bounds.width
//        let screenHeight = UIScreen.main.bounds.height
//        animalDetailVC.preferredContentSize = CGSize(width: screenWidth, height: screenHeight - 300)
//        var previewFrame = cell.frame
//        previewFrame.origin.y += heightOffset
//        previewingContext.sourceRect = previewFrame
//
//        return animalDetailVC
//    }
//
//    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
//        //performSegue(withIdentifier: segueIdentifier, sender: nil)
//        let story = UIStoryboard(name: "Main", bundle: nil)
//        // let animalDetailVC = story.instantiateViewController(withIdentifier: "AnimalDetailVC")as! AnimalDetailViewController
//
//    }
//}
