//
//  CartController.swift
//  Odoo application
//
//  Created by vipin sahu on 9/5/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Toaster

class CartController: UIViewController {

    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var cartTableView: UITableView!
    var cartViewModalObject  = CartViewModal()
    var apiUse: WhichApiUse?

    var emptyView: EmptyView?
    override func viewDidLoad() {
        super.viewDidLoad()
        cartViewModalObject.delegate = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        checkOutButton.setTitle(proceedCheckout.localized, for: .normal)
        checkOutButton.backgroundColor = GlobalData.Credentials.accentColor
        navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        // Do any additional setup after loading the view.
        cartTableView.register(CartProductTableViewCell.nib, forCellReuseIdentifier: CartProductTableViewCell.identifier)
        cartTableView.register(CartPriceTableViewCell.nib, forCellReuseIdentifier: CartPriceTableViewCell.identifier)
        cartTableView.delegate = cartViewModalObject
         cartTableView.dataSource = cartViewModalObject
        cartTableView.estimatedRowHeight = 100
        cartTableView.rowHeight = UITableView.automaticDimension
        cartTableView.separatorStyle = .none
        cartViewModalObject.passdataDelegate = self
        navigationItem.title = cart.localized
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeRequest(id: String, dict: [String: Any]) {
        var params = ""

        var verb: RequestType?
        switch apiUse {
        case .updateCartPlus?:
            params = GlobalData.apiName.getCartApi + id
           verb = .put
        case .updateCartMinus?:
            params = GlobalData.apiName.getCartApi + id
            verb = .put
        case .RemoveFromCart?:
            params = GlobalData.apiName.getCartApi + id
            verb = .delete
        case .cartEmpty?:
            params = GlobalData.apiName.emptyCart
            verb = .delete
        case .moveWishlist?:
            params = GlobalData.apiName.cartToWishlist
            verb = .post
        default:
            params =  GlobalData.apiName.getCartApi
            verb = .post
        }

        
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: verb!, controler: Controllers.cart) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                if responseObject!["cartCount"] != JSON.null {
                    self.tabBarController?.setCartCount(value: responseObject!["cartCount"].stringValue)
                }
                    switch self.apiUse {
                    case .updateCartPlus?:
                        if responseObject!["success"].boolValue {
                            ErrorChecking.successView(message: responseObject!["message"].stringValue)
                            self.apiUse = nil
                            self.makeRequest(id: "", dict: [:])
                        } else {
                            ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                        }
                    case .updateCartMinus?:
                        if responseObject!["success"].boolValue {
                             ErrorChecking.successView(message: responseObject!["message"].stringValue)
                            self.apiUse = nil
                            self.makeRequest(id: "", dict: [:])
                        } else {
                           ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                        }
                    case .RemoveFromCart?:
                        if responseObject!["success"].boolValue {
                              ErrorChecking.successView(message: responseObject!["message"].stringValue)
                            self.apiUse = nil
                            self.makeRequest(id: "", dict: [:])
                        } else {
                            ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                        }
                    case .moveWishlist?:
                        if responseObject!["success"].boolValue {
                              ErrorChecking.successView(message: responseObject!["message"].stringValue)
                            self.apiUse = nil
                            self.makeRequest(id: "", dict: [:])
                        } else {
                            ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                        }
                    case .cartEmpty?:
                        if responseObject!["success"].boolValue {
                            ErrorChecking.successView(message: responseObject!["message"].stringValue)
                            self.apiUse = nil
                            self.makeRequest(id: "", dict: [:])
                        } else {
                            ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                        }
                    default:
                        self.cartViewModalObject.getValue(jsonData: responseObject!) {
                            (data: Bool) in

                            if data {
                                self.emptyView?.isHidden = true
                                self.cartTableView.isHidden = false
                                self.checkOutButton.isHidden = false
                                self.cartTableView.reloadData()
                            } else {
                                 self.tabBarController?.setCartCount(value: "")
                                self.emptyView?.isHidden = false
                                self.cartTableView.isHidden = true
                                self.checkOutButton.isHidden = true
                            }
                    }
                }
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {

    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
         SVProgressHUD.dismiss()
        self.emptyView?.isHidden = true
        self.cartTableView.isHidden = true
        self.checkOutButton.isHidden = true
         self.apiUse = nil
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.delegate = self
            emptyView?.labelText.text = cartEmpty.localized
            emptyView?.button.setTitle(browseCategories.localized, for: .normal)
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-cart")
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
        self.makeRequest(id: "", dict: [:])
    }

    @IBAction func CheckoutPress(_ sender: UIButton) {
        let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "checkoutDataViewController") as! CheckoutDataViewController
        let nav  = UINavigationController(rootViewController: view)
        self.present(nav, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CartController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiUse) {
        print(call)
        apiUse = call
        switch call {
        case .updateCartPlus:
            self.makeRequest(id: id, dict: dict)
        case .updateCartMinus:
            self.makeRequest(id: id, dict: dict)
        case .RemoveFromCart:
            self.makeRequest(id: id, dict: [:])
        case .cartEmpty:
            self.makeRequest(id: "", dict: [:])
        case .moveWishlist:
            self.makeRequest(id: "", dict: dict)
        default:
            print("")
        }

    }

}

extension CartController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: productStoryBoard, forViewControllerName : productDataViewControllerIdentifier) as! ProductDataViewController
        view.templateId = id
        view.name = name
        self.navigationController?.pushViewController(view, animated: true)
    }
}

extension CartController: EmptyDelegate {
    func buttonPressed() {
        self.tabBarController?.selectedIndex = 2
    }
}
