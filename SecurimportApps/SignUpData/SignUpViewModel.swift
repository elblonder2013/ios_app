//
//  SignUpViewModel.swift
//  SecurimportApps
//
//  Created by debabrata on 25/11/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON
class SignUpViewModel : NSObject{
    var dismissViewController : ((_ result:Bool) -> Void)?
    var reloadTableView: (() -> Void)?
    var countryArray = [CountryData]()
    var isAdded = false
    var form = Form()
    var headerList = ["Your Personal Details".localized,"Your Address".localized,"Your Contact Information".localized,"Additional Information".localized,"Your Password".localized]
    var companyType = ["Private / Independent".localized : "person","company".localized : "company"]
    override init() {
        super.init()
        makeRequest(dict: [:], call: WhichApiUse.none)
    }
    
    func createDropDown(index:Int,placeholder: String, key: String = "", key2: String = "", countryData: [CountryData], isRequired: Bool = false, heading: String = "", image: UIImage?, right: UIImage?,placeholder2: String, image2: UIImage?,value:String,value2:String) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.dropDown
        fieldItem.countryData = countryData
        fieldItem.keyType = key
        fieldItem.keyType2 = key2
        fieldItem.isRequired = isRequired
        fieldItem.image = image
        fieldItem.rightIcon = right
        fieldItem.heading = heading
        fieldItem.placeholder2 = placeholder2
        fieldItem.image2 = image2
        fieldItem.value = value
        fieldItem.value2 = value2
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
    }
    func creatbutton(index:Int,placeholder: String) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.button
        fieldItem.actionCompletion = { [weak self] value in
            if value == "submit"{
                self?.signUp()
            }
        }
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
    }
    func createSingleDropDown(index:Int,placeholder: String, key: String = "",  arrayList: [String], isRequired: Bool = false, heading: String = "", image: UIImage?, right: UIImage?,value:String) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.singleDropDown
        fieldItem.dropDownArray = arrayList
        fieldItem.keyType = key
        fieldItem.image = image
        fieldItem.value = value
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
        fieldItem.actionCompletion = { [weak self] value in
            if value == "chnageCompanyType"{
                self?.addRemoveCompanyName()
            }
        }
        
        fieldItem.isRequired = isRequired
        fieldItem.rightIcon = right
        fieldItem.heading = heading
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
        
    }
    func addRemoveCompanyName(){
        if self.form.formItems[0][0].value as? String == "Private / Independent".localized{
            if isAdded{
                isAdded = !isAdded
               self.form.formItems[0].remove(at: 1)
            }
        }else{
            if !isAdded{
                 isAdded = !isAdded
                self.createTextField(index: 0,position:1,placeholder: "Company Name".localized, isRequired: true, key: "wk_company_name", value: "", heading: "Company Name".localized,valiidationType : "validate-alphanum", image: nil)
            }
        }
         self.reloadTableView?()
    }
    
    func createForm() {
        self.form.formItems = []
        self.createSingleDropDown(index: 0, placeholder: "Gender".localized,key: "company_type", arrayList: ["Private / Independent".localized,"company".localized], image: nil, right: nil, value: "Private / Independent")
        
       
        self.createTextField(index: 0,placeholder: "NIE / NIF / CIF (ES12345678L) *".localized, isRequired: true, key: "vat", value: "", heading: "NIE / NIF / CIF (ES12345678L)".localized, valiidationType : "validate-alphanum", image: nil, keyboard: .emailAddress)
        self.createTextField(index: 0,placeholder: "First Name".localized, isRequired: true, key: "firstname", value:  "", heading: "First Name".localized,valiidationType : "validate-alphanum", image: nil)
        self.createTextField(index: 0,placeholder: "Last Name".localized, isRequired: true, pwdType: false, key: "lastname", value:  "", heading: "Last Name".localized, valiidationType: "validate-alphanum", image: nil)
        

        self.createTextField(index: 0,placeholder: "E-Mail Address".localized,isRequired: true, key: "login", value: "", heading: "E-Mail Address".localized, valiidationType : "validate-email", image: nil, keyboard: .emailAddress)

        self.createTextField(index: 1,placeholder: "Street Address".localized, isRequired: true, key: "street", value: "", heading: "Street Address".localized,valiidationType : "validate-alphanum", image: nil)
        self.createTextField(index: 1,placeholder: "Post code".localized, isRequired: true, key: "zipcode", value: "", heading: "Post code".localized, image: nil, keyboard: .numberPad)
        
        if self.countryArray.count > 0 {
            self.createDropDown(index: 1,placeholder:"country".localized, key: "country_id", key2: "state_id", countryData: self.countryArray, isRequired: true, heading: "country".localized, image: nil, right: nil, placeholder2: "State".localized, image2: nil, value: "", value2: "")
        }
        self.createTextField(index: 1,placeholder: "City".localized, isRequired: true, key: "city", value: "", heading: "City".localized,valiidationType : "validate-alphanum", image: nil)
        
        self.createTextField(index: 2,placeholder: "Telephone Number: 1".localized, isRequired: true, key: "mobile", value:  "", heading: "Telephone Number: 1".localized,valiidationType : "validate-number", image: nil, keyboard: .numberPad)
        self.createTextField(index: 2,placeholder: "Telephone Number: 2".localized, isRequired: false, key: "phone", value:  "", heading:  "Telephone Number: 2".localized,valiidationType : "validate-number", image: nil, keyboard: .numberPad)
        
        
        self.createTextField(index: 3,placeholder: "Regular providers".localized, isRequired: false, key: "addproveedores", value: "", heading: "Regular providers".localized,valiidationType : "validate-alphanum", image: nil)
        self.createTextField(index: 3,placeholder: "Annual purchase volume".localized, isRequired: false, key: "addvolumen", value: "", heading: "Annual purchase volume".localized,valiidationType : "validate-alphanum", image: nil)
        self.createTextField(index: 3,placeholder: "How have you known us".localized, isRequired: false, key: "addcomonoshas", value: "", heading: "How have you known us".localized,valiidationType : "validate-alphanum", image: nil)
        self.createTextField(index: 3,placeholder: "Observations".localized, isRequired: false, key: "addobservaciones", value: "", heading: "Observations".localized,valiidationType : "validate-alphanum", image: nil)
        
        
        self.createTextField(index: 4,placeholder: "enterPassword".localized, isRequired: true, key: "password", value: "", heading: "New Password".localized, image: nil,isSecure:true)
        self.createTextField(index: 4,placeholder: "EnterConfirmPassword".localized, isRequired: true, key: "cPassword", value: "", heading: "confirmPassword".localized, image: nil,isSecure:true)
        
        self.creatbutton(index: 4,placeholder: "SIGNUP".localized)
        self.reloadTableView?()
        
    }
    
    func createSwitchControl(index:Int,placeholder: String, isRequired: Bool = false, key: String = "") {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.switchControl
        fieldItem.isRequired = isRequired
        fieldItem.keyType = key
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
    }
    
    func createBoolCheck(index:Int,placeholder: String, isRequired: Bool = false, key: String = "", valiidationType: String = "") {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.addressCheck
        fieldItem.isRequired = isRequired
        fieldItem.keyType = key
        fieldItem.valiidationType = valiidationType
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
        if self.form.formItems[safe:index] != nil{
            self.form.formItems[index] += [fieldItem]
        }else{
            self.form.formItems.append([fieldItem])
        }
    }
    
    func createTextField(index:Int,position:Int = -1,placeholder: String, isRequired: Bool = false, pwdType: Bool = false, key: String = "",value : String, heading: String, valiidationType: String = "" , image: UIImage?, keyboard : UIKeyboardType = .default,isSecure : Bool = false) {
        let fieldItem = FormItem(placeholder: placeholder)
        fieldItem.uiProperties.cellType = FormItemCellType.textField
        fieldItem.isRequired = isRequired
        fieldItem.isSecure = pwdType
        fieldItem.keyType = key
        fieldItem.heading = heading
        fieldItem.uiProperties.keyboardType = keyboard
        fieldItem.valiidationType = valiidationType
        fieldItem.image = image
        fieldItem.isSecure = isSecure
        fieldItem.value = value
        fieldItem.isShowPassword = isSecure
        fieldItem.valueCompletion = { [weak fieldItem] value in
            fieldItem?.value = value
        }
       
        if self.form.formItems[safe:index] != nil{
            if position > 0{
                self.form.formItems[index].insert(fieldItem, at: 1)
            }else{
            self.form.formItems[index] += [fieldItem]
            }
        }else{
          
            self.form.formItems.append([fieldItem])
        }
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiUse) {
        
        var params = ""
        
        var verb: RequestType = .post
        switch call {
        case .signUp:
            params = GlobalData.apiName.signUp
            verb = .post
        case .getcustomerform:
            params = GlobalData.apiName.getcustomerform
            verb = .get
        default:
            params =  GlobalData.apiName.getCountries
            verb = .post
        }
        
        NetworkManager.shared.fetchData(params: params, login: false, view: nil, dict: dict, verbs: verb, controler: Controllers.signUp ) { (responseObject: JSON?, error: Error?) in
            if (error != nil) {
                print("Error logging you in!")
            } else {
                switch call {
                case .signUp:
                    if responseObject!["login"]["success"].boolValue {
                        UserDefaults.standard.set(true, forKey: loggedInUserKey)
                        let dict: [String: Any] = ["login" : responseObject!["cred"]["login"].stringValue, "pwd" : responseObject!["cred"]["pwd"].stringValue]
                         UserDefaults.standard.set( responseObject!["login"].dictionaryObject, forKey: "userDetails")
                        UserDefaults.standard.set(dict, forKey: "loginDict")
                        UserDefaults.standard.set(responseObject!["login"][customerEmailKey].stringValue, forKey: customerEmailKey)
                        UserDefaults.standard.set(responseObject!["login"][customerIdKey].stringValue, forKey: customerIdKey)
                        UserDefaults.standard.set(responseObject!["login"][customerNameKey].stringValue, forKey: customerNameKey)
                        UserDefaults.standard.synchronize()
                        self.dismissViewController?(true)
                        
                        if responseObject!["login"]["is_seller"].boolValue {
                            GlobalData.AddOnsEnabled.is_seller = true
                        } else {
                            GlobalData.AddOnsEnabled.is_seller = false
                        }
                    } else {
                        ErrorChecking.ErrorView(message: responseObject!["message"].stringValue)
                        
                    }
                    
                default:
                    self.setCountries(jsonData: responseObject!) { (data) in
                        self.createForm()
                    }
                }
                
            }
        }
    }
    
    func setCountries( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = NewAddressModal(data : jsonData ) else {
            return
        }
        if !data.countryArray.isEmpty {
            self.countryArray = data.countryArray
            completion(true)
        } else {
            completion(false)
        }
    }
    
    func signUp(){
        if !form.isValid().0 {
            ErrorChecking.ErrorView(message:form.isValid().1 + " input value is not valid".localized)
        } else {
            var data = form.getFormData()
            data["company_type"] = companyType[(data["company_type"] as? String) ?? ""]
            print(data)
            if data["password"] as? String !=  data["cPassword"] as? String{
                ErrorChecking.ErrorView(message: "PasswordShouldMatch".localized)
            }else{
                data["name"] = (data["firstname"] as? String ?? "") + " " + (data["lastname"] as? String ?? "")
                data.map{
                    if $0.value as? String == ""{
                        data.removeValue(forKey: $0.key)
                    }
                }
                makeRequest(dict: data, call: WhichApiUse.signUp)
            }
        }
    }
}
