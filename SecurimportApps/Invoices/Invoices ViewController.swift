//
//  Invoices ViewController.swift
//  SecurimportApps
//
//  Created by debabrata on 07/10/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON
class InvoicesViewController: UIViewController {
    var emptyView: EmptyView?
    @IBOutlet weak var InvoicesTableView: UITableView!
    fileprivate let invoicesViewModelObject = InvoicesViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Invoices and Payments".localized
        InvoicesTableView.delegate = invoicesViewModelObject
        invoicesViewModelObject.tableView = InvoicesTableView
        invoicesViewModelObject.delegate = self
        invoicesViewModelObject.delegatePass = self
        InvoicesTableView.dataSource = invoicesViewModelObject
        InvoicesTableView.estimatedRowHeight = 100
        InvoicesTableView.rowHeight = UITableView.automaticDimension
        InvoicesTableView.separatorStyle = .none
        InvoicesTableView.register(InvoicesTableViewCell.nib, forCellReuseIdentifier: InvoicesTableViewCell.identifier)
        self.makeRequest(page: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = orderEmpty.localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-order")
            emptyView?.button.setTitle(browseCategories.localized, for: .normal)
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }
    
    
    func makeRequest(page:Int) {
        
        let params = GlobalData.apiName.invoices
        var dict = [String:Any]()
        dict["limit"] = 25
        dict["offset"] = page
        dict["date_to"] = ""
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-dd-MM"
        dict["date_to"] = ""
        dict["date_from"] = dateFormate.string(from: Date())
        
        
        NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: RequestType.post, controler: Controllers.invoicesViewController) { (responseObject: JSON?, error: Error?) in
            
            if (error != nil) {
                print("Error logging you in!")
                
            } else {
                self.invoicesViewModelObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    if data {
                        self.emptyView?.isHidden = true
                        self.InvoicesTableView.isHidden = false
                        self.InvoicesTableView.reloadData()
                    } else {
                        self.emptyView?.isHidden = false
                        self.InvoicesTableView.isHidden = true
                        
                    }
                }
            }
        }
    }

}
extension InvoicesViewController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view  = viewController(forStoryboardName: customerStoryboard, forViewControllerName: viewInvoicesViewControllerIdentifier) as! ViewInvoicesViewController
            view.title1 = name
            view.url = id
        self.navigationController?.pushViewController(view, animated: true)
    }
}
extension InvoicesViewController : PassData{
    func passData(id: String, name: String, dict: [String : Any], jsonData: JSON, index: Int, call: WhichApiUse) {
         self.makeRequest(page: index)
    }
    
    
}
