//
//  ProductCategoryCollectionViewCell.swift
//  Odoo application
//
//  Created by Bhavuk on 31/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var wishlistICOn: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productOldPrice: UILabel!
    
    var delgate: PassData?
    var wishlistId: String?
    var wishlistIds = [WishlistIds]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productView.myBorder()
        productOldPrice.SetDefaultTextColor()
        
    }
    @IBAction func WishlistPress(_ sender: UIButton) {
        if LoginCustomer.loginCheckCustomer() {
            
            delgate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiUse.addToWishlist)
        } else {
            LoginCustomer.showLogginMessage()
        }
        
    }
    
    var item: Product? {
        didSet {
            
            if GlobalData.AddOnsEnabled.wishlistEnable == false {
                wishlistICOn.isHidden = true
            }
            if wishlistIds.contains(where: {$0.wishlistId == item?.productId!}) {
                
                wishlistICOn.setImage(UIImage(named : "Icon-Wishlist-Fill"), for: .normal)
            } else {
                wishlistICOn.setImage(UIImage(named : "Icon-Wishlist"), for: .normal)
                
            }
            productName.text = item?.name
            price.text = item?.reducePrice
            
            let val = (item?.pictureUrl!)!
            productImage.setImage(imageUrl: val, controller: .productCategory)
            
            if !(item?.reducePrice!.isEmpty)! {
                price.text = item?.reducePrice
                productOldPrice.text = item?.price
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: (item?.price!)!)
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                productOldPrice.attributedText = attributeString
            } else {
                price.text = item?.price
                productOldPrice.text = item?.reducePrice
            }
            
            //            if wishlistIds[0].wishlistId ==
            
        }
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

