//
//  CategoryHeadeViewCell.swift
//  CS-Cart MVVM
//
//  Created by vipin sahu on 8/25/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: class {
    func toggleSection(header: CategoryHeadeViewCell, section: Int)
}

class CategoryHeadeViewCell: UITableViewHeaderFooterView {

     var section: Int = 0
    weak var delegate: HeaderViewDelegate?

    @IBOutlet weak var titleLabel: UILabel?

    @IBOutlet weak var arrowLabel: UIImageView!
    @IBOutlet weak var lineView: UIView!
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: Category? {
        didSet {
            guard let item = item else {
                return
            }
            titleLabel?.text = item.name
            if item.SubcategoryArray.count > 0 {
                arrowLabel.image = #imageLiteral(resourceName: "icon-drop-down")
                 setCollapsed(collapsed: item.isCollapsed)
            } else {
              arrowLabel.image = #imageLiteral(resourceName: "icon-angle-right")
                arrowLabel.contentMode = .scaleAspectFit
                 setCollapsed(collapsed: true)
            }

        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
        lineView.backgroundColor = GlobalData.Credentials.defaultTextColor
    }

    @objc private func didTapHeader() {
        delegate?.toggleSection(header: self, section: section)
    }
    func setCollapsed(collapsed: Bool) {
        arrowLabel?.rotate(collapsed ? 0.0 : .pi)
    }
}

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards

        self.layer.add(animation, forKey: nil)
    }
}
