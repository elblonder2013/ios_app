//
//  MyDatabase.swift
//  Odoo iOS
//
//  Created by Bhavuk on 25/11/17.
//Copyright © 2017 Bhavuk. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import UIKit

@objcMembers
class MyDatabase: Object {

    dynamic var name = ""
    dynamic var createdAt = NSDate()
    dynamic var notes = ""
    dynamic var isCompleted = false

    dynamic var myData = NSData()

// Specify properties to ignore (Realm won't persist these)

//  override static func ignoredProperties() -> [String] {
//    return []
//  }

}

@objcMembers
class Model: Object {
    @objc private dynamic var dictionaryData: Data?
    var dictionary: String {
        get {
            guard let dictionaryData = dictionaryData else {
                return ""
            }

            let dataString = String(data: dictionaryData, encoding: String.Encoding.utf8)
            return dataString!

        }

        set {
            let data = newValue.data(using: .utf8)
            dictionaryData = data
        }
    }

    override static func ignoredProperties() -> [String] {
        return ["dictionary"]
    }
}

@objcMembers
class Item: Object {
    dynamic var data = ""
    dynamic var className = ""

}

//class Item: Object {
//
//    dynamic var ID : Controllers = .home
//    dynamic var textString = ""
//
//    override static func primaryKey() -> String? {
//        return "ID"
//    }
//
//}

//class DBManager {
//    private var   database:Realm
//    static let   sharedInstance = DBManager()
//    private init() {
//        database = try! Realm()
//    }
//    func getDataFromDB() ->   Results<Item> {
//        let results: Results<Route> =   database.objects(Item.self)
//        return results
//    }
//    func addData(object: Item)   {
//        try! database.write {
//            database.add(object, update: true)
//            print("Added new object")
//        }
//    }
//    func deleteAllFromDatabase()  {
//        try!   database.write {
//            database.deleteAll()
//        }
//    }
//    func deleteFromDb(object: Item)   {
//        try!   database.write {
//            database.delete(object)
//        }
//    }
//}
