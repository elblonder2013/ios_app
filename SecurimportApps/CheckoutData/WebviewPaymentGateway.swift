//
//  WebviewPaymentGateway.swift
//  SecurimportApps
//
//  Created by debabrata on 09/12/19.
//  Copyright © 2019 Webkul. All rights reserved.
//

import SVProgressHUD
import UIKit
import SwiftyJSON
import Toaster
import WebKit

protocol paymentCheckDelegate {
    func paymentCheck(status: Bool, reference : String)
}
protocol backFromWebView {
    func backFromWebViewcontroller(status: Bool)
}

class PaymentezPaymentGatewayViewController: UIViewController {
    var delegate:paymentCheckDelegate?
    var backDelegate:backFromWebView?
    var apiUse : WhichApiUse?
    var count = 0
    @IBOutlet weak var myWebView: WKWebView!
    var url = ""
    override func viewDidLoad() {
        self.title = "REDSYS".localized
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        let backButton: UIBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(back))
        backButton.setBackgroundImage(UIImage(named: "Status-Canceled"), for: .normal, barMetrics: .default)
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalData.Credentials.gradientArray)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        self.applyGradient(colours: GlobalData.Credentials.gradientArray)
        myWebView.navigationDelegate = self
        myWebView.uiDelegate = self
        let formattedString = "".replacingOccurrences(of: " ", with: "")
        let url = URL (string: "\(self.url + formattedString)")
        let requestObj = URLRequest(url: url!)
        myWebView.load(requestObj)
    }
    
    @IBAction func dismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: {
             self.delegate?.paymentCheck(status: false, reference: "cancel")
        })
    }
    override func viewDidDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    @objc func back() {
        self.dismiss(animated: true, completion: {
             self.delegate?.paymentCheck(status: false, reference: "cancel")
        })
    }
   
    func applyGradient(colours: [UIColor]) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = colours.map { $0.cgColor }
        self.view.layer.insertSublayer(gradient, at: 0)
    }
}

extension PaymentezPaymentGatewayViewController: WKUIDelegate,WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (_: WKNavigationActionPolicy) -> Void){
        if let data = navigationAction.request.url?.absoluteString {
            print(navigationAction.request.url ?? "")
            if data.contains("app/payment/redsys/status") {
                let dict : [String : Any] = ["paymentReference" : "" , "paymentStatus" : getQueryStringParameter(url: data, param: "state")?.replacingOccurrences(of: "\'", with: "") ?? ""]
                apiUse = WhichApiUse.placeOrder
                SVProgressHUD.dismiss()
                self.makeRequest(id: "", dict: dict)

            }
        }
        decisionHandler(.allow)
    }
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    func makeRequest(id : String , dict : [String : Any]){
        if count == 0 {
            SVProgressHUD.show(withStatus: "Loading....")
            count = 1
            var params = ""
            var verb : RequestType?
            switch apiUse {
            case .placeOrder?:
                params = GlobalData.apiName.placeOrder
                verb = .post
            default:
                print("")
            }
             NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict , verbs: verb! , controler: Controllers.checkout) { (responseObject:JSON?, error:Error?) in
                if (error != nil) {
                    print("Error logging you in!")
                    
                } else {
                    switch self.apiUse {
                    case .placeOrder?:
                        if responseObject!["success"].boolValue {
                            SVProgressHUD.dismiss()
                            let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "orderPlaceDataViewController") as! OrderPlaceDataViewController
                            if let data = responseObject {
                                view.desc = data["message"].stringValue + "\n" + data["txn_msg"].stringValue
                                view.id = data["name"].stringValue
                                 view.isSuccess = true
                                let nav = UINavigationController(rootViewController: view)
                                self.present(nav, animated: true, completion: nil)
                            }
                            else{
                                self.dismiss(animated: true, completion: {
                                    self.delegate?.paymentCheck(status: false, reference: "cancel")
                                })
                            }
                            
                        } else{
                            SVProgressHUD.dismiss()
                            let view = viewController(forStoryboardName: "Checkout", forViewControllerName: "orderPlaceDataViewController") as! OrderPlaceDataViewController
                            if let data = responseObject {
                                view.desc =  data["txn_msg"].stringValue.html2AttributedString?.string
                                view.id = data["name"].stringValue
                                view.isSuccess = false
                                let nav = UINavigationController(rootViewController: view)
                                self.present(nav, animated: true, completion: nil)
                            }
                            else{
                                self.dismiss(animated: true, completion: {
                                    self.delegate?.paymentCheck(status: false, reference: "cancel")
                                })
                            }
                            
                        }
                    default :
                        print("")
                    }
                }
            }
        }
    }

}
