//
//  OrderHistoryViewController.swift
//  Odoo application
//
//  Created by Bhavuk on 27/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderHistoryViewController: UIViewController {
      var emptyView: EmptyView?
     @IBOutlet weak var orderTableView: UITableView!
    fileprivate let orderHistoryViewModalObject = OrderHistoryViewModal()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = allOrder.localized
        orderTableView.delegate = orderHistoryViewModalObject
        orderHistoryViewModalObject.delegate = self
        orderTableView.dataSource = orderHistoryViewModalObject
        orderTableView.estimatedRowHeight = 100
        orderTableView.rowHeight = UITableView.automaticDimension
        orderTableView.separatorStyle = .none
        orderTableView.register(OrderHistoryViewCell.nib, forCellReuseIdentifier: OrderHistoryViewCell.identifier)
        self.makeRequest()
        // Do any additional setup after loading the view.
    }

    func makeRequest() {

        let params = GlobalData.apiName.orders
        

         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: [:], verbs: RequestType.post, controler: Controllers.orderDetails) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {
                self.orderHistoryViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    if data {
                        self.emptyView?.isHidden = true
                        self.orderTableView.isHidden = false
                        self.orderTableView.reloadData()
                    } else {
                        self.emptyView?.isHidden = false
                        self.orderTableView.isHidden = true

                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = orderEmpty.localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-order")
            emptyView?.button.setTitle(browseCategories.localized, for: .normal)
            emptyView?.isHidden = true
            emptyView?.delegate = self
            self.view.addSubview(emptyView!)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrderHistoryViewController: NewViewControllerOpen {
    func moveToController(id: String, name: String, dict: JSON, Controller: Controllers) {
        let view = viewController(forStoryboardName: customerStoryboard, forViewControllerName: orderDetailViewControllerIdentifier) as! OrderDetailViewController
        view.id = id
        view.name = name
        self.navigationController?.pushViewController(view, animated: true)
    }
}

extension OrderHistoryViewController: EmptyDelegate {
    func buttonPressed() {
        self.tabBarController?.selectedIndex = 2
    }
}
