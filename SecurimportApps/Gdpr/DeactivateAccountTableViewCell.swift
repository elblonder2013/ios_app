//
/**
Odoo Marketplace
@Category Webkul
@author    Webkul
Created by: jitendra on 06/08/18
FileName: DeactivateAccountTableViewCell.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license   https://store.webkul.com/license.html
*/


import UIKit

class DeactivateAccountTableViewCell: UITableViewCell {
    @IBOutlet weak var temporaryBtn: UIButton!
    var delegate:AccountDeacticvateAction!
    
    @IBOutlet weak var deactiveLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        temporaryBtn.setTitle("deactivate".localized, for: .normal)
        deactiveLbl.text = "deactivateTemporary".localized
        temporaryBtn.layer.cornerRadius = 5
        temporaryBtn.layer.masksToBounds = true
        // Initialization code
    }
  
    @IBAction func temporaryBtnAction(_ sender: Any) {
        delegate.accountDeactivate(type: "Temporary")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}
