//
//  ChangeTableViewCell.swift
//  Odoo iOS
//
//  Created by Bhavuk on 01/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import SwiftyJSON
import Reusable

class ChangeTableViewCell: UITableViewCell, FormConformity, NibReusable {

     @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var Switchh: UISwitch!
    var tableView: UITableView?
    var formItem: FormItem?
    @IBOutlet weak var changePasswordLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        Switchh.isOn = false
        changePasswordLabel.text = changePassword.localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
   
    @IBAction func SwitchAction(_ sender: UISwitch) {
        if sender.isOn{
            formItem?.selectedIndex = 1
        } else {
            formItem?.selectedIndex = 0
        }
        self.formItem?.actionCompletionWithValue?("changePassword",sender.isOn)
    }
}
extension ChangeTableViewCell: FormUpdatable {
    func update(with formItem: FormItem) {
        self.formItem = formItem
        self.headingLabel.text = formItem.placeholder
        Switchh.isOn = formItem.selectedIndex == 1 ? true : false
    }
}
