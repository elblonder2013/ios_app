//
//  SignInViewController+Protocol.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 25/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import Foundation

protocol SocialLoginFacebook: class {
    func fbLogin()
}

protocol SocialLoginGoogle: class {
    func googleLogin()
}

