//
//  AddReviewViewController.swift
//  Odoo iOS
//
//  Created by Bhavuk on 04/11/17.
//  Copyright © 2017 Bhavuk. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SwiftyJSON
import Toaster

@objcMembers
class AddReviewViewController: UIViewController {
    var reviewStarValue = 0
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var reviewtextArea: UITextView!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var starView: HCSStarRatingView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var youAreViewing: UILabel!

    var PName: String?
    var templateId: String!

    var controller: Controllers!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        youAreViewing.SetDefaultTextColor()
        ratingLabel.SetDefaultTextColor()
        nameLabel.SetDefaultTextColor()
        reviewLabel.SetDefaultTextColor()
        
        self.navigationItem.title = "addYourReview".localized
        starView.tintColor = UIColor().HexToColor(hexString: "FFB926")
        starView.isUserInteractionEnabled = true
        let borderColor: UIColor? = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
        reviewtextArea.layer.borderColor = borderColor?.cgColor
        reviewtextArea.layer.borderWidth = 1.0
        reviewtextArea.layer.cornerRadius = 5.0
        productName.text = PName!
        starView.addTarget(self, action: #selector(self.didChangeValue1), for: .touchDown)
        youAreViewing.text = "youReviewing".localized
        ratingLabel.text = "rating".localized
        nameLabel.text = "title".localized
        reviewLabel.text = "reviews".localized
        submitButton.backgroundColor = GlobalData.Credentials.accentColor
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func submitClicked(_ sender: Any) {

        self.dismissKeyboard()

        if controller != nil {
            if reviewStarValue == 0 {
                ErrorChecking.warningView(message: "selectRating".localized)
            } else if nameTextField.text! == "" {
                ErrorChecking.warningView(message: "titleEmpty".localized)
            } else if reviewtextArea.text == "" {
                ErrorChecking.warningView(message: "reviewEmpty".localized)
            } else {
            let dict = ["title" : nameTextField.text!, "msg" : reviewtextArea.text, "rating" : reviewStarValue ] as [String: Any]
            self.makeRequest(dict: dict)
            }
        } else {
            if reviewStarValue == 0 {
                 ErrorChecking.warningView(message: "selectRating".localized)
            } else if nameTextField.text! == "" {
                ErrorChecking.warningView(message: "titleEmpty".localized)
            } else if reviewtextArea.text == "" {
                ErrorChecking.warningView(message: "reviewEmpty".localized)
                
            } else {
            let dict = ["title" : nameTextField.text!, "detail" : reviewtextArea.text, "rate" : reviewStarValue, "template_id" : templateId!] as [String: Any]
            self.makeRequest(dict: dict)
            }
        }

    }

    func didChangeValue1(_ sender: HCSStarRatingView) {
        reviewStarValue = Int(sender.value)
        switch reviewStarValue {
        case 1:
            ratingLabel.text = "rating".localized + ": " + "poor".localized
        case 2:
            ratingLabel.text = "rating".localized + ": " + "OK".localized
        case 3:
            ratingLabel.text = "rating".localized + ": " + "good".localized
        case 4:
            ratingLabel.text = "rating".localized + ": " + "veryGood".localized
        case 5:
            ratingLabel.text = "rating".localized + ": " + "excellent".localized
        default:
            ratingLabel.text = "rating".localized
        }
        print(reviewStarValue)
    }

    func makeRequest(dict: [String: Any]) {
        var params = ""
           if controller != nil {
             params = GlobalData.apiName.sellerReviews + templateId
           } else {
                params = GlobalData.apiName.saveReview
          }

       
         NetworkManager.shared.fetchData(params: params, login: false, view: self.view, dict: dict, verbs: RequestType.post, controler: Controllers.addReview ) { (responseObject: JSON?, error: Error?) in

            if (error != nil) {
                print("Error logging you in!")

            } else {

                if responseObject!["success"].boolValue {
                    ErrorChecking.successView(message: responseObject!["message"].stringValue)
                    self.navigationController?.popViewController(animated: true)
                } else {
                    ErrorChecking.warningView(message: responseObject!["message"].stringValue)
                }
            }
        }
    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
